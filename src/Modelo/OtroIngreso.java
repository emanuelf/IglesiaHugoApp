package Modelo;

import java.util.GregorianCalendar;

/**
 *
 * @author Emanuel
 */
public class OtroIngreso extends Ingreso {

    public OtroIngreso(Cuenta cuenta, GregorianCalendar fecha, double monto) {
        super(cuenta, fecha, monto);
    }

    public OtroIngreso() {
    }

    @Override
    public boolean sosOtroIngreso() {
        return true;
    }
}
