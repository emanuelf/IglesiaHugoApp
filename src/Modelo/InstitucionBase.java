package Modelo;

import Excepciones.DAOProblemaConTransaccionException;
import Excepciones.DAOYaExisteRegistroException;
import Excepciones.ObjetoNoEncontradoException;
import Persistencia.CuentaDAO;
import Persistencia.FilialDAO;
import Persistencia.Persistencia;
import Persistencia.PersonaDAO;
import Persistencia.UsuarioDAO;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author emanuel
 */
public class InstitucionBase implements OperacionesDeSecretario, AdministradorDeInicio { /*Operaciones de los secretarios son comunes a todos
     los usuarios*/


    private String nombre;
    private Direccion direccion;
    private long telefono;
    private String email;
    private int ficheDeCulto;
    private String personeriaJuridica;
    private Persistencia persistencia;
    private Usuario usuarioConectado;
    private Configuracion configuracion;

    public InstitucionBase() throws DAOProblemaConTransaccionException, org.postgresql.util.PSQLException {
        this.persistencia = new Persistencia();
        persistencia.addMiembroAnonimoSiEsNecesario();
        CuentaDAO cuentaDAO = (CuentaDAO) persistencia.get("CUENTADAO");
        cuentaDAO.addCuentasClasicasSiEsNecesario();
        persistencia.createDefaultConfiguracion();
        persistencia.añadirUsuarioTesoreroSiEsNecesario();
    }

    public InstitucionBase(UsuarioConectado usuarioConectado) throws org.postgresql.util.PSQLException, DAOProblemaConTransaccionException {
        this();
        this.usuarioConectado = usuarioConectado;

    }

    public InstitucionBase(String nombre, Direccion direccion, long telefono, String email,
            int ficheDeCulto, String personeriaJuridica) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        this.ficheDeCulto = ficheDeCulto;
        this.personeriaJuridica = personeriaJuridica;


    }

    public boolean esSocioActivo(Miembro miembro) {
        boolean retorno = false;
        if (miembro.isMiembroActivo())
            return retorno = true;
        return retorno;
    }

    public Usuario buscarUsuario(String nombreUsuario, String contrasenia) {
        UsuarioDAO usuarioDAO = (UsuarioDAO) this.persistencia.get("USUARIODAO");
        Usuario unUsuario = null;
        unUsuario = usuarioDAO.buscarUsuario(nombreUsuario, contrasenia);
        return unUsuario;
    }

    public Pastor devolverDatosDelPastor() throws DAOProblemaConTransaccionException {
        PersonaDAO personaDAO = (PersonaDAO) this.getPersistencia().get("PERSONADAO");
        Pastor pastor = personaDAO.getDatosDelPastor();
        return pastor;
    }

    public Set<Miembro> buscarMiembros(String nombre, String apellido) throws DAOProblemaConTransaccionException {//arreglar metodo
        HashSet<Miembro> miembros = new HashSet<Miembro>();
        PersonaDAO personaDAO = (PersonaDAO) this.getPersistencia().get("PERSONADAO");
        List<Miembro> miembrosDevueltosPorPersistencia = personaDAO.getMiembros(nombre, apellido);
        miembros.addAll(miembrosDevueltosPorPersistencia);
        return miembros;
    }

    public PastorFilial datosDePastor(String cuidad) {
        PastorFilial retorno = null;
        PersonaDAO personaDAO = (PersonaDAO) this.getPersistencia().get("PERSONADAO");
        retorno = personaDAO.getDatosDelPastorFilial(cuidad);
        return retorno;
    }

    public PastorFilial devolverDatosDePastor(Filial filial) {
        return filial.getPastorFilial();
    }

    public void cambiarContrasenia(Usuario usuario, String contrasenia) throws DAOProblemaConTransaccionException {
        usuario.setContrasenia(contrasenia);
        UsuarioDAO usuarioDAO = (UsuarioDAO) persistencia.get("USUARIODAO");
        if (usuario.sosTesorero())
            usuarioDAO.persistirOGuardar((Tesorero) usuario);
        else
            usuarioDAO.persistirOGuardar((Secretario) usuario);
    }

    public void salirDelSistema() {
        this.usuarioConectado = null;
        persistencia.getSessionFactory().getCurrentSession().disconnect();
    }

    public Miembro registrarNuevoMiembro(String nombre, String apellido, Direccion direccion,
            long telefonoFijo, long telefonoCelular, GregorianCalendar fechaBautismo) {
        Miembro nuevoMiembro = new Miembro(fechaBautismo, nombre, apellido, direccion, telefonoFijo, telefonoCelular);
        PersonaDAO personaDAO = (PersonaDAO) this.persistencia.get("PERSONADAO");
        personaDAO.agregarMiembro(nuevoMiembro);
        return nuevoMiembro;
    }

    public Filial modificarDatosDeFilial(Filial filialAModificar, String nombre, Direccion direccion, long telefono, PastorFilial pastor)
            throws Excepciones.DAOProblemaConTransaccionException {
        filialAModificar.setDireccion(direccion);
        filialAModificar.setNombre(nombre);
        filialAModificar.setPastorFilial(pastor);
        filialAModificar.setTelefono(telefono);
        FilialDAO filialDAO = (FilialDAO) this.persistencia.get("FILIALDAO");
        filialDAO.actualizarFilial(filialAModificar);
        return filialAModificar;
    }

    public Filial registrarFilial(String nombre, Direccion direccion, long telefono,
            PastorFilial pastorFilial) throws DAOProblemaConTransaccionException, DAOYaExisteRegistroException {
        if (this.buscarFilial(nombre) == null) {
            FilialDAO filialDAO = (FilialDAO) this.persistencia.get("FILIALDAO");
            if (pastorFilial != null)
                if (filialDAO.existePastor(pastorFilial.getDni()))
                    throw new Excepciones.DAOYaExisteRegistroException("Ya se encuentra registrado el pastor");
            Filial filialNueva = new Filial(nombre, direccion, pastorFilial, telefono);
            filialDAO.persistir(filialNueva);
            return filialNueva;
        } else
            throw new Excepciones.DAOYaExisteRegistroException("Ya se encuentra registrarda una filial con este nombre de filial");
    }

    public Filial buscarFilial(String nombre) {
        Filial filial = null;
        FilialDAO filialDAO = (FilialDAO) this.persistencia.get("FILIALDAO");
        filial = filialDAO.getFilial(nombre);
        return filial;

    }

    /**
     * Se asume que el miembro existe, porque ha sido buscado ya.
     *
     * @param El miembro a desvincular.
     */
    public void desvincularMiembro(Miembro miembro) {
        PersonaDAO personaDAO = (PersonaDAO) this.persistencia.get("PERSONADAO");
        personaDAO.desvincularMiembro(miembro);
    }

    public ArrayList<PastorFilial> getAllPastores() {
        ArrayList<PastorFilial> retorno = new ArrayList<PastorFilial>();
        PersonaDAO personaDAO = (PersonaDAO) this.persistencia.get("PERSONADAO");
        retorno = (ArrayList<PastorFilial>) personaDAO.getPastoresDeFiliales();
        return retorno;
    }

    public Set<Miembro> buscarMiembrosPorApellido(String apellido) throws DAOProblemaConTransaccionException {
        HashSet miembros = new HashSet();
        PersonaDAO personaDAO = (PersonaDAO) this.getPersistencia().get("PERSONADAO");
        List<Miembro> miembrosList = personaDAO.getMiembros(apellido);
        miembros.addAll(miembrosList);
        return miembros;
    }

    public Usuario ingresarAlSistema(String nombreUsuario, String contrasenia) throws ObjetoNoEncontradoException {
        Usuario usuario = this.buscarUsuario(nombreUsuario, contrasenia);
        if (usuario == null)
            throw new ObjetoNoEncontradoException("Nombre de usuario y/o contrasenia incorrectos. "
                    + "Intente nuevamente");

        if (!usuario.esMiContrasenia(contrasenia))
            throw new ObjetoNoEncontradoException("Nombre de usuario y/o contrasenia incorrectos. "
                    + "Intente nuevamente");
        return usuario;
    }

    public String getNombreFilial(PastorFilial pastorFilial) {
        String retorno = "";
        FilialDAO filialDAO = (FilialDAO) this.persistencia.get("FILIALDAO");
        retorno = filialDAO.getFilial(pastorFilial);
        return retorno;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public long getTelefono() {
        return telefono;
    }

    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getFicheDeCulto() {
        return ficheDeCulto;
    }

    public void setFicheDeCulto(int ficheDeCulto) {
        this.ficheDeCulto = ficheDeCulto;
    }

    public String getPersoneriaJuridica() {
        return personeriaJuridica;
    }

    public void setPersoneriaJuridica(String personeriaJuridica) {
        this.personeriaJuridica = personeriaJuridica;
    }

    public ArrayList<Filial> getFiliales() throws DAOProblemaConTransaccionException {
        FilialDAO filialDAO = (FilialDAO) this.persistencia.get("FILIALDAO");
        return (ArrayList<Filial>) filialDAO.getFiliales();
    }

    public Persistencia getPersistencia() {
        return persistencia;
    }

    public void setPersistencia(Persistencia persistencia) {
        this.persistencia = persistencia;
    }

    public Usuario getUsuarioConectado() {
        return usuarioConectado;
    }

    public void setUsuarioConectado(Usuario usuarioConectado) {
        this.usuarioConectado = usuarioConectado;
    }

    public void registrarFilial(String nombre, Direccion direccion, String telefono, PastorFilial pastorFilial, int ficheroDeCulto) throws DAOProblemaConTransaccionException, Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Configuracion getConfiguracion() {
        return configuracion != null ? configuracion : persistencia.getConfiguracion();
    }

    public void setConfiguracion(Configuracion configuracion) {
        this.configuracion = configuracion;
    }
}
