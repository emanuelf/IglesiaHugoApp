package Modelo;

public class IngresoRelatorio {

    private double monto;
    private CuentaFilialIngreso cuenta;
    private long nroAsientoIngresoRelatorio;

    public IngresoRelatorio() {
    }

    public IngresoRelatorio(CuentaFilialIngreso cuenta, double monto) {
        this.monto = monto;
        this.cuenta = cuenta;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double val) {
        this.monto = val;
    }

    public long getNroAsientoIngresoRelatorio() {
        return nroAsientoIngresoRelatorio;
    }

    public void setNroAsientoIngresoRelatorio(long nroAsientoIngresoRelatorio) {
        this.nroAsientoIngresoRelatorio = nroAsientoIngresoRelatorio;
    }

    public CuentaFilialIngreso getCuenta() {
        return cuenta;
    }

    public void setCuenta(CuentaFilialIngreso cuenta) {
        this.cuenta = cuenta;
    }
}
