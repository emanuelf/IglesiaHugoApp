package Modelo;

import java.util.GregorianCalendar;

public class Diezmo extends Ingreso {

    public Diezmo() {
    }

    public Diezmo(Cuenta cuenta, GregorianCalendar periodo, double monto) {
        super(cuenta, periodo, monto);
    }

    @Override
    public boolean sosIngresoDeCulto() {
        return true;
    }

    @Override
    public String toString() {
        return "Diezmo";
    }

    @Override
    public boolean sosDiezmo() {
        return true;
    }
}
