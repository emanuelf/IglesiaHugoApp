package Modelo;

/**
 *
 * @author emanuel
 */


public class Persona {

    private long identificadorPersona;
    private String nombres;
    private String apellidos;
    private Direccion direccion;
    private long telefonoFijo;
    private long telefonoCelular;

    public Persona() {
    }

    public Persona(String nombres, String apellidos, Direccion direccion, long telefonoFijo, long telefonoCelular) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.telefonoFijo = telefonoFijo;
        this.telefonoCelular = telefonoCelular;

    }

    public Direccion getDireccion() {
        return direccion;
    }


    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public long getTelefonoFijo() {
        return telefonoFijo;
    }


    public void setTelefonoFijo(long telefonoFijo) {
        this.telefonoFijo = telefonoFijo;
    }

    public long getTelefonoCelular() {
        return telefonoCelular;
    }


    public void setTelefonoCelular(long telefonoCelular) {
        this.telefonoCelular = telefonoCelular;
    }


    public long getIdentificadorPersona() {
        return identificadorPersona;
    }


    public void setIdentificadorPersona(long identificadorPersona) {
        this.identificadorPersona = identificadorPersona;
    }


    public String getNombres() {
        return nombres;
    }


    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }


    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

}
