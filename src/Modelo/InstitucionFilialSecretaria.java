package Modelo;

/**
 *
 * @author emanuel
 */
public class InstitucionFilialSecretaria extends InstitucionFilial {

    InstitucionFilialSecretaria(String nombre, Direccion direccion, long telefono, String email, 
            int ficheDeCulto, String personeriaJuridica) {
        super(nombre, direccion, telefono, email, ficheDeCulto, personeriaJuridica);
    }
}
