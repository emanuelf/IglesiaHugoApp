package Modelo;



public interface AdministradorDeInicio {

    
    public Usuario ingresarAlSistema (String nombreUsuario, String contrasenia) throws Excepciones.ObjetoNoEncontradoException;

    
    public Usuario buscarUsuario (String nombre, String contrasenia);

}

