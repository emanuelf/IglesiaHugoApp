/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;

/**
 *
 * @author emanuel
 */
public class Secretario extends Usuario{

    public Secretario() {
    }

    Secretario(String nombreUsuario, String contrasenia){
        super(nombreUsuario, contrasenia);
    }

    @Override
    public boolean sosTesorero() {
        return false;
    }


}
