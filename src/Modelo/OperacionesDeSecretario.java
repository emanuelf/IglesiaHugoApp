package Modelo;

import Excepciones.DAOProblemaConTransaccionException;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Set;

public interface OperacionesDeSecretario {

    public boolean esSocioActivo(Miembro miembro);

    public Pastor devolverDatosDelPastor() throws DAOProblemaConTransaccionException;

    public Set buscarMiembros(String nombre, String apellido) throws DAOProblemaConTransaccionException;

    public PastorFilial datosDePastor(String cuidad);

    public PastorFilial devolverDatosDePastor(Filial filial);

    public void cambiarContrasenia(Usuario usuario, String contrasenia) throws DAOProblemaConTransaccionException;

    public void salirDelSistema();

    public Miembro registrarNuevoMiembro(String nombre, String apellido, Direccion direccion, long telefonoFijo, long telefonoCelular, GregorianCalendar fechaBautismo);

    public void registrarFilial(String nombre, Direccion direccion, String telefono, PastorFilial pastorFilial,
            int ficheroDeCulto) throws DAOProblemaConTransaccionException, Exception;

    public void desvincularMiembro(Miembro miembro);

     public Collection getAllPastores();

    public Set buscarMiembrosPorApellido(String apellido) throws DAOProblemaConTransaccionException;
}
