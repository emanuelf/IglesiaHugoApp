package Modelo;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Set;

/**
 *
 * @author emanuel
 */
public class RelatorioFilialMensual extends RelatorioFilial {

    private int mes;

    public RelatorioFilialMensual() {
    }

    public RelatorioFilialMensual(Filial filial, GregorianCalendar periodo,
            Set< IngresoRelatorio> ingresos, 
            Set<EgresoRelatorio> egresos, 
            Set<DeudaRelatorio> deudaRelatorios, 
            double cajaInicial, 
            double cajaFinal) {
        super(periodo, filial, ingresos, egresos, deudaRelatorios, cajaInicial, cajaFinal);
        mes = periodo.get(Calendar.MONTH);
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public Double getTotalAdeudado() {
        double total = 0;
        for (DeudaRelatorio deuda : getDeudaRelatorios()) {
            total += deuda.getMonto();
        }
        return total;
    }

    public Double getTotalEgresos() {
        double total = 0;
        for (EgresoRelatorio egreso : getEgresoRelatorios()) {
            total += egreso.getMonto();
        }
        return total;
    }

    public double getTotalIngresos() {
        double totalIngresos = 0;
        for (IngresoRelatorio ingreso : getIngresoRelatorios()) {
            totalIngresos += ingreso.getMonto();
        }
        return totalIngresos;
    }
}
