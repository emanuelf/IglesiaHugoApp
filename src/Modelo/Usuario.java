package Modelo;

public class Usuario {

    private int id;
    private String nombre;
    private String contrasenia;

    public Usuario() {
    }

    public Usuario(String nombreUsuario, String contrasenia) {
        this.nombre = nombreUsuario;
        this.contrasenia = contrasenia;
    }

    public int getId() {
        return id;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean esMiContrasenia(String contrasenia) {
        if (contrasenia.equals(contrasenia)) {
            return true;
        }
        return false;
    }

    public boolean sosTesorero() {
        return true;
    }
}
