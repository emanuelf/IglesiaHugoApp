package Modelo;

/**
 * @author emanuel
 */
public class CuentaFilial {

    private Integer id;     
    private String nombre;

    public CuentaFilial(String nombre) {
          this.nombre = nombre;
    }

    public CuentaFilial() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public boolean sosIngreso(){
        return false;
    }
    
    public boolean sosEgreso(){
        return false;
    }
    
    public boolean sosDeuda(){
        return false;
    }

    boolean esTuNombre(String nombreDeLaCuenta) {
        return this.getNombre().equals(nombreDeLaCuenta);
    }
}
