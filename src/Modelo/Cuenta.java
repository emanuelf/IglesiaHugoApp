package Modelo;

import java.io.Serializable;

public class Cuenta implements Serializable {

    private String nombre;
    private int codigo;
    private boolean activo;

    public Cuenta(String nombre, int codigo) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.activo = true;
    }

    public Cuenta() {
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int unCodigo) {
        this.codigo = unCodigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String unNombre) {
        this.nombre = unNombre;
    }

    public boolean sosCuentaPrincipal() {
        return this.getNombre().equals("Diezmo") || getNombre().equals("Ofrenda") || getNombre().equals("Donacion");
    }

    @Override
    public String toString() {
        return nombre;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cuenta other = (Cuenta) obj;
        if ((this.nombre == null) ? (other.nombre != null) : !this.nombre.equals(other.nombre)) {
            return false;
        }
        return true;
    }

    

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }
    
}
