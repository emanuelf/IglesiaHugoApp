package Modelo;

import java.util.GregorianCalendar;
import java.util.Map;

public class Lider extends Miembro {

    private long dni;
    private Miembro conyuge;

    public Lider() {
    }

    public Lider(long dni, Miembro conyuge, GregorianCalendar fechaBautismo, Map<GregorianCalendar, Diezmo> misDiezmos, String nombres, String apellidos, Direccion direccion, long telefonoFijo, long telefonoCelular) {
        super(fechaBautismo,nombres,apellidos,direccion,telefonoFijo,telefonoCelular)  ;
        this.dni = dni;
        this.conyuge = conyuge;
    }


    public Miembro getConyuge() {
        return conyuge;
    }

    public void setConyuge(Miembro val) {
        this.conyuge = val;
    }

    public long getDni() {
        return dni;
    }

    public void setDni(long val) {
        this.dni = val;
    }

    public boolean diezmoAlDia() {
        return true;
    }
}

