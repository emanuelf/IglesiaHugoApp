package Modelo;

import java.util.GregorianCalendar;

public class Donacion extends Ingreso {

    private Donante donante;
    private String detalleDonacion;

    public Donacion() {
    }

    public Donacion(Donante donante, String detalleDonacion, Cuenta cuenta, GregorianCalendar fecha, double monto) {
        super(cuenta, fecha, monto);
        this.donante = donante;
        this.detalleDonacion = detalleDonacion;

    }

    public String getDetalleDonacion() {
        return detalleDonacion;
    }

    public void setDetalleDonacion(String detalleDonacion) {
        this.detalleDonacion = detalleDonacion;
    }

    public Donante getDonante() {
        return donante;
    }

    public void setDonante(Donante unDonante) {
        this.donante = unDonante;
    }

    @Override
    public boolean sosDonacion() {
        return true;
    }

    public boolean tieneDonanteRegistrado() {
        return donante != null;
    }

    public String getApellidoYNombreDeDonante() {
        if (donante == null) return "";
        return donante.getApellidos() + ", " + donante.getNombres();
    }
}
