package Modelo;

/**
 * @author emanuel
 */
public class DeudaRelatorio {

    private CuentaFilialDeuda cuentaFilialDeuda;
    private double monto;
    private long nroAsientoEgresoRelatorio;

    public DeudaRelatorio() {
    }

    public DeudaRelatorio(CuentaFilialDeuda cuentaFilialDeuda, double monto) {
        this.cuentaFilialDeuda = cuentaFilialDeuda;
        this.monto = monto;
    }

    public CuentaFilialDeuda getCuentaFilialDeuda() {
        return cuentaFilialDeuda;
    }

    public double getMonto() {
        return monto;
    }

    public long getNroAsientoEgresoRelatorio() {
        return nroAsientoEgresoRelatorio;
    }

    public void setCuentaFilialDeuda(CuentaFilialDeuda cuentaFilialDeuda) {
        this.cuentaFilialDeuda = cuentaFilialDeuda;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public void setNroAsientoEgresoRelatorio(long nroAsientoEgresoRelatorio) {
        this.nroAsientoEgresoRelatorio = nroAsientoEgresoRelatorio;
    }
}
