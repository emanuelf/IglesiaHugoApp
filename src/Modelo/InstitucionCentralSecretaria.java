/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Persistencia.Persistencia;
import java.util.ArrayList;

/**
 *
 * @author emanuel
 */
public class InstitucionCentralSecretaria extends InstitucionCentral implements OperacionesDeSecretario{

    public InstitucionCentralSecretaria(String nombre, Direccion direccion, long telefono, String email, int ficheDeCulto, String personeriaJuridica, ArrayList<Miembro> miembros, ArrayList<Usuario> mUsuario, ArrayList<Filial> filiales, ArrayList<Cuenta> cuentas, Persistencia persistencia, UsuarioConectado usuarioConectado) {
        super(nombre, direccion, telefono, email, ficheDeCulto, personeriaJuridica);
    }

    public InstitucionCentralSecretaria(Usuario usuarioConectado) throws Excepciones.DAOProblemaConTransaccionException, org.postgresql.util.PSQLException{
        this.setUsuarioConectado(usuarioConectado);
    }


    public InstitucionCentralSecretaria() throws Excepciones.DAOProblemaConTransaccionException, org.postgresql.util.PSQLException{
    }

    @Override
    public boolean sosInstitucionCentralTesoreria() {
        return false;
    }
}
