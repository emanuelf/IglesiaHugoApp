package Modelo;

public class Donante {

    private long id;
    private long dni;
    private int parteDelantera;
    private int parteTrasera;
    private String apellidos;
    private String nombres;

    public Donante() {
    }

    public Donante(long dni, int parteDelantera, int parteTrasera, String nombres, String apellidos) {
        this.dni = dni;
        this.apellidos= apellidos;
        this.nombres = nombres;
        this.parteDelantera=parteDelantera;
        this.parteTrasera=parteTrasera;
    }

    /*public long getCuitDonante() {
    return dni;
    }*/
    public String getCuitEnString() {
        return String.valueOf(parteDelantera) + "-" + String.valueOf(dni) + "-" + String.valueOf(parteTrasera);
    }

    public boolean esTuDni(long dni) {
        return this.dni == dni ? true : false;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getNombres() {
        return nombres;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDni() {
        return dni;
    }

    public int getParteTrasera() {
        return parteTrasera;
    }

    public int getParteDelantera() {
        return parteDelantera;
    }

    public void setDni(long dni) {
        this.dni = dni;
    }

    public void setParteTrasera(int pareteTrasera) {
        this.parteTrasera = pareteTrasera;
    }

    public void setParteDelantera(int parteDelantera) {
        this.parteDelantera = parteDelantera;
    }

    public boolean esTuParteTrasera(int parteTrasera) {
        return this.parteDelantera == parteDelantera ? true : false;
    }

    public boolean esTuParteDelantera(int parteDelantera) {
        return this.parteDelantera == parteDelantera ? true : false;
    }
}
