package Modelo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * IngresoEnCulto.java
 *
 * Created on 17/10/2011, 20:08:25
 *
 * @author CeroYUno Informática. Soluciones Integrales
 */
public class IngresoEnCulto {

    private double monto;
    private Date fechaDeCulto;
    private int nroIngresoEnCulto;
    private String descripcion;
    private Set<Ingreso> ingresos = new HashSet<Ingreso>();

    public IngresoEnCulto() {
    }

    public IngresoEnCulto(Date fechaDeCulto, String descripcion) {
        this.fechaDeCulto = fechaDeCulto;
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Set<Ingreso> getIngresos() {
        return ingresos;
    }

    public void setIngresos(Set<Ingreso> ingresos) {
        this.ingresos = ingresos;
    }

    public Date getFechaDeCulto() {
        return fechaDeCulto;
    }

    public double getMonto() {
        double retorno = 0;
        for (Ingreso ingreso : ingresos) {
            retorno +=ingreso.getMonto();
        }
        return retorno;
    }

    public int getNroIngresoEnCulto() {
        return nroIngresoEnCulto;
    }

    public void setFechaDeCulto(Date fechaDeCulto) {
        this.fechaDeCulto = fechaDeCulto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public void setNroIngresoEnCulto(int nroIngresoEnCulto) {
        this.nroIngresoEnCulto = nroIngresoEnCulto;
    }

    public void sumarMonto(double monto) {
        this.monto += monto;
    }

    public void addIngreso(Ingreso ingreso) {
        ingresos.add(ingreso);
    }
}
