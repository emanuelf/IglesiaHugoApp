package Modelo;

import Excepciones.DAOProblemaConTransaccionException;
import Excepciones.ObjetoNoEncontradoException;
import Persistencia.Persistencia;
import Persistencia.PersonaDAO;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;

/**
 *
 * @author emanuel
 */
public class InstitucionFilialTesoreria extends InstitucionFilial implements OperacionesDeTesorero {

    public InstitucionFilialTesoreria(String nombre, Direccion direccion, long telefono, String email, int ficheDeCulto, String personeriaJuridica, ArrayList<Miembro> miembros, ArrayList<Usuario> mUsuario, ArrayList<Filial> filiales, ArrayList<Cuenta> cuentas, Persistencia persistencia, UsuarioConectado usuarioConectado) {
        super(nombre, direccion, telefono, email, ficheDeCulto, personeriaJuridica);
    }

    public void registrarIngresoRelatorio(String nombreCuenta, RelatorioFilial relatorio, double monto) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void modificarDatosDeMiembro(Miembro miembroSeleccionadoAModificar, String apellido, String nombre, Direccion direccion, long telefonoCelular, long telefonoFijo, GregorianCalendar fechaDeBautismo) throws DAOProblemaConTransaccionException {
        miembroSeleccionadoAModificar.modificarDatos(apellido, nombre, direccion, telefonoCelular, telefonoFijo, fechaDeBautismo);
        PersonaDAO personaDAO = (PersonaDAO) getPersistencia().get("PERSONADAO");
        try {
            personaDAO.persistir(miembroSeleccionadoAModificar);
        } catch (DAOProblemaConTransaccionException ex) {
            try {
                personaDAO.persistir(miembroSeleccionadoAModificar);
            } catch (DAOProblemaConTransaccionException ex2) {
                personaDAO.persistir(miembroSeleccionadoAModificar);
            }
        }

    }

    public Cuenta buscarCuentaPorcodigo(int codigo) throws ObjetoNoEncontradoException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Balance consultarBalance(String codigoBalance, String tipo) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Balance consultarBalance(String tipo, GregorianCalendar periodo) throws ObjetoNoEncontradoException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void actualizarCaja(double monto) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public double calcularSueldoPastor(GregorianCalendar periodo) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public BalanceAnual generarBalanceAnual() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Collection consultarIngresosDelMes() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public BalanceMensual generarBalanceMensual(GregorianCalendar periodo) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Collection consultarEgresosDelMes() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Cuenta buscarCuentaPorNombre(String nombre) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public double sumarTotalMensualCuenta(Collection asientosDelMes) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public double sumarTotalAnualCuenta(Collection asientosDelAnio) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void registrarIngresoDiezmo(Miembro miembro, GregorianCalendar periodo, double monto, GregorianCalendar fechaDelIngreso) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void registrarNuevoUsuarioTesorero(String nombre, String contrasenia) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Secretario registrarNuevoUsuarioSecretario(String nombre, String contrasenia) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void guardarRelatorio(RelatorioFilial relatorioCreado) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void registrarIngresoDonacion(Donante donante, double montoDonacion, GregorianCalendar fecha, String detalleDonacion) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public RelatorioFilial devolverRelatorioMensual(Filial filial, int anio, int mes) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public RelatorioFilial devolverRelatorioAnual(Filial filial, int anio) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public RelatorioFilial devolverRelatorioFilialMensual(Filial filial, int anio, int mes) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public RelatorioFilial devolverRelatorioFilialAnual(Filial filial, int anio) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public RelatorioFilialMensual crearRelatorio() {
        return null;
    }

    public void registrarEgreso(Cuenta cuenta, GregorianCalendar fechaEgreso, double monto, String detalle) throws DAOProblemaConTransaccionException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void registrarIngreso(Cuenta cuenta, GregorianCalendar fechaIngreso, double monto) throws DAOProblemaConTransaccionException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public HashMap<Integer, DetalleEgreso> getDetallesDeEgresos() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void registrarNuevaCuentaIngreso(String nombre, int codigo) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void registrarNuevaCuentaEgreso(String nombre, int codigo) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public ArrayList<CuentaIngreso> getCuentasIngreso() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public ArrayList<CuentaEgreso> getCuentasEgreso() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public double consultarCajaDisponible() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void actualizarDatosDeMiembro(Miembro miembro) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Cuenta gistrarNuevaCuenta(String nombre, int codigo, String tipoCuenta) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public BalanceAnual generarBalanceAnual(GregorianCalendar fechaDeEmision) throws DAOProblemaConTransaccionException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public BalanceMensual generarBalanceMensual(GregorianCalendar periodo, GregorianCalendar fechaDeEmision) throws DAOProblemaConTransaccionException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public BalanceAnual generarBalanceAnual(int anioDelBalanceAGenerar) throws DAOProblemaConTransaccionException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public BalanceMensual generarBalanceMensual(int mes, int anio) throws DAOProblemaConTransaccionException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Cuenta registrarNuevaCuenta(String nombre, int codigo, String tipoCuenta) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
