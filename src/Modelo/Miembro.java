package Modelo;

import Util.UtilFechas;
import java.util.*;

public class Miembro extends Persona {

    private GregorianCalendar fechaBautismo;
    private Set<Diezmo> misDiezmos = new HashSet<Diezmo>();
    private Set<Ofrenda> misOfrendas = new HashSet<Ofrenda>();
    private boolean activo;

    public Miembro() {
    }

    public Miembro(GregorianCalendar fechaBautismo, String nombres, String apellidos, Direccion direccion, long telefonoFijo, long telefonoCelular) {
        super(nombres, apellidos, direccion, telefonoFijo, telefonoCelular);
        this.fechaBautismo = fechaBautismo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public GregorianCalendar getFechaBautismo() {
        return fechaBautismo;
    }

    public void setFechaBautismo(GregorianCalendar unaFechaDeBautismo) {
        this.fechaBautismo = unaFechaDeBautismo;
    }

    public GregorianCalendar ultimoDiezmoPago() {
        GregorianCalendar fechaDiezmoMasAntiguo = fechaBautismo;
        if (misDiezmos.isEmpty())
            return fechaDiezmoMasAntiguo; //Si no tiene diezmos hechos el miembro, entonces se toma la fecha de bautismo para calcular si es activo o no
        // Este metodo justamente solo provee la funcionalidad necesaria para isActivo()
        Iterator<Diezmo> it = misDiezmos.iterator();
        while (it.hasNext()) {
            Diezmo diezmo = it.next();
            int resultadoComparacion = diezmo.getFecha().getTime().
                    compareTo(fechaDiezmoMasAntiguo.getTime());
            fechaDiezmoMasAntiguo = resultadoComparacion < 0 ? diezmo.getFecha() : fechaDiezmoMasAntiguo;
        }
        return fechaDiezmoMasAntiguo;
    }

    public boolean isMiembroActivo() {
        Date hoy = new Date();
        GregorianCalendar fechaDiezmoMasAntiguo = ultimoDiezmoPago();
        Date fechaUltimoDiezmo = new Date(fechaDiezmoMasAntiguo.get(Calendar.YEAR),
                fechaDiezmoMasAntiguo.get(Calendar.MONTH),
                fechaDiezmoMasAntiguo.get(Calendar.DAY_OF_MONTH));
        int diffInMonths = UtilFechas.getDateDiffInMonths(fechaUltimoDiezmo, hoy);
        return diffInMonths <= 3;
            //preguntar a hugo si puede ser tres meses de deuda,
             //o si son menos de tres
    }

    public void modificarDatos(String apellido, String nombre, Direccion direccion, long telefonoCelular,
            long telefonoFijo, GregorianCalendar fechaBautismo) {
        setApellidos(apellido);
        setNombres(nombre);
        setDireccion(direccion);
        setFechaBautismo(fechaBautismo);
        setTelefonoCelular(telefonoCelular);
        setTelefonoFijo(telefonoFijo);

    }

    public void setMisDiezmos(Set<Diezmo> misDiezmos) {
        this.misDiezmos = misDiezmos;
    }

    public Set<Diezmo> getMisDiezmos() {
        return misDiezmos;
    }

    public Date fechaBautismoEnDate() {
        if (fechaBautismo == null)
            return null;
        return fechaBautismo.getTime();
    }

    public void agregarDiezmo(Diezmo diezmo) {
        if (misDiezmos == null)
            misDiezmos = new HashSet<Diezmo>();
        misDiezmos.add(diezmo);
    }

    public boolean esAnonimo() {
        return this.getApellidos().equalsIgnoreCase("Anonimo") ? true : false;
    }

    public Set<Ofrenda> getMisOfrendas() {
        return misOfrendas;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setMisOfrendas(Set<Ofrenda> misOfrendas) {
        this.misOfrendas = misOfrendas;
    }

    public void addOfrenda(Ofrenda ofrenda) {
        misOfrendas.add(ofrenda);
    }

    public String nombresYApellidos() {
        return getApellidos().concat(", ").concat(getNombres());
    }
}