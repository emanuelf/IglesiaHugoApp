package Modelo;

import java.util.HashSet;
import java.util.Set;

public class Filial {

    private int id;
    private String nombre;
    private Direccion direccion;
    private PastorFilial pastorFilial;
    private long telefono;
    private Set<RelatorioFilial> misRelatorios = new HashSet<RelatorioFilial>();

    public Filial() {
    }

    public Filial(String nombre, Direccion direccion, PastorFilial pastorFilial, long telefono) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.pastorFilial = pastorFilial;
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String unNombre) {
        this.nombre = unNombre;
    }

    public long getTelefono() {
        return telefono;
    }

    public long setTelefono(long val) {
        return this.telefono = val;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public PastorFilial getPastorFilial() {
        return pastorFilial;
    }

    public void setPastorFilial(PastorFilial pastorFilial) {
        this.pastorFilial = pastorFilial;
    }

    public void agregarRelatorio(RelatorioFilial rel) {
        this.getMisRelatorios().add(rel);
    }

    public Set<RelatorioFilial> getMisRelatorios() {
        return misRelatorios;
    }

    public void setMisRelatorios(Set<RelatorioFilial> misRelatorios) {
        this.misRelatorios = misRelatorios;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean tieneRelatorios() {
        return misRelatorios != null && !misRelatorios.isEmpty() ? true: false;
    }

    @Override
    public String toString() {
        return nombre;
    }
   
}
