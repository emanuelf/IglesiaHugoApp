package Modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Set;

public class Egreso extends Asiento implements Serializable {

    private double pagado;
    private DetalleEgreso detalle;

    public Egreso() {
    }

    public Egreso(DetalleEgreso detalle, Cuenta cuenta, GregorianCalendar fecha, double monto, double pagado) {
        super(cuenta, fecha, monto);
        this.detalle = detalle;
        this.pagado = pagado;

    }

    public double getTotalNeto() {
        return pagado;
    }

    public boolean adeudado() {
        return pagado == 0;
    }

    public boolean pagadoEnParte() {
        return !pagadoPorCompleto() && !adeudado();
    }

    public double getDeuda() {
        return this.getMonto() - this.getPagado();
    }

    public boolean pagadoPorCompleto() {
        boolean retorno = false;
        if (this.getMonto() == this.getPagado()) {
            retorno = true;
        }
        return retorno;
    }

    public DetalleEgreso getDetalle() {
        return detalle;
    }

    public void setDetalle(DetalleEgreso detalle) {
        this.detalle = detalle;
    }

    public double getPagado() {
        return pagado;
    }

    public void setPagado(double pagado) {
        this.pagado = pagado;
    }

    void sumarPagado(double pagado) {
        setPagado(getPagado() + pagado);
    }
    
    //TODO: parece que funciona. Devuelve una coleccion con asientos(ingresos) que contienen la sumatoria de los ingresos para cada cuenta. o sea, 
    //TODO: los totales por cuenta jeje
    public Collection<Egreso> getEgresosDeCadaCuenta(ArrayList<Cuenta> cuentasEgresos, Set<Egreso> egresos) {
        Collection<Egreso> egresosDivididos = new ArrayList<Egreso>();
        for (Cuenta cuenta : cuentasEgresos) {
            Egreso egresoSumatoria = new Egreso();
            egresoSumatoria.setCuenta(cuenta);
            egresosDivididos.add(egresoSumatoria);
            for (Iterator<Egreso> egreso = egresosDivididos.iterator(); egreso.hasNext();) {
                Egreso egreso1 = egreso.next();
                for (Egreso egreso2 : egresos) {
                    if(egreso2.getCuenta().equals(cuenta) && egreso1.getCuenta().equals(cuenta)){
                        egreso1.sumarMonto(egreso2.getMonto());
                        egreso1.sumarPagado(egreso2.getPagado());
                    }
                }
            }
        }
        return egresosDivididos;
    }
}
