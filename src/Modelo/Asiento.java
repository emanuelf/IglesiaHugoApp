package Modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author emanuel
 */
public class Asiento implements Serializable {

    private Cuenta cuenta;
    private GregorianCalendar fecha;
    private double monto;
    private int nroAsiento;

    public Asiento() {
    }

    public Asiento(Cuenta cuenta, GregorianCalendar fecha, double monto) {
        this.cuenta = cuenta;
        this.fecha = fecha;
        this.monto = monto;
    }

    void sumarMonto(double monto) {
        setMonto(getMonto() + monto);
    }

    public boolean sosIngreso() {
        return false;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public GregorianCalendar getFecha() {
        return fecha;
    }

    public void setFecha(GregorianCalendar fecha) {
        this.fecha = fecha;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public int getNroAsiento() {
        return nroAsiento;
    }

    public void setNroAsiento(int nroAsiento) {
        this.nroAsiento = nroAsiento;
    }

    protected static ArrayList<Cuenta> getCuentas(Set<? extends Asiento> ingresos) {
        ArrayList<Cuenta> retorno = new ArrayList<Cuenta>();
        for (Iterator<? extends Asiento> asiento = ingresos.iterator(); asiento.hasNext();) {
            Asiento asiento1 = asiento.next();
            if (!retorno.contains(asiento1.getCuenta())) {
                retorno.add(asiento1.getCuenta());
            }
        }
        return retorno;
    }
}
