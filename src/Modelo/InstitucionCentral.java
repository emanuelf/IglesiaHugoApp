package Modelo;

import Excepciones.DAOProblemaConTransaccionException;
import Persistencia.FilialDAO;
import Persistencia.PersonaDAO;
import java.util.GregorianCalendar;
import org.postgresql.util.PSQLException;

/**
 *
 * @author emanuel
 */
public class InstitucionCentral extends InstitucionBase {

    public InstitucionCentral() throws DAOProblemaConTransaccionException, PSQLException {
        super();
    }

    
    public InstitucionCentral(UsuarioConectado usuarioConectado) throws DAOProblemaConTransaccionException, PSQLException {
        super(usuarioConectado);
    }

    InstitucionCentral(String nombre, Direccion direccion, long telefono, String email,
            int ficheDeCulto, String personeriaJuridica) {
        super(nombre, direccion, telefono, email, ficheDeCulto, personeriaJuridica);
    }
    
    /**
     * Este metodo es sobreescrito en la clase InstitucionCentralSecretaria para que devuelva false
     * @return 
     */
    public boolean sosInstitucionCentralTesoreria(){
        return false;
    }
    
    public Miembro getMiembroAnonimo() {
        PersonaDAO personaDAO = (PersonaDAO) this.getPersistencia().get("PERSONADAO");
        return personaDAO.getMiembroAnonimo();
    }
    
    public Miembro modificarDatosDeMiembro(Miembro miembroSeleccionadoAModificar, String apellido, String nombre, Direccion direccion, long telefonoCelular, long telefonoFijo, GregorianCalendar fechaDeBautismo) throws DAOProblemaConTransaccionException {
        miembroSeleccionadoAModificar.modificarDatos(apellido, nombre, direccion, telefonoCelular, telefonoFijo, fechaDeBautismo);
        PersonaDAO personaDAO = (PersonaDAO) getPersistencia().get("PERSONADAO");
        try {
            personaDAO.persistir(miembroSeleccionadoAModificar);
        } catch (DAOProblemaConTransaccionException ex) {
            try {
                personaDAO.persistir(miembroSeleccionadoAModificar);
            } catch (DAOProblemaConTransaccionException ex2) {
                personaDAO.persistir(miembroSeleccionadoAModificar);
            }
        }
        return miembroSeleccionadoAModificar; //Si se pudo realizar la transacción sin problemas... 
    }
    
    public void eliminarFilial(Filial filialSeleccionada) throws DAOProblemaConTransaccionException {
        FilialDAO filialDAO = (FilialDAO) this.getPersistencia().get("FILIALDAO");
        filialDAO.deleteFilial(filialSeleccionada);
    }
    
}
