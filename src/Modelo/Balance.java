package Modelo;

import java.util.Set;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;

public class Balance {

    private long nroBalance;
    private int anio;
    private Set<Ingreso> ingresos = new HashSet<Ingreso>();
    private Set<Egreso> egresos = new HashSet<Egreso>();
    private CajaEnBalance cajaFinal;
    private GregorianCalendar fechaDeEmision;

    public Balance() {
    }

    public Balance(int anio, CajaEnBalance cajaEnBalance) {
        this.cajaFinal = cajaEnBalance;
        this.anio = anio;
    }

    public void setFechaDeEmision(GregorianCalendar fechaDeEmision) {
        this.fechaDeEmision = fechaDeEmision;
    }

    public GregorianCalendar getFechaDeEmision() {
        return fechaDeEmision;
    }

    public boolean sosBalanceMensual() {
        return false;
    }

    public long getNroBalance() {
        return nroBalance;
    }

    public void setNroBalance(long nroBalance) {
        this.nroBalance = nroBalance;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public Set<Ingreso> getIngresos() {
        return ingresos;
    }

    public void setIngresos(Set<Ingreso> ingresos) {
        this.ingresos = ingresos;
    }

    public Set<Egreso> getEgresos() {
        return egresos;
    }

    public void setEgresos(Set<Egreso> egresos) {
        this.egresos = egresos;
    }

    public double getTotalNeto() {
        double totalIngresos = getTotalIngresos();
        double totalEgresos = getTotalEgresos();
        return totalIngresos - totalEgresos;
    }

    /**
     *
     * @return El monto disponible en caja para este balance, al final del mes.
     */
    public double getDisponibleEnCaja() {
        return cajaFinal.getDisponibilidadEnPesos(); //+ getTotalNeto()??????? así lo tenia anteriormente
    }

    public CajaEnBalance getCajaFinal() {
        return cajaFinal;
    }

    /**
     *
     * @return @return <strong> cero </strong> si no hay ingresos , lo cual no
     * tendría sentido, pero quien dice
     */
    public double getTotalEgresos() {
        Iterator it = this.getEgresos().iterator();
        double totalEgresos = 0;
        while (it.hasNext()) {
            Egreso egreso = (Egreso) it.next();
            totalEgresos += egreso.getTotalNeto();
        }
        return totalEgresos;
    }

    /**
     *
     * @return <strong> cero </strong> si no hay ingresos , lo cual no tendría
     * sentido, pero quien dice
     */
    public double getTotalIngresos() {
        Iterator it = this.getIngresos().iterator();
        double totalIngresos = 0;
        while (it.hasNext()) {
            Ingreso ingreso = (Ingreso) it.next();
            totalIngresos += ingreso.getMonto();
        }
        return totalIngresos;
    }

    public void setCajaFinal(CajaEnBalance cajaFinal) {
        this.cajaFinal = cajaFinal;
    }

    public boolean vacio() {
        return egresos.isEmpty() && ingresos.isEmpty();
    }
}
