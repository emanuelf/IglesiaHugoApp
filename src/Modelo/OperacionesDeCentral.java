package Modelo;

import java.util.GregorianCalendar;



public interface OperacionesDeCentral {

    
    public void registrarIngresoRelatorio (GregorianCalendar periodo, GregorianCalendar fechaDeEmision, Filial filialDelRelatorio, double monto);

}

