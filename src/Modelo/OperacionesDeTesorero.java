/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Excepciones.DAOProblemaConTransaccionException;
import Excepciones.ObjetoNoEncontradoException;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;

/**
 *
 * @author emanuel
 */
public interface OperacionesDeTesorero {

    public java.util.ArrayList<CuentaIngreso> getCuentasIngreso();

    public java.util.ArrayList<CuentaEgreso> getCuentasEgreso();

    public Cuenta buscarCuentaPorcodigo(int codigo) throws Excepciones.ObjetoNoEncontradoException, DAOProblemaConTransaccionException;

    public double consultarCajaDisponible();

    public double calcularSueldoPastor(GregorianCalendar periodo) throws DAOProblemaConTransaccionException, Exception;

    public BalanceAnual generarBalanceAnual(int anioDelBalanceAGenerar) throws DAOProblemaConTransaccionException;

    public HashMap<Integer, DetalleEgreso> getDetallesDeEgresos();

    public Collection consultarIngresosDelMes();

    public BalanceMensual generarBalanceMensual(int mes, int anio) throws Excepciones.DAOProblemaConTransaccionException;

    public Collection consultarEgresosDelMes();

    public Cuenta buscarCuentaPorNombre(String nombre);

    public double sumarTotalMensualCuenta(Collection asientosDelMes);

    public double sumarTotalAnualCuenta(Collection asientosDelAnio);

    public void registrarIngreso(Cuenta cuenta, GregorianCalendar fechaIngreso, double monto) throws DAOProblemaConTransaccionException;

    public void registrarIngresoDiezmo(Miembro miembro, GregorianCalendar periodo, double monto, GregorianCalendar fechaDelIngreso) throws DAOProblemaConTransaccionException;

    public Cuenta registrarNuevaCuenta(String nombre, int codigo, String tipoCuenta) throws Exception;

    public Secretario registrarNuevoUsuarioSecretario(String nombre, String contrasenia) throws Exception;

    public boolean esSocioActivo(Miembro miembro);

    public Pastor devolverDatosDelPastor() throws DAOProblemaConTransaccionException;

    public PastorFilial datosDePastor(String cuidad);

    public void registrarIngresoDonacion(Donante donante, double montoDonacion, GregorianCalendar fecha, String detalleDonacion) throws Exception;

    public PastorFilial devolverDatosDePastor(Filial filial);

    public void cambiarContrasenia(Usuario usuario, String contrasenia) throws DAOProblemaConTransaccionException;

    public void salirDelSistema();

    public Miembro registrarNuevoMiembro(String nombre, String apellido, Direccion direccion, long telefonoFijo, long telefonoCelular, GregorianCalendar fechaBautismo);

    public void actualizarDatosDeMiembro(Miembro miembro) throws Excepciones.DAOProblemaConTransaccionException;

    public void registrarFilial(String nombre, Direccion direccion, String telefono, PastorFilial pastorFilial,
            int ficheroDeCulto) throws DAOProblemaConTransaccionException, Exception;

    public void desvincularMiembro(Miembro miembro);

    public RelatorioFilial devolverRelatorioFilialMensual(Filial filial, int anio, int mes) throws DAOProblemaConTransaccionException, ObjetoNoEncontradoException;

    public RelatorioFilial devolverRelatorioFilialAnual(Filial filial, int anio);
}
