package Modelo;

/**
 * @author emanuel
 */

public class CuentaFilialDeuda extends CuentaFilial {

    public CuentaFilialDeuda(String nombreCuenta) {
        super(nombreCuenta);
    }

    public CuentaFilialDeuda() {
    }

    @Override
    public boolean sosDeuda() {
        return true;
    }  
    
}
