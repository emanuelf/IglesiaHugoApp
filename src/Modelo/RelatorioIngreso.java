package Modelo;

import java.util.GregorianCalendar;


public class RelatorioIngreso extends Ingreso {

   public RelatorioIngreso() {
    }

    RelatorioIngreso(Cuenta cuenta, GregorianCalendar fecha, double monto){
        super(cuenta, fecha, monto);
    }
    
    public double getTotalRelatorio() {
        return this.getMonto();
    }

    @Override
    public String toString() {
        return "Relatorio";
    }
    
    
}

