package Modelo;

import java.util.*;

public class Ingreso extends Asiento {

    public Ingreso() {
    }

    Ingreso(Cuenta cuenta, GregorianCalendar fecha, double monto) {
        super(cuenta, fecha, monto);
    }

    @Override
    public boolean sosIngreso() {
        return true;
    }

    public boolean sosOfrenda() {
        return false;
    }

    public boolean sosDiezmo() {
        return false;
    }

    public boolean sosDonacion() {
        return false;
    }

    public boolean sosOtroIngreso() {
        return false;
    }

    public boolean sosIngresoDeCulto() {
        return false;
    }

    //TODO: parece que funciona. Devuelve una coleccion con asientos(ingresos) que contienen la sumatoria de los ingresos para cada cuenta. o sea, 
    //TODO: los totales por cuenta jeje
    public Collection<Ingreso> getIngresosDeCadaCuenta(ArrayList<Cuenta> cuentasIngresos, Set<Ingreso> ingresos) {
        Collection<Ingreso> ingresosDivididos = new ArrayList<Ingreso>();
        for (Cuenta cuenta : cuentasIngresos) {
            Ingreso ingresoSumatoriaNuevo = new Ingreso();
            ingresoSumatoriaNuevo.setCuenta(cuenta);
            ingresosDivididos.add(ingresoSumatoriaNuevo);
            for (Iterator<Ingreso> it = ingresosDivididos.iterator(); it.hasNext();) {
                Ingreso ingresoSumatoria = it.next();
                for (Ingreso ingreso : ingresos) {
                    if (ingreso.getCuenta().equals(cuenta) && ingresoSumatoria.getCuenta().equals(cuenta)) {
                        ingresoSumatoria.sumarMonto(ingreso.getMonto());
                    }
                }
            }
        }
        return ingresosDivididos;
    }
}
