package Modelo;

/**
 *
 * @author emanuel
 */
public class CuentaIngreso extends Cuenta {
    
    public CuentaIngreso() {
    }

    public CuentaIngreso(String nombreCuenta, int codigo) {
        super(nombreCuenta, codigo);
    }

    public boolean sosDonacion() {
        return this.getNombre().equalsIgnoreCase("Donacion") ? true:false;
    }

    public boolean sosOfrenda() {
        return this.getNombre().equalsIgnoreCase("Ofrenda") ? true:false;
    }

    public boolean sosDiezmo() {
        return this.getNombre().equalsIgnoreCase("Diezmo") ? true:false;
    }
}
