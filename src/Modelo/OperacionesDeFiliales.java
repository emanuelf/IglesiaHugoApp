package Modelo;

import java.util.Collection;
import java.util.GregorianCalendar;



public interface OperacionesDeFiliales {

 
    public RelatorioFilial crearRelatorio (GregorianCalendar periodo, Collection Ingresos, Collection Egresos);

     
    public void guardarRelatorio (RelatorioFilial relatorioCreado);

    
    public RelatorioFilial devolverRelatorio (GregorianCalendar periodo);

}

