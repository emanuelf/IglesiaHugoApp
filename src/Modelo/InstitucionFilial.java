package Modelo;

/**
 *
 * @author emanuel
 */
public class InstitucionFilial extends InstitucionBase {

    InstitucionFilial(String nombre, Direccion direccion, long telefono, String email,
            int ficheDeCulto, String personeriaJuridica) {
        super(nombre, direccion, telefono, email, ficheDeCulto, personeriaJuridica);
    }
}
