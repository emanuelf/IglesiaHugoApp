package Modelo;



public class CajaDisponible extends Caja {

    public CajaDisponible() {
        super();
    }

    CajaDisponible(double disponibilidadEnPesos){
    super(disponibilidadEnPesos,1);
    }

    void sumarCaja(double montoASumar) {
        this.setDisponibilidadEnPesos(this.getDisponibilidadEnPesos()+montoASumar) ;
    }

}

