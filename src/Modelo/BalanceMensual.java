package Modelo;

public class BalanceMensual extends Balance {

    private int mes;

    public BalanceMensual() {
    }

    BalanceMensual(int anio, int mes, CajaEnBalance cajaEnBalance) {
        super(anio, cajaEnBalance);
        this.mes = mes;
    }

    /**
     * @return the mes
     */
    public int getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(int mes) {
        this.mes = mes;
    }

    @Override
    public boolean sosBalanceMensual() {
        return true;
    }
    
    
}
