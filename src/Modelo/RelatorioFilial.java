package Modelo;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

public class RelatorioFilial {

    private long nroRelatorio;
    private GregorianCalendar fechaDeEmision;
    private Filial filial;
    private Set<IngresoRelatorio> ingresoRelatorios = new HashSet<IngresoRelatorio>();
    private Set<EgresoRelatorio> egresoRelatorios = new HashSet<EgresoRelatorio>();
    private Set<DeudaRelatorio> deudaRelatorios = new HashSet<DeudaRelatorio>();
    private double cajaFinalDeMes;
    private double cajaInicialDeMes;
    private int anio;
    private double diezmoFilial;
    private double diezmoPastoral;

    public RelatorioFilial() {
    }

    public RelatorioFilial(GregorianCalendar periodo, Filial filial,
            Set< IngresoRelatorio> ingresos, Set<EgresoRelatorio> egresos, Set<DeudaRelatorio> deudaRelatorios, double cajaInicial, double cajaFinal) {
        fechaDeEmision = periodo;
        this.filial = filial;
        ingresoRelatorios = ingresos;
        egresoRelatorios = egresos;
        this.deudaRelatorios = deudaRelatorios;
        anio = periodo.get(Calendar.YEAR);
        cajaFinalDeMes = cajaFinal;
        cajaInicialDeMes = cajaInicial;
    }

    public RelatorioFilial(long nroRelatorio, GregorianCalendar periodo, Filial filial, double cajaFinalDeMes, double cajaInicialDeMes) {
        this.nroRelatorio = nroRelatorio;
        this.fechaDeEmision = periodo;
        this.filial = filial;
        this.cajaFinalDeMes = cajaFinalDeMes;
        this.cajaInicialDeMes = cajaInicialDeMes;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public GregorianCalendar getFechaDeEmision() {
        return fechaDeEmision;
    }

    public void setFechaDeEmision(GregorianCalendar fechaDeEmision) {
        this.fechaDeEmision = fechaDeEmision;
    }

    public double getCajaFinalDeMes() {
        return cajaFinalDeMes;
    }

    public double getCajaInicialDeMes() {
        return cajaInicialDeMes;
    }

    public void setCajaFinalDeMes(double cajaFinalDeMes) {
        this.cajaFinalDeMes = cajaFinalDeMes;
    }

    public void setCajaInicialDeMes(double cajaInicialDeMes) {
        this.cajaInicialDeMes = cajaInicialDeMes;
    }

    public GregorianCalendar getPeriodo() {
        return fechaDeEmision;
    }

    public double getDiezmoFilial() {
        return diezmoFilial;
    }

    public void setDiezmoFilial(double diezmoFilial) {
        this.diezmoFilial = diezmoFilial;
    }

    public double getDiezmoPastoral() {
        return diezmoPastoral;
    }

    public void setDiezmoPastoral(double diezmoPastoral) {
        this.diezmoPastoral = diezmoPastoral;
    }

    public void setPeriodo(GregorianCalendar val) {
        this.fechaDeEmision = val;
    }

    public Filial getFilial() {
        return filial;
    }

    public void setFilial(Filial val) {
        this.filial = val;
    }

    public Set<IngresoRelatorio> getIngresoRelatorios() {
        return ingresoRelatorios;
    }

    public Set<EgresoRelatorio> getEgresoRelatorios() {
        return egresoRelatorios;
    }

    public int getAnio() {
        return anio;
    }

    public long getNroRelatorio() {
        return nroRelatorio;
    }

    public void setNroRelatorio(long nroRelatorio) {
        this.nroRelatorio = nroRelatorio;
    }

    public void setIngresoRelatorios(Set<IngresoRelatorio> ingresoRelatorios) {
        this.ingresoRelatorios = ingresoRelatorios;
    }

    public void setEgresoRelatorios(Set<EgresoRelatorio> egresoRelatorios) {
        this.egresoRelatorios = egresoRelatorios;
    }

    public Set<DeudaRelatorio> getDeudaRelatorios() {
        return deudaRelatorios;
    }

    public void setDeudaRelatorios(Set<DeudaRelatorio> deudaRelatorios) {
        this.deudaRelatorios = deudaRelatorios;
    }
}
