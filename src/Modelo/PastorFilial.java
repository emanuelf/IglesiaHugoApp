/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.GregorianCalendar;

/**
 *
 * @author emanuel
 */
public class PastorFilial extends Persona {

    private String conyuge;
    private GregorianCalendar fechaDeOrdenacion;
    private long dni;

    public PastorFilial() {
    }

    public PastorFilial(String nombres, String apellidos, Direccion direccion,
        long telefonoFijo, long telefonoCelular,String conyuge, GregorianCalendar fechaDeOrdenacion, long dni) {
        super(nombres, apellidos, direccion, telefonoFijo, telefonoCelular);
        this.conyuge = conyuge;
        this.fechaDeOrdenacion = fechaDeOrdenacion;
        this.dni = dni;
        
    }




    /**
     * @return the conyuge
     */
    public String getConyuge() {
        return conyuge;
    }

    /**
     * @param conyuge the conyuge to set
     */
    public void setConyuge(String conyuge) {
        this.conyuge = conyuge;
    }

    /**
     * @return the fechaDeOrdenacion
     */
    public GregorianCalendar getFechaDeOrdenacion() {
        return fechaDeOrdenacion;
    }

    /**
     * @param fechaDeOrdenacion the fechaDeOrdenacion to set
     */
    public void setFechaDeOrdenacion(GregorianCalendar fechaDeOrdenacion) {
        this.fechaDeOrdenacion = fechaDeOrdenacion;
    }

    /**
     * @return the dni
     */
    public long getDni() {
        return dni;
    }

    /**
     * @param dni the dni to set
     */
    public void setDni(long dni) {
        this.dni = dni;
    }

    public String getApellidoYNombre() {
        return this.getApellidos() + ", " + this.getNombres();
    }
}
