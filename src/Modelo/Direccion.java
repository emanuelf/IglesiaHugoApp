package Modelo;

/**
 *
 * @author Alumno
 */
public class Direccion {

    private String calleOBarrio;
    private int altura;
    private String ciudad;
    private int idDireccion;

    public Direccion() {
    }

    public Direccion(String calleOBarrio, int altura, String ciudad) {
        this.calleOBarrio = calleOBarrio;
        this.altura = altura;
        this.ciudad = ciudad;

    }

    public String getCalleOBarrio() {
        return calleOBarrio;

    }

    public void setCalleOBarrio(String calleOBarrio) {
        this.calleOBarrio = calleOBarrio;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(int idDireccion) {
        this.idDireccion = idDireccion;
    }

    @Override
    public String toString() {
        String direccion = "";
        direccion = direccion.concat(ciudad.isEmpty() ? "[no ciudad]" : ciudad);
        if (getCalleOBarrio().isEmpty())
            direccion = direccion.concat(", [no domicilio]");
        else
            direccion = direccion.concat(", "+ getCalleOBarrio().
                    concat(getAltura() == 0 ? "" : ", " + Integer.toString(altura)));
        return direccion;
    }
}
