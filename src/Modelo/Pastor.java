/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;

/**
 *
 * @author emanuel
 */
public class Pastor extends Lider{
       private double porcentajeSueldoSobreDiezmo;

    public Pastor() {
    }

    public Pastor(double porcentajeSueldoSobreDiezmo,String identificadorPersona, String nombres, String apellidos, Direccion direccion, long telefonoFijo, long telefonoCelular) {
        super();
        this.porcentajeSueldoSobreDiezmo = porcentajeSueldoSobreDiezmo;
    }



    /**
     * @return the porcentajeSueldoSobreDiezmo
     */
    public double getPorcentajeSueldoSobreDiezmo() {
        return porcentajeSueldoSobreDiezmo;
    }

    /**
     * @param porcentajeSueldoSobreDiezmo the porcentajeSueldoSobreDiezmo to set
     */
    public void setPorcentajeSueldoSobreDiezmo(double porcentajeSueldoSobreDiezmo) {
        this.porcentajeSueldoSobreDiezmo = porcentajeSueldoSobreDiezmo;
    }

       
}
