package Modelo;

import Excepciones.DAONoExisteRegistroException;
import Excepciones.DAOProblemaConTransaccionException;
import Excepciones.DAOYaExisteRegistroException;
import Excepciones.ObjetoNoEncontradoException;
import Persistencia.*;
import Vistas.JFrameIngresoAlSistema;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.postgresql.util.PSQLException;

public class InstitucionCentralTesoreria extends InstitucionCentral implements OperacionesDeTesorero {

    private static InstitucionCentral institucion;

    public static InstitucionCentral getInstance() throws DAOProblemaConTransaccionException, PSQLException {
        if(institucion == null)
            institucion = new InstitucionCentralTesoreria();
        return institucion;
    }

    public InstitucionCentralTesoreria() throws DAOProblemaConTransaccionException, PSQLException {
        super();
    }

    public InstitucionCentralTesoreria(UsuarioConectado usuarioConectado) throws DAOProblemaConTransaccionException, PSQLException {
        super(usuarioConectado);
    }

    InstitucionCentralTesoreria(String nombre, Direccion direccion, long telefono, String email, int ficheDeCulto, String personeriaJuridica) {
        super(nombre, direccion, telefono, email, ficheDeCulto, personeriaJuridica);
    }

    public ArrayList<CuentaFilial> getCuentasFiliales() throws DAOProblemaConTransaccionException {
        CuentaDAO cuentaDao = (CuentaDAO) this.getPersistencia().get("CUENTADAO");
        ArrayList<CuentaFilial> cuentasFiliales = new ArrayList<CuentaFilial>();
        cuentasFiliales.addAll(cuentaDao.getCuentasFilialesEgreso());
        cuentasFiliales.addAll(cuentaDao.getCuentasFilialesIngreso());
        cuentasFiliales.addAll(cuentaDao.getCuentasFilialesDeuda());
        return cuentasFiliales;
    }

    public double getDiezmoPastoral(double montoBruto) {
        return getConfiguracion().getPorcentajeParaElPastorSobreLosDiezmosYOfrendas() * montoBruto * 0.1;
    }

    public void updateConfiguracion(double limiteDonacion, double porcentajePastoral, boolean permitirMontoEnCero) throws IllegalArgumentException {
        if ((porcentajePastoral > 100 || porcentajePastoral < 0)) {
            throw new IllegalArgumentException("El porcentaje pastoral debe ser un número entre 0 y 100");
        }
        if ((limiteDonacion == 0 || porcentajePastoral == 0) && !permitirMontoEnCero) {
            throw new IllegalArgumentException("El limite de donación y el porcentaje pastoral no pueden estar en 0");
        }
        porcentajePastoral /= 100;
        getPersistencia().updateConfiguracion(limiteDonacion, porcentajePastoral);
    }

    public void eliminarCuenta(Cuenta cuenta) throws DAOProblemaConTransaccionException {
        CuentaDAO cuentaDAO = (CuentaDAO) this.getPersistencia().get("CUENTADAO");
        cuentaDAO.eliminarCuenta(cuenta);
    }

    public Cuenta buscarCuentaPorcodigo(int codigo) throws Excepciones.ObjetoNoEncontradoException, DAOProblemaConTransaccionException {
        Cuenta retorno = null;
        Iterator it = this.getCuentas().iterator();
        boolean bandera = false;
        while (it.hasNext() && !bandera) {
            Cuenta cuenta = (Cuenta) it.next();
            if (cuenta.getCodigo() == codigo) {
                retorno = cuenta;
                bandera = true;
            }
        }
        if (!bandera) {
            CuentaDAO cuentaDAO = (CuentaDAO) this.getPersistencia().get("CUENTADAO");
            retorno = cuentaDAO.buscarCuentaPorCodigo(codigo);
            if (retorno == null) {
                throw new Excepciones.ObjetoNoEncontradoException("Cuenta inexistente");
            }
        }
        return retorno;
    }

    public Balance consultarBalanceMensual(int anio, int mes) throws ObjetoNoEncontradoException, DAOProblemaConTransaccionException {
        Balance retorno = new Balance();
        BalanceDAO balanceDAO = (BalanceDAO) this.getPersistencia().get("BALANCEDAO");
        retorno = balanceDAO.getBalanceMensual(anio, mes);
        if (retorno == null) {
            throw new ObjetoNoEncontradoException("El balance especificado no existe.");
        }
        return retorno;

    }

    public void actualizarCaja(double montoASumar) throws DAOProblemaConTransaccionException {
        CajaDAO cajaDao = (CajaDAO) this.getPersistencia().get("CAJADAO");
        CajaDisponible cajaDisponible = (CajaDisponible) cajaDao.getCajaDisponible();
        cajaDisponible.sumarCaja(montoASumar);
        cajaDao.actualizarCaja(cajaDisponible);
    }

    public double consultarCajaDisponible() {
        CajaDisponible retorno = null;
        CajaDAO cajaDao = (CajaDAO) this.getPersistencia().get("CAJADAO");
        retorno = (CajaDisponible) cajaDao.getCajaDisponible();
        return retorno.getDisponibilidadEnPesos();
    }

    public double calcularSueldoPastor(GregorianCalendar periodo) throws DAOProblemaConTransaccionException, Exception {
        double retorno = 0;
        PersonaDAO personaDAO = (PersonaDAO) this.getPersistencia().get("PERSONADAO");
        Pastor pastor = personaDAO.getDatosDelPastor();
        BalanceDAO balanceDAO = (BalanceDAO) this.getPersistencia().get("BALANCEDAO");
        Balance balance = balanceDAO.getBalanceMensual(periodo.YEAR, periodo.MONTH);
        if (balance != null) {
            retorno = balance.getDisponibleEnCaja() * pastor.getPorcentajeSueldoSobreDiezmo();
        } else {
            AsientoDAO asientoDAO = (AsientoDAO) this.getPersistencia().get("ASIENTODAO");
            ArrayList<Ingreso> lista = (ArrayList) asientoDAO.buscarAsientosEnBalanceMensual(periodo.YEAR, periodo.MONTH);
            if (lista.isEmpty() || lista == null) {
                throw new Exception("Imposible. No hay ingresos sobre los cuales calcular "
                        + "el sueldo del pastor para el periodo indicado");
            }
            for (Ingreso ingreso : lista) {
                retorno = ingreso.getMonto();
            }

        }

        return retorno * pastor.getPorcentajeSueldoSobreDiezmo();
    }

    public BalanceAnual generarBalanceAnual(int anioBalance) throws DAOProblemaConTransaccionException {
        BalanceAnual balanceAnual = new BalanceAnual();
        BalanceDAO balanceDAO = (BalanceDAO) this.getPersistencia().get("BALANCEDAO");
        List<Egreso> egresosDelAnio = balanceDAO.getEgresosDelAnio(anioBalance);
        List<Ingreso> ingresosDelAnio = balanceDAO.getIngresosDelAnio(anioBalance);
        CajaDAO cajaDAO = (CajaDAO) this.getPersistencia().get("CAJADAO");
        CajaEnBalance cajaEnBalance = new CajaEnBalance(cajaDAO.getCajaDisponible().getDisponibilidadEnPesos());
        balanceAnual.setAnio(anioBalance);
        balanceAnual.setCajaFinal(cajaEnBalance);
        balanceAnual.setEgresos(new HashSet(egresosDelAnio));
        balanceAnual.setIngresos(new HashSet(ingresosDelAnio));
        balanceAnual.setFechaDeEmision((GregorianCalendar) GregorianCalendar.getInstance());
        return balanceAnual;
    }

    /**
     * Este método devuelve Set.empty()=true si no hay ingresos guardados en la
     * base de datos para este mes
     */
    public HashSet<Ingreso> consultarIngresosDelMes() {
        HashSet ingresosDelMes = new HashSet();
        AsientoDAO asientoDAO = (AsientoDAO) this.getPersistencia().get("ASIENTODAO");
        List listaDeIngresos = asientoDAO.buscarIngresos(GregorianCalendar.getInstance().
                get(Calendar.YEAR), GregorianCalendar.getInstance().MONTH);
        if (listaDeIngresos != null || !listaDeIngresos.isEmpty()) {
            ingresosDelMes.addAll(listaDeIngresos);
        }

        return ingresosDelMes;

    }

    public void guardarBalanceAnual(BalanceAnual balance) throws DAOProblemaConTransaccionException {
        BalanceDAO balanceDAO = (BalanceDAO) this.getPersistencia().get("BALANCEDAO");
        try {
            balanceDAO.persistirBalance(balance);
        } catch (DAOProblemaConTransaccionException ex) {
            try {
                balanceDAO.persistirBalance(balance);
            } catch (DAOProblemaConTransaccionException ex1) {
                throw new DAOProblemaConTransaccionException();
            }
        }
    }

    public boolean existeCuenta(int codigo) throws DAOProblemaConTransaccionException {
        try {
            this.buscarCuentaPorcodigo(codigo);
            return true;
        } catch (ObjetoNoEncontradoException ex) {
            return false;
        }
    }

    public void guardarBalanceMensual(BalanceMensual balance) throws DAOProblemaConTransaccionException {
        BalanceDAO balanceDAO = (BalanceDAO) this.getPersistencia().get("BALANCEDAO");
        try {
            balanceDAO.persistirBalance(balance);
        } catch (DAOProblemaConTransaccionException ex) {
            try {
                balanceDAO.persistirBalance(balance);
            } catch (DAOProblemaConTransaccionException ex1) {
                throw new DAOProblemaConTransaccionException();
            }
        }
    }

    public BalanceMensual generarBalanceMensual(int mes, int anio) throws DAOProblemaConTransaccionException {
        BalanceDAO balanceDAO = (BalanceDAO) this.getPersistencia().get("BALANCEDAO");
        List<Egreso> egresosDelAnio = balanceDAO.getEgresosDelMes(anio, mes);
        List<Ingreso> ingresosDelAnio = balanceDAO.getIngresosDelMes(anio, mes);
        CajaDAO cajaDAO = (CajaDAO) this.getPersistencia().get("CAJADAO");
        CajaEnBalance cajaEnBalance = new CajaEnBalance(cajaDAO.getCajaDisponible().getDisponibilidadEnPesos());
        BalanceMensual balanceMensual = new BalanceMensual(anio, mes, cajaEnBalance);
        balanceMensual.setEgresos(new HashSet(egresosDelAnio));
        balanceMensual.setIngresos(new HashSet(ingresosDelAnio));
        balanceMensual.setFechaDeEmision((GregorianCalendar) Calendar.getInstance());
        return balanceMensual;
    }

    public void persistirBalanceMensual(BalanceMensual balance) throws DAOProblemaConTransaccionException {
        BalanceDAO balanceDAO = (BalanceDAO) this.getPersistencia().get("BALANCEDAO");
        balanceDAO.persistirBalance(balance);
    }

    public void persistirBalanceAnual(BalanceAnual balance) throws DAOProblemaConTransaccionException {
        BalanceDAO balanceDAO = (BalanceDAO) this.getPersistencia().get("BALANCEDAO");
        balanceDAO.persistirBalance(balance);
    }

    public HashSet<Egreso> consultarEgresosDelMes() {
        HashSet egresosDelMes = new HashSet();
        AsientoDAO asientoDAO = (AsientoDAO) this.getPersistencia().get("ASIENTODAO");
        List listaDeEgresos = asientoDAO.buscarEgresos(new GregorianCalendar().get(Calendar.YEAR), new GregorianCalendar().get(Calendar.MONTH));
        if (listaDeEgresos != null || !listaDeEgresos.isEmpty()) {
            egresosDelMes.addAll(listaDeEgresos);
        }

        return egresosDelMes;
    }

    /**
     * @return null, si no encuentra la cuenta. O
     * @return una instancia de Cuenta específica, si existe una con el nombre
     * especificado.
     */
    public Cuenta buscarCuentaPorNombre(String nombre) {
        Cuenta retorno = null;
        try {
            if (!this.getCuentas().isEmpty()) {
                Iterator it = this.getCuentas().iterator();
                boolean bandera = false;
                while (it.hasNext() && !bandera) {
                    Cuenta cuenta = (Cuenta) it.next();
                    if (cuenta.getNombre().equals(nombre)) {
                        retorno = cuenta;
                        bandera = true;
                    }
                }
            } else {
                CuentaDAO cuentaDAO = (CuentaDAO) this.getPersistencia().get("CUENTADAO");
                retorno = cuentaDAO.buscarCuentaPorNombre(nombre);
            }
        } catch (DAOProblemaConTransaccionException ex) {
            Logger.getLogger(InstitucionCentralTesoreria.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }

    public double sumarTotalMensualCuenta(Collection asientosDelMes) {
        double retorno = 0;
        Iterator it = asientosDelMes.iterator();
        while (it.hasNext()) {
            Ingreso ingreso = (Ingreso) it.next();
            retorno = ingreso.getMonto() + retorno;
        }
        return retorno;
    }

    public double sumarTotalAnualCuenta(Collection asientosDelAnio) {
        double retorno = 0;
        Iterator it = asientosDelAnio.iterator();
        while (it.hasNext()) {
            Ingreso ingreso = (Ingreso) it.next();
            retorno = ingreso.getMonto() + retorno;
        }
        return retorno;
    }

    public void registrarEgreso(Cuenta cuenta, GregorianCalendar fechaEgreso, double monto, String detalle, double pagado)
            throws DAOProblemaConTransaccionException {
        DetalleEgreso detalleEgreso = new DetalleEgreso(detalle);
        Egreso egresoNuevo = new Egreso(detalleEgreso, cuenta, fechaEgreso, monto, pagado);
        AsientoDAO asientoDAO = (AsientoDAO) this.getPersistencia().get("ASIENTODAO");
        actualizarCaja(monto);
        asientoDAO.persistir(egresoNuevo);
    }

    public void registrarIngreso(Cuenta cuenta, GregorianCalendar fechaIngreso, double monto) throws DAOProblemaConTransaccionException {
        Ingreso ingresoNuevo = new Ingreso(cuenta, fechaIngreso, monto);
        AsientoDAO asientoDAO = (AsientoDAO) this.getPersistencia().get("ASIENTODAO");
        asientoDAO.persistir(ingresoNuevo);
        actualizarCaja(monto);
    }

    public void registrarIngresoDiezmo(Miembro miembro, GregorianCalendar periodo, double monto, GregorianCalendar fechaDelIngreso) throws DAOProblemaConTransaccionException {
        Diezmo diezmoNuevo = new Diezmo(this.buscarCuentaPorNombre("diezmo"), periodo, monto);
        diezmoNuevo.setFecha(fechaDelIngreso);
        AsientoDAO asientoDAO = (AsientoDAO) this.getPersistencia().get("ASIENTODAO");
        asientoDAO.persistir(diezmoNuevo);
        actualizarCaja(monto);

    }

    public void registrarIngresoDonacion(Donante donante, double montoDonacion, GregorianCalendar fecha,
            String detalleDonacion) throws Exception {
        Cuenta cuenta = this.buscarCuentaPorNombre("donacion");
        Donacion donacion = null;
        if (cuenta == null) {
            throw new Exception("Debe primero generar los datos de la cuenta Donacion.");
        } else {
            donacion = new Donacion(donante, detalleDonacion, cuenta, fecha, montoDonacion);
            AsientoDAO asientoDAO = (AsientoDAO) this.getPersistencia().get("ASIENTODAO");
            asientoDAO.persistir(donacion);
            actualizarCaja(montoDonacion);
        }
    }

    /**
     * @exception si el usuario ya existe
     */
    public Secretario registrarNuevoUsuarioSecretario(String nombre, String contrasenia) throws DAOYaExisteRegistroException, DAOProblemaConTransaccionException {
        if (this.buscarUsuario(nombre, contrasenia) != null) {
            throw new DAOYaExisteRegistroException("Ya existe un usuario con esta nombre de usuario. "
                    + "Escoja otro.");
        }
        Secretario usuarioSecretario = new Secretario(nombre, contrasenia);
        UsuarioDAO usuarioDAO = (UsuarioDAO) this.getPersistencia().get("USUARIODAO");
        usuarioDAO.persistirOGuardar(usuarioSecretario);
        return usuarioSecretario;
    }

    /**
     * si tiene para el periodo pedido, devuelve el relatorio, sino, retorno
     * <code> null </code>
     *
     * @param filial
     * @param anio
     * @param mes
     * @return
     * @throws DAOProblemaConTransaccionException
     */
    public RelatorioFilialMensual devolverRelatorioFilialMensual(Filial filial, int anio, int mes) throws DAOProblemaConTransaccionException,
            ObjetoNoEncontradoException {
        RelatorioFilialMensual retorno = null;
        RelatorioDAO relatorioDAO = (RelatorioDAO) this.getPersistencia().get("RELATORIODAO");
        filial = relatorioDAO.loadRelatoriosDeFilial(filial);
        if (filial.tieneRelatorios()) {
            Iterator it = (Iterator) filial.getMisRelatorios().iterator();
            boolean encontrado = false;
            while (it.hasNext() && !encontrado) {
                RelatorioFilialMensual relatorio = (RelatorioFilialMensual) it.next();
                if (relatorio.getAnio() == anio && relatorio.getMes() == mes) {
                    retorno = relatorio;
                }
            }
        }
        if (retorno == null) {
            throw new ObjetoNoEncontradoException("No existe el relatorio cargado");
        }
        return retorno;

    }

    public RelatorioFilialAnual devolverRelatorioFilialAnual(Filial filial, int anio) {
        //este metodo no debería ni siquiera existir, lo dejo por si las moscas luego lo necesito para devolver relatorios de filiales anuales, que por
        //ahora no necesito
        Iterator it = (Iterator) filial.getMisRelatorios();
        boolean encontrado = false;
        while (it.hasNext() && !encontrado) {
            RelatorioFilialAnual relatorio = (RelatorioFilialAnual) it.next();
            if (relatorio.getAnio() == anio) {
                return relatorio;
            }
        }
        return null;
    }

    /**
     * @return Null si no existe ningun miembro registrado. O un Set cargado de
     * Miembros, en caso contrario.
     */
    @Override
    public Set<Miembro> buscarMiembros(String nombre, String apellido) throws DAOProblemaConTransaccionException {
        HashSet<Miembro> miembros = new HashSet<Miembro>();
        PersonaDAO personaDAO = (PersonaDAO) this.getPersistencia().get("PERSONADAO");
        List<Miembro> miembrosDevueltosPorPersistencia = personaDAO.getMiembros(nombre, apellido);
        miembros.addAll(miembrosDevueltosPorPersistencia);
        return miembros;
    }

    public Balance consultarBalanceAnual(int anio) throws ObjetoNoEncontradoException, DAOProblemaConTransaccionException {
        Balance retorno = new Balance();
        BalanceDAO balanceDAO = (BalanceDAO) this.getPersistencia().get("BALANCEDAO");
        retorno = balanceDAO.getBalanceAnual(anio);
        if (retorno == null) {
            throw new Excepciones.ObjetoNoEncontradoException("Balance inexistente");
        }
        return retorno;
    }

    public RelatorioFilialMensual saveRelatorio(GregorianCalendar fechaDeEmision, Filial filialDelRelatorio,
            Set< IngresoRelatorio> ingresos, Set<EgresoRelatorio> egresos, Set<DeudaRelatorio> deudas, double cajaInicial, double cajaFinal,
            double diezmoFilial, double diezmoPastoral)
            throws DAOProblemaConTransaccionException {
        RelatorioFilialMensual relatorioDeFilial = new RelatorioFilialMensual(filialDelRelatorio, fechaDeEmision, ingresos, egresos, deudas,
                cajaInicial, cajaFinal);
        relatorioDeFilial.setDiezmoFilial(diezmoFilial);
        relatorioDeFilial.setDiezmoPastoral(diezmoPastoral);
        RelatorioDAO relatorioDAO = (RelatorioDAO) this.getPersistencia().get("RELATORIODAO");
        relatorioDAO.save(relatorioDeFilial);
        return relatorioDeFilial;

    }

    public HashMap<Integer, DetalleEgreso> getDetallesDeEgresos() {
        HashMap retorno = null;
        AsientoDAO asientoDAO = (AsientoDAO) this.getPersistencia().get("ASIENTODAO");
        retorno = asientoDAO.getDetallesDeEgresos();
        return retorno;
    }

    /**
     *
     * @param nombre
     * @param codigo
     * @throws Exception La excepción es solo para dar aviso a que
     */
    public Cuenta registrarNuevaCuenta(String nombre, int codigo, String tipoCuenta) throws DAOProblemaConTransaccionException, DAOYaExisteRegistroException {
        CuentaDAO cuentaDAO = (CuentaDAO) this.getPersistencia().get("CUENTADAO");
        if (cuentaDAO.buscarCuentaPorCodigo(codigo) != null || cuentaDAO.buscarCuentaPorNombre(nombre) != null) {
            throw new Excepciones.DAOYaExisteRegistroException("Ya existe una cuenta con este código y/o nombre.");
        } else {
            if (tipoCuenta.equalsIgnoreCase("Egreso")) {
                CuentaEgreso cuentaNueva = new CuentaEgreso(nombre, codigo);
                cuentaDAO.agregarNuevaCuenta(cuentaNueva);
                return cuentaNueva = (CuentaEgreso) cuentaDAO.buscarCuentaPorCodigo(cuentaNueva.getCodigo());
            } else {
                CuentaIngreso cuentaNueva = new CuentaIngreso(nombre, codigo);
                cuentaDAO.agregarNuevaCuenta(cuentaNueva);
                return cuentaNueva = (CuentaIngreso) cuentaDAO.buscarCuentaPorCodigo(cuentaNueva.getCodigo());
            }
        }

    }

    public ArrayList<CuentaIngreso> getCuentasIngreso() {
        ArrayList<CuentaIngreso> cuentasIngreso = new ArrayList<CuentaIngreso>();
        CuentaDAO cuentaDAO = (CuentaDAO) this.getPersistencia().get("CUENTADAO");
        try {
            ArrayList<CuentaIngreso> cuentasIngresos = (ArrayList<CuentaIngreso>) cuentaDAO.getCuentasIngreso();
            cuentasIngreso.addAll(cuentasIngresos);
        } catch (DAOProblemaConTransaccionException ex) {
            Logger.getLogger(InstitucionCentralTesoreria.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cuentasIngreso;
    }

    public ArrayList<CuentaEgreso> getCuentasEgreso() {
        ArrayList<CuentaEgreso> cuentasEgreso = new ArrayList<CuentaEgreso>();
        CuentaDAO cuentaDAO = (CuentaDAO) this.getPersistencia().get("CUENTADAO");
        try {
            ArrayList<CuentaEgreso> cuentaEgresos = (ArrayList<CuentaEgreso>) cuentaDAO.getCuentasEgreso();
            cuentasEgreso.addAll(cuentaEgresos);
        } catch (DAOProblemaConTransaccionException ex) {
            Logger.getLogger(InstitucionCentralTesoreria.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cuentasEgreso;
    }

    public void guardarEgresos(ArrayList<Egreso> egresosCargados) throws DAOProblemaConTransaccionException {
        AsientoDAO asientoDAO = (AsientoDAO) this.getPersistencia().get("ASIENTODAO");
        asientoDAO.pesistir(egresosCargados);
        Iterator it = egresosCargados.iterator();
        double montoTotalAActualizarEnCaja = 0;
        while (it.hasNext()) {
            Egreso egreso = (Egreso) it.next();
            if (egreso.pagadoPorCompleto()) {
                montoTotalAActualizarEnCaja -= egreso.getMonto();
            } else if (egreso.pagadoEnParte()) {
                montoTotalAActualizarEnCaja -= egreso.getPagado();
            }
        }
        actualizarCaja(montoTotalAActualizarEnCaja);
    }

    public void actualizarDatosDeMiembro(Miembro miembro) throws DAOProblemaConTransaccionException {
        PersonaDAO personaDAO = (PersonaDAO) this.getPersistencia().get("PERSONADAO");
        personaDAO.persistir(miembro);
    }

    /**
     * @return <strong> null </strong> si el miembro no existe, una instancia de
     * <strong> Miembro </strong> si el miembro con el parametro <strong> id
     * </strong> existe.
     */
    public Miembro buscarMiembroPorId(long idMiembro) {
        PersonaDAO personaDAO = (PersonaDAO) this.getPersistencia().get("PERSONADAO");
        Miembro retorno = null;
        retorno = personaDAO.buscarMiembro(idMiembro);
        return retorno;
    }

    public void guardarIngresos(ArrayList<Ingreso> ingresosCargados) throws DAOProblemaConTransaccionException {
        AsientoDAO asientoDAO = (AsientoDAO) this.getPersistencia().get("ASIENTODAO");
        ArrayList<Diezmo> diezmos = new ArrayList<Diezmo>();
        ArrayList<Ofrenda> ofrendas = new ArrayList<Ofrenda>();
        ArrayList<Donacion> donaciones = new ArrayList<Donacion>();
        ArrayList<OtroIngreso> otrosIngresos = new ArrayList<OtroIngreso>();
        Iterator< Ingreso> it = ingresosCargados.iterator();
        double totalIngresos = 0;
        while (it.hasNext()) {
            Ingreso ingreso = it.next();
            if (ingreso.sosDiezmo()) {
                diezmos.add((Diezmo) ingreso);
                totalIngresos += ingreso.getMonto();
            }
            if (ingreso.sosDonacion()) {
                donaciones.add((Donacion) ingreso);
                totalIngresos += ingreso.getMonto();
            }
            if (ingreso.sosOfrenda()) {
                ofrendas.add((Ofrenda) ingreso);
                totalIngresos += ingreso.getMonto();
            }
            if (ingreso.sosOtroIngreso()) {
                otrosIngresos.add((OtroIngreso) ingreso);
            }
        }
        if (!diezmos.isEmpty()) {
            asientoDAO.persistirDiezmos(diezmos);
        }
        if (!ofrendas.isEmpty()) {
            asientoDAO.persistirOfrendas(ofrendas);
        }
        if (!donaciones.isEmpty()) {
            asientoDAO.persistirDonaciones(donaciones);
        }
        if (!otrosIngresos.isEmpty()) {
            asientoDAO.persistirOtrosIngresos(otrosIngresos);
        }
        this.actualizarCaja(totalIngresos);
    }

    public void modificarDatosDeCuenta(Cuenta cuentaAModificar, String nombreCuenta, Integer codigo, String tipoCuenta) throws DAOProblemaConTransaccionException {
        CuentaDAO cuentaDAO = (CuentaDAO) this.getPersistencia().get("CUENTADAO");
        cuentaDAO.eliminarCuenta(cuentaAModificar);
        if (tipoCuenta.equalsIgnoreCase("Ingreso")) {
            CuentaEgreso cuentaEgreso = new CuentaEgreso(nombreCuenta, codigo);
            cuentaDAO.agregarNuevaCuenta(cuentaEgreso);
        } else {
            CuentaIngreso cuentaIngreso = new CuentaIngreso(nombreCuenta, codigo);
            cuentaDAO.agregarNuevaCuenta(cuentaIngreso);
        }
    }

    /**
     * @param anio
     * @param mes
     * @return <strong> null </strong> si no hay ingresos en ese tiempo, los
     * ingresos en caso contrario.
     */
    public ArrayList<Ingreso> buscarIngresos(int anio, int mes) {
        ArrayList<Ingreso> asientosBuscados = null;
        AsientoDAO cuentaDAO = (AsientoDAO) this.getPersistencia().get("ASIENTODAO");
        asientosBuscados = (ArrayList<Ingreso>) cuentaDAO.buscarIngresos(anio, mes);
        return asientosBuscados;
    }

    /**
     *
     * @param anio
     * @param mes
     * @return <strong> null </strong> si no hay ingresos en ese tiempo, los
     * ingresos en caso contrario.
     */
    public ArrayList<Egreso> buscarEgresos(int anio, int mes) {
        ArrayList<Egreso> asientosBuscados = null;
        AsientoDAO cuentaDAO = (AsientoDAO) this.getPersistencia().get("ASIENTODAO");
        asientosBuscados = (ArrayList<Egreso>) cuentaDAO.buscarEgresos(anio, mes);
        return asientosBuscados;
    }

    /**
     *
     * @param cuentaFilial
     * @throws DAOProblemaConTransaccionException
     * @throws Exception si la cuenta ya existe
     */
    public void addCuentaFilial(CuentaFilial cuentaFilial) throws DAOProblemaConTransaccionException, DAOYaExisteRegistroException {
        if (getCuentaFilial(cuentaFilial.getNombre()) != null) {
            throw new Excepciones.DAOYaExisteRegistroException();
        }
        cuentaFilial.setNombre(cuentaFilial.getNombre().trim());
        CuentaDAO cuentaDao = (CuentaDAO) getPersistencia().get("CUENTADAO");
        cuentaDao.addCuenta(cuentaFilial);
    }

    /**
     *
     * @param nombreDeLaCuenta
     * @return <strong> null </strong> si no hay una cuenta con este <strong>
     * nombreDeLaCuenta </strong>, la cuenta en caso contrario.
     */
    public CuentaFilial getCuentaFilial(String nombreDeLaCuenta) throws DAOProblemaConTransaccionException {
        CuentaFilial retorno = null;
        Iterator<CuentaFilial> cuentas = (Iterator<CuentaFilial>) this.getCuentasFiliales().iterator();
        while (cuentas.hasNext()) {
            CuentaFilial cuentaFilial = cuentas.next();
            if (cuentaFilial.esTuNombre(nombreDeLaCuenta)) {
                retorno = cuentaFilial;
                break;
            }
        }
        return retorno;

    }

    public BalanceAnual getBalanceAnual(int anioDeBalance) throws DAOProblemaConTransaccionException {
        BalanceDAO balanceDAO = (BalanceDAO) this.getPersistencia().get("BALANCEDAO");
        BalanceAnual retorno = (BalanceAnual) balanceDAO.getBalanceAnual(anioDeBalance);
        return retorno;
    }

    public BalanceMensual getBalanceMensual(int anioDelBalanceAGenerar, int mesDelBalanceAGenerar) throws DAOProblemaConTransaccionException {
        BalanceDAO balanceDAO = (BalanceDAO) this.getPersistencia().get("BALANCEDAO");
        BalanceMensual retorno = (BalanceMensual) balanceDAO.getBalanceMensual(anioDelBalanceAGenerar, mesDelBalanceAGenerar);
        return retorno;
    }

    public ArrayList<Cuenta> getCuentas() throws DAOProblemaConTransaccionException {
        ArrayList<Cuenta> cuentasARetornar = new ArrayList<Cuenta>();
        cuentasARetornar.addAll(this.getCuentasEgreso());
        cuentasARetornar.addAll(this.getCuentasIngreso());
        return cuentasARetornar;
    }

    public void eliminarIngreso(Ingreso ingresoAEliminar) throws DAOProblemaConTransaccionException {
        AsientoDAO asientoDAO = (AsientoDAO) this.getPersistencia().get("ASIENTODAO");
        asientoDAO.eliminarAsiento(ingresoAEliminar);
    }

    public void eliminarEgreso(Egreso egresoAEliminar) throws DAOProblemaConTransaccionException {
        AsientoDAO asientoDAO = (AsientoDAO) this.getPersistencia().get("ASIENTODAO");
        asientoDAO.eliminarAsiento(egresoAEliminar);
    }

    public void actualizarEgreso(Egreso egresoAModificar) throws DAOProblemaConTransaccionException {
        AsientoDAO asientoDAO = (AsientoDAO) this.getPersistencia().get("ASIENTODAO");
        asientoDAO.actualizarEgreso(egresoAModificar);
    }

    public void actualizarIngreso(Ingreso ingresoAModificar) throws DAOProblemaConTransaccionException {
        AsientoDAO asientoDAO = (AsientoDAO) this.getPersistencia().get("ASIENTODAO");
        asientoDAO.actualizarIngreso(ingresoAModificar);
    }

    public ArrayList<Usuario> getUsuarios() {
        ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
        UsuarioDAO usuarioDAO = (UsuarioDAO) this.getPersistencia().get("USUARIODAO");
        usuarios = usuarioDAO.getAllUsuarios();
        return usuarios;
    }

    public void deleteUsuario(Usuario usuarioSeleccionado) throws DAOProblemaConTransaccionException {
        UsuarioDAO usuarioDAO = (UsuarioDAO) this.getPersistencia().get("USUARIODAO");
        usuarioDAO.eliminarUsuario(usuarioSeleccionado);
    }

    /**
     *
     * @param usuarioAModificar
     * @param nombreUsuario
     * @param contrasenia
     * @return El usuario luego de ser modificado
     */
    public Usuario modificarUsuario(Usuario usuarioAModificar, String nombreUsuario, String contrasenia) {
        usuarioAModificar.setContrasenia(contrasenia);
        usuarioAModificar.setNombre(nombreUsuario);
        UsuarioDAO usuarioDAO = (UsuarioDAO) this.getPersistencia().get("USUARIODAO");
        usuarioDAO.updateUsuario(usuarioAModificar);
        return usuarioAModificar;
    }
    //De aquí se inicia el sistema

    public int getCantidadDeMiembros() {
        int retorno = 0;
        PersonaDAO personaDAO = (PersonaDAO) this.getPersistencia().get("PERSONADAO");
        retorno = personaDAO.getCantidadDeMiembros();
        return retorno;
    }

    public void guardarIngresoEnCulto(IngresoEnCulto ingresoEnCulto) throws DAOProblemaConTransaccionException {
        AsientoDAO asientoDAO = (AsientoDAO) this.getPersistencia().get("ASIENTODAO");
        asientoDAO.saveIngresoEnCulto(ingresoEnCulto);
    }

    public void actualizarDatosDeMiembros(ArrayList<Miembro> miembrosAActualizar) throws DAOProblemaConTransaccionException {
        PersonaDAO personaDAO = (PersonaDAO) this.getPersistencia().get("PERSONADAO");
        Iterator<Miembro> iterator = miembrosAActualizar.iterator();
        while (iterator.hasNext()) {
            Miembro miembro = iterator.next();
            personaDAO.persistir(miembro);
        }
    }

    public ArrayList<Donante> getDonantes() {
        PersonaDAO personaDAO = (PersonaDAO) this.getPersistencia().get("PERSONADAO");
        return personaDAO.getDonantes();
    }

    public boolean hayIngresosEnCultoParaEstaFecha(Date date) {
        boolean retorno = false;
        AsientoDAO asientoDAO = (AsientoDAO) this.getPersistencia().get("ASIENTODAO");
        IngresoEnCulto ingresoEnCulto = asientoDAO.getIngresoEnCulto(date);
        if (ingresoEnCulto != null) {
            return true;
        } else {
            return false;
        }
    }

    public void saveOrUpdateDatosDeInstitucion() {
        this.getPersistencia().updateDatosDeInstitucion(this);
    }

    public boolean hayDatosDeInstitucion() {
        InstitucionCentral a = this.getPersistencia().getInstitucion();
        if(a == null){
            return false;
        }
        this.setDireccion(a.getDireccion());
        this.setEmail(a.getEmail());
        this.setFicheDeCulto(a.getFicheDeCulto());
        this.setNombre(a.getNombre());
        this.setPersoneriaJuridica(a.getPersoneriaJuridica());
        this.setTelefono(a.getTelefono());
        return true;
    }

    public boolean hayCuentasEgreso() {
        return ((CuentaDAO) this.getPersistencia().get("CUENTADAO")).cantidadCuentasEgreso() > 0;
    }

    public static void main(String args[]) throws DAOProblemaConTransaccionException, DAONoExisteRegistroException {
        JFrameIngresoAlSistema frameIngresoAlSistema = null;
        InstitucionCentral institucionCentral = null;
        try {
            institucionCentral = InstitucionCentralTesoreria.getInstance();
            institucionCentral.getPersistencia().setNotNullFalseInEgresoAndIngresoRelatorioIfNecessary();
        } catch (PSQLException ex) {

        }
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            frameIngresoAlSistema = new Vistas.JFrameIngresoAlSistema(institucionCentral);
            frameIngresoAlSistema.setLocationRelativeTo(null);
            frameIngresoAlSistema.setVisible(true);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InstitucionCentralTesoreria.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(InstitucionCentralTesoreria.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(InstitucionCentralTesoreria.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(InstitucionCentralTesoreria.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Asiento> toAsientosPorCuentas(Set<Ingreso> ingresos, Set<Egreso> egresos) {
        @SuppressWarnings("unchecked")
        ArrayList<Asiento> retorno = new ArrayList();
        ArrayList<Cuenta> cuentasIngresos = Ingreso.getCuentas(ingresos);
        ArrayList<Cuenta> cuentasEgresos = Egreso.getCuentas(egresos);
        Collection<Ingreso> ingresosDeCadaCuenta = new Ingreso().getIngresosDeCadaCuenta(cuentasIngresos, ingresos);
        Collection<Egreso> egresosDeCadaCuenta = new Egreso().getEgresosDeCadaCuenta(cuentasEgresos, egresos);
        for (Iterator<Egreso> it = egresosDeCadaCuenta.iterator(); it.hasNext();) {
            Egreso egreso = it.next();
            retorno.add(egreso);
        }
        for (Iterator<Ingreso> it = ingresosDeCadaCuenta.iterator(); it.hasNext();) {
            Ingreso ingreso = it.next();
            retorno.add(ingreso);
        }
        return retorno;
    }

    public void eliminarRelatorio(RelatorioFilialMensual relatorio) throws DAOProblemaConTransaccionException {
        RelatorioDAO dao = (RelatorioDAO) getPersistencia().get("RELATORIODAO");
        dao.remove(relatorio);
    }

    public boolean hayMiembrosCargados() {
        if (getCantidadDeMiembros() == 1 && getMiembroAnonimo() != null) {
            return false;
        }
        return true;
    }
}
