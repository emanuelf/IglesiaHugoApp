package Modelo;

public class Caja {

    private double disponibilidadEnPesos;
    private int id;

    public Caja(double disponibilidadEnPesos, int id) {
        this.disponibilidadEnPesos = disponibilidadEnPesos;
        this.id = id;
    }

    public Caja() {
    }
    
    public Caja(double disponibilidadEnPesos){
        this.disponibilidadEnPesos= disponibilidadEnPesos;
    }

    /**
     * @return the disponibilidadEnPesos
     */
    public double getDisponibilidadEnPesos() {
        return disponibilidadEnPesos;
    }

    /**
     * @param disponibilidadEnPesos the disponibilidadEnPesos to set
     */
    public void setDisponibilidadEnPesos(double disponibilidadEnPesos) {
        this.disponibilidadEnPesos = disponibilidadEnPesos;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

}
