package Modelo;

import java.util.GregorianCalendar;

public class Ofrenda extends Ingreso {

    public Ofrenda() {
    }

    public Ofrenda(Cuenta cuenta, GregorianCalendar fecha, double monto) {
        super(cuenta, fecha, monto);
    }

    @Override
    public String toString() {
        return "Ofrenda";
    }

    @Override
    public boolean sosOfrenda() {
        return true;
    }

    @Override
    public boolean sosIngresoDeCulto() {
        return true;
    }
}