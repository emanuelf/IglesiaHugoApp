package Modelo;

public class EgresoRelatorio {

    private CuentaFilialEgreso cuentaFilialEgreso;
    private double monto;
    private long nroAsientoEgresoRelatorio;

    public EgresoRelatorio() {
    }

    public EgresoRelatorio(CuentaFilialEgreso cuentaFilialEgreso, double monto) {
        this.cuentaFilialEgreso = cuentaFilialEgreso;
        this.monto = monto;
    }

    public CuentaFilialEgreso getCuentaFilialEgreso() {
        return cuentaFilialEgreso;
    }

    public void setCuentaFilialEgreso(CuentaFilialEgreso cuentaFilialEgreso) {
        this.cuentaFilialEgreso = cuentaFilialEgreso;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double unMonto) {
        this.monto = unMonto;
    }

    public long getNroAsientoEgresoRelatorio() {
        return nroAsientoEgresoRelatorio;
    }

    public void setNroAsientoEgresoRelatorio(long nroAsientoEgresoRelatorio) {
        this.nroAsientoEgresoRelatorio = nroAsientoEgresoRelatorio;
    }
}
