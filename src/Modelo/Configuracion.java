package Modelo;

/**
 * Configuracion.java
 * 
 * Created on 24/10/2011, 19:06:48
 * 
 * @author CeroYUno Informática. Soluciones Integrales
 */
public class Configuracion {
    
    private double porcentajeParaElPastorSobreLosDiezmosYOfrendas;
    private double limiteDeDonacion;
    private int id; 
    
    //<editor-fold defaultstate="collapsed" desc="constructores">
    public Configuracion() {
    }
    
    public Configuracion(double porcentajeParaElPastorSobreLosDiezmosYOfrendas, double limiteDeDonacion) {
        this.porcentajeParaElPastorSobreLosDiezmosYOfrendas = porcentajeParaElPastorSobreLosDiezmosYOfrendas;
        this.limiteDeDonacion = limiteDeDonacion;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters y setters">
    public void setLimiteDeDonacion(double limiteDeDonacion) {
        this.limiteDeDonacion = limiteDeDonacion;
    }
    
    public void setPorcentajeParaElPastorSobreLosDiezmosYOfrendas(double porcentajeParaElPastorSobreLosDiezmosYOfrendas) {
        this.porcentajeParaElPastorSobreLosDiezmosYOfrendas = porcentajeParaElPastorSobreLosDiezmosYOfrendas;
    }
    
    public double getLimiteDeDonacion() {
        return limiteDeDonacion;
    }
    
    public double getPorcentajeParaElPastorSobreLosDiezmosYOfrendas() {
        return porcentajeParaElPastorSobreLosDiezmosYOfrendas;
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    //</editor-fold>
    
    public double getPorcentajeDeDiezmoSobreMonto(double monto){
        if(porcentajeParaElPastorSobreLosDiezmosYOfrendas ==0) return monto;
        return porcentajeParaElPastorSobreLosDiezmosYOfrendas*monto;
    }
    
}
