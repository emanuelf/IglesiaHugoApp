package Modelo;

/**
 *
 * @author emanuel
 */
public class CuentaFilialEgreso extends CuentaFilial{

    public CuentaFilialEgreso(String nombre) {
        super(nombre);
    }

    public CuentaFilialEgreso() {
    }

    @Override
    public boolean sosEgreso() {
        return true;
    }
    
    
}
