package Util;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.util.Arrays;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRXlsExporter;

public class ReportUtils {

    private static Connection connection;
    private static final String EXTENSION_REPORTE = ".jrxml";
    private static final String CARPETA_REPORTES = "/reportes/";

    static {
        try {
            connection = Persistencia.Persistencia.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void showImage(final Image image) {
        JPanel muestra = new JPanel() {
            @Override
            public void paintComponent(Graphics graphics) {
                Graphics2D g2d = (Graphics2D) graphics;
                try {
                    g2d.drawImage(image, 0, 0, null);
                } catch (NullPointerException ex) {
                }
            }
        };
        muestra.setPreferredSize(new Dimension(image.getWidth(muestra), image.getHeight(muestra)));
        JFrame ventana = new JFrame();
        ventana.setContentPane(muestra);
        ventana.pack();
        ventana.setVisible(true);
        ventana.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    private ReportUtils() {
    }

    public static void previewReport(String nombreReporte, Map<String, Object> parametros) {
        parametros.put("SUBREPORT_DIR", getPathOfReports());
        JasperPrint jasperPrint = prepareReport(nombreReporte, parametros);
        final Image reportAsImage;
        try {
            reportAsImage = JasperPrintManager.printPageToImage(jasperPrint, 0, 1);
            showImage(reportAsImage);
        } catch (JRException ex) {
            ex.printStackTrace();
        }
    }

    private static JasperPrint prepareReport(String nombreReporte, Map<String, Object> parametros) {
        JasperReport jasperReport = null;
        JasperPrint print = null;
        String nombreCompletoReporte = getPathToReport(nombreReporte);
        try {
            jasperReport = JasperCompileManager.compileReport(nombreCompletoReporte);
        } catch (JRException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        try {
            print = JasperFillManager.fillReport(jasperReport, parametros, connection);
        } catch (JRException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return print;
    }

    public static void printReport(String nombreReporte, Map<String, Object> parametros) {
        JasperPrint print = prepareReport(nombreReporte, parametros);
        try {
            JasperPrintManager.printReport(print, true);
        } catch (JRException ex) {
            ex.printStackTrace();
            MessageUtils.showWarning(null, "Hubo problemas con la impresión. Por favor, reintente.");
        }
    }

    private static String getPathToReport(String nombreReporte) {
        return (getPathOfReports() + nombreReporte + EXTENSION_REPORTE).replace("%20", " ");
    }

    public static String getPathOfReports() {
        if (isWindows()) {
            return System.getProperty("user.dir").replace("%20", " ") + "\\reportes\\";
        }
        return System.getProperty("user.dir").replace("%20", " ") + "/reportes/";
    }

    private static boolean isWindows() {
        return System.getProperty("os.name").toLowerCase().contains("windows");
    }
    
    public static void reportToExcel(String nombreReporte, Map<String, Object>parametros){
        JasperPrint print = prepareReport(nombreReporte, parametros);
        try {
            JRXlsExporter a = new JRXlsExporter();
            a.setParameter(JRExporterParameter.JASPER_PRINT, print);
            try {
                a.setParameter(JRExporterParameter.OUTPUT_STREAM, new FileOutputStream("C:\\reporte.xls"));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ReportUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
            a.exportReport();
        } catch (JRException ex) {
            ex.printStackTrace();
            MessageUtils.showWarning(null, "Hubo problemas con la exportacion. Por favor, reintente.");
        }
    }
}