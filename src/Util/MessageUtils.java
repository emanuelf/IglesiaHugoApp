package Util;

import java.awt.Component;
import javax.swing.JOptionPane;

public final class MessageUtils {

    public static void showValidationWarning(Component jPanel, String mensaje) {
        JOptionPane.showMessageDialog(jPanel, mensaje, "Advertencia", JOptionPane.WARNING_MESSAGE);
    }

    public static void showNothingFoundMessage(Component jPanel, String message) {
        JOptionPane.showMessageDialog(jPanel, message, "No se encontraron resultados", JOptionPane.INFORMATION_MESSAGE);
    }

    public static void showWarning(Component origen, String message) {
        JOptionPane.showMessageDialog(origen, message, "Aviso", JOptionPane.INFORMATION_MESSAGE);
        //origen.setBackground(new Color(251,57,53));
    }
    public static boolean showConfirmacionDeEliminacion(Component origen, String message) {
        int respuesta = JOptionPane.showConfirmDialog(origen, "¿Está seguro que desea borrar los datos del " + message + " seleccionado?", "Aviso", JOptionPane.YES_NO_OPTION);
        return respuesta == JOptionPane.YES_OPTION;
    }

    private MessageUtils() {
    }
}