package Util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import sun.util.BuddhistCalendar;

/**
 *
 * @author A. Emanuel Friedrich, en colaboración con Armando J. Alaniz Aragon
 * @version 1.0
 */
public class UtilFechas {

    public static final int YEAR = 0;
    public static final int MONTH = 1;

    public UtilFechas() {
    }

    public static int getDateDiffInYears(Date startDate, Date endDate) {
        return getDateDiffInMonths(startDate, endDate) / 12;
    }

    public static GregorianCalendar dateToGregorianCalendar(Date date) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar;
    }

    public static int getDiaDeFinDeMes(int mes) {
        GregorianCalendar auxiliar = new GregorianCalendar();
        auxiliar.setTime(new Date(Calendar.getInstance().get(Calendar.YEAR), mes, 1));
        return auxiliar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static int getDateDiffInMonths(Date startDate, Date endDate) {
        Calendar startCal = Calendar.getInstance();
        Calendar endCal = Calendar.getInstance();
        int startYear = -1, startMonth = -1;
        int endYear = -1, endMonth = -1;
        int months = 0;
        int factor = 1;
        if (startDate.after(endDate)) {
            factor = -1;
            startCal.setTime(endDate);
            endCal.setTime(startDate);
        } else {
            startCal.setTime(startDate);
            endCal.setTime(endDate);
        }

        startYear = startCal.get(Calendar.YEAR);
        startMonth = startCal.get(Calendar.MONTH) + 1;
        endYear = endCal.get(Calendar.YEAR);
        endMonth = endCal.get(Calendar.MONTH) + 1;

        if (startYear == endYear) {
            months = endMonth - startMonth;
        } else {
            months = 12 - startMonth;
            months += endMonth;
            --endYear;
            if (endYear - startYear > 0) {
                months += (endYear - startYear) * 12;
            }
        }
        months *= factor;
        return months;
    }

    public static int getDateDiff(int field, Date startDate, Date endDate) {
        if (field == YEAR) {
            return getDateDiffInYears(startDate, endDate);
        } else if (field == MONTH) {
            return getDateDiffInMonths(startDate, endDate);
        } else {
            return 0;
        }
    }

    public int diferenciaFechas(String fec1, String fec2, int valor) {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        int retorno = 0;
        java.util.Date date1;
        java.util.Date date2;
        try {
            Calendar cal1;
            date1 = df.parse(fec1);
            cal1 = Calendar.getInstance();
            Calendar cal2;
            date2 = df.parse(fec2);
            cal2 = Calendar.getInstance();
            // different date might have different offset
            cal1.setTime(date1);
            long ldate1 = date1.getTime() + cal1.get(Calendar.ZONE_OFFSET) + cal1.get(Calendar.DST_OFFSET);
            cal2.setTime(date2);
            long ldate2 = date2.getTime() + cal2.get(Calendar.ZONE_OFFSET) + cal2.get(Calendar.DST_OFFSET);
            // Use integer calculation, truncate the decimals
            int hr1 = (int) (ldate1 / 3600000); //60*60*1000
            int hr2 = (int) (ldate2 / 3600000);
            int days1 = (int) hr1 / 24;
            int days2 = (int) hr2 / 24;
            int dateDiff = days2 - days1;
            int yearDiff = cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR);
            int monthDiff = yearDiff * 12 + cal2.get(Calendar.MONTH) - cal1.get(Calendar.MONTH);
            if (valor == 1) {
                if (dateDiff < 0) {
                    dateDiff *= (-1);
                }
                retorno = dateDiff;
            } else if (valor == 2) {
                if (monthDiff < 0) {
                    monthDiff *= (-1);
                }
                retorno = monthDiff;
            } else if (valor == 3) {
                if (yearDiff < 0) {
                    yearDiff *= (-1);
                }
                retorno = yearDiff;
            }
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return retorno;
    }

    public static String getMesEnString(int mes) {
        String retorno = null;
        switch (mes) {
            case 0:
                retorno = "Enero";
                break;
            case 1:
                retorno = "Febrero";
                break;
            case 2:
                retorno = "Marzo";
                break;
            case 3:
                retorno = "Abril";
                break;
            case 4:
                retorno = "Mayo";
                break;
            case 5:
                retorno = "Junio";
                break;
            case 6:
                retorno = "Julio";
                break;
            case 7:
                retorno = "Agosto";
                break;
            case 8:
                retorno = "Septiembre";
                break;
            case 9:
                retorno = "Octubre";
                break;
            case 10:
                retorno = "Noviembre";
                break;
            case 11:
                retorno = "Diciembre";
                break;
        }
        return retorno;
    }

    public static int mesEnNumero(String mesEnTexto) {
        int retorno = 0;
        if ("Enero".equals(mesEnTexto)) {
            return retorno;
        }
        if ("Febrero".equals(mesEnTexto)) {
            retorno = 1;
        }

        if ("Marzo".equals(mesEnTexto)) {
            retorno = 2;
        }
        if ("Abril".equals(mesEnTexto)) {
            retorno = 3;
        }
        if ("Mayo".equals(mesEnTexto)) {
            retorno = 4;
        }
        if ("Junio".equals(mesEnTexto)) {
            retorno = 5;
        }

        if ("Julio".equals(mesEnTexto)) {
            retorno = 6;
        }
        if ("Agosto".equals(mesEnTexto)) {
            retorno = 7;
        }
        if ("Septiembre".equals(mesEnTexto)) {
            retorno = 8;
        }
        if ("Octubre".equals(mesEnTexto)) {
            retorno = 9;
        }
        if ("Noviembre".equals(mesEnTexto)) {
            retorno = 10;
        }
        if ("Diciembre".equals(mesEnTexto)) {
            retorno = 11;
        }
        return retorno;

    }

    public static String getFechaEnFormatoMM_AA(GregorianCalendar fecha) {
        String fechaFormateada = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yy");
        Date fechaEnDate = new Date(fecha.get(Calendar.YEAR), fecha.get(Calendar.MONTH), fecha.get(Calendar.DAY_OF_MONTH));
        fechaFormateada = dateFormat.format(fechaEnDate);
        return fechaFormateada;
    }

    public static String getFechaEnFormatoDD_MM_AA(GregorianCalendar fecha) {
        SimpleDateFormat dateFormat = (SimpleDateFormat) DateFormat.getDateInstance(DateFormat.SHORT);
        Date fechaEnDate = parseDate(fecha);
        return dateFormat.format(fechaEnDate);
    }

    public static Date parseDate(GregorianCalendar fechaAParsear) {
        Date retorno = new Date();
        retorno.setDate(fechaAParsear.get(Calendar.DAY_OF_MONTH));
        retorno.setMonth(fechaAParsear.get(Calendar.MONTH));
        retorno.setYear(fechaAParsear.get(Calendar.YEAR));
        return retorno;
    }

    public static void main(String[] args) throws ParseException {
    }

    public static boolean esDelMesActual(int anio, int mes) {
        Date fechaActual = new java.util.Date();
        if (anio == fechaActual.getYear() && mes == fechaActual.getMonth()) {
            return true;
        } else {
            return false;
        }
    }
}
