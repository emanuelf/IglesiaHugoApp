package reportes;
import java.awt.print.PrinterException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;

public class PrinterUtils {

    public static boolean hayImpresoraDisponible() {
        PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);
        return services.length > 0;
    }

    public static boolean hayImpresoraPorDefaultDisponible(){
        if(hayImpresoraDisponible()){
            return getImpresoraDefault() != null;
        }else{
            return false;
        }
    }

    public static PrintService[] getImpresorasDisponibles() {
        PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);
        return services;
    }

    public static PrintService getImpresoraDefault() {
        return PrintServiceLookup.lookupDefaultPrintService();
    }

     public static void pruebaImpresion() throws PrinterException {
        if(!hayImpresoraPorDefaultDisponible()){
            throw new PrinterException("No hay ninguna impresora configurada como predeterminada.");
        }
    }
}
