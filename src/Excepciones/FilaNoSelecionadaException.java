package Excepciones;

/**
 *
 * @author emanuel
 */
public class FilaNoSelecionadaException extends Exception {

    public FilaNoSelecionadaException() {
        super("Ninguna fila fue seleccionada");
    }
    
}
