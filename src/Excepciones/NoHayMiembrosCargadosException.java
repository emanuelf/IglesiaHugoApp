package Excepciones;

/**
 * NoHayMiembrosCargadosException.java
 * 
 * Created on 18/10/2011, 09:45:15
 * 
 * @author CeroYUno Informática. Soluciones Integrales
 */
public class NoHayMiembrosCargadosException extends Exception{
    
    public NoHayMiembrosCargadosException() {
    }
}
