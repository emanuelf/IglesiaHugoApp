/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Excepciones;

/**
 *
 * @author emanuel
 */
public class DAOProblemaConTransaccionException extends Exception {

    public DAOProblemaConTransaccionException() {
        super("No se pudo realizar la transacción. \n Por favor inténtelo nuevamente y si el problema persiste, "
                + "contáctese con algún administrador del sistema.");
    }
}
