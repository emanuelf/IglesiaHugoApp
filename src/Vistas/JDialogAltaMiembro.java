package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.Direccion;
import Modelo.InstitucionCentral;
import Modelo.InstitucionCentralTesoreria;
import Modelo.Miembro;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.postgresql.util.PSQLException;

/**
 *
 * @author emanuel
 */
public class JDialogAltaMiembro extends javax.swing.JDialog {

    private JDialogAbmMiembros padreABMMiembros;
    private InstitucionCentral institution;
    private boolean llamadoParaModificarUnMiembro;
    private Miembro miembroSeleccionadoAModificar;
    private boolean miembroSeleccionadoModificado;
    private Miembro ultimoAgregado = null;

    /**
     * Creates new form JDialogAltaMiembro
     */
    public JDialogAltaMiembro(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        setLocationRelativeTo(null);
        initComponents();
    }

    public JDialogAltaMiembro() {
        initComponents();
        try {
            institution = InstitucionCentralTesoreria.getInstance();
        } catch (DAOProblemaConTransaccionException ex) {
            Logger.getLogger(JDialogAltaMiembro.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PSQLException ex) {
            Logger.getLogger(JDialogAltaMiembro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public JDialogAltaMiembro(JDialogAbmMiembros jDialogABMMiembros, boolean modal, InstitucionCentral institucionCentralTesoreria,
            Miembro miembroSeleccionado) {
        this.padreABMMiembros = jDialogABMMiembros;
        this.setModal(modal);
        this.initComponents();
        this.institution = institucionCentralTesoreria;
        llamadoParaModificarUnMiembro = true;
        miembroSeleccionadoModificado = false;
        miembroSeleccionadoAModificar = miembroSeleccionado;
        showViewForModificacion();
        show(miembroSeleccionado);
    }

    public JDialogAltaMiembro(JDialogAbmMiembros jDialogABMMiembros, boolean modal, InstitucionCentral institucionCentralTesoreria) {
        this.padreABMMiembros = jDialogABMMiembros;
        this.setModal(modal);
        this.initComponents();
        this.institution = institucionCentralTesoreria;
        llamadoParaModificarUnMiembro = false;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jXSeparadorDeTituloDatosDeNuevoMiembro = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelNombre = new javax.swing.JLabel();
        jTxtNombre = new javax.swing.JTextField();
        jLabelApellido = new javax.swing.JLabel();
        jTxtApellido = new javax.swing.JTextField();
        jLabelTelFijo = new javax.swing.JLabel();
        jTxtTelFijo = new javax.swing.JTextField();
        jLabelTelCel = new javax.swing.JLabel();
        jTxtTelCel = new javax.swing.JTextField();
        jXSeparadorDeTituloDireccion = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelDireccionCalle = new javax.swing.JLabel();
        jTxtDireccionCalle = new javax.swing.JTextField();
        jLabelDireccionAltura = new javax.swing.JLabel();
        jTxtDireccionAltura = new javax.swing.JTextField();
        jTxtDireccionCiudad = new javax.swing.JTextField();
        jLabelCiudad = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jBtnAgregarMiembro_GuardarCambios = new javax.swing.JButton();
        jBtnVolverInicio_ABMMiembros = new javax.swing.JButton();
        jXSeparadorDeTituloDireccion1 = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelDireccionCalle1 = new javax.swing.JLabel();
        jXDatePickerFechaBautismo = new org.jdesktop.swingx.JXDatePicker();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setPreferredSize(new java.awt.Dimension(600, 500));

        jXSeparadorDeTituloDatosDeNuevoMiembro.setFont(new java.awt.Font("Tahoma", 0, 12));
        jXSeparadorDeTituloDatosDeNuevoMiembro.setHorizontalAlignment(0
        );
        jXSeparadorDeTituloDatosDeNuevoMiembro.setTitle("Datos de miembro");

        jLabelNombre.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelNombre.setText("Nombre/s:");

        jTxtNombre.setName("Nombre"); // NOI18N
        jTxtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtNombreKeyTyped(evt);
            }
        });

        jLabelApellido.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelApellido.setText("Apellido/s:");

        jTxtApellido.setName("Apellido"); // NOI18N
        jTxtApellido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtApellidoActionPerformed(evt);
            }
        });
        jTxtApellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtApellidoKeyTyped(evt);
            }
        });

        jLabelTelFijo.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelTelFijo.setText("Teléfono fijo:");

        jTxtTelFijo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtTelFijoKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtTelFijoKeyTyped(evt);
            }
        });

        jLabelTelCel.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelTelCel.setText("Teléfono celular:");

        jTxtTelCel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxtTelCelKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtTelCelKeyTyped(evt);
            }
        });

        jXSeparadorDeTituloDireccion.setFont(new java.awt.Font("Tahoma", 0, 12));
        jXSeparadorDeTituloDireccion.setHorizontalAlignment(0);
        jXSeparadorDeTituloDireccion.setHorizontalTextPosition(0);
        jXSeparadorDeTituloDireccion.setTitle("Dirección");

        jLabelDireccionCalle.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelDireccionCalle.setText("Calle:");

        jTxtDireccionCalle.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtDireccionCalleKeyTyped(evt);
            }
        });

        jLabelDireccionAltura.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelDireccionAltura.setText("Número/altura:");

        jTxtDireccionAltura.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtDireccionAlturaKeyTyped(evt);
            }
        });

        jTxtDireccionCiudad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtDireccionCiudadKeyTyped(evt);
            }
        });

        jLabelCiudad.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelCiudad.setText("Ciudad:");

        jBtnAgregarMiembro_GuardarCambios.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnAgregarMiembro_GuardarCambios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Agregar miembro20x20.png"))); // NOI18N
        jBtnAgregarMiembro_GuardarCambios.setText("Agregar");
        jBtnAgregarMiembro_GuardarCambios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnAgregarMiembro_GuardarCambiosActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnAgregarMiembro_GuardarCambios);

        jBtnVolverInicio_ABMMiembros.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnVolverInicio_ABMMiembros.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolverInicio_ABMMiembros.setText("Volver");
        jBtnVolverInicio_ABMMiembros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverInicio_ABMMiembrosActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnVolverInicio_ABMMiembros);

        jXSeparadorDeTituloDireccion1.setFont(new java.awt.Font("Tahoma", 0, 12));
        jXSeparadorDeTituloDireccion1.setHorizontalAlignment(0);
        jXSeparadorDeTituloDireccion1.setHorizontalTextPosition(0);
        jXSeparadorDeTituloDireccion1.setTitle("Otros datos");

        jLabelDireccionCalle1.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelDireccionCalle1.setText("Fecha de bautismo:");

        jXDatePickerFechaBautismo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jXDatePickerFechaBautismoActionPerformed(evt);
            }
        });
        jXDatePickerFechaBautismo.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jXDatePickerFechaBautismoPropertyChange(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelApellido)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jTxtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabelNombre))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jTxtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelTelFijo, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
                        .addGap(63, 63, 63))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jTxtTelFijo, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(12, 12, 12)
                            .addComponent(jTxtTelCel, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jLabelTelCel)))
                .addGap(84, 84, 84))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 187, Short.MAX_VALUE)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jXSeparadorDeTituloDireccion1, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jXSeparadorDeTituloDatosDeNuevoMiembro, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jXSeparadorDeTituloDireccion, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addComponent(jTxtDireccionCalle, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabelDireccionCalle))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addComponent(jTxtDireccionCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabelCiudad)))
                            .addComponent(jLabelDireccionAltura)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jTxtDireccionAltura, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(84, 84, 84))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jXDatePickerFechaBautismo, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabelDireccionCalle1))
                        .addContainerGap(242, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXSeparadorDeTituloDatosDeNuevoMiembro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelNombre)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabelApellido)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTxtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(85, 85, 85)
                            .addComponent(jTxtTelCel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabelTelFijo)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTxtTelFijo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jLabelTelCel))))
                .addGap(18, 18, 18)
                .addComponent(jXSeparadorDeTituloDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jTxtDireccionCalle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelDireccionCalle))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxtDireccionCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jLabelDireccionAltura)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTxtDireccionAltura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jXSeparadorDeTituloDireccion1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelDireccionCalle1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jXDatePickerFechaBautismo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnAgregarMiembro_GuardarCambiosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnAgregarMiembro_GuardarCambiosActionPerformed
        Date fechaDeBatuismoDate = jXDatePickerFechaBautismo.getDate();
        GregorianCalendar fechaDeBautismo = new GregorianCalendar(fechaDeBatuismoDate.getYear(),
                fechaDeBatuismoDate.getMonth(),
                fechaDeBatuismoDate.getDay());
        String apellido = "";
        String nombre = "";
        int altura = jTxtDireccionAltura.getText().isEmpty() ? 0 : Integer.valueOf(jTxtDireccionAltura.getText());
        long telefonoFijo = jTxtTelCel.getText().isEmpty() ? 0 : Long.valueOf(jTxtTelFijo.getText());
        long telefonoCelular = jTxtTelFijo.getText().isEmpty() ? 0 : Long.valueOf(jTxtTelCel.getText());
        String calle = jTxtDireccionCalle.getText();
        calle = calle == null ? "" : calle.trim();
        String ciudad = jTxtDireccionCiudad.getText();
        ciudad = ciudad == null ? "" : ciudad.trim();
        if (llamadoParaModificarUnMiembro) {
            if (!hayCamposObligatoriosNoCargados()) {
                Direccion direccion = this.miembroSeleccionadoAModificar.getDireccion();
                apellido = this.jTxtApellido.getText();
                nombre = this.jTxtNombre.getText();
                if (!hayCamposNoObligatoriosNoCargados()) {
                    direccion.setAltura(altura);
                    direccion.setCalleOBarrio(calle);
                    direccion.setCiudad(ciudad);
                    try {
                        miembroSeleccionadoAModificar = institution.modificarDatosDeMiembro(miembroSeleccionadoAModificar, apellido, nombre, direccion,
                                telefonoCelular, telefonoFijo, fechaDeBautismo);
                        dispose();
                        return;
                    } catch (DAOProblemaConTransaccionException ex) {
                        JOptionPane.showMessageDialog(this, ex);
                        return;
                    }
                } else {
                    String datosNoIngresados = getDatosNoIngresados();
                    int respuesta = JOptionPane.showConfirmDialog(this,
                            "Hay algunos datos no ingresados. ¿Desea modificar los datos del miembro sin estos datos? \n"
                            + datosNoIngresados, "Aviso", JOptionPane.YES_NO_OPTION);
                    if (respuesta == JOptionPane.YES_OPTION) {
                        direccion.setAltura(altura);
                        direccion.setCalleOBarrio(calle);
                        direccion.setCiudad(ciudad);
                        try {
                            miembroSeleccionadoAModificar = this.institution.modificarDatosDeMiembro(miembroSeleccionadoAModificar, apellido, nombre, direccion,
                                    telefonoCelular, telefonoFijo, fechaDeBautismo);
                            dispose();
                            return;

                        } catch (DAOProblemaConTransaccionException ex) {
                            JOptionPane.showMessageDialog(this, ex);
                            return;
                        }
                    } else {
                        return; //No hacer nada
                    }
                }
            } else {
                JOptionPane.showMessageDialog(this, "Ingrese los nombres, apellidos y fecha de bautismo del miembro.", "Aviso",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        if (!hayCamposObligatoriosNoCargados()) {
            apellido = this.jTxtApellido.getText();
            nombre = this.jTxtNombre.getText();
            if (!hayCamposNoObligatoriosNoCargados()) {
                try {
                    telefonoFijo = Long.parseLong(jTxtTelFijo.getText());
                } catch (NumberFormatException ex) {
                    telefonoFijo = 0;
                }
                try {
                    telefonoCelular = Long.parseLong(this.jTxtTelCel.getText());
                } catch (NumberFormatException ex) {
                    telefonoCelular = 0;
                }
                if (fechaDeBatuismoDate != null) {
                    fechaDeBautismo.set(fechaDeBatuismoDate.getYear(), fechaDeBatuismoDate.getMonth(), fechaDeBatuismoDate.getDay());
                } else {
                    fechaDeBautismo = null;
                }
                calle = jTxtDireccionCalle.getText();
                ciudad = jTxtDireccionCiudad.getText();
                try {
                    altura = Integer.valueOf(jTxtDireccionAltura.getText());
                } catch (NumberFormatException ex) {
                    altura = 0;
                }
                Direccion direccion = new Direccion(calle, altura, ciudad);
                ultimoAgregado = institution.registrarNuevoMiembro(nombre, apellido, direccion, telefonoFijo, telefonoCelular, fechaDeBautismo);
                this.dispose();
            } else {
                String datosNoIngresados = getDatosNoIngresados();
                int respuesta = JOptionPane.showConfirmDialog(this,
                        "Hay algunos datos no ingresados. ¿Desea agregar el miembro sin estos datos? \n"
                        + datosNoIngresados, "Aviso", JOptionPane.YES_NO_OPTION);
                if (respuesta == JOptionPane.YES_OPTION) {
                    this.institution.registrarNuevoMiembro(nombre, apellido, new Direccion(calle, altura, ciudad),
                            telefonoFijo, telefonoCelular, fechaDeBautismo);
                    this.dispose();
                } else {
                    return; //No hacer nada
                }
            }
        } else {
            JOptionPane.showMessageDialog(this, "Ingrese los nombres y apellidos del miembro.", "Aviso",
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }
}//GEN-LAST:event_jBtnAgregarMiembro_GuardarCambiosActionPerformed

    private void jBtnVolverInicio_ABMMiembrosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVolverInicio_ABMMiembrosActionPerformed
        if (llamadoParaModificarUnMiembro && !miembroSeleccionadoModificado) {
            int respuesta = JOptionPane.showConfirmDialog(this, "No ha guardado cambios en los datos del miembro"
                    + " ¿Salir de todos modos?", "Aviso", JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_OPTION) {
                this.dispose();
            }
        } else {
            this.dispose();
        }
    }//GEN-LAST:event_jBtnVolverInicio_ABMMiembrosActionPerformed

    private void jTxtTelFijoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtTelFijoKeyPressed
    }//GEN-LAST:event_jTxtTelFijoKeyPressed

    private void jTxtTelFijoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtTelFijoKeyTyped
        Vistas.Util.Util.ignorarSiNoEsEntero(evt);
    }//GEN-LAST:event_jTxtTelFijoKeyTyped

    private void jTxtNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtNombreKeyTyped
        Vistas.Util.Util.ignorarSiNoEsLetra(evt);
        Vistas.Util.Util.limitarCaracteres(evt, jTxtNombre.getText(), 40);
    }//GEN-LAST:event_jTxtNombreKeyTyped

    private void jTxtDireccionAlturaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtDireccionAlturaKeyTyped
        Vistas.Util.Util.ignorarSiNoEsEntero(evt);
    }//GEN-LAST:event_jTxtDireccionAlturaKeyTyped

    private void jTxtTelCelKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtTelCelKeyTyped
        Vistas.Util.Util.ignorarSiNoEsEntero(evt);
}//GEN-LAST:event_jTxtTelCelKeyTyped

    private void jTxtTelCelKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtTelCelKeyReleased
}//GEN-LAST:event_jTxtTelCelKeyReleased

    private void jTxtApellidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtApellidoKeyTyped
        Vistas.Util.Util.limitarCaracteres(evt, jTxtApellido.getText(), 40);
        Vistas.Util.Util.ignorarSiNoEsLetra(evt);
}//GEN-LAST:event_jTxtApellidoKeyTyped

    private void jTxtApellidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtApellidoActionPerformed
}//GEN-LAST:event_jTxtApellidoActionPerformed

    private void jTxtDireccionCalleKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtDireccionCalleKeyTyped
        Vistas.Util.Util.limitarCaracteres(evt, jTxtDireccionCalle.getText(), 40);
    }//GEN-LAST:event_jTxtDireccionCalleKeyTyped

    private void jTxtDireccionCiudadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtDireccionCiudadKeyTyped
        Vistas.Util.Util.limitarCaracteres(evt, jTxtDireccionCiudad.getText(), 40);
    }//GEN-LAST:event_jTxtDireccionCiudadKeyTyped

    private void jXDatePickerFechaBautismoPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jXDatePickerFechaBautismoPropertyChange
    }//GEN-LAST:event_jXDatePickerFechaBautismoPropertyChange

    private void jXDatePickerFechaBautismoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jXDatePickerFechaBautismoActionPerformed
        Date fechaDeBautismoSeleccionado = jXDatePickerFechaBautismo.getDate();
        GregorianCalendar hoy = (GregorianCalendar) Calendar.getInstance();
        GregorianCalendar fechaDeBautismoSeleccionado_g = new GregorianCalendar();
        fechaDeBautismoSeleccionado_g.setTime(fechaDeBautismoSeleccionado);
        if (fechaDeBautismoSeleccionado != null) {
            if (fechaDeBautismoSeleccionado_g.after(hoy)) {
                JOptionPane.showMessageDialog(this, "La fecha de bautismo del miembro no puede ser una fecha posterior a la de hoy.");
                if (!llamadoParaModificarUnMiembro) {
                    jXDatePickerFechaBautismo.setDate(null);
                } else {
                    jXDatePickerFechaBautismo.setDate(miembroSeleccionadoAModificar.fechaBautismoEnDate());
                }
            }
        } else {
            return;
        }
    }//GEN-LAST:event_jXDatePickerFechaBautismoActionPerformed

    private boolean hayCamposObligatoriosNoCargados() {
        if (jTxtApellido.getText().isEmpty() || jTxtNombre.getText().isEmpty() || jXDatePickerFechaBautismo.getDate() == null) {
            return true;
        } else {
            return false;
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnAgregarMiembro_GuardarCambios;
    private javax.swing.JButton jBtnVolverInicio_ABMMiembros;
    private javax.swing.JLabel jLabelApellido;
    private javax.swing.JLabel jLabelCiudad;
    private javax.swing.JLabel jLabelDireccionAltura;
    private javax.swing.JLabel jLabelDireccionCalle;
    private javax.swing.JLabel jLabelDireccionCalle1;
    private javax.swing.JLabel jLabelNombre;
    private javax.swing.JLabel jLabelTelCel;
    private javax.swing.JLabel jLabelTelFijo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JTextField jTxtApellido;
    private javax.swing.JTextField jTxtDireccionAltura;
    private javax.swing.JTextField jTxtDireccionCalle;
    private javax.swing.JTextField jTxtDireccionCiudad;
    private javax.swing.JTextField jTxtNombre;
    private javax.swing.JTextField jTxtTelCel;
    private javax.swing.JTextField jTxtTelFijo;
    private org.jdesktop.swingx.JXDatePicker jXDatePickerFechaBautismo;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloDatosDeNuevoMiembro;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloDireccion;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloDireccion1;
    // End of variables declaration//GEN-END:variables

    private String getDatosNoIngresados() {
        StringBuilder retorno = new StringBuilder("");
        if (jTxtDireccionCiudad.getText().isEmpty()) {
            retorno.append("* Ciudad \n");
        }
        if (jTxtDireccionCalle.getText().isEmpty()) {
            retorno.append("* Calle \n");
        }
        if (jTxtDireccionAltura.getText().isEmpty()) {
            retorno.append("* Altura \n");
        }
        if (jTxtTelCel.getText().isEmpty()) {
            retorno.append("* Teléfono celular \n");
        }
        if (jTxtTelFijo.getText().isEmpty()) {
            retorno.append("* Teléfono fijo \n");
        }
        return !retorno.toString().isEmpty() ? retorno.toString() : null;
    }

    public JDialogAbmMiembros getPadre() {
        return padreABMMiembros;
    }

    public void setPadre(JDialogAbmMiembros padre) {
        this.padreABMMiembros = padre;
    }

    public boolean isLlamadoDesdeABMMiembros() {
        return llamadoParaModificarUnMiembro;
    }

    public void setLlamadoDesdeABMMiembros(boolean llamadoDesdeABMMiembros) {
        llamadoParaModificarUnMiembro = llamadoDesdeABMMiembros;
    }

    private void show(Miembro miembroSeleccionado) {
        jTxtApellido.setText(miembroSeleccionado.getApellidos());
        jTxtNombre.setText(miembroSeleccionado.getNombres());
        jTxtDireccionAltura.setText(String.valueOf(miembroSeleccionado.getDireccion().getAltura()));
        jTxtDireccionCalle.setText(miembroSeleccionado.getDireccion().getCalleOBarrio());
        jTxtDireccionCiudad.setText(miembroSeleccionado.getDireccion().getCiudad());
        jTxtTelCel.setText(String.valueOf(miembroSeleccionado.getTelefonoCelular()));
        jTxtTelFijo.setText(String.valueOf(miembroSeleccionado.getTelefonoFijo()));
        jXDatePickerFechaBautismo.setDate(miembroSeleccionado.fechaBautismoEnDate());
    }

    public Miembro getUltimoAgregado() {
        return ultimoAgregado;
    }

    private boolean hayCamposNoObligatoriosNoCargados() {
        if (jTxtDireccionAltura.getText().isEmpty() || jTxtDireccionCalle.getText().isEmpty()
                || jTxtDireccionCiudad.getText().isEmpty() || jTxtTelCel.getText().isEmpty() || jTxtTelFijo.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    public void setAModificar(Miembro miembroAModificar) {
        this.miembroSeleccionadoAModificar = miembroAModificar;
        show(miembroAModificar);
    }

    public Miembro getModificado() {
        return miembroSeleccionadoAModificar;
    }

    public void setLlamadoParaModificarUnMiembro(boolean llamadoParaModificarUnMiembro) {
        this.llamadoParaModificarUnMiembro = llamadoParaModificarUnMiembro;
        if (llamadoParaModificarUnMiembro) {
            showViewForModificacion();
        } else {
            showViewForAlta();
        }
    }

    private void showViewForModificacion() {
        jBtnAgregarMiembro_GuardarCambios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Guardar 30x30.png")));
        jBtnAgregarMiembro_GuardarCambios.setText("Guardar y seguir");
    }

    private void showViewForAlta() {
        jBtnAgregarMiembro_GuardarCambios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Agregar miembro20x20.png"))); // NOI18N
        jBtnAgregarMiembro_GuardarCambios.setText("Agregar");
    }
}
