/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * JDialogDatosDeLaInstitucion.java
 *
 * Created on 03/06/2011, 10:37:58
 */
package Vistas;

import Modelo.Direccion;
import Modelo.InstitucionCentral;
import Modelo.InstitucionCentralTesoreria;
import Vistas.Util.Util;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author emanuel
 */
public class JDialogDatosDeLaInstitucion extends javax.swing.JDialog {

    private InstitucionCentral institucionCentral;

    /** Creates new form JDialogDatosDeLaInstitucion */
    public JDialogDatosDeLaInstitucion(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    JDialogDatosDeLaInstitucion(JFrameSistema padre, boolean modal, InstitucionCentral institucion) {
        this(padre, modal);
        this.institucionCentral = institucion;
        cargarDatosDeLaInstitucionEnLaVista(institucion);
        deshabilitarOHabilitarCampos(false);
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXTitledPanel1 = new org.jdesktop.swingx.JXTitledPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jXSeparadorDeTituloDatosDeInstitucion = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelNombreInstitucion = new javax.swing.JLabel();
        jTxtNombreInstitucion = new javax.swing.JTextField();
        jTxtFicheroDeCulto = new javax.swing.JTextField();
        jLabelFicheroDeCulto = new javax.swing.JLabel();
        jTxtTelefono = new javax.swing.JTextField();
        jLabelTelCelular = new javax.swing.JLabel();
        jLabelPersoneriaJuridica = new javax.swing.JLabel();
        jTxtPersoneriaJuridica = new javax.swing.JTextField();
        jLabelEmail = new javax.swing.JLabel();
        jTxtEmail = new javax.swing.JTextField();
        jXSeparadorDeTituloDireccion = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelNroCodigo3 = new javax.swing.JLabel();
        jTxtCiudad = new javax.swing.JTextField();
        jTxtCalle = new javax.swing.JTextField();
        jLabelCalle = new javax.swing.JLabel();
        jLabelNumeroAltura = new javax.swing.JLabel();
        jTxtNroAltura = new javax.swing.JTextField();
        jPnlBotonera = new javax.swing.JPanel();
        jChekBoxEditar = new javax.swing.JCheckBox();
        jBtnGuardar = new javax.swing.JButton();
        jBtnVolver = new javax.swing.JButton();

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setPreferredSize(new java.awt.Dimension(600, 500));

        jXSeparadorDeTituloDatosDeInstitucion.setFont(new java.awt.Font("Tahoma", 0, 12));
        jXSeparadorDeTituloDatosDeInstitucion.setHorizontalAlignment(0);
        jXSeparadorDeTituloDatosDeInstitucion.setTitle("Datos de institución");

        jLabelNombreInstitucion.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelNombreInstitucion.setText("Nombre/s:");

        jTxtNombreInstitucion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtNombreInstitucionKeyTyped(evt);
            }
        });

        jTxtFicheroDeCulto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtFicheroDeCultoKeyTyped(evt);
            }
        });

        jLabelFicheroDeCulto.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelFicheroDeCulto.setText("Fichero de culto:");

        jTxtTelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtTelefonoKeyTyped(evt);
            }
        });

        jLabelTelCelular.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelTelCelular.setText("Teléfono:");

        jLabelPersoneriaJuridica.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelPersoneriaJuridica.setText("Personería jurídica:");

        jTxtPersoneriaJuridica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtPersoneriaJuridicaActionPerformed(evt);
            }
        });

        jLabelEmail.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelEmail.setText("Email:");

        jTxtEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtEmailActionPerformed(evt);
            }
        });

        jXSeparadorDeTituloDireccion.setFont(new java.awt.Font("Tahoma", 0, 12));
        jXSeparadorDeTituloDireccion.setHorizontalAlignment(0);
        jXSeparadorDeTituloDireccion.setTitle("Dirección");

        jLabelNroCodigo3.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelNroCodigo3.setText("Ciudad:");

        jLabelCalle.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelCalle.setText("Calle:");

        jLabelNumeroAltura.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelNumeroAltura.setText("Número/altura:");

        jTxtNroAltura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtNroAlturaActionPerformed(evt);
            }
        });
        jTxtNroAltura.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtNroAlturaKeyTyped(evt);
            }
        });

        jChekBoxEditar.setBackground(new java.awt.Color(204, 204, 204));
        jChekBoxEditar.setText("Editar");
        jChekBoxEditar.setToolTipText("Modificar los datos de la institucion");
        jChekBoxEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jChekBoxEditarActionPerformed(evt);
            }
        });
        jPnlBotonera.add(jChekBoxEditar);

        jBtnGuardar.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Guardar 30x30.png"))); // NOI18N
        jBtnGuardar.setText("Guardar");
        jBtnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnGuardarActionPerformed(evt);
            }
        });
        jPnlBotonera.add(jBtnGuardar);

        jBtnVolver.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolver.setText("Volver");
        jBtnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverActionPerformed(evt);
            }
        });
        jPnlBotonera.add(jBtnVolver);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jTxtNroAltura, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelNombreInstitucion)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addGap(12, 12, 12)
                                            .addComponent(jTxtNombreInstitucion, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGap(18, 18, 18)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelFicheroDeCulto)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addGap(12, 12, 12)
                                            .addComponent(jTxtFicheroDeCulto, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addComponent(jXSeparadorDeTituloDatosDeInstitucion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 399, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelPersoneriaJuridica)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addGap(12, 12, 12)
                                            .addComponent(jTxtPersoneriaJuridica, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGap(18, 18, 18)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelTelCelular)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addGap(12, 12, 12)
                                            .addComponent(jTxtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGap(12, 12, 12)
                                    .addComponent(jTxtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jLabelEmail)
                                .addComponent(jXSeparadorDeTituloDireccion, javax.swing.GroupLayout.DEFAULT_SIZE, 399, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addGap(12, 12, 12)
                                            .addComponent(jTxtCalle, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jLabelCalle))
                                    .addGap(18, 18, 18)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelNroCodigo3)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addGap(12, 12, 12)
                                            .addComponent(jTxtCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addComponent(jLabelNumeroAltura))
                            .addContainerGap())
                        .addComponent(jPnlBotonera, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(13, Short.MAX_VALUE)
                .addComponent(jXSeparadorDeTituloDatosDeInstitucion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNombreInstitucion)
                    .addComponent(jLabelFicheroDeCulto))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTxtNombreInstitucion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTxtFicheroDeCulto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPersoneriaJuridica)
                    .addComponent(jLabelTelCelular))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTxtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTxtPersoneriaJuridica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelEmail)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jXSeparadorDeTituloDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelCalle)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxtCalle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelNroCodigo3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxtCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelNumeroAltura)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtNroAltura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24)
                .addComponent(jPnlBotonera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 419, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 384, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTxtPersoneriaJuridicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtPersoneriaJuridicaActionPerformed
}//GEN-LAST:event_jTxtPersoneriaJuridicaActionPerformed

    private void jTxtEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtEmailActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_jTxtEmailActionPerformed

    private void jTxtNroAlturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtNroAlturaActionPerformed
    }//GEN-LAST:event_jTxtNroAlturaActionPerformed

    private void jTxtNroAlturaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtNroAlturaKeyTyped
        Vistas.Util.Util.ignorarSiNoEsEntero(evt);
    }//GEN-LAST:event_jTxtNroAlturaKeyTyped

    private void jTxtTelefonoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtTelefonoKeyTyped
        Vistas.Util.Util.ignorarSiNoEsEntero(evt);
    }//GEN-LAST:event_jTxtTelefonoKeyTyped

    private void jTxtFicheroDeCultoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtFicheroDeCultoKeyTyped
        Vistas.Util.Util.ignorarSiNoEsEntero(evt);
    }//GEN-LAST:event_jTxtFicheroDeCultoKeyTyped

    private void jTxtNombreInstitucionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtNombreInstitucionKeyTyped
        Vistas.Util.Util.ignorarSiNoEsLetra(evt);
    }//GEN-LAST:event_jTxtNombreInstitucionKeyTyped

    private void jBtnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnGuardarActionPerformed
        boolean datosNecesariosNoCargados = Util.isEmpty(new JTextField[]{jTxtCalle, jTxtCiudad, jTxtFicheroDeCulto,
                    jTxtNombreInstitucion, jTxtNroAltura, jTxtPersoneriaJuridica, jTxtTelefono});
        boolean datosNoNecesariosCargados = Util.isEmpty(new JTextField[]{jTxtEmail});
        if (datosNecesariosNoCargados) {
            JOptionPane.showMessageDialog(this, "Hay datos obligatorios no cargados. \n Verifique e intente nuevamente.");
            return;
        } else {
            if (datosNoNecesariosCargados) {
                int respuesta = JOptionPane.showConfirmDialog(this, "El email no fue cargado. ¿Guardar los demás datos igualmente?", "Aviso", JOptionPane.YES_NO_OPTION);
                if (respuesta == JOptionPane.NO_OPTION) {
                    return;
                }
            }
        }
        int respuesta = JOptionPane.showConfirmDialog(this, "¿Desea guardar los cambios hechos en los datos de la institucion?",
                "Aviso", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (respuesta == JOptionPane.YES_OPTION) {
            this.actualizarOGuardarDatosDeLaInstitucion();
            this.cargarDatosDeLaInstitucionEnLaVista(institucionCentral);
        }
    }//GEN-LAST:event_jBtnGuardarActionPerformed

    private void jChekBoxEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jChekBoxEditarActionPerformed
        if (jChekBoxEditar.isSelected()) {
            deshabilitarOHabilitarCampos(true);
        } else {
            deshabilitarOHabilitarCampos(false);
            this.cargarDatosDeLaInstitucionEnLaVista(institucionCentral); //para resetearlos
        }
    }//GEN-LAST:event_jChekBoxEditarActionPerformed

    private void jBtnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVolverActionPerformed
        this.dispose();
    }//GEN-LAST:event_jBtnVolverActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogDatosDeLaInstitucion dialog = new JDialogDatosDeLaInstitucion(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnGuardar;
    private javax.swing.JButton jBtnVolver;
    private javax.swing.JCheckBox jChekBoxEditar;
    private javax.swing.JLabel jLabelCalle;
    private javax.swing.JLabel jLabelEmail;
    private javax.swing.JLabel jLabelFicheroDeCulto;
    private javax.swing.JLabel jLabelNombreInstitucion;
    private javax.swing.JLabel jLabelNroCodigo3;
    private javax.swing.JLabel jLabelNumeroAltura;
    private javax.swing.JLabel jLabelPersoneriaJuridica;
    private javax.swing.JLabel jLabelTelCelular;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPnlBotonera;
    private javax.swing.JTextField jTxtCalle;
    private javax.swing.JTextField jTxtCiudad;
    private javax.swing.JTextField jTxtEmail;
    private javax.swing.JTextField jTxtFicheroDeCulto;
    private javax.swing.JTextField jTxtNombreInstitucion;
    private javax.swing.JTextField jTxtNroAltura;
    private javax.swing.JTextField jTxtPersoneriaJuridica;
    private javax.swing.JTextField jTxtTelefono;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloDatosDeInstitucion;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloDireccion;
    private org.jdesktop.swingx.JXTitledPanel jXTitledPanel1;
    // End of variables declaration//GEN-END:variables

    private void cargarDatosDeLaInstitucionEnLaVista(InstitucionCentral institucion) {
        jTxtEmail.setText(institucion.getEmail());
        jTxtFicheroDeCulto.setText(String.valueOf(institucion.getFicheDeCulto()));
        jTxtNombreInstitucion.setText(institucion.getNombre());
        jTxtPersoneriaJuridica.setText(institucion.getPersoneriaJuridica());
        jTxtTelefono.setText(String.valueOf(institucion.getTelefono()));
        if (institucion.getDireccion() == null) {
            return;
        }
        jTxtCalle.setText(institucion.getDireccion().getCalleOBarrio());
        jTxtCiudad.setText(institucion.getDireccion().getCiudad());
        jTxtNroAltura.setText(String.valueOf(institucion.getDireccion().getAltura()));
    }

    public void actualizarOGuardarDatosDeLaInstitucion() {
        if (institucionCentral.getDireccion() == null) {
            institucionCentral.setDireccion(new Direccion());
        }
        institucionCentral.getDireccion().setAltura(Integer.parseInt(jTxtNroAltura.getText()));
        institucionCentral.getDireccion().setCiudad(jTxtCiudad.getText());
        institucionCentral.getDireccion().setCalleOBarrio(jTxtCalle.getText());
        institucionCentral.setEmail(jTxtEmail.getText());
        institucionCentral.setFicheDeCulto(Integer.parseInt(jTxtFicheroDeCulto.getText()));
        institucionCentral.setNombre(jTxtNombreInstitucion.getText());
        institucionCentral.setPersoneriaJuridica(jTxtPersoneriaJuridica.getText());
        institucionCentral.setTelefono(Long.parseLong(jTxtTelefono.getText()));
        ((InstitucionCentralTesoreria) institucionCentral).saveOrUpdateDatosDeInstitucion();
    }

    private void deshabilitarOHabilitarCampos(boolean habilitarODeshabilitar) {
        Vistas.Util.Util.setEnabled(new java.awt.Component[]{this.jTxtCalle,
                    jTxtCiudad, jTxtEmail, jTxtFicheroDeCulto, jTxtNombreInstitucion, jTxtNroAltura, jTxtPersoneriaJuridica,
                    jTxtTelefono}, habilitarODeshabilitar);

    }
}
