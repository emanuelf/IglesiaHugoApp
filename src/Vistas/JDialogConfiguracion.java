/**
 * JDialogConfiguracion.java
 * Created on 24/10/2011, 20:07:53
 * @author CeroYUno Informática. Soluciones Integrales
 */
package Vistas;

import Modelo.Configuracion;
import Modelo.InstitucionCentralTesoreria;
import Vistas.Util.Util;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * @author emanuel
 */
public class JDialogConfiguracion extends javax.swing.JDialog {

    private InstitucionCentralTesoreria institucion;

    public JDialogConfiguracion(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
    }

    public JDialogConfiguracion(java.awt.Frame parent, boolean modal, InstitucionCentralTesoreria institucion) {
        super(parent, modal);
        initComponents();
        this.institucion = institucion;
        cargarVistaConConfiguracion(this.institucion.getConfiguracion());
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jXTituloConfiguracion = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelDireccionCalle = new javax.swing.JLabel();
        jTxtPorcentajePastoral = new javax.swing.JTextField();
        jLabelDireccionAltura = new javax.swing.JLabel();
        jTxtLimiteDonacion = new javax.swing.JTextField();
        jPanel8 = new javax.swing.JPanel();
        jBtnGuardar = new javax.swing.JButton();
        jBtnVolverInicio = new javax.swing.JButton();
        jCheckPermitirMontosEnCero = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jXTituloConfiguracion.setTitle("Configuración");

        jLabelDireccionCalle.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelDireccionCalle.setText("Porcentaje pastoral:");

        jTxtPorcentajePastoral.setColumns(5);
        jTxtPorcentajePastoral.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jTxtPorcentajePastoralPropertyChange(evt);
            }
        });
        jTxtPorcentajePastoral.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtPorcentajePastoralKeyTyped(evt);
            }
        });

        jLabelDireccionAltura.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelDireccionAltura.setText("Límite de donación:");

        jTxtLimiteDonacion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtLimiteDonacionKeyTyped(evt);
            }
        });

        jBtnGuardar.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Guardar 30x30.png"))); // NOI18N
        jBtnGuardar.setText("Guardar");
        jBtnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnGuardarActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnGuardar);

        jBtnVolverInicio.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnVolverInicio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolverInicio.setText("Volver");
        jBtnVolverInicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverInicioActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnVolverInicio);

        jCheckPermitirMontosEnCero.setText("Permitir montos en cero");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jTxtLimiteDonacion, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabelDireccionCalle)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jTxtPorcentajePastoral, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabelDireccionAltura)
                            .addComponent(jCheckPermitirMontosEnCero)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jXTituloConfiguracion, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXTituloConfiguracion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabelDireccionCalle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtPorcentajePastoral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jLabelDireccionAltura)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtLimiteDonacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jCheckPermitirMontosEnCero)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTxtPorcentajePastoralKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtPorcentajePastoralKeyTyped
        Util.ignorarSiNoEsEntero(evt);
        Util.limitarCaracteres(evt, jTxtPorcentajePastoral.getText(), 3);

    }//GEN-LAST:event_jTxtPorcentajePastoralKeyTyped

    private void jTxtLimiteDonacionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtLimiteDonacionKeyTyped
        Util.ignorarSiNoEsDouble(evt, jTxtPorcentajePastoral.getText());
    }//GEN-LAST:event_jTxtLimiteDonacionKeyTyped

    private void jBtnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnGuardarActionPerformed
        double porcentajePastoral = Double.valueOf(jTxtPorcentajePastoral.getText());
        double limiteDonacion = Double.valueOf(this.jTxtLimiteDonacion.getText());
        if(Util.isEmpty( new JTextField [] {jTxtLimiteDonacion, jTxtPorcentajePastoral})){
            JOptionPane.showMessageDialog(this, "Rellene todos los campos");
            return;
        }
        try{
            institucion.updateConfiguracion(limiteDonacion, porcentajePastoral, jCheckPermitirMontosEnCero.isSelected());
        }catch (IllegalArgumentException ex){
            JOptionPane.showMessageDialog(this, ex.getMessage());
            return;
        }
        this.dispose();
    }//GEN-LAST:event_jBtnGuardarActionPerformed

    private void jBtnVolverInicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVolverInicioActionPerformed
        this.dispose();
    }//GEN-LAST:event_jBtnVolverInicioActionPerformed

    private void jTxtPorcentajePastoralPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jTxtPorcentajePastoralPropertyChange
    }//GEN-LAST:event_jTxtPorcentajePastoralPropertyChange

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JDialogConfiguracion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JDialogConfiguracion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JDialogConfiguracion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JDialogConfiguracion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogConfiguracion dialog = new JDialogConfiguracion(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnGuardar;
    private javax.swing.JButton jBtnVolverInicio;
    private javax.swing.JCheckBox jCheckPermitirMontosEnCero;
    private javax.swing.JLabel jLabelDireccionAltura;
    private javax.swing.JLabel jLabelDireccionCalle;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JTextField jTxtLimiteDonacion;
    private javax.swing.JTextField jTxtPorcentajePastoral;
    private org.jdesktop.swingx.JXTitledSeparator jXTituloConfiguracion;
    // End of variables declaration//GEN-END:variables

    private void cargarVistaConConfiguracion(Configuracion configuracion) {
        jTxtPorcentajePastoral.setText(Long.toString(Math.round(configuracion.getPorcentajeParaElPastorSobreLosDiezmosYOfrendas() * 100)));
        jTxtLimiteDonacion.setText(Long.toString(Math.round(configuracion.getLimiteDeDonacion())));
    }
}
