/**
 * JDialogSeleccionFecha.java
 * 
 * Created on 22/10/2011, 20:22:51
 * 
 * @author CeroYUno Informática. Soluciones Integrales
 */
package Vistas;

import Modelo.IngresoEnCulto;
import Modelo.InstitucionCentralTesoreria;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author emanuel
 */
public class JDialogSeleccionFecha extends javax.swing.JDialog {

    InstitucionCentralTesoreria institucionCentralTesoreria;
    JFrameSistema frameSistema;

    public JDialogSeleccionFecha(java.awt.Frame parent, boolean modal, InstitucionCentralTesoreria centralTesoreria) {
        super(parent, modal);
        initComponents();
        this.institucionCentralTesoreria = centralTesoreria;
        this.frameSistema = (JFrameSistema) parent;
        this.setLocationRelativeTo(null);
        Vistas.Util.Util.cargaSecuenciaEnCombo(0, 23, jComboHora);
        Vistas.Util.Util.cargaSecuenciaEnCombo(0, 59, jComboMinuto);
    }

    public JDialogSeleccionFecha(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPnlCuerpo = new javax.swing.JPanel();
        jXDatePickerFechaASeleccionar = new org.jdesktop.swingx.JXDatePicker();
        jBtnSeleccionar = new javax.swing.JButton();
        jLblAviso = new javax.swing.JLabel();
        jXTitledSeparator1 = new org.jdesktop.swingx.JXTitledSeparator();
        jTextFieldDescripcion = new javax.swing.JTextField();
        jLblDescripcion = new javax.swing.JLabel();
        jComboHora = new javax.swing.JComboBox();
        jComboMinuto = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jXDatePickerFechaASeleccionar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jXDatePickerFechaASeleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jXDatePickerFechaASeleccionarActionPerformed(evt);
            }
        });

        jBtnSeleccionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Ok 20x20.png"))); // NOI18N
        jBtnSeleccionar.setText("Seleccionar");
        jBtnSeleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnSeleccionarActionPerformed(evt);
            }
        });

        jLblAviso.setText("Fecha y hora:");

        jXTitledSeparator1.setTitle("Datos del culto");

        jLblDescripcion.setText("Descripción:");

        javax.swing.GroupLayout jPnlCuerpoLayout = new javax.swing.GroupLayout(jPnlCuerpo);
        jPnlCuerpo.setLayout(jPnlCuerpoLayout);
        jPnlCuerpoLayout.setHorizontalGroup(
            jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlCuerpoLayout.createSequentialGroup()
                .addGroup(jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlCuerpoLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jXTitledSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPnlCuerpoLayout.createSequentialGroup()
                        .addContainerGap(153, Short.MAX_VALUE)
                        .addComponent(jBtnSeleccionar))
                    .addGroup(jPnlCuerpoLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLblAviso)
                            .addGroup(jPnlCuerpoLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jXDatePickerFechaASeleccionar, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jComboHora, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jComboMinuto, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(35, 35, 35))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPnlCuerpoLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPnlCuerpoLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jTextFieldDescripcion, javax.swing.GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE))
                            .addComponent(jLblDescripcion))))
                .addContainerGap())
        );
        jPnlCuerpoLayout.setVerticalGroup(
            jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXTitledSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLblAviso)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jXDatePickerFechaASeleccionar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboHora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboMinuto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLblDescripcion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                .addComponent(jBtnSeleccionar)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPnlCuerpo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPnlCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnSeleccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnSeleccionarActionPerformed
        //si ya hay ingresos para este fecha, retornar, sino abrir jdialogaltaingreso con la fecha del culto cargada
        Date fechaSeleccionada = jXDatePickerFechaASeleccionar.getDate();
        String descripcion = jTextFieldDescripcion.getText();
        if (fechaSeleccionada == null || descripcion.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Seleccione una fecha");
            return;
        }
        int hora = Integer.parseInt(jComboHora.getSelectedItem().toString());
        int minutos = Integer.parseInt(jComboMinuto.getSelectedItem().toString());
        fechaSeleccionada.setHours(hora);
        fechaSeleccionada.setMinutes(minutos);
        IngresoEnCulto ingresoEnCulto = new IngresoEnCulto(fechaSeleccionada, descripcion);
        this.dispose();
        JDialogAltaIngreso altaIngreso = new JDialogAltaIngreso(frameSistema, true, institucionCentralTesoreria, ingresoEnCulto);
        altaIngreso.setVisible(true);

    }//GEN-LAST:event_jBtnSeleccionarActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        this.frameSistema.setVisible(true);
    }//GEN-LAST:event_formWindowClosing

    private void jXDatePickerFechaASeleccionarActionPerformed(java.awt.event.ActionEvent evt) {
        if (jXDatePickerFechaASeleccionar.getDate() == null) {
            return;
        }
        if (jXDatePickerFechaASeleccionar.getDate().after(new Date())) {
            JOptionPane.showMessageDialog(this, "Debe seleccionar una fecha anterior a hoy.");
            jXDatePickerFechaASeleccionar.setDate(null);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JDialogSeleccionFecha.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JDialogSeleccionFecha.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JDialogSeleccionFecha.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JDialogSeleccionFecha.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogSeleccionFecha dialog = new JDialogSeleccionFecha(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnSeleccionar;
    private javax.swing.JComboBox jComboHora;
    private javax.swing.JComboBox jComboMinuto;
    private javax.swing.JLabel jLblAviso;
    private javax.swing.JLabel jLblDescripcion;
    private javax.swing.JPanel jPnlCuerpo;
    private javax.swing.JTextField jTextFieldDescripcion;
    private org.jdesktop.swingx.JXDatePicker jXDatePickerFechaASeleccionar;
    private org.jdesktop.swingx.JXTitledSeparator jXTitledSeparator1;
    // End of variables declaration//GEN-END:variables
}
