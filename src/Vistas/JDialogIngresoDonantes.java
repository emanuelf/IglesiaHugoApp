/**
 * JDialogIngresoDonantes.java
 * 
 * Created on 18/10/2011, 22:34:08
 * 
 * @author CeroYUno Informática. Soluciones Integrales
 */
package Vistas;

import Modelo.Donante;
import Modelo.InstitucionCentralTesoreria;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JLabel;

/**
 *
 * @author emanuel
 */
public class JDialogIngresoDonantes extends javax.swing.JDialog {

    private InstitucionCentralTesoreria institucionCentralTesoreria;
    private JDialogAltaIngreso padre;
    private ArrayList<Donante> donantes;
    private Donante donanteEncontrado;

    public JDialogIngresoDonantes(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
    }

    public JDialogIngresoDonantes(JDialogAltaIngreso padre, boolean modal, InstitucionCentralTesoreria institucion) {
        super(padre, modal);
        initComponents();
        this.institucionCentralTesoreria = institucion;
        this.padre = padre;
        this.donanteEncontrado = null;
        this.donantes = this.institucionCentralTesoreria.getDonantes();
        donanteEncontrado=null;
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPnlCuerpo = new javax.swing.JPanel();
        jXTitleIngresoNuevoDonante = new org.jdesktop.swingx.JXTitledSeparator();
        jLblCuitDonante = new javax.swing.JLabel();
        jLblApellidoDonante = new javax.swing.JLabel();
        jLblNombreDonante = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jBtnAgregarDonante = new javax.swing.JButton();
        jBtnVolverAlInicio = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jTxtParteDni = new javax.swing.JTextField();
        jTxtParteTrasera = new javax.swing.JTextField();
        jTxtParteDelantera = new javax.swing.JTextField();
        jTxtApellido = new javax.swing.JTextField();
        jTxtNombre = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Ingreso donante");
        setBackground(java.awt.Color.lightGray);
        setForeground(java.awt.Color.orange);
        setModal(true);

        jPnlCuerpo.setForeground(new java.awt.Color(204, 204, 204));

        jXTitleIngresoNuevoDonante.setFont(new java.awt.Font("Dialog", 0, 14));
        jXTitleIngresoNuevoDonante.setHorizontalAlignment(JLabel.CENTER);
        jXTitleIngresoNuevoDonante.setTitle("Datos del donante");

        jLblCuitDonante.setFont(new java.awt.Font("Dialog", 0, 12));
        jLblCuitDonante.setText("Cuit:");

        jLblApellidoDonante.setFont(new java.awt.Font("Dialog", 0, 12));
        jLblApellidoDonante.setText("Apellido:");

        jLblNombreDonante.setFont(new java.awt.Font("Dialog", 0, 12));
        jLblNombreDonante.setText("Nombre:");

        jBtnAgregarDonante.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnAgregarDonante.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Add usuario.png"))); // NOI18N
        jBtnAgregarDonante.setText("Agregar");
        jBtnAgregarDonante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnAgregarDonanteActionPerformed(evt);
            }
        });
        jPanel2.add(jBtnAgregarDonante);

        jBtnVolverAlInicio.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnVolverAlInicio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolverAlInicio.setText("Volver");
        jBtnVolverAlInicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverAlInicioActionPerformed(evt);
            }
        });
        jPanel2.add(jBtnVolverAlInicio);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        jTxtParteDni.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtParteDni.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtParteDniKeyTyped(evt);
            }
        });

        jTxtParteTrasera.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtParteTrasera.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtParteTraseraKeyTyped(evt);
            }
        });

        jTxtParteDelantera.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtParteDelantera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtParteDelanteraActionPerformed(evt);
            }
        });
        jTxtParteDelantera.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtParteDelanteraKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jTxtParteDelantera, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtParteDni, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtParteTrasera, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(2, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTxtParteDni, javax.swing.GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE)
                    .addComponent(jTxtParteDelantera, javax.swing.GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE)
                    .addComponent(jTxtParteTrasera, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTxtApellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtApellidoKeyTyped(evt);
            }
        });

        jTxtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtNombreKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPnlCuerpoLayout = new javax.swing.GroupLayout(jPnlCuerpo);
        jPnlCuerpo.setLayout(jPnlCuerpoLayout);
        jPnlCuerpoLayout.setHorizontalGroup(
            jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlCuerpoLayout.createSequentialGroup()
                .addGroup(jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlCuerpoLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jXTitleIngresoNuevoDonante, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPnlCuerpoLayout.createSequentialGroup()
                            .addGap(37, 37, 37)
                            .addComponent(jTxtApellido))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPnlCuerpoLayout.createSequentialGroup()
                            .addGap(25, 25, 25)
                            .addGroup(jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLblCuitDonante)
                                .addGroup(jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLblNombreDonante)
                                        .addComponent(jLblApellidoDonante)
                                        .addGroup(jPnlCuerpoLayout.createSequentialGroup()
                                            .addGap(12, 12, 12)
                                            .addComponent(jTxtNombre, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPnlCuerpoLayout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPnlCuerpoLayout.createSequentialGroup()
                .addContainerGap(35, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPnlCuerpoLayout.setVerticalGroup(
            jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXTitleIngresoNuevoDonante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLblCuitDonante)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLblApellidoDonante)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLblNombreDonante)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPnlCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPnlCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnAgregarDonanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnAgregarDonanteActionPerformed
        if (losDatosEstanCargados()) {
            if (donanteEncontrado()) {
                this.padre.setDonante(donanteEncontrado);
                this.dispose();
            } else {
                long dni = getDni();
                int parteDelantera = getParteDelantera();
                int parteTrasera = getParteTrasera();
                String apellidos = this.jTxtApellido.getText();
                String nombres = this.jTxtNombre.getText();
                Donante donante = new Donante(dni, parteDelantera, parteTrasera, nombres, apellidos);
                this.padre.setDonante(donante);
                this.dispose();
            }
        }
    }//GEN-LAST:event_jBtnAgregarDonanteActionPerformed

    private void jBtnVolverAlInicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVolverAlInicioActionPerformed
        this.dispose();
}//GEN-LAST:event_jBtnVolverAlInicioActionPerformed

    private void jTxtParteDelanteraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtParteDelanteraActionPerformed
    }//GEN-LAST:event_jTxtParteDelanteraActionPerformed

    private void jTxtParteDelanteraKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtParteDelanteraKeyTyped
        Vistas.Util.Util.ignorarSiNoEsEntero(evt);
        Vistas.Util.Util.limitarCaracteres(evt, jTxtParteDelantera.getText(), 2);
        if (jTxtParteDelantera.getText().length() == 2 && jTxtParteDni.getText().length() > 6 && jTxtParteTrasera.getText().length() == 1) {
            this.buscarYCargarDonantePorCuit();
        }
    }//GEN-LAST:event_jTxtParteDelanteraKeyTyped

    private void jTxtParteTraseraKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtParteTraseraKeyTyped
        Vistas.Util.Util.ignorarSiNoEsEntero(evt);
        Vistas.Util.Util.limitarCaracteres(evt, jTxtParteTrasera.getText(), 1);
        if (jTxtParteDelantera.getText().length() == 2 && jTxtParteDni.getText().length() > 6 && jTxtParteTrasera.getText().length() == 1) {
            this.buscarYCargarDonantePorCuit();
        }
    }//GEN-LAST:event_jTxtParteTraseraKeyTyped

    private void jTxtParteDniKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtParteDniKeyTyped
        Vistas.Util.Util.ignorarSiNoEsEntero(evt);
        Vistas.Util.Util.limitarCaracteres(evt, jTxtParteDni.getText(), 10);
        if (jTxtParteDni.getText().length() >= 6) {
            this.buscarYCargarDonantePorDni();
        }
    }//GEN-LAST:event_jTxtParteDniKeyTyped

    private void jTxtApellidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtApellidoKeyTyped
        Vistas.Util.Util.ignorarSiNoEsLetra(evt);
    }//GEN-LAST:event_jTxtApellidoKeyTyped

    private void jTxtNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtNombreKeyTyped
        Vistas.Util.Util.ignorarSiNoEsLetra(evt);
    }//GEN-LAST:event_jTxtNombreKeyTyped

    /**ComboBox to only allow one
    character to be entered? I can do 
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogIngresoDonantes dialog = new JDialogIngresoDonantes(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnAgregarDonante;
    private javax.swing.JButton jBtnVolverAlInicio;
    private javax.swing.JLabel jLblApellidoDonante;
    private javax.swing.JLabel jLblCuitDonante;
    private javax.swing.JLabel jLblNombreDonante;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPnlCuerpo;
    private javax.swing.JTextField jTxtApellido;
    private javax.swing.JTextField jTxtNombre;
    private javax.swing.JTextField jTxtParteDelantera;
    private javax.swing.JTextField jTxtParteDni;
    private javax.swing.JTextField jTxtParteTrasera;
    private org.jdesktop.swingx.JXTitledSeparator jXTitleIngresoNuevoDonante;
    // End of variables declaration//GEN-END:variables

    private void buscarYCargarDonantePorCuit() {
        if (!(donantes == null) && !donantes.isEmpty()) {
            long dni = getDni();
            int parteDelantera = getParteDelantera();
            int parteTrasera = getParteTrasera();
            Iterator<Donante> it = donantes.iterator();
            Donante donante = null;
            while (it.hasNext()) {
                Donante donanteIt = it.next();
                if (donanteIt.esTuDni(dni) && donanteIt.esTuParteDelantera(parteDelantera) && donanteIt.esTuParteTrasera(parteTrasera)) {
                    donante = donanteIt;
                    donanteEncontrado = donanteIt;
                    break;
                }
            }
            if (donante != null) {
                cargarDonanteEnVista(donante);
            }
        }
    }

    private void buscarYCargarDonantePorDni() {
        if (!(donantes == null) && !donantes.isEmpty()) {
            long dni = getDni();
            Iterator<Donante> it = donantes.iterator();
            Donante donante = null;
            while (it.hasNext()) {
                Donante donanteIt = it.next();
                if (donanteIt.esTuDni(dni)) {
                    donante = donanteIt;
                    donanteEncontrado = donanteIt;
                    break;
                }
            }
            if (donante != null) {
                cargarDonanteEnVista(donante);
            }
        }
    }

    private long getDni() {
        return Long.parseLong(this.jTxtParteDni.getText());
    }

    private int getParteDelantera() {
        return Integer.parseInt(this.jTxtParteDelantera.getText());
    }

    private int getParteTrasera() {
        // ya me asegure que este dato sea cargado y con el formato debido, así que este control no lo hago aquí
        return Integer.parseInt(this.jTxtParteTrasera.getText());
    }

    private void cargarDonanteEnVista(Donante donante) {
        jTxtApellido.setText(donante.getApellidos());
        jTxtNombre.setText(donante.getNombres());
        jTxtParteDelantera.setText(String.valueOf(donante.getParteDelantera()));
        jTxtParteDni.setText(String.valueOf(donante.getDni()));
        jTxtParteTrasera.setText(String.valueOf(donante.getParteTrasera()));
    }

    private boolean losDatosEstanCargados() {
        boolean datosCargados = this.jTxtApellido.getText().isEmpty() || this.jTxtNombre.getText().isEmpty() || this.jTxtParteDelantera.getText().isEmpty()
                || this.jTxtParteDni.getText().isEmpty() || this.jTxtParteTrasera.getText().isEmpty() ? false : true;
        return datosCargados;
    }

    private boolean donanteEncontrado() {
        return donanteEncontrado == null ? false : true;
    }
}
