package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.*;
import Util.ReportUtils;
import Util.UtilFechas;
import Vistas.Util.ImporteCellRenderer;
import java.text.NumberFormat;
import java.util.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author emanuel
 */
public class JDialogBalanceGenerado extends javax.swing.JDialog {

    InstitucionCentralTesoreria institucion;
    Balance balance;
    double cajaInicial;

    public JDialogBalanceGenerado(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
    }

    JDialogBalanceGenerado(JDialogGenerarOConsultarBalance padre, boolean modal, BalanceAnual balanceAnual, 
            InstitucionCentralTesoreria institucion, double cajaInicial) {
        super(padre, modal);
        initComponents();
        jXTableAsientos.setDefaultRenderer(Double.class, new ImporteCellRenderer());
        jXTableResumenPorCuentas.setDefaultRenderer(Double.class, new ImporteCellRenderer());
        this.cajaInicial= cajaInicial;
        this.balance = balanceAnual;
        this.institucion = institucion;
        cargarDatosDeBalance(balanceAnual);
        setLocationRelativeTo(null);
    }

    JDialogBalanceGenerado(JDialogGenerarOConsultarBalance padre, boolean modal, Modelo.BalanceMensual balanceMensual, 
            InstitucionCentralTesoreria institucion, double cajaInicial) {
        super(padre, modal);
        initComponents();
        jXTableAsientos.setDefaultRenderer(Double.class, new ImporteCellRenderer());
        jXTableResumenPorCuentas.setDefaultRenderer(Double.class, new ImporteCellRenderer());
        this.cajaInicial= cajaInicial;
        balance = balanceMensual;
        this.institucion = institucion;
        cargarDatosDeBalance(balanceMensual);
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXPanelCuerpo = new org.jdesktop.swingx.JXPanel();
        jXSeparadorDeTituloDatosBalanceGenerado = new org.jdesktop.swingx.JXTitledSeparator();
        jScrollPaneAsientosRelatorio = new javax.swing.JScrollPane();
        jXTableAsientos = new org.jdesktop.swingx.JXTable();
        jLabelNroBalance = new javax.swing.JLabel();
        jLabelNroBalanceDato = new javax.swing.JLabel();
        jLabelFechaDeEmision = new javax.swing.JLabel();
        jLabelPeriodoBalance = new javax.swing.JLabel();
        jLabelTotalIngresos = new javax.swing.JLabel();
        jLabelCajaFinal = new javax.swing.JLabel();
        jLabelTotalEgresos = new javax.swing.JLabel();
        jLabelTotalIngresosDato = new javax.swing.JLabel();
        jLabelFechaDeEmisionDato = new javax.swing.JLabel();
        jLabelCajaFinalDato = new javax.swing.JLabel();
        jLabelPeriodoBalanceDato = new javax.swing.JLabel();
        jLabelTotalEgresosDato = new javax.swing.JLabel();
        jXSeparadorDeTituloDatosDetallesDeAsientos = new org.jdesktop.swingx.JXTitledSeparator();
        jPanel4 = new javax.swing.JPanel();
        jBtnGurdarSolamente = new javax.swing.JButton();
        jBtnExportarAExcel = new javax.swing.JButton();
        jBtnImprimirBalance = new javax.swing.JButton();
        jBtnIgnorarYVolver = new javax.swing.JButton();
        jLabelTotalNetoDato = new javax.swing.JLabel();
        jLabelTotalNeto = new javax.swing.JLabel();
        jXTitledPorCuentas = new org.jdesktop.swingx.JXTitledSeparator();
        jScrollPaneAsientosRelatorio1 = new javax.swing.JScrollPane();
        jXTableResumenPorCuentas = new org.jdesktop.swingx.JXTable();
        jLblCajaInicial = new javax.swing.JLabel();
        jLblCajaInicialDato = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jXPanelCuerpo.setMinimumSize(new java.awt.Dimension(1130, 548));

        jXSeparadorDeTituloDatosBalanceGenerado.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jXSeparadorDeTituloDatosBalanceGenerado.setHorizontalAlignment(0);
        jXSeparadorDeTituloDatosBalanceGenerado.setTitle("Datos del balance");

        jXTableAsientos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Tipo cuenta", "Cuenta", "Fecha", "Monto", "Pagado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jXTableAsientos.getTableHeader().setReorderingAllowed(false);
        jScrollPaneAsientosRelatorio.setViewportView(jXTableAsientos);
        jXTableAsientos.getColumnModel().getColumn(0).setMinWidth(130);
        jXTableAsientos.getColumnModel().getColumn(0).setPreferredWidth(160);
        jXTableAsientos.getColumnModel().getColumn(0).setMaxWidth(160);
        jXTableAsientos.getColumnModel().getColumn(2).setMinWidth(75);
        jXTableAsientos.getColumnModel().getColumn(2).setPreferredWidth(100);
        jXTableAsientos.getColumnModel().getColumn(2).setMaxWidth(120);
        jXTableAsientos.getColumnModel().getColumn(3).setMinWidth(75);
        jXTableAsientos.getColumnModel().getColumn(3).setPreferredWidth(100);
        jXTableAsientos.getColumnModel().getColumn(3).setMaxWidth(120);
        jXTableAsientos.getColumnModel().getColumn(4).setMinWidth(75);
        jXTableAsientos.getColumnModel().getColumn(4).setPreferredWidth(100);
        jXTableAsientos.getColumnModel().getColumn(4).setMaxWidth(120);

        jLabelNroBalance.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelNroBalance.setText("Número de balance:");

        jLabelNroBalanceDato.setText("32");

        jLabelFechaDeEmision.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelFechaDeEmision.setText("Fecha de emisión:");

        jLabelPeriodoBalance.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelPeriodoBalance.setText("Período:");

        jLabelTotalIngresos.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelTotalIngresos.setText("Total ingresos:");

        jLabelCajaFinal.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelCajaFinal.setText("Caja final:");

        jLabelTotalEgresos.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelTotalEgresos.setText("Total egresos:");

        jLabelTotalIngresosDato.setText("123");

        jLabelFechaDeEmisionDato.setText("12/12/12");

        jLabelCajaFinalDato.setText("324");

        jLabelPeriodoBalanceDato.setText("marzo");

        jLabelTotalEgresosDato.setText("noas");

        jXSeparadorDeTituloDatosDetallesDeAsientos.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jXSeparadorDeTituloDatosDetallesDeAsientos.setHorizontalAlignment(0);
        jXSeparadorDeTituloDatosDetallesDeAsientos.setTitle("Asientos");

        jBtnGurdarSolamente.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnGurdarSolamente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Guardar 30x30.png"))); // NOI18N
        jBtnGurdarSolamente.setText("Guardar solamente");
        jBtnGurdarSolamente.setToolTipText("Quitado por no ser util. No es conveniente guardar balances. Siempre es mejor volverlos a generar. \nSerá eliminado en próximas versiones.");
        jBtnGurdarSolamente.setEnabled(false);
        jBtnGurdarSolamente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnGurdarSolamenteActionPerformed(evt);
            }
        });
        jPanel4.add(jBtnGurdarSolamente);

        jBtnExportarAExcel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Excel-icon.png"))); // NOI18N
        jBtnExportarAExcel.setText("Exportar a Excel");
        jBtnExportarAExcel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnExportarAExcelActionPerformed(evt);
            }
        });
        jPanel4.add(jBtnExportarAExcel);

        jBtnImprimirBalance.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Imprimir 20x20.png"))); // NOI18N
        jBtnImprimirBalance.setText("Imprimir balance");
        jBtnImprimirBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnImprimirBalanceActionPerformed(evt);
            }
        });
        jPanel4.add(jBtnImprimirBalance);

        jBtnIgnorarYVolver.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnIgnorarYVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnIgnorarYVolver.setText("Ignorar y  volver");
        jBtnIgnorarYVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnIgnorarYVolverActionPerformed(evt);
            }
        });
        jPanel4.add(jBtnIgnorarYVolver);

        jLabelTotalNetoDato.setText("noas");

        jLabelTotalNeto.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelTotalNeto.setText("Total neto: ");

        jXTitledPorCuentas.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jXTitledPorCuentas.setHorizontalAlignment(0);
        jXTitledPorCuentas.setTitle("Resumen por cuentas");

        jXTableResumenPorCuentas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Tipo cuenta", "Cuenta", "Monto", "Pagado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jXTableResumenPorCuentas.getTableHeader().setReorderingAllowed(false);
        jScrollPaneAsientosRelatorio1.setViewportView(jXTableResumenPorCuentas);
        jXTableResumenPorCuentas.getColumnModel().getColumn(0).setMinWidth(130);
        jXTableResumenPorCuentas.getColumnModel().getColumn(0).setPreferredWidth(160);
        jXTableResumenPorCuentas.getColumnModel().getColumn(0).setMaxWidth(160);
        jXTableResumenPorCuentas.getColumnModel().getColumn(2).setMinWidth(75);
        jXTableResumenPorCuentas.getColumnModel().getColumn(2).setPreferredWidth(100);
        jXTableResumenPorCuentas.getColumnModel().getColumn(2).setMaxWidth(120);
        jXTableResumenPorCuentas.getColumnModel().getColumn(3).setMinWidth(75);
        jXTableResumenPorCuentas.getColumnModel().getColumn(3).setPreferredWidth(100);
        jXTableResumenPorCuentas.getColumnModel().getColumn(3).setMaxWidth(120);

        jLblCajaInicial.setText("Caja inicial:");

        jLblCajaInicialDato.setText("algo");

        javax.swing.GroupLayout jXPanelCuerpoLayout = new javax.swing.GroupLayout(jXPanelCuerpo);
        jXPanelCuerpo.setLayout(jXPanelCuerpoLayout);
        jXPanelCuerpoLayout.setHorizontalGroup(
            jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                        .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                                .addGap(29, 29, 29)
                                .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelNroBalance)
                                    .addComponent(jLabelTotalIngresos)
                                    .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addComponent(jLabelNroBalanceDato, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jXPanelCuerpoLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabelTotalIngresosDato, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(72, 72, 72)
                        .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLabelTotalEgresosDato, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                                .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelFechaDeEmision)
                                    .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addComponent(jLabelFechaDeEmisionDato, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabelTotalEgresos))
                                .addGap(98, 98, 98)
                                .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelTotalNeto)
                                    .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                                        .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabelPeriodoBalance)
                                            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                                                .addGap(12, 12, 12)
                                                .addComponent(jLabelTotalNetoDato, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                                                .addGap(12, 12, 12)
                                                .addComponent(jLabelPeriodoBalanceDato, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(9, 9, 9)
                                        .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLblCajaInicial)
                                            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                                                .addGap(13, 13, 13)
                                                .addComponent(jLblCajaInicialDato, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(31, 31, 31)
                                        .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                                                .addGap(12, 12, 12)
                                                .addComponent(jLabelCajaFinalDato, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(jLabelCajaFinal)))))))
                    .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jXSeparadorDeTituloDatosBalanceGenerado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jXPanelCuerpoLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPaneAsientosRelatorio)
                            .addComponent(jXSeparadorDeTituloDatosDetallesDeAsientos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jXTitledPorCuentas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPaneAsientosRelatorio1, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap())
        );
        jXPanelCuerpoLayout.setVerticalGroup(
            jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXSeparadorDeTituloDatosBalanceGenerado, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                        .addComponent(jLabelFechaDeEmision)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabelFechaDeEmisionDato)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabelTotalEgresos)
                        .addGap(9, 9, 9)
                        .addComponent(jLabelTotalEgresosDato))
                    .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                        .addComponent(jLabelNroBalance)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabelNroBalanceDato)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabelTotalIngresos)
                        .addGap(12, 12, 12)
                        .addComponent(jLabelTotalIngresosDato))
                    .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                        .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                                .addComponent(jLabelCajaFinal)
                                .addGap(9, 9, 9)
                                .addComponent(jLabelCajaFinalDato))
                            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                                .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelPeriodoBalance)
                                    .addComponent(jLblCajaInicial))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelPeriodoBalanceDato)
                                    .addComponent(jLblCajaInicialDato))))
                        .addGap(15, 15, 15)
                        .addComponent(jLabelTotalNeto)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelTotalNetoDato)))
                .addGap(20, 20, 20)
                .addComponent(jXSeparadorDeTituloDatosDetallesDeAsientos, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPaneAsientosRelatorio, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jXTitledPorCuentas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPaneAsientosRelatorio1, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jXPanelCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, 893, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jXPanelCuerpo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnIgnorarYVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnIgnorarYVolverActionPerformed
        this.dispose();
        this.getParent().setVisible(true);
}//GEN-LAST:event_jBtnIgnorarYVolverActionPerformed

    private void jBtnGurdarSolamenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnGurdarSolamenteActionPerformed
        int respuesta = JOptionPane.showConfirmDialog(this, "<html> ¿Guardar el balance generado? <br>"
                + "<b> TENGA EN CUENTA QUE NO PODRÁ GENERAR DOS BALANCES MENSUALES PARA UN MISMO MES <br> "
                + "O DOS BALANCES ANUALES PARA UN MISMO AÑO<br> </html> ", "Aviso", JOptionPane.YES_NO_OPTION);
        if (respuesta == JOptionPane.YES_OPTION) {
            if (balance.sosBalanceMensual()) {
                try {
                    this.institucion.persistirBalanceMensual((BalanceMensual) balance);
                } catch (DAOProblemaConTransaccionException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.INFORMATION_MESSAGE);
                }
            } else {
                try {
                    this.institucion.persistirBalanceAnual((BalanceAnual) balance);
                } catch (DAOProblemaConTransaccionException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }
    }//GEN-LAST:event_jBtnGurdarSolamenteActionPerformed

    @SuppressWarnings("deprecation")
    private void jBtnImprimirBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnImprimirBalanceActionPerformed
        if(!institucion.hayDatosDeInstitucion()){
            JOptionPane.showMessageDialog(this, "Es necesario que cargue los datos de la iglesia primero. \n Esta información se imprime en el balance.");
            return;
        }
        boolean balanceMensual = balance instanceof BalanceMensual;
        Map<String, Object> parametrosReporte = crearMapParams(balanceMensual);
        if(!balanceMensual) {
            parametrosReporte.put("ANIO", balance.getAnio());
            ReportUtils.printReport("balance_anual", parametrosReporte);
        }else{
            ReportUtils.printReport("balance_mensual", parametrosReporte);
        }
    }//GEN-LAST:event_jBtnImprimirBalanceActionPerformed

    private void jBtnExportarAExcelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnExportarAExcelActionPerformed
        try{
            JOptionPane.showMessageDialog(this, "Exportando reporte a excel...");
            ReportUtils.reportToExcel(balance instanceof BalanceMensual ? "balance_mensual":"balance_anual", getParametrosReporte());
            JOptionPane.showMessageDialog(this, "Reporte exportado a excel. \n Ubicación del archivo: C:\\reporte");
        }catch (Exception e){
            JOptionPane.showMessageDialog(this, "Hubo problemas con la exportación. Intente de nuevo");
        }
        
    }//GEN-LAST:event_jBtnExportarAExcelActionPerformed

    public Map<String,Object> getParametrosReporte(){
        boolean balanceMensual = balance instanceof BalanceMensual;
        Map<String, Object> parametrosReporte = new HashMap<String, Object>();
        GregorianCalendar comienzo = new GregorianCalendar();
        GregorianCalendar fin = new GregorianCalendar();
        comienzo.set(Calendar.DAY_OF_MONTH, 1);
        comienzo.set(Calendar.MONTH, balanceMensual ? ((BalanceMensual) balance).getMes(): 0);
        comienzo.set(Calendar.YEAR, balance.getAnio());
        fin.set(Calendar.DAY_OF_MONTH, UtilFechas.getDiaDeFinDeMes(balanceMensual? ((BalanceMensual)balance).getMes():0));
        fin.set(Calendar.MONTH, balanceMensual ? ((BalanceMensual) balance).getMes(): 11);
        fin.set(Calendar.YEAR, balance.getAnio());
        parametrosReporte.put("FECHA_COMIENZO_MES", comienzo.getTime());
        parametrosReporte.put("FECHA_FIN_MES", fin.getTime());
        parametrosReporte.put("CAJA_INICIAL", cajaInicial);
        if(!balanceMensual) {
            parametrosReporte.put("ANIO", balance.getAnio());
        }
        return parametrosReporte;
    }
    public void cargarDatosDeBalance(Balance balance) {
        cargarDatosEnLabels(balance);
        cagarDatosEnJTables(balance);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogBalanceGenerado dialog = new JDialogBalanceGenerado(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnExportarAExcel;
    private javax.swing.JButton jBtnGurdarSolamente;
    private javax.swing.JButton jBtnIgnorarYVolver;
    private javax.swing.JButton jBtnImprimirBalance;
    private javax.swing.JLabel jLabelCajaFinal;
    private javax.swing.JLabel jLabelCajaFinalDato;
    private javax.swing.JLabel jLabelFechaDeEmision;
    private javax.swing.JLabel jLabelFechaDeEmisionDato;
    private javax.swing.JLabel jLabelNroBalance;
    private javax.swing.JLabel jLabelNroBalanceDato;
    private javax.swing.JLabel jLabelPeriodoBalance;
    private javax.swing.JLabel jLabelPeriodoBalanceDato;
    private javax.swing.JLabel jLabelTotalEgresos;
    private javax.swing.JLabel jLabelTotalEgresosDato;
    private javax.swing.JLabel jLabelTotalIngresos;
    private javax.swing.JLabel jLabelTotalIngresosDato;
    private javax.swing.JLabel jLabelTotalNeto;
    private javax.swing.JLabel jLabelTotalNetoDato;
    private javax.swing.JLabel jLblCajaInicial;
    private javax.swing.JLabel jLblCajaInicialDato;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPaneAsientosRelatorio;
    private javax.swing.JScrollPane jScrollPaneAsientosRelatorio1;
    private org.jdesktop.swingx.JXPanel jXPanelCuerpo;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloDatosBalanceGenerado;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloDatosDetallesDeAsientos;
    private org.jdesktop.swingx.JXTable jXTableAsientos;
    private org.jdesktop.swingx.JXTable jXTableResumenPorCuentas;
    private org.jdesktop.swingx.JXTitledSeparator jXTitledPorCuentas;
    // End of variables declaration//GEN-END:variables

    private void cagarDatosEnJTables(Balance balance) {
        DefaultTableModel modeloTabla = (DefaultTableModel) jXTableAsientos.getModel();
        Iterator<Modelo.Ingreso> ingresos = balance.getIngresos().iterator();
        while (ingresos.hasNext()) {
            Ingreso ingreso = ingresos.next();
            Object[] FilaTabla = {"Ingreso", ingreso.getCuenta().getNombre(), Util.UtilFechas.getFechaEnFormatoDD_MM_AA(ingreso.getFecha()),
                new Double(ingreso.getMonto()), "-"};
            modeloTabla.addRow(FilaTabla);
        }
        Iterator<Modelo.Egreso> egresos = balance.getEgresos().iterator();
        while (egresos.hasNext()) {
            Modelo.Egreso egreso = egresos.next();
            Object[] FilaTabla = {"Egreso", egreso.getCuenta().getNombre(), Util.UtilFechas.getFechaEnFormatoDD_MM_AA(egreso.getFecha()),
                new Double(egreso.getMonto()), new Double(egreso.getPagado())};
            modeloTabla.addRow(FilaTabla);
        }
        ArrayList<Asiento> asientosPorCuentas = institucion.toAsientosPorCuentas(balance.getIngresos(), balance.getEgresos());
        Object[] asientosArray =  asientosPorCuentas.toArray(new Asiento[]{});
        DefaultTableModel modeloTablaResumenPorCuentas = (DefaultTableModel) jXTableResumenPorCuentas.getModel();
        for (Object asiento : asientosArray) {
            Asiento _asiento = (Asiento) asiento;
            modeloTablaResumenPorCuentas.addRow(new Object[] {_asiento.sosIngreso() ? "Ingreso" : "Egreso", 
            _asiento.getCuenta(), _asiento.getMonto(), _asiento.sosIngreso() ? "-" : (((Egreso)_asiento).getPagado())});
        }
        jXTableResumenPorCuentas.setModel(modeloTablaResumenPorCuentas);
    }

    private void cargarDatosEnLabels(Balance balance) {
        jLblCajaInicialDato.setText(NumberFormat.getCurrencyInstance(Locale.US).format(cajaInicial));
        jLabelTotalEgresosDato.setText(NumberFormat.getCurrencyInstance(Locale.US).format(balance.getTotalEgresos()));
        jLabelTotalIngresosDato.setText(NumberFormat.getCurrencyInstance(Locale.US).format(balance.getTotalIngresos()));
        jLabelFechaDeEmisionDato.setText(Util.UtilFechas.getFechaEnFormatoDD_MM_AA(balance.getFechaDeEmision()));
        jLabelTotalNetoDato.setText(NumberFormat.getCurrencyInstance(Locale.US).format(balance.getTotalNeto()));
        jLabelNroBalanceDato.setText(String.valueOf(balance.getNroBalance())); //puede que de vacío, sería normal.. probar
        if (balance.sosBalanceMensual()) {
            BalanceMensual aux = (BalanceMensual) balance;
            jLabelPeriodoBalanceDato.setText(Util.UtilFechas.getFechaEnFormatoMM_AA(new GregorianCalendar(balance.getAnio(), aux.getMes(),
                    balance.getFechaDeEmision().get(GregorianCalendar.DAY_OF_MONTH))));
        } else {
            jLabelPeriodoBalanceDato.setText(String.valueOf(balance.getAnio()));
        }
        jLabelCajaFinalDato.setText(NumberFormat.getCurrencyInstance(Locale.US).format(cajaInicial + balance.getTotalNeto()));
    }

    private Map<String, Object> crearMapParams(boolean balanceMensual) {
        Map<String, Object> parametrosReporte = new HashMap<String, Object>();
        GregorianCalendar comienzo = new GregorianCalendar();
        GregorianCalendar fin = new GregorianCalendar();
        comienzo.set(Calendar.DAY_OF_MONTH, 1);
        comienzo.set(Calendar.MONTH, balanceMensual ? ((BalanceMensual) balance).getMes(): 0);
        comienzo.set(Calendar.YEAR, balance.getAnio());
        fin.set(Calendar.DAY_OF_MONTH, UtilFechas.getDiaDeFinDeMes(balanceMensual? ((BalanceMensual)balance).getMes():0));
        fin.set(Calendar.MONTH, balanceMensual ? ((BalanceMensual) balance).getMes(): 11);
        fin.set(Calendar.YEAR, balance.getAnio());
        parametrosReporte.put("FECHA_COMIENZO_MES", comienzo.getTime());
        parametrosReporte.put("FECHA_FIN_MES", fin.getTime());
        parametrosReporte.put("CAJA_INICIAL", cajaInicial);
        parametrosReporte.put("SUBREPORT_DIR", ReportUtils.getPathOfReports());
        return parametrosReporte;
    }
}
