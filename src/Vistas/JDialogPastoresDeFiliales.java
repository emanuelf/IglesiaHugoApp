package Vistas;

import Modelo.InstitucionCentral;
import Modelo.PastorFilial;
import Util.UtilFechas;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author emanuel
 */
public class JDialogPastoresDeFiliales extends javax.swing.JDialog {

    InstitucionCentral insticucion;
    ArrayList<PastorFilial> pastores;

    public JDialogPastoresDeFiliales(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
    }

    public JDialogPastoresDeFiliales(java.awt.Frame parent, boolean modal, InstitucionCentral institucion) {
        super(parent, modal);
        initComponents();
        this.insticucion = institucion;
        pastores = this.insticucion.getAllPastores();
        loadPastoresInJTable(pastores);
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXPanelCuerpo = new org.jdesktop.swingx.JXPanel();
        jXSeparadorDeTituloDatosPastores = new org.jdesktop.swingx.JXTitledSeparator();
        jScrollPane2 = new javax.swing.JScrollPane();
        jScrollPaneDatosPastor = new javax.swing.JScrollPane();
        jXTableDatosPastor = new org.jdesktop.swingx.JXTable();
        jXLinkVolverAlInicio = new org.jdesktop.swingx.JXHyperlink();
        jPnlDatosGenerales = new javax.swing.JPanel();
        jLabelApellidos = new javax.swing.JLabel();
        jLabelFechaDeOrdenacion = new javax.swing.JLabel();
        jLabelConyuge = new javax.swing.JLabel();
        jLabelNombresDato = new javax.swing.JLabel();
        jLabelTelFijo = new javax.swing.JLabel();
        jLabelDireccionDato = new javax.swing.JLabel();
        jLabelTelCelDato = new javax.swing.JLabel();
        jLabelFechaDeOrdenacionDato = new javax.swing.JLabel();
        jLabelApellidosDato = new javax.swing.JLabel();
        jLabelConyugeDato = new javax.swing.JLabel();
        jLabelTelFijoDato = new javax.swing.JLabel();
        jLabelNombres = new javax.swing.JLabel();
        jLabelDireccion = new javax.swing.JLabel();
        jLabelTelCel = new javax.swing.JLabel();
        jBtnInicio_seleccionarMiembro = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jXPanelCuerpo.setMinimumSize(new java.awt.Dimension(1130, 548));
        jXPanelCuerpo.setPreferredSize(new java.awt.Dimension(1130, 548));

        jXSeparadorDeTituloDatosPastores.setFont(new java.awt.Font("Tahoma", 0, 14));
        jXSeparadorDeTituloDatosPastores.setHorizontalAlignment(0);
        jXSeparadorDeTituloDatosPastores.setTitle("Datos de los pastores");

        jXTableDatosPastor.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Iglesia ", "Apellido", "Nombre ", "Cónyuge"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jXTableDatosPastor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jXTableDatosPastorMouseClicked(evt);
            }
        });
        jScrollPaneDatosPastor.setViewportView(jXTableDatosPastor);

        jScrollPane2.setViewportView(jScrollPaneDatosPastor);

        jXLinkVolverAlInicio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jXLinkVolverAlInicio.setText("Volver al inicio...");
        jXLinkVolverAlInicio.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jXLinkVolverAlInicio.setFont(new java.awt.Font("Tahoma", 0, 12));

        jPnlDatosGenerales.setMaximumSize(new java.awt.Dimension(901, 32767));
        jPnlDatosGenerales.setPreferredSize(new java.awt.Dimension(700, 116));

        jLabelApellidos.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelApellidos.setText("Apellido/s:");

        jLabelFechaDeOrdenacion.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelFechaDeOrdenacion.setText("Fecha de ordenación:");

        jLabelConyuge.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelConyuge.setText("Conyugue:");

        jLabelNombresDato.setFont(new java.awt.Font("Tahoma", 1, 12));

        jLabelTelFijo.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelTelFijo.setText("Teléfono fijo:");

        jLabelDireccionDato.setFont(new java.awt.Font("Tahoma", 1, 12));

        jLabelTelCelDato.setFont(new java.awt.Font("Tahoma", 1, 12));

        jLabelFechaDeOrdenacionDato.setFont(new java.awt.Font("Tahoma", 1, 12));

        jLabelApellidosDato.setFont(new java.awt.Font("Tahoma", 1, 12));

        jLabelConyugeDato.setFont(new java.awt.Font("Tahoma", 1, 12));

        jLabelTelFijoDato.setFont(new java.awt.Font("Tahoma", 1, 12));

        jLabelNombres.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelNombres.setText("Nombre/s:");

        jLabelDireccion.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelDireccion.setText("Dirección:");

        jLabelTelCel.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelTelCel.setText("Teléfono celular:");

        javax.swing.GroupLayout jPnlDatosGeneralesLayout = new javax.swing.GroupLayout(jPnlDatosGenerales);
        jPnlDatosGenerales.setLayout(jPnlDatosGeneralesLayout);
        jPnlDatosGeneralesLayout.setHorizontalGroup(
            jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                        .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelFechaDeOrdenacion, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelApellidos, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPnlDatosGeneralesLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLabelApellidosDato, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPnlDatosGeneralesLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLabelFechaDeOrdenacionDato, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 126, Short.MAX_VALUE)
                        .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabelNombres)
                            .addComponent(jLabelTelFijo)
                            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLabelTelFijoDato, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLabelNombresDato, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                                .addGap(155, 155, 155)
                                .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabelTelCel)
                                    .addComponent(jLabelDireccion)
                                    .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addComponent(jLabelDireccionDato, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                                .addGap(167, 167, 167)
                                .addComponent(jLabelTelCelDato, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(215, 215, 215))
                    .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                        .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                                .addComponent(jLabelConyuge)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLabelConyugeDato, javax.swing.GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)))
                        .addGap(682, 682, 682))))
        );
        jPnlDatosGeneralesLayout.setVerticalGroup(
            jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                        .addComponent(jLabelApellidos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelApellidosDato)
                        .addGap(18, 18, 18)
                        .addComponent(jLabelFechaDeOrdenacion, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelFechaDeOrdenacionDato, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                        .addComponent(jLabelNombres)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelNombresDato)
                        .addGap(18, 18, 18)
                        .addComponent(jLabelTelFijo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelTelFijoDato))
                    .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                        .addComponent(jLabelDireccion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelDireccionDato)
                        .addGap(18, 18, 18)
                        .addComponent(jLabelTelCel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelTelCelDato, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelConyuge, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelConyugeDato, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
        );

        jBtnInicio_seleccionarMiembro.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnInicio_seleccionarMiembro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnInicio_seleccionarMiembro.setText("Inicio");
        jBtnInicio_seleccionarMiembro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnInicio_seleccionarMiembroActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jXPanelCuerpoLayout = new javax.swing.GroupLayout(jXPanelCuerpo);
        jXPanelCuerpo.setLayout(jXPanelCuerpoLayout);
        jXPanelCuerpoLayout.setHorizontalGroup(
            jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPnlDatosGenerales, javax.swing.GroupLayout.PREFERRED_SIZE, 793, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jXSeparadorDeTituloDatosPastores, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 793, Short.MAX_VALUE)))
                    .addComponent(jBtnInicio_seleccionarMiembro))
                .addGap(64, 64, 64)
                .addComponent(jXLinkVolverAlInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jXPanelCuerpoLayout.setVerticalGroup(
            jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXSeparadorDeTituloDatosPastores, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                        .addGap(169, 169, 169)
                        .addComponent(jXLinkVolverAlInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jPnlDatosGenerales, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jBtnInicio_seleccionarMiembro)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jXPanelCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, 816, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jXPanelCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, 520, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnInicio_seleccionarMiembroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnInicio_seleccionarMiembroActionPerformed
        this.dispose();
}//GEN-LAST:event_jBtnInicio_seleccionarMiembroActionPerformed

    private void jXTableDatosPastorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jXTableDatosPastorMouseClicked
        int filaSelecionada = jXTableDatosPastor.getSelectedRow();
        PastorFilial pastorSeleccionado = this.buscarPastorSeleccionado();
        cargarDatosDePastorEnLabels(pastorSeleccionado);
    }//GEN-LAST:event_jXTableDatosPastorMouseClicked

    public DefaultTableModel getModeloTabla() {
        DefaultTableModel modelo = (DefaultTableModel) this.jXTableDatosPastor.getModel();
        return modelo;
    }

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogPastoresDeFiliales dialog = new JDialogPastoresDeFiliales(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnInicio_seleccionarMiembro;
    private javax.swing.JLabel jLabelApellidos;
    private javax.swing.JLabel jLabelApellidosDato;
    private javax.swing.JLabel jLabelConyuge;
    private javax.swing.JLabel jLabelConyugeDato;
    private javax.swing.JLabel jLabelDireccion;
    private javax.swing.JLabel jLabelDireccionDato;
    private javax.swing.JLabel jLabelFechaDeOrdenacion;
    private javax.swing.JLabel jLabelFechaDeOrdenacionDato;
    private javax.swing.JLabel jLabelNombres;
    private javax.swing.JLabel jLabelNombresDato;
    private javax.swing.JLabel jLabelTelCel;
    private javax.swing.JLabel jLabelTelCelDato;
    private javax.swing.JLabel jLabelTelFijo;
    private javax.swing.JLabel jLabelTelFijoDato;
    private javax.swing.JPanel jPnlDatosGenerales;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPaneDatosPastor;
    private org.jdesktop.swingx.JXHyperlink jXLinkVolverAlInicio;
    private org.jdesktop.swingx.JXPanel jXPanelCuerpo;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloDatosPastores;
    private org.jdesktop.swingx.JXTable jXTableDatosPastor;
    // End of variables declaration//GEN-END:variables

    private void loadPastoresInJTable(ArrayList<PastorFilial> pastores) {
        if (pastores == null) {
            return;
        }
        if (pastores.isEmpty()) {
            return;
        }
        Iterator<PastorFilial> it = this.pastores.iterator();
        while (it.hasNext()) {
            PastorFilial pastorFilial = it.next();
            addRowWithDataPastor(pastorFilial);
        }
    }

    private void addRowWithDataPastor(PastorFilial pastorFilial) {
        String[] rowDataPastor = {this.insticucion.getNombreFilial(pastorFilial), pastorFilial.getApellidos(), pastorFilial.getNombres(), pastorFilial.getDireccion().toString()};
        getModeloTabla().addRow(rowDataPastor);
    }

    private PastorFilial buscarPastorSeleccionado() {
        PastorFilial pastorFilialARetornar = null;
        Iterator<PastorFilial> it = this.pastores.iterator();
        String iglesiaSeleccionada = this.jXTableDatosPastor.getValueAt(this.jXTableDatosPastor.getSelectedRow(), 0).toString();
        while (it.hasNext()) {
            PastorFilial pastorFilial = it.next();
            if (insticucion.getNombreFilial(pastorFilial).equalsIgnoreCase(iglesiaSeleccionada)) {
                pastorFilialARetornar = pastorFilial;
                break;
            }
        }
        return pastorFilialARetornar;
    }

    private void cargarDatosDePastorEnLabels(PastorFilial pastorSeleccionado) {
        jLabelApellidosDato.setText(pastorSeleccionado.getApellidos());
        jLabelConyugeDato.setText(pastorSeleccionado.getConyuge());
        jLabelDireccionDato.setText(pastorSeleccionado.getDireccion().toString());
        jLabelFechaDeOrdenacionDato.setText(UtilFechas.getFechaEnFormatoDD_MM_AA(pastorSeleccionado.getFechaDeOrdenacion()));
        jLabelNombresDato.setText(pastorSeleccionado.getNombres());
        jLabelTelCelDato.setText(String.valueOf(pastorSeleccionado.getTelefonoCelular()));
        jLabelTelFijoDato.setText(String.valueOf(pastorSeleccionado.getTelefonoFijo()));
    }
}
