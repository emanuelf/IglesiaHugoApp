package Vistas.wrappers;

import Modelo.Miembro;
import Vistas.Util.wrappers.Wrapper;
import java.text.DateFormat;

public class WrapperMiembro extends Wrapper<Miembro> {

    public WrapperMiembro(Miembro objetoAWrapear) {
        super(objetoAWrapear);
    }

    public Object[] toRow() {
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
        return new Object[]{objetoWrapeado.nombresYApellidos(),
                            objetoWrapeado.getDireccion() == null ? "" : objetoWrapeado.getDireccion().toString(),
                            objetoWrapeado.fechaBautismoEnDate() == null ? ""
                    : df.format(objetoWrapeado.fechaBautismoEnDate())};
    }

    public void setValueAt(int column, Object aValue) {
    }

    public boolean isCellEditable(int column) {
        return false;
    }
}