/*
 * JDialogAltaCuentaFilial.java
 *
 * Created on 22/09/2011, 22:47:01
 */
package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Excepciones.DAOYaExisteRegistroException;
import Modelo.CuentaFilial;
import Modelo.CuentaFilialDeuda;
import Modelo.CuentaFilialEgreso;
import Modelo.CuentaFilialIngreso;
import Modelo.InstitucionCentralTesoreria;
import Vistas.Util.Util;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author emanuel
 */
public class JDialogAltaCuentaFilial extends javax.swing.JDialog {

    InstitucionCentralTesoreria institucionCentralTesoreria;
    private JDialogAltaRelatorioDeFilial padre;

    /** Creates new form JDialogAltaCuentaFilial */
    public JDialogAltaCuentaFilial(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
    }

    public JDialogAltaCuentaFilial(JDialogAltaRelatorioDeFilial parent, boolean modal, InstitucionCentralTesoreria institucion) {
        this.padre = parent;
        institucionCentralTesoreria = institucion;
        setModal(modal);
        initComponents();
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPnlCuerpo = new javax.swing.JPanel();
        jXSeparadorDeTituloNuevaCuenta = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelNumeroDeCodigo = new javax.swing.JLabel();
        jTxtNombreDeCuenta = new javax.swing.JTextField();
        jBtnGuardarCuenta = new javax.swing.JButton();
        jBtnVolverAlInicio = new javax.swing.JButton();
        jComboTipoCuenta = new javax.swing.JComboBox();
        jLabelNumeroDeCodigo1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jXSeparadorDeTituloNuevaCuenta.setFont(new java.awt.Font("Tahoma", 0, 12));
        jXSeparadorDeTituloNuevaCuenta.setHorizontalAlignment(0);
        jXSeparadorDeTituloNuevaCuenta.setTitle("Nueva cuenta");

        jLabelNumeroDeCodigo.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelNumeroDeCodigo.setText("Nombre de nueva cuenta:");

        jTxtNombreDeCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtNombreDeCuentaActionPerformed(evt);
            }
        });
        jTxtNombreDeCuenta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtNombreDeCuentaKeyPressed(evt);
            }
        });

        jBtnGuardarCuenta.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnGuardarCuenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Guardar 30x30.png"))); // NOI18N
        jBtnGuardarCuenta.setText("Guardar");
        jBtnGuardarCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnGuardarCuentaActionPerformed(evt);
            }
        });

        jBtnVolverAlInicio.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnVolverAlInicio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolverAlInicio.setText("Volver");
        jBtnVolverAlInicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverAlInicioActionPerformed(evt);
            }
        });

        jComboTipoCuenta.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ingreso", "Egreso", "Deuda" }));

        jLabelNumeroDeCodigo1.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelNumeroDeCodigo1.setText("Tipo de cuenta:");

        javax.swing.GroupLayout jPnlCuerpoLayout = new javax.swing.GroupLayout(jPnlCuerpo);
        jPnlCuerpo.setLayout(jPnlCuerpoLayout);
        jPnlCuerpoLayout.setHorizontalGroup(
            jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlCuerpoLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelNumeroDeCodigo)
                            .addGroup(jPnlCuerpoLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jComboTipoCuenta, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jTxtNombreDeCuenta, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabelNumeroDeCodigo1)))
                    .addComponent(jXSeparadorDeTituloNuevaCuenta, javax.swing.GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPnlCuerpoLayout.createSequentialGroup()
                        .addComponent(jBtnGuardarCuenta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBtnVolverAlInicio)))
                .addContainerGap())
        );
        jPnlCuerpoLayout.setVerticalGroup(
            jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXSeparadorDeTituloNuevaCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelNumeroDeCodigo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtNombreDeCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelNumeroDeCodigo1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jComboTipoCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addGroup(jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBtnVolverAlInicio)
                    .addComponent(jBtnGuardarCuenta))
                .addGap(50, 50, 50))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPnlCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPnlCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTxtNombreDeCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtNombreDeCuentaActionPerformed
}//GEN-LAST:event_jTxtNombreDeCuentaActionPerformed

    private void jTxtNombreDeCuentaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtNombreDeCuentaKeyPressed
        Vistas.Util.Util.ignorarSiNoEsLetra(evt);
}//GEN-LAST:event_jTxtNombreDeCuentaKeyPressed

    private void jBtnGuardarCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnGuardarCuentaActionPerformed
        JTextField[] jTextFields = {this.jTxtNombreDeCuenta};
        if (Util.isEmpty(jTextFields)) {
            JOptionPane.showMessageDialog(this, "Ingrese nombre de la cuenta.", "Aviso", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        String nombreCuenta = this.jTxtNombreDeCuenta.getText();
        CuentaFilial cuenta = null;
        if (jComboTipoCuenta.getSelectedItem().toString().equals("Ingreso")) {
            cuenta = new CuentaFilialIngreso(nombreCuenta);
        }
        if (jComboTipoCuenta.getSelectedItem().toString().equals("Deuda")) {
            cuenta = new CuentaFilialDeuda(nombreCuenta);
        }
        if (jComboTipoCuenta.getSelectedItem().toString().equals("Egreso")) {
            cuenta = new CuentaFilialEgreso(nombreCuenta);
        }
        try {
            this.institucionCentralTesoreria.addCuentaFilial(cuenta);
            padre.addNuevaCuentaAJComboYAColeccion(cuenta);
            this.dispose();
        } catch (DAOYaExisteRegistroException e) {
            JOptionPane.showConfirmDialog(this, "Ya existe una cuenta con este nombre", "Aviso", JOptionPane.ERROR_MESSAGE);
            return;
        } catch (DAOProblemaConTransaccionException ex) {
            JOptionPane.showConfirmDialog(this, ex.getMessage(), "Aviso", JOptionPane.ERROR_MESSAGE);
            return;
        }

    }//GEN-LAST:event_jBtnGuardarCuentaActionPerformed

    private void jBtnVolverAlInicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVolverAlInicioActionPerformed
    }//GEN-LAST:event_jBtnVolverAlInicioActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogAltaCuentaFilial dialog = new JDialogAltaCuentaFilial(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnGuardarCuenta;
    private javax.swing.JButton jBtnVolverAlInicio;
    private javax.swing.JComboBox jComboTipoCuenta;
    private javax.swing.JLabel jLabelNumeroDeCodigo;
    private javax.swing.JLabel jLabelNumeroDeCodigo1;
    private javax.swing.JPanel jPnlCuerpo;
    private javax.swing.JTextField jTxtNombreDeCuenta;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloNuevaCuenta;
    // End of variables declaration//GEN-END:variables
}
