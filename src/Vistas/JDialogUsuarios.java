/*
 * JDialogUsuarios.java
 *
 * Created on 24/05/2011, 11:14:27
 */
package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Excepciones.FilaNoSelecionadaException;
import Modelo.InstitucionCentralTesoreria;
import Modelo.Usuario;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

/**
 *
 * @author emanuel
 */
public class JDialogUsuarios extends javax.swing.JDialog {

    private InstitucionCentralTesoreria institucionCentralTesoreria;
    private ArrayList<Usuario> usuarios;

    public JDialogUsuarios(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
    }

    JDialogUsuarios(JFrameSistema padre, boolean modal, InstitucionCentralTesoreria institucionCentralTesoreria) {
        this(padre, modal);
        this.institucionCentralTesoreria = institucionCentralTesoreria;
        usuarios = this.institucionCentralTesoreria.getUsuarios();
        loadUsuariosInView(usuarios);
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jXSeparadorDeTituloUsuario = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelNombreUsuario = new javax.swing.JLabel();
        jLabelPasswordUsuario = new javax.swing.JLabel();
        jLabelUsuariosRegistrados = new javax.swing.JLabel();
        jComboUsuarios = new javax.swing.JComboBox();
        jLabelNombreUsuarioDato = new javax.swing.JLabel();
        jLabelPasswordUsuarioDato = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jBtnBorrarUsuario = new javax.swing.JButton();
        jBtnModificarUsuarios = new javax.swing.JButton();
        jBtnAddUsuario = new javax.swing.JButton();
        jBtnVolverInicio_ABMFilial = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jXSeparadorDeTituloUsuario.setFont(new java.awt.Font("Tahoma", 0, 12));
        jXSeparadorDeTituloUsuario.setHorizontalAlignment(0);
        jXSeparadorDeTituloUsuario.setTitle("Usuarios");

        jLabelNombreUsuario.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelNombreUsuario.setText("Nombre de usuario:");

        jLabelPasswordUsuario.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelPasswordUsuario.setText("Contraseña de usuario:");

        jLabelUsuariosRegistrados.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelUsuariosRegistrados.setText("Usuarios registrados:");

        jComboUsuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboUsuariosActionPerformed(evt);
            }
        });
        jComboUsuarios.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jComboUsuariosPropertyChange(evt);
            }
        });

        jLabelNombreUsuarioDato.setFont(new java.awt.Font("Tahoma", 0, 12));

        jLabelPasswordUsuarioDato.setFont(new java.awt.Font("Tahoma", 0, 12));

        jBtnBorrarUsuario.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnBorrarUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Eliminar usuario.png"))); // NOI18N
        jBtnBorrarUsuario.setText("Eliminar");
        jBtnBorrarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnBorrarUsuarioActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnBorrarUsuario);

        jBtnModificarUsuarios.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnModificarUsuarios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Modificar usuario.png"))); // NOI18N
        jBtnModificarUsuarios.setText("Modificar");
        jBtnModificarUsuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnModificarUsuariosActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnModificarUsuarios);

        jBtnAddUsuario.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnAddUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Modificar usuario.png"))); // NOI18N
        jBtnAddUsuario.setText("Agregar");
        jBtnAddUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnAddUsuarioActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnAddUsuario);

        jBtnVolverInicio_ABMFilial.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnVolverInicio_ABMFilial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolverInicio_ABMFilial.setText("Volver");
        jBtnVolverInicio_ABMFilial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverInicio_ABMFilialActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnVolverInicio_ABMFilial);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXSeparadorDeTituloUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, 421, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jComboUsuarios, 0, 190, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jLabelNombreUsuarioDato, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jLabelPasswordUsuarioDato, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelNombreUsuario)
                            .addComponent(jLabelPasswordUsuario)
                            .addComponent(jLabelUsuariosRegistrados))))
                .addGap(216, 216, 216))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXSeparadorDeTituloUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabelUsuariosRegistrados)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboUsuarios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19)
                .addComponent(jLabelNombreUsuario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelNombreUsuarioDato, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelPasswordUsuario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelPasswordUsuarioDato, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnModificarUsuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnModificarUsuariosActionPerformed
        try {
            Usuario usuarioSeleccionado = this.getUsuarioSeleccionado();
            JDialogAltaUsuario jDialogAltaUsuario = new JDialogAltaUsuario(this, true,
                    institucionCentralTesoreria, usuarioSeleccionado);
            jDialogAltaUsuario.setVisible(true);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Ningún usuario seleccionado.");
        }
}//GEN-LAST:event_jBtnModificarUsuariosActionPerformed

    private void jBtnVolverInicio_ABMFilialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVolverInicio_ABMFilialActionPerformed
        this.dispose();
    }//GEN-LAST:event_jBtnVolverInicio_ABMFilialActionPerformed

    private void jBtnBorrarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnBorrarUsuarioActionPerformed
        try {
            Usuario usuarioSeleccionado = this.getUsuarioSeleccionado();
            if (usuarioSeleccionado.getNombre().equals(institucionCentralTesoreria.getUsuarioConectado().getNombre())) {
                JOptionPane.showConfirmDialog(this, "No puede borrar su propio usuario.", 
                        "Aviso", JOptionPane.OK_OPTION, JOptionPane.INFORMATION_MESSAGE);
            }
            if (JOptionPane.showConfirmDialog(this, "¿Está seguro que quiere borrar PERMANENTEMENTE a este usuario del sistema? Para volver a "
                    + "habilitarlo deberá crear de nuevo su usuario y contraseña.", "Aviso", JOptionPane.YES_OPTION, JOptionPane.INFORMATION_MESSAGE)
                    == JOptionPane.YES_OPTION) {
                this.institucionCentralTesoreria.deleteUsuario(usuarioSeleccionado);
                removeUserInTheView(usuarioSeleccionado);
            }
        } catch (FilaNoSelecionadaException ex) {
            JOptionPane.showMessageDialog(this, "Ningún usuario seleccionado.");
        } catch (DAOProblemaConTransaccionException ex2) {
            JOptionPane.showMessageDialog(this, ex2.getMessage(), "Aviso", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_jBtnBorrarUsuarioActionPerformed

    private void jBtnAddUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnAddUsuarioActionPerformed
        JDialogAltaUsuario altaUsuario = new JDialogAltaUsuario(this, rootPaneCheckingEnabled, institucionCentralTesoreria, null);
        altaUsuario.setVisible(true);
    }//GEN-LAST:event_jBtnAddUsuarioActionPerformed

    private void jComboUsuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboUsuariosActionPerformed
        String nombreUsuario = this.jComboUsuarios.getSelectedItem().toString();
        String cadenaConAsteriscos = "";
        Iterator<Usuario> it = usuarios.iterator();
        while (it.hasNext()) {
            Usuario usuario = it.next();
            if (nombreUsuario.contains(usuario.getNombre())) {
                this.jLabelNombreUsuarioDato.setText(usuario.getNombre());
                for (int i = 0; i < usuario.getContrasenia().length(); i++) {
                    cadenaConAsteriscos = cadenaConAsteriscos + "*";
                }
                this.jLabelPasswordUsuarioDato.setText(cadenaConAsteriscos);
                if (usuario.sosTesorero()) {
                    jBtnBorrarUsuario.setEnabled(false);
                } else {
                    jBtnBorrarUsuario.setEnabled(true);
                }
            }


        }
    }//GEN-LAST:event_jComboUsuariosActionPerformed

    private void jComboUsuariosPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jComboUsuariosPropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboUsuariosPropertyChange

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogUsuarios dialog = new JDialogUsuarios(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnAddUsuario;
    private javax.swing.JButton jBtnBorrarUsuario;
    private javax.swing.JButton jBtnModificarUsuarios;
    private javax.swing.JButton jBtnVolverInicio_ABMFilial;
    private javax.swing.JComboBox jComboUsuarios;
    private javax.swing.JLabel jLabelNombreUsuario;
    private javax.swing.JLabel jLabelNombreUsuarioDato;
    private javax.swing.JLabel jLabelPasswordUsuario;
    private javax.swing.JLabel jLabelPasswordUsuarioDato;
    private javax.swing.JLabel jLabelUsuariosRegistrados;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel8;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloUsuario;
    // End of variables declaration//GEN-END:variables

    private void loadUsuariosInView(ArrayList<Usuario> usuarios) {
        if (usuarios == null) {
            return;
        }
        if (usuarios.isEmpty()) {
            return;
        }
        Iterator<Usuario> it = usuarios.iterator();
        while (it.hasNext()) {
            Usuario usuario = it.next();
            userLoadInTheView(usuario, false);
        }

    }

    private void userLoadInTheView(Usuario usuario, boolean usuarioModificado) {
        String tipoUsuario = usuario.sosTesorero() ? "Tesorera/o" : "Secretaria/o";
        if (usuarioModificado) {
            jComboUsuarios.removeItemAt(jComboUsuarios.getSelectedIndex());
        }
        jComboUsuarios.addItem(usuario.getNombre().concat(" - ").concat(tipoUsuario));
    }

    /**
     * 
     * @return
     * @throws Exception Si ningún usuario fue seleccionado en la vista
     */
    private Usuario getUsuarioSeleccionado() throws FilaNoSelecionadaException {
        Usuario usuarioARetornar = null;
        if (this.jComboUsuarios.getSelectedIndex() == -1) {
            throw new Excepciones.FilaNoSelecionadaException();
        } else {
            String nombreDeUsuarioSeleccionado = this.jComboUsuarios.getSelectedItem().toString();
            nombreDeUsuarioSeleccionado = nombreDeUsuarioSeleccionado.substring(0, nombreDeUsuarioSeleccionado.indexOf(" "));
            Iterator<Usuario> it = usuarios.iterator();
            while (it.hasNext()) {
                Usuario usuario = it.next();
                if (usuario.getNombre().equals(nombreDeUsuarioSeleccionado)) {
                    usuarioARetornar = usuario;
                }
            }
        }
        return usuarioARetornar;
    }

    void agregarOModificarUsuarioEnVista(Usuario usuarioModificadoOAgregado) {
        if (usuarios.contains(usuarioModificadoOAgregado)) {
            usuarios.remove(usuarios.indexOf(usuarioModificadoOAgregado));
            usuarios.add(usuarioModificadoOAgregado);
            userLoadInTheView(usuarioModificadoOAgregado, true);
        } else {
            usuarios.add(usuarioModificadoOAgregado);
            userLoadInTheView(usuarioModificadoOAgregado, false);
        }
    }

    void modificarUsuarioEnVistaYEnColeccion(Usuario usuario) {
        usuarios.remove(usuarios.indexOf(usuario));
        usuarios.add(usuario);
        jComboUsuarios.removeItemAt(jComboUsuarios.getSelectedIndex());
        userLoadInTheView(usuario, false);
    }

    private void removeUserInTheView(Usuario usuarioSeleccionado) {
        DefaultComboBoxModel boxModel = (DefaultComboBoxModel) this.jComboUsuarios.getModel();
        boxModel.removeElementAt(jComboUsuarios.getSelectedIndex());
        try {
            Usuario usuario = getUsuarioSeleccionado();
            this.jLabelNombreUsuarioDato.setText(usuario.getNombre());
            String cadenaConAsteriscos = "";
            for (int i = 0; i < usuario.getContrasenia().length(); i++) {
                cadenaConAsteriscos = cadenaConAsteriscos + "*";
            }
            this.jLabelPasswordUsuarioDato.setText(cadenaConAsteriscos);

        } catch (FilaNoSelecionadaException ex) {
            System.out.println("no funciona");
        }
    }
}
