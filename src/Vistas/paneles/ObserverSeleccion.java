package Vistas.paneles;

/**
 * @author Emanuel
 */
public interface ObserverSeleccion<T> {
    //cambiar para que contenga un solo metodo que sea para  informar al observer que la seleccion del observable a cambiado
    public void seleccionCambiada();

}
