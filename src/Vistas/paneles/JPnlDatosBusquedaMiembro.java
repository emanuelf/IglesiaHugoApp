package Vistas.paneles;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.InstitucionCentral;
import Modelo.Miembro;
import java.awt.event.ActionEvent;
import java.beans.Beans;
import java.util.ArrayList;
import java.util.List;
import java.util.Observer;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import org.postgresql.util.PSQLException;
import Modelo.InstitucionCentralTesoreria;

public class JPnlDatosBusquedaMiembro extends javax.swing.JPanel implements ObservableBusqueda<List<Miembro>> {

    private AbstractAction buscar;
    private ObserverBusqueda observer;
    private InstitucionCentral institucion;

    public JPnlDatosBusquedaMiembro() {
        initComponents();
        if (!Beans.isDesignTime()) {
            addActions();
            try {
                institucion = InstitucionCentralTesoreria.getInstance();
            } catch (DAOProblemaConTransaccionException ex) {
                Logger.getLogger(JPnlDatosBusquedaMiembro.class.getName()).log(Level.SEVERE, null, ex);
            } catch (PSQLException ex) {
                Logger.getLogger(JPnlDatosBusquedaMiembro.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void setObserver(ObserverBusqueda observer) {
        this.observer = observer;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLblApellido = new javax.swing.JLabel();
        jTxtApellido = new org.jdesktop.swingx.JXTextField();
        jLblNombre = new javax.swing.JLabel();
        jTxtNombre = new org.jdesktop.swingx.JXTextField();

        jLblApellido.setText("Apellido:");

        jLblNombre.setText("Nombre:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLblApellido)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTxtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLblNombre)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTxtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(16, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLblNombre)
                        .addComponent(jTxtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLblApellido)
                        .addComponent(jTxtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLblApellido;
    private javax.swing.JLabel jLblNombre;
    private org.jdesktop.swingx.JXTextField jTxtApellido;
    private org.jdesktop.swingx.JXTextField jTxtNombre;
    // End of variables declaration//GEN-END:variables

    private void addActions() {
        buscar = new AbstractAction("") {

            public void actionPerformed(ActionEvent e) {
                observer.notifyFinBusqueda();
            }
        };
        jTxtApellido.setAction(buscar);
        jTxtNombre.setAction(buscar);
    }

    public List<Miembro> resultado() {
        try {
            Set<Miembro> buscarMiembros = institucion.buscarMiembros(jTxtNombre.getText(), jTxtApellido.getText());
            return new ArrayList(buscarMiembros);
        } catch (DAOProblemaConTransaccionException ex) {
            Logger.getLogger(JPnlDatosBusquedaMiembro.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Hubo problemas con la consulta a la base de datos.");
            return new ArrayList<Miembro>();
        }

    }
}
