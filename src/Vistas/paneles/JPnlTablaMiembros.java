package Vistas.paneles;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.Beans;
import Modelo.Miembro;
import Vistas.Util.CustomTableModel;
import Vistas.wrappers.WrapperMiembro;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import Vistas.presentadores.MiembroPresenter.PRESENTACION;

public class JPnlTablaMiembros extends javax.swing.JPanel implements Seleccionable<Miembro, JPnlTablaMiembros>, ObserverBusqueda {

    private List<ObserverSeleccion<Miembro>> observerSeleccion = new ArrayList<ObserverSeleccion<Miembro>>();
    private ObservableBusqueda<List<Miembro>> observableBusqueda;
    private MouseListener listenerDoubleClick;

    public JPnlTablaMiembros() {
        initComponents();
        if (!Beans.isDesignTime()) {
            addListenersBasicos();
        }
    }

    private void removeListenersSeleccionMiembro() {
        jTblMiembros.removeMouseListener(listenerDoubleClick);
    }

    public void setObservableBusqueda(ObservableBusqueda<List<Miembro>> observableBusqueda) {
        this.observableBusqueda = observableBusqueda;
    }

    public void addObserverSeleccion(ObserverSeleccion<Miembro> observerSeleccion) {
        this.observerSeleccion.add(observerSeleccion);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTblMiembros = new org.jdesktop.swingx.JXTable();

        jTblMiembros.setModel(new CustomTableModel("Nombre/s y apellido/s,Direccion,Miembro desde".split(",")));
        jScrollPane1.setViewportView(jTblMiembros);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 745, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 214, Short.MAX_VALUE)
                .addGap(14, 14, 14))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private org.jdesktop.swingx.JXTable jTblMiembros;
    // End of variables declaration//GEN-END:variables

    public void seleccionCambiada() {
        for (ObserverSeleccion<Miembro> observer : observerSeleccion) {
            observer.seleccionCambiada();
        }
    }

    public CustomTableModel<WrapperMiembro> getTableModel() {
        return (CustomTableModel<WrapperMiembro>) jTblMiembros.getModel();
    }

    public void notifyFinBusqueda() {
        cargarTabla(toWrappers(observableBusqueda.resultado()));
    }

    public void cargarTabla(List<WrapperMiembro> miembros) {
        getTableModel().setDatos(miembros);
    }

    private List<WrapperMiembro> toWrappers(List<Miembro> resultado) {
        List<WrapperMiembro> lista = new ArrayList<WrapperMiembro>();
        for (Miembro miembro : resultado)
            lista.add(new WrapperMiembro(miembro));
        return lista;
    }

    private void addListenersBasicos() {
        jTblMiembros.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            public void valueChanged(ListSelectionEvent e) {
                seleccionCambiada();
            }
        });
    }

    private void addListenersSeleccionMiembro(){
        listenerDoubleClick = new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (!jTblMiembros.getSelectionModel().isSelectionEmpty()) {
                    if (e.getClickCount() >= 2)
                        SwingUtilities.getWindowAncestor(JPnlTablaMiembros.this).setVisible(false);
                }
            }
        };
        jTblMiembros.addMouseListener(listenerDoubleClick);
    }

    public Miembro getSeleccion() {
        WrapperMiembro miembro = getTableModel().getDatos().get(jTblMiembros.convertRowIndexToModel(jTblMiembros.getSelectedRow()));
        return miembro.getObjetoWrapeado();
    }

    public JPnlTablaMiembros getPanel() {
        return this;
    }

    public void addMiembro(Miembro resultado) {
        getTableModel().addRow(new WrapperMiembro(resultado));
    }

    public void mostrarModificacion(Miembro modificado) {
        jTblMiembros.repaint();
    }

    public void quitarSeleccion() {
        getTableModel().removeRow(jTblMiembros.convertRowIndexToModel(jTblMiembros.getSelectedRow()));
    }

    public void setModo(PRESENTACION modo){
        if(modo.equals(PRESENTACION.SELECCION_MIEMBRO)){
            addListenersSeleccionMiembro();
        }else{
            removeListenersSeleccionMiembro();
        }
    }
}
