package Vistas.paneles;

import java.awt.Color;
import java.beans.Beans;
import java.text.DateFormat;

import javax.swing.text.JTextComponent;

import Modelo.Miembro;
import Vistas.Util.paneles.JPnlContainerDetalle;

public class JPnlDatosMiembro extends JPnlContainerDetalle<Miembro> implements ObserverSeleccion<Miembro> {

    private Seleccionable<Miembro, JPnlTablaMiembros> seleccionable;

    public JPnlDatosMiembro() {
        initComponents();
        if(!Beans.isDesignTime()){
            setEnabledComponents(false);
        }
    }

    public void setSeleccionable(Seleccionable<Miembro, JPnlTablaMiembros> seleccionable) {
        this.seleccionable = seleccionable;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLblNombreYApellido = new javax.swing.JLabel();
        txtNombreYApellido = new org.jdesktop.swingx.JXTextField();
        lblDireccion = new javax.swing.JLabel();
        txtDireccion = new org.jdesktop.swingx.JXTextField();
        jLblFechaBautismo = new javax.swing.JLabel();
        txtFechaBautismo = new org.jdesktop.swingx.JXTextField();
        lblActivo = new javax.swing.JLabel();
        lblTelefono = new javax.swing.JLabel();
        txtTelefono = new org.jdesktop.swingx.JXTextField();
        lblCelular = new javax.swing.JLabel();
        txtCelular = new org.jdesktop.swingx.JXTextField();
        lblActivoDato = new javax.swing.JLabel();
        jLblFechaBautismo1 = new javax.swing.JLabel();
        txtUltimoDiezmo = new org.jdesktop.swingx.JXTextField();

        jLblNombreYApellido.setText("Nombre/s y apellido/s:");

        lblDireccion.setText("Direccion:");

        jLblFechaBautismo.setText("Fecha de bautismo:");

        lblActivo.setText("Activo:");

        lblTelefono.setText("Telefono:");

        lblCelular.setText("Celular:");

        lblActivoDato.setText(" ");

        jLblFechaBautismo1.setText("Ultimo diezmo:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLblNombreYApellido)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtNombreYApellido, javax.swing.GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblDireccion)
                            .addComponent(lblTelefono))
                        .addGap(70, 70, 70)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtTelefono, javax.swing.GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE)
                                .addGap(18, 18, 18)
                                .addComponent(lblCelular)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtCelular, javax.swing.GroupLayout.DEFAULT_SIZE, 231, Short.MAX_VALUE))
                            .addComponent(txtDireccion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblActivo)
                        .addGap(18, 18, 18)
                        .addComponent(lblActivoDato))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLblFechaBautismo)
                            .addComponent(jLblFechaBautismo1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtUltimoDiezmo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtFechaBautismo, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLblNombreYApellido)
                    .addComponent(txtNombreYApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLblFechaBautismo)
                    .addComponent(txtFechaBautismo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDireccion)
                    .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblActivo)
                    .addComponent(lblActivoDato))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLblFechaBautismo1)
                        .addComponent(txtUltimoDiezmo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblCelular)
                        .addComponent(txtCelular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblTelefono)
                        .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLblFechaBautismo;
    private javax.swing.JLabel jLblFechaBautismo1;
    private javax.swing.JLabel jLblNombreYApellido;
    private javax.swing.JLabel lblActivo;
    private javax.swing.JLabel lblActivoDato;
    private javax.swing.JLabel lblCelular;
    private javax.swing.JLabel lblDireccion;
    private javax.swing.JLabel lblTelefono;
    private org.jdesktop.swingx.JXTextField txtCelular;
    private org.jdesktop.swingx.JXTextField txtDireccion;
    private org.jdesktop.swingx.JXTextField txtFechaBautismo;
    private org.jdesktop.swingx.JXTextField txtNombreYApellido;
    private org.jdesktop.swingx.JXTextField txtTelefono;
    private org.jdesktop.swingx.JXTextField txtUltimoDiezmo;
    // End of variables declaration//GEN-END:variables

    @Override
    public void limpiar() {
        Vistas.Util.Util.limpiar(new JTextComponent[]{txtCelular, txtDireccion, txtFechaBautismo, txtNombreYApellido, txtTelefono});
    }

    private void setActivo(boolean activo) {
        if (activo) {
            lblActivoDato.setForeground(Color.GREEN);
            lblActivoDato.setText("ACTIVO");
        } else {
            lblActivoDato.setForeground(Color.RED);
            lblActivoDato.setText("NO ACTIVO");
        }
    }

    public void seleccionCambiada() {
        final Miembro seleccion = seleccionable.getSeleccion();
        if (seleccion != null) {
            txtCelular.setText(Long.toString(seleccion.getTelefonoCelular()));
            txtTelefono.setText(Long.toString(seleccion.getTelefonoFijo()));
            if (seleccion.getDireccion() != null)
                txtDireccion.setText(seleccion.getDireccion().toString());
            txtNombreYApellido.setText(seleccion.nombresYApellidos());
            DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
            if (seleccion.getFechaBautismo() != null)
                txtFechaBautismo.setText(df.format(seleccion.fechaBautismoEnDate()));
            txtUltimoDiezmo.setText(df.format(seleccion.ultimoDiezmoPago().getTime()));
            setActivo(seleccion.isMiembroActivo());
        }
    }

    @Override
    public final void setEnabledComponents(boolean enabled) {
        txtCelular.setEnabled(enabled);
        txtDireccion.setEnabled(enabled);
        txtFechaBautismo.setEnabled(enabled);
        txtNombreYApellido.setEnabled(enabled);
        txtTelefono.setEnabled(enabled);
        txtUltimoDiezmo.setEnabled(enabled);
    }
}