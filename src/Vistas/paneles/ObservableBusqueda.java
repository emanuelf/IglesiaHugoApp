package Vistas.paneles;

/**
 * @author Emanuel
 */
public interface ObservableBusqueda<T> {
    
    public T resultado();
}
