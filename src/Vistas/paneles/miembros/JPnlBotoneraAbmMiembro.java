package Vistas.paneles.miembros;

import java.awt.event.ActionEvent;
import java.awt.print.PrinterException;
import java.beans.Beans;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;

import org.postgresql.util.PSQLException;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.InstitucionCentralTesoreria;
import Modelo.Miembro;
import Util.MessageUtils;
import Util.ReportUtils;
import Vistas.paneles.JPnlTablaMiembros;
import Vistas.paneles.ObserverSeleccion;
import Vistas.paneles.Seleccionable;
import Vistas.presentadores.MiembroPresenter;
import Vistas.presentadores.MiembroPresenter.PRESENTACION;
import reportes.PrinterUtils;

/**
 *
 * @author Emanuel
 */
public class JPnlBotoneraAbmMiembro extends javax.swing.JPanel implements ObserverSeleccion<Miembro> {

    public JPnlBotoneraAbmMiembro() {
        initComponents();
        if (!Beans.isDesignTime()) {
            addActions();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnListadoDeMiembros = new javax.swing.JButton();
        btnEliminarMiembro = new javax.swing.JButton();
        btnEditarMiembro = new javax.swing.JButton();
        btnAgregar = new javax.swing.JButton();

        btnListadoDeMiembros.setText("Imprimir listado de miembros");

        btnEliminarMiembro.setText("Eliminar");

        btnEditarMiembro.setText("Editar");

        btnAgregar.setText("Agregar");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnListadoDeMiembros)
                .addGap(30, 30, 30)
                .addComponent(btnEliminarMiembro)
                .addGap(5, 5, 5)
                .addComponent(btnEditarMiembro)
                .addGap(5, 5, 5)
                .addComponent(btnAgregar)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnListadoDeMiembros)
                    .addComponent(btnEliminarMiembro)
                    .addComponent(btnEditarMiembro)
                    .addComponent(btnAgregar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnEditarMiembro;
    private javax.swing.JButton btnEliminarMiembro;
    private javax.swing.JButton btnListadoDeMiembros;
    // End of variables declaration//GEN-END:variables
    private Seleccionable<Miembro, JPnlTablaMiembros> seleccionable;
    private AbstractAction editar;
    private AbstractAction eliminar;
    private AbstractAction agregar;
    private AbstractAction imprimirListado;

    public void seleccionCambiada() {
        if (seleccionable.getSeleccion() == null) {
            btnEditarMiembro.setEnabled(false);
            btnEliminarMiembro.setEnabled(false);
        } else {
            btnEditarMiembro.setEnabled(true);
            btnEliminarMiembro.setEnabled(true);
        }
    }

    private void addActions() {
        agregar = new AbstractAction("Agregar") {
            public void actionPerformed(ActionEvent e) {
                Miembro agregado = MiembroPresenter.getInstance().presentarAlta();
                if (agregado != null) {
                    seleccionable.getPanel().addMiembro(agregado);
                }
            }
        };
        btnAgregar.setAction(agregar);
        eliminar = new AbstractAction("Eliminar") {
            public void actionPerformed(ActionEvent e) {
                if (areYouSure()) {
                    try {
                        InstitucionCentralTesoreria.getInstance().desvincularMiembro(seleccionable.getSeleccion());
                        seleccionable.quitarSeleccion();
                    } catch (DAOProblemaConTransaccionException ex) {
                        Logger.getLogger(JPnlBotoneraAbmMiembro.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (PSQLException ex) {
                        Logger.getLogger(JPnlBotoneraAbmMiembro.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        btnEliminarMiembro.setAction(eliminar);
        editar = new AbstractAction("Editar") {
            public void actionPerformed(ActionEvent e) {
                if (seleccionable.getSeleccion() == null) {
                    JOptionPane.showMessageDialog(null, "No ha seleccionado ningun miembro");
                } else {
                    Miembro modificado = MiembroPresenter.getInstance().presentarModificacion(seleccionable.getSeleccion());
                    seleccionable.getPanel().mostrarModificacion(modificado);
                }
            }
        };
        btnEditarMiembro.setAction(editar);
        imprimirListado = new AbstractAction("Imprimir listado (completo) de miembros") {
            public void actionPerformed(ActionEvent e) {
                try {
                    PrinterUtils.pruebaImpresion();
                    HashMap<String, Object> parametros = new HashMap<String, Object>();
                    parametros.put("solo_activos", true);
                    parametros.put("SUBREPORT_DIR", ReportUtils.getPathOfReports());
                    ReportUtils.printReport("listado_de_miembros", parametros);
                } catch (PrinterException ex) {
                    MessageUtils.showWarning(null, ex.getMessage());
                }
            }
        };
        btnListadoDeMiembros.setAction(imprimirListado);
    }

    public void setModo(PRESENTACION modo) {
        switch (modo) {
            case ABM:
                btnAgregar.setVisible(true);
                btnEditarMiembro.setVisible(true);
                btnEliminarMiembro.setVisible(true);
                btnListadoDeMiembros.setVisible(true);
                break;
            case SELECCION_MIEMBRO:
                btnAgregar.setVisible(false);
                btnEditarMiembro.setVisible(false);
                btnEliminarMiembro.setVisible(false);
                btnListadoDeMiembros.setVisible(false);
                break;
        }
    }

    private boolean areYouSure() {
        return JOptionPane.showConfirmDialog(null, "¿Está seguro?", "Aviso", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
    }

    public void setSeleccionable(Seleccionable<Miembro, JPnlTablaMiembros> seleccionable) {
        this.seleccionable = seleccionable;
    }
}
