package Vistas.paneles;

public interface Seleccionable<T, P> {

    public void seleccionCambiada();
    public T getSeleccion();
    public P getPanel();
    public void quitarSeleccion();
}
