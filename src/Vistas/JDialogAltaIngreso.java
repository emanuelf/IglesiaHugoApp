package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Excepciones.NoHayCuentasException;
import Modelo.*;
import Util.UtilFechas;
import Vistas.Util.Util;
import java.awt.Color;
import java.text.DateFormat;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import Vistas.presentadores.MiembroPresenter;

/**
 *
 * @author emanuel
 */
public class JDialogAltaIngreso extends javax.swing.JDialog {

    private ArrayList<Ingreso> ingresosCargados = new ArrayList<Ingreso>();
    private InstitucionCentralTesoreria institucion;
    private ArrayList<CuentaIngreso> cuentas = new ArrayList<CuentaIngreso>();
    private Miembro miembro; //solo usado para las devoluciones de la vista de jdialogABMmiembros
    private ArrayList<Miembro> miembrosAActualizar = new ArrayList<Miembro>();
    private IngresoEnCulto ingresoEnCulto = null;
    private Donante donante;
    private ArrayList<Entry<Miembro, Ingreso>> miembrosConDiezmo = new ArrayList<Entry<Miembro, Ingreso>>();
    private boolean llamadoParaRegistarIngresosDeCulto = false;
    private Miembro miembroAnonimo;

    public JDialogAltaIngreso(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    public JDialogAltaIngreso(java.awt.Frame parent, boolean modal, InstitucionCentralTesoreria institucion) {
        super(parent, modal);
        initComponents();
        deshabilitarPanelMiembro();
        this.institucion = institucion;
        ingresoEnCulto = null;
        cuentas = institucion.getCuentasIngreso();
        this.miembro = null;
        this.donante = null;
        agregarPopupMenuAJXDateFecha();
        try {
            cargarJComboTipoCuenta();
        } catch (NoHayCuentasException ex) {
            JOptionPane.showMessageDialog(null, "No hay cuentas de ingreso cargadas. \n Por favor, cárguelas y luego vuelva.");
            this.setEnabled(false);
        }
        modificarVistaParaRegistrarIngresosQueNoSonDeCulto();
        jLblSaldoCajaDato.setText(Vistas.Util.Util.formatearTipoMoneda(this.institucion.consultarCajaDisponible()));
        jLabelAvisoDeGuardar.setVisible(false);
        this.setLocationRelativeTo(null);
    }

    public JDialogAltaIngreso(java.awt.Frame parent, boolean modal, InstitucionCentralTesoreria institucion, IngresoEnCulto ingresoEnCulto) {
        super(parent, modal);
        initComponents();
        deshabilitarPanelMiembro();
        this.institucion = institucion;
        this.ingresoEnCulto = ingresoEnCulto;
        cuentas = this.institucion.getCuentasIngreso();
        this.miembro = null;
        this.donante = null;
        this.ingresoEnCulto = ingresoEnCulto;
        this.datePicker.setDate(ingresoEnCulto.getFechaDeCulto());
        this.datePicker.setEnabled(false);
        this.llamadoParaRegistarIngresosDeCulto = true;
        agregarPopupMenuAJXDateFecha();
        try {
            cargarJComboTipoCuenta();
        } catch (NoHayCuentasException ex) {
            JOptionPane.showMessageDialog(this, "No hay cuentas de ingreso cargadas. \n Por favor, cárguelas y luego vuelva.");
            this.setEnabled(false);
        }
        modificarVistaParaRegistrarIngresosQueSonDeCulto();
        jLblSaldoCajaDato.setText(Vistas.Util.Util.formatearTipoMoneda(this.institucion.consultarCajaDisponible()));
        jLabelAvisoDeGuardar.setVisible(false);
        this.setLocationRelativeTo(null);
    }

    private void cargarJComboTipoCuenta() throws NoHayCuentasException {
        if (!cuentas.isEmpty()) {
            Iterator<CuentaIngreso> it = cuentas.iterator();
            while (it.hasNext()) {
                CuentaIngreso cuentaIngreso = it.next();
                if (llamadoParaRegistarIngresosDeCulto) {
                    if (esDiezmoUOfrenda(cuentaIngreso)) {
                        jComboTipoCuenta.addItem(cuentaIngreso.getNombre());
                    } 
                }else{
                    if (!esDiezmoUOfrenda(cuentaIngreso)) {
                        jComboTipoCuenta.addItem(cuentaIngreso.getNombre());
                    }
                }
            }
        }else{
            throw new Excepciones.NoHayCuentasException();
        }
    }

    private void deshabilitarPanelMiembro() {
        //jXSeparadorDeTituloMiembro.setForeground(Color.GRAY);
        jLbelApellidoYNombre.setEnabled(false);
        jLinkBuscarMiembro.setEnabled(false);
    }

    private void habilitarPanelMiembro() {
        jLbelApellidoYNombre.setEnabled(true);
        jLinkBuscarMiembro.setEnabled(true);
    }

    private void limpiarCampos() {
        jTxtMonto.setText("");
        if (!llamadoParaRegistarIngresosDeCulto) {
            datePicker.setDate(null);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="generatecode">
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        buttonGroupTipoIngreso = new javax.swing.ButtonGroup();
        jPopupMenuJXDatePicker = new javax.swing.JPopupMenu();
        jPanel2 = new javax.swing.JPanel();
        jPanelIngreso = new javax.swing.JPanel();
        jPanelMontoYBuscarMiembro = new javax.swing.JPanel();
        jLabelMonto = new javax.swing.JLabel();
        jTxtMonto = new javax.swing.JTextField();
        jBtnCargarUnIngreso = new javax.swing.JButton();
        jPanelFechaYTipoCuenta = new javax.swing.JPanel();
        jLabelFecha = new javax.swing.JLabel();
        datePicker = new org.jdesktop.swingx.JXDatePicker();
        jLabelTipoCuenta = new javax.swing.JLabel();
        jComboTipoCuenta = new javax.swing.JComboBox();
        jCheckCambiarFecha = new javax.swing.JCheckBox();
        jXSeparadorDeTituloNuevosIngresos = new org.jdesktop.swingx.JXTitledSeparator();
        jPanelMiembro = new javax.swing.JPanel();
        jLbelApellidoYNombre = new javax.swing.JLabel();
        jLblApellidoYNombreDato = new javax.swing.JLabel();
        jLinkBuscarMiembro = new org.jdesktop.swingx.JXHyperlink();
        jCheckAnonimo = new javax.swing.JCheckBox();
        jPanelPieConSaldos = new javax.swing.JPanel();
        jLabelAvisoDeGuardar = new javax.swing.JLabel();
        jLblOficioDeCulto = new javax.swing.JLabel();
        jLblOficioDeCultoDato = new javax.swing.JLabel();
        jLblDiezmoPastoral = new javax.swing.JLabel();
        jLblDiezmoPastoralDato = new javax.swing.JLabel();
        jLblIngresoEnCulto = new javax.swing.JLabel();
        jLblIngresoEnCultoDato = new javax.swing.JLabel();
        jLblSaldoCaja = new javax.swing.JLabel();
        jLblSaldoCajaDato = new javax.swing.JLabel();
        jLblAvisoDeQueNoHayCuentas = new javax.swing.JLabel();
        jPanelButtons = new javax.swing.JPanel();
        jBtnVolverAlInicio = new javax.swing.JButton();
        jBtnLimpiarCampos = new javax.swing.JButton();
        jBtnVerCarga = new javax.swing.JButton();
        jBtnGuardar = new javax.swing.JButton();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Asiento de ingresos");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel2.setPreferredSize(new java.awt.Dimension(660, 483));

        jLabelMonto.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelMonto.setText("Monto:");

        jTxtMonto.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jTxtMonto.setForeground(new java.awt.Color(0, 204, 51));
        jTxtMonto.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtMonto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTxtMontoMouseClicked(evt);
            }
        });
        jTxtMonto.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTxtMontoCaretUpdate(evt);
            }
        });
        jTxtMonto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtMontoActionPerformed(evt);
            }
        });
        jTxtMonto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTxtMontoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTxtMontoFocusLost(evt);
            }
        });
        jTxtMonto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtMontoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxtMontoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtMontoKeyTyped(evt);
            }
        });

        jBtnCargarUnIngreso.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnCargarUnIngreso.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/arrow_down_32.png"))); // NOI18N
        jBtnCargarUnIngreso.setText("Cargar");
        jBtnCargarUnIngreso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnCargarUnIngresoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelMontoYBuscarMiembroLayout = new javax.swing.GroupLayout(jPanelMontoYBuscarMiembro);
        jPanelMontoYBuscarMiembro.setLayout(jPanelMontoYBuscarMiembroLayout);
        jPanelMontoYBuscarMiembroLayout.setHorizontalGroup(
            jPanelMontoYBuscarMiembroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelMontoYBuscarMiembroLayout.createSequentialGroup()
                .addGroup(jPanelMontoYBuscarMiembroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabelMonto, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelMontoYBuscarMiembroLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanelMontoYBuscarMiembroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jBtnCargarUnIngreso, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                            .addComponent(jTxtMonto))))
                .addContainerGap())
        );
        jPanelMontoYBuscarMiembroLayout.setVerticalGroup(
            jPanelMontoYBuscarMiembroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelMontoYBuscarMiembroLayout.createSequentialGroup()
                .addComponent(jLabelMonto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtMonto, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBtnCargarUnIngreso)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabelFecha.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelFecha.setText("Fecha:");

        datePicker.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                datePickerMouseClicked(evt);
            }
        });
        datePicker.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                datePickerActionPerformed(evt);
            }
        });

        jLabelTipoCuenta.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelTipoCuenta.setText("Tipo cuenta:");

        jComboTipoCuenta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jComboTipoCuentaMouseClicked(evt);
            }
        });
        jComboTipoCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboTipoCuentaActionPerformed(evt);
            }
        });

        jCheckCambiarFecha.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jCheckCambiarFecha.setText("Cambiar fecha");
        jCheckCambiarFecha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckCambiarFechaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelFechaYTipoCuentaLayout = new javax.swing.GroupLayout(jPanelFechaYTipoCuenta);
        jPanelFechaYTipoCuenta.setLayout(jPanelFechaYTipoCuentaLayout);
        jPanelFechaYTipoCuentaLayout.setHorizontalGroup(
            jPanelFechaYTipoCuentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelFechaYTipoCuentaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelFechaYTipoCuentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelFechaYTipoCuentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jComboTipoCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanelFechaYTipoCuentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanelFechaYTipoCuentaLayout.createSequentialGroup()
                                .addComponent(jLabelFecha)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelFechaYTipoCuentaLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jCheckCambiarFecha, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanelFechaYTipoCuentaLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(datePicker, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addComponent(jLabelTipoCuenta))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelFechaYTipoCuentaLayout.setVerticalGroup(
            jPanelFechaYTipoCuentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelFechaYTipoCuentaLayout.createSequentialGroup()
                .addComponent(jLabelFecha)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(datePicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jCheckCambiarFecha)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelTipoCuenta)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboTipoCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jXSeparadorDeTituloNuevosIngresos.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jXSeparadorDeTituloNuevosIngresos.setHorizontalAlignment(0);
        jXSeparadorDeTituloNuevosIngresos.setMaximumSize(new java.awt.Dimension(620, 17));
        jXSeparadorDeTituloNuevosIngresos.setMinimumSize(new java.awt.Dimension(620, 17));
        jXSeparadorDeTituloNuevosIngresos.setScrollableTracksViewportHeight(false);
        jXSeparadorDeTituloNuevosIngresos.setScrollableTracksViewportWidth(false);
        jXSeparadorDeTituloNuevosIngresos.setTitle("Nuevos ingresos");

        jPanelMiembro.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, java.awt.Color.lightGray, java.awt.Color.gray), "Datos de miembro", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 14))); // NOI18N

        jLbelApellidoYNombre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLbelApellidoYNombre.setText("Apellido y nombre:");

        jLblApellidoYNombreDato.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jLinkBuscarMiembro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Buscar.png"))); // NOI18N
        jLinkBuscarMiembro.setText("Buscar miembro");
        jLinkBuscarMiembro.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLinkBuscarMiembro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jLinkBuscarMiembroActionPerformed(evt);
            }
        });

        jCheckAnonimo.setText("Anónimo");
        jCheckAnonimo.setHideActionText(true);
        jCheckAnonimo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jCheckAnonimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckAnonimoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelMiembroLayout = new javax.swing.GroupLayout(jPanelMiembro);
        jPanelMiembro.setLayout(jPanelMiembroLayout);
        jPanelMiembroLayout.setHorizontalGroup(
            jPanelMiembroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelMiembroLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelMiembroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLinkBuscarMiembro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jCheckAnonimo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(43, 43, 43)
                .addGroup(jPanelMiembroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelMiembroLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLblApellidoYNombreDato, javax.swing.GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE))
                    .addComponent(jLbelApellidoYNombre))
                .addContainerGap(224, Short.MAX_VALUE))
        );
        jPanelMiembroLayout.setVerticalGroup(
            jPanelMiembroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelMiembroLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanelMiembroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLinkBuscarMiembro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLbelApellidoYNombre))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelMiembroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLblApellidoYNombreDato, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckAnonimo))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        jLabelAvisoDeGuardar.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabelAvisoDeGuardar.setForeground(new java.awt.Color(204, 0, 0));
        jLabelAvisoDeGuardar.setText("* Hay datos por guardar (los cambios impactarán cuando haga click en 'Guardar')");

        jLblOficioDeCulto.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLblOficioDeCulto.setText("Oficio de culto:");

        jLblOficioDeCultoDato.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLblOficioDeCultoDato.setText("-");

        jLblDiezmoPastoral.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLblDiezmoPastoral.setText("Diezmo pastoral:");

        jLblDiezmoPastoralDato.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        jLblIngresoEnCulto.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLblIngresoEnCulto.setText("Ingreso en culto:");

        jLblIngresoEnCultoDato.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLblIngresoEnCultoDato.setText("-");

        jLblSaldoCaja.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLblSaldoCaja.setText("Saldo caja:");

        jLblSaldoCajaDato.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanelPieConSaldosLayout = new javax.swing.GroupLayout(jPanelPieConSaldos);
        jPanelPieConSaldos.setLayout(jPanelPieConSaldosLayout);
        jPanelPieConSaldosLayout.setHorizontalGroup(
            jPanelPieConSaldosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPieConSaldosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelAvisoDeGuardar)
                .addContainerGap())
            .addGroup(jPanelPieConSaldosLayout.createSequentialGroup()
                .addGroup(jPanelPieConSaldosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLblOficioDeCulto)
                    .addGroup(jPanelPieConSaldosLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLblOficioDeCultoDato, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addGroup(jPanelPieConSaldosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelPieConSaldosLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLblDiezmoPastoralDato, javax.swing.GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE))
                    .addComponent(jLblDiezmoPastoral))
                .addGap(18, 18, 18)
                .addGroup(jPanelPieConSaldosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelPieConSaldosLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLblIngresoEnCultoDato, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addComponent(jLblIngresoEnCulto))
                .addGap(31, 31, 31)
                .addGroup(jPanelPieConSaldosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelPieConSaldosLayout.createSequentialGroup()
                        .addComponent(jLblSaldoCaja, javax.swing.GroupLayout.DEFAULT_SIZE, 158, Short.MAX_VALUE)
                        .addGap(78, 78, 78))
                    .addGroup(jPanelPieConSaldosLayout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(jLblSaldoCajaDato, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        jPanelPieConSaldosLayout.setVerticalGroup(
            jPanelPieConSaldosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPieConSaldosLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelPieConSaldosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelPieConSaldosLayout.createSequentialGroup()
                        .addComponent(jLblOficioDeCulto)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblOficioDeCultoDato))
                    .addGroup(jPanelPieConSaldosLayout.createSequentialGroup()
                        .addComponent(jLblSaldoCaja)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblSaldoCajaDato, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelPieConSaldosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPieConSaldosLayout.createSequentialGroup()
                            .addComponent(jLblIngresoEnCulto)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLblIngresoEnCultoDato))
                        .addGroup(jPanelPieConSaldosLayout.createSequentialGroup()
                            .addComponent(jLblDiezmoPastoral)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLblDiezmoPastoralDato, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelAvisoDeGuardar)
                .addGap(21, 21, 21))
        );

        jPanelButtons.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        jBtnVolverAlInicio.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnVolverAlInicio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolverAlInicio.setText("Volver");
        jBtnVolverAlInicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverAlInicioActionPerformed(evt);
            }
        });
        jPanelButtons.add(jBtnVolverAlInicio);

        jBtnLimpiarCampos.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnLimpiarCampos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Limpiar campos.png"))); // NOI18N
        jBtnLimpiarCampos.setText("Limpiar campos");
        jBtnLimpiarCampos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnLimpiarCamposActionPerformed(evt);
            }
        });
        jPanelButtons.add(jBtnLimpiarCampos);

        jBtnVerCarga.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnVerCarga.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Balance ver 20x20.png"))); // NOI18N
        jBtnVerCarga.setText("Ver carga");
        jBtnVerCarga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVerCargaActionPerformed(evt);
            }
        });
        jPanelButtons.add(jBtnVerCarga);

        jBtnGuardar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Guardar 30x30.png"))); // NOI18N
        jBtnGuardar.setText("Guardar carga");
        jBtnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnGuardarActionPerformed(evt);
            }
        });
        jPanelButtons.add(jBtnGuardar);

        javax.swing.GroupLayout jPanelIngresoLayout = new javax.swing.GroupLayout(jPanelIngreso);
        jPanelIngreso.setLayout(jPanelIngresoLayout);
        jPanelIngresoLayout.setHorizontalGroup(
            jPanelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelIngresoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelIngresoLayout.createSequentialGroup()
                        .addComponent(jPanelFechaYTipoCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanelMontoYBuscarMiembro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(443, 443, 443)
                        .addComponent(jLblAvisoDeQueNoHayCuentas, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jXSeparadorDeTituloNuevosIngresos, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanelButtons, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jPanelPieConSaldos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanelMiembro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelIngresoLayout.setVerticalGroup(
            jPanelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelIngresoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXSeparadorDeTituloNuevosIngresos, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelIngresoLayout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(jLblAvisoDeQueNoHayCuentas, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanelFechaYTipoCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanelMontoYBuscarMiembro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelMiembro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanelButtons, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(jPanelPieConSaldos, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, 763, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanelIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 7, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 763, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 436, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnGuardarActionPerformed(java.awt.event.ActionEvent evt) {
        boolean hayMiembrosCargados = this.miembrosConDiezmo == null || this.miembrosConDiezmo.isEmpty() ? false : true;
        boolean hayIngresosCargados = this.ingresosCargados == null || this.ingresosCargados.isEmpty() ? false : true;
        if (!hayIngresosCargados && !hayMiembrosCargados) {
            JOptionPane.showMessageDialog(this, "No hay asientos cargados aún.", "Aviso",
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        } else {
            int respuesta = JOptionPane.showConfirmDialog(this,
                    "¿Desea guardar los ingresos cargados ? ", "Pregunta", JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (respuesta == JOptionPane.YES_OPTION) {
                if (hayIngresosCargados) { //porque o puede haber ingresos, o puede haber miembros para actualizar... necesito volver a preguntar esto
                    try {
                        institucion.guardarIngresos(ingresosCargados);
                    } catch (DAOProblemaConTransaccionException ex) {
                        JOptionPane.showMessageDialog(this, ex.getMessage());
                    }
                }
                if (hayMiembrosCargados) {
                    try {
                        registrarIngresosDiezmos(miembrosConDiezmo);
                    } catch (DAOProblemaConTransaccionException ex) {
                        JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.INFORMATION_MESSAGE);
                    }
                }
                modificarLabelsConIngresos(ingresosCargados, miembrosConDiezmo);
            }
            if (llamadoParaRegistarIngresosDeCulto) {
                try {
                    //implica que ingresoEnCulto no sea null
                    for (Entry<Miembro, Ingreso> entry : miembrosConDiezmo) {
                        ingresoEnCulto.addIngreso(entry.getValue());
                    }
                    institucion.guardarIngresoEnCulto(ingresoEnCulto);
                } catch (DAOProblemaConTransaccionException ex) {
                    Logger.getLogger(JDialogAltaIngreso.class.getName()).log(Level.SEVERE, null, ex);
                }
            } 
        }
        miembrosConDiezmo.clear();
        ingresosCargados.clear();
        if(llamadoParaRegistarIngresosDeCulto)deshabilitarEdicion();
    }

    private void jBtnVerCargaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVerCargaActionPerformed
        if (ingresosCargados == null || ingresosCargados.isEmpty()) {
            if (miembrosConDiezmo == null || miembrosConDiezmo.isEmpty()) {
                JOptionPane.showMessageDialog(this, "No hay ingresos cargados aún.");
                return;
            }
        }
        JDialogConsultaCargaIngreso jDialogConsultaCargaIngreso = new JDialogConsultaCargaIngreso(this, true, institucion,
                ingresosCargados, miembrosConDiezmo);
        jDialogConsultaCargaIngreso.setVisible(true);
        jDialogConsultaCargaIngreso.setLocationRelativeTo(null);
    }//GEN-LAST:event_jBtnVerCargaActionPerformed

    private void jBtnLimpiarCamposActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnLimpiarCamposActionPerformed
        limpiarCampos();
    }//GEN-LAST:event_jBtnLimpiarCamposActionPerformed

    private void jButtonDeshacerAsientoActionPerformed(java.awt.event.ActionEvent evt) {
    }

    private void jRadioButtonDeCultoActionPerformed(java.awt.event.ActionEvent evt) {
        if (ingresoEnCulto == null) {
            this.datePicker.setEnabled(true);
        } else {
            this.datePicker.setDate(ingresoEnCulto.getFechaDeCulto());
            this.datePicker.setEnabled(false);
        }
    }

    private void jRadioButtonOtrosActionPerformed(java.awt.event.ActionEvent evt) {
        this.datePicker.setEnabled(true);
        this.datePicker.setDate(null);
    }

    private void jLinkBuscarMiembroActionPerformed(java.awt.event.ActionEvent evt) {
        miembro = MiembroPresenter.getInstance().presentarSeleccionMiembro();
        if (miembro != null)
            jLblApellidoYNombreDato.setText(miembro.nombresYApellidos());
    }

    private void jComboTipoCuentaActionPerformed(java.awt.event.ActionEvent evt) {
        if (!institucion.hayMiembrosCargados()) {
            JOptionPane.showMessageDialog(this, "No hay miembros cargados, tan solo podrá guardar diezmos de miembros anónimos.");
            miembro = getMiembroAnonimo();
            jLblApellidoYNombreDato.setText(miembro.getApellidos());
            deshabilitarPanelMiembro();
        } else {
            habilitarPanelMiembro();
        }
    }

    private void jComboTipoCuentaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboTipoCuentaMouseClicked
    }//GEN-LAST:event_jComboTipoCuentaMouseClicked

    private void datePickerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_datePickerActionPerformed
        if (!(ingresoEnCulto == null)) {
            int respuesta = JOptionPane.showConfirmDialog(this, "<html> <p> ¿Desea selecciondar la fecha: <b> "
                    + DateFormat.getDateInstance(DateFormat.SHORT).format(datePicker.getDate())
                    + "</b></p> <p> como fecha de culto?</p>", "Pregunta", JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_OPTION) {
                ingresoEnCulto.setFechaDeCulto(datePicker.getDate());
                this.datePicker.setEnabled(false);
            }
        }
        if (this.datePicker.getDate() == null) {
            return;
        }
        if (this.datePicker.getDate().after(new Date())) {
            JOptionPane.showMessageDialog(this, "Debe ser de una fecha anterior.");
            this.datePicker.setDate(null);
        }
    }//GEN-LAST:event_datePickerActionPerformed

    private void jBtnCargarUnIngresoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnCargarUnIngresoActionPerformed
        String mensaje = hayCamposVacios();
        if (!mensaje.isEmpty()) {
            JOptionPane.showMessageDialog(this, mensaje);
        } else {
            double monto;
            GregorianCalendar fecha = UtilFechas.dateToGregorianCalendar(datePicker.getDate());
            monto = Util.getMontoSinSimbolo(jTxtMonto.getText());
            CuentaIngreso cuentaDeIngreso = getCuenta(jComboTipoCuenta.getSelectedItem().toString());
            if (cuentaDeIngreso.sosDiezmo() || cuentaDeIngreso.sosOfrenda()) {
                Ingreso ingresoNuevo;
                if(!jCheckAnonimo.isSelected()){
                    if (miembro == null && institucion.hayMiembrosCargados()) {
                        int respuesta = JOptionPane.showConfirmDialog(this,
                                "No ha seleccionado el miembro. ¿El diezmo es anónimo?",
                                "Aviso", JOptionPane.YES_NO_OPTION);
                        if (respuesta == JOptionPane.NO_OPTION) {//mostrar JDialogABMMiembros
                            while (miembro == null) {
                                miembro = MiembroPresenter.getInstance().presentarSeleccionMiembro();
                            }
                        }else{
                            miembro = getMiembroAnonimo();
                        }
                    }
                }else{
                    miembro = getMiembroAnonimo();
                }
                if(cuentaDeIngreso.sosDiezmo()) {
                    ingresoNuevo = new Diezmo(cuentaDeIngreso, fecha, monto);
                }else{
                    ingresoNuevo = new Ofrenda(cuentaDeIngreso, fecha,monto);
                }
                cargarMiembroYActulizarIngresoEnCulto(miembro, ingresoNuevo);
                limpiarCampos();
                jLabelAvisoDeGuardar.setVisible(true);
            } else {
                if (cuentaDeIngreso.sosDonacion()) {
                    boolean bandera = true;
                    while (bandera) {
                        new JDialogIngresoDonantes(this, true, institucion).setVisible(true);//acá me genera y devuelve el donante
                        if (donante == null) {
                            int respuesta = JOptionPane.showConfirmDialog(this, "<html> No ha ingresado los datos del donante. ¿Desea volver para ingresarlos? <p>"
                                    + "Si no quiere ingresar los datos el donante, presione <b> no</b></p></html>", "Pregunta", JOptionPane.YES_NO_OPTION);
                            if (respuesta == JOptionPane.YES_OPTION) {
                                continue;
                            } else {
                                bandera = false;
                            }

                        } else {
                            bandera = false;
                        }
                    }
                    String detalleMensaje = JOptionPane.showInputDialog("Ingrese un detalle para la donacion.");
                    if (detalleMensaje == null) {
                        detalleMensaje = "";
                    }
                    Donacion donacion = new Donacion(donante, detalleMensaje,
                            cuentaDeIngreso, fecha, monto);
                    cargarIngresoYActulizarIngresoEnCultoSiCorresponde(donacion);
                    jLabelAvisoDeGuardar.setVisible(true);
                    limpiarCampos();
                    return;

                }
                if (cuentaDeIngreso.sosOfrenda()) {
                    Ofrenda ofrendaNueva = new Ofrenda();
                    ofrendaNueva.setMonto(monto);
                    ofrendaNueva.setCuenta(cuentaDeIngreso);
                    ofrendaNueva.setFecha(fecha);
                    cargarIngresoYActulizarIngresoEnCultoSiCorresponde(ofrendaNueva);
                } else {
                    OtroIngreso ingresoNuevo = new OtroIngreso(cuentaDeIngreso, fecha, monto);
                    cargarIngresoYActulizarIngresoEnCultoSiCorresponde(ingresoNuevo);
                }
                this.jLabelAvisoDeGuardar.setVisible(true);
                this.jTxtMonto.setText(null);
            }

        }
    }//GEN-LAST:event_jBtnCargarUnIngresoActionPerformed

    private void jTxtMontoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoKeyTyped

        Vistas.Util.Util.formatearJTextFieldTipoMonedaLuegoDeUnKeyEvent(evt, jTxtMonto);

        }//GEN-LAST:event_jTxtMontoKeyTyped

    private void jTxtMontoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoKeyReleased
        Vistas.Util.Util.limpiarTextoSiSoloHaySigno(jTxtMonto);
    }//GEN-LAST:event_jTxtMontoKeyReleased

    private void jTxtMontoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoKeyPressed
    }//GEN-LAST:event_jTxtMontoKeyPressed

    private void jTxtMontoFocusLost(java.awt.event.FocusEvent evt)
    {//GEN-FIRST:event_jTxtMontoFocusLost

        Vistas.Util.Util.redondearUnMontoADosCifras(jTxtMonto);
        }//GEN-LAST:event_jTxtMontoFocusLost

    private void jTxtMontoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtMontoFocusGained
    }//GEN-LAST:event_jTxtMontoFocusGained

    private void jTxtMontoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtMontoActionPerformed
    }//GEN-LAST:event_jTxtMontoActionPerformed

    private void jTxtMontoCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTxtMontoCaretUpdate
    }//GEN-LAST:event_jTxtMontoCaretUpdate

    private void jTxtMontoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTxtMontoMouseClicked
    }//GEN-LAST:event_jTxtMontoMouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        if (ingresosCargados == null || ingresosCargados.isEmpty()) {
            this.dispose();
            this.getParent().setVisible(true);
        } else {
            int respuesta = JOptionPane.showConfirmDialog(this, "Hay ingresos en la carga temporal. \n  ¿Salir sin guardar cambios?", "Aviso",
                    JOptionPane.YES_NO_CANCEL_OPTION);
            if (respuesta == JOptionPane.YES_OPTION) {
                this.dispose();
                ingresosCargados = null;
            } else {
                return;
            }
        }
    }//GEN-LAST:event_formWindowClosing

    private void datePickerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_datePickerMouseClicked
    }//GEN-LAST:event_datePickerMouseClicked

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
    }//GEN-LAST:event_formWindowClosed

    private void jCheckCambiarFechaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckCambiarFechaActionPerformed
        if (jCheckCambiarFecha.isSelected()) {
            this.datePicker.setEnabled(true);
        } else {
            this.datePicker.setEnabled(false);
        }
    }//GEN-LAST:event_jCheckCambiarFechaActionPerformed

    private void jCheckAnonimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckAnonimoActionPerformed
        if (jCheckAnonimo.isSelected()) {
            jLinkBuscarMiembro.setEnabled(false);
            jLblApellidoYNombreDato.setText(getMiembroAnonimo().getApellidos());
        } else {
            jLinkBuscarMiembro.setEnabled(true);
            jLblApellidoYNombreDato.setText("");
        }
    }//GEN-LAST:event_jCheckAnonimoActionPerformed

    private CuentaIngreso getCuenta(String itemSeleccionadoDeJCombo) {
        CuentaIngreso cuentaARetornar = null;
        Iterator<CuentaIngreso> it = cuentas.iterator();
        while (it.hasNext()) {
            CuentaIngreso cuentaIngresoEnColeccion = it.next();
            if (cuentaIngresoEnColeccion.getNombre().equals(itemSeleccionadoDeJCombo)) {
                cuentaARetornar = cuentaIngresoEnColeccion;
            }
        }
        return cuentaARetornar;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupTipoIngreso;
    private org.jdesktop.swingx.JXDatePicker datePicker;
    private javax.swing.JButton jBtnCargarUnIngreso;
    private javax.swing.JButton jBtnGuardar;
    private javax.swing.JButton jBtnLimpiarCampos;
    private javax.swing.JButton jBtnVerCarga;
    private javax.swing.JButton jBtnVolverAlInicio;
    private javax.swing.JCheckBox jCheckAnonimo;
    private javax.swing.JCheckBox jCheckCambiarFecha;
    private javax.swing.JComboBox jComboTipoCuenta;
    private javax.swing.JLabel jLabelAvisoDeGuardar;
    private javax.swing.JLabel jLabelFecha;
    private javax.swing.JLabel jLabelMonto;
    private javax.swing.JLabel jLabelTipoCuenta;
    private javax.swing.JLabel jLbelApellidoYNombre;
    private javax.swing.JLabel jLblApellidoYNombreDato;
    private javax.swing.JLabel jLblAvisoDeQueNoHayCuentas;
    private javax.swing.JLabel jLblDiezmoPastoral;
    private javax.swing.JLabel jLblDiezmoPastoralDato;
    private javax.swing.JLabel jLblIngresoEnCulto;
    private javax.swing.JLabel jLblIngresoEnCultoDato;
    private javax.swing.JLabel jLblOficioDeCulto;
    private javax.swing.JLabel jLblOficioDeCultoDato;
    private javax.swing.JLabel jLblSaldoCaja;
    private javax.swing.JLabel jLblSaldoCajaDato;
    private org.jdesktop.swingx.JXHyperlink jLinkBuscarMiembro;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanelButtons;
    private javax.swing.JPanel jPanelFechaYTipoCuenta;
    private javax.swing.JPanel jPanelIngreso;
    private javax.swing.JPanel jPanelMiembro;
    private javax.swing.JPanel jPanelMontoYBuscarMiembro;
    private javax.swing.JPanel jPanelPieConSaldos;
    private javax.swing.JPopupMenu jPopupMenuJXDatePicker;
    private javax.swing.JTextField jTxtMonto;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloNuevosIngresos;
    // End of variables declaration//GEN-END:variables

    public ArrayList<Ingreso> getIngresosCargados() {
        return ingresosCargados;
    }

    public void setIngresosCargados(ArrayList<Ingreso> ingresosCargados) {
        this.ingresosCargados = ingresosCargados;
    }

    public InstitucionCentralTesoreria getInstitucion() {
        return institucion;
    }

    public void setInstitucion(InstitucionCentralTesoreria institucion) {
        this.institucion = institucion;
    }

    /**
     * Este método comprueba si hay campos vacíos. Si los hay, devuelve un aviso
     * generado para que se muestre al usuario, indicando cuales son los campos
     * que están vacíos.
     *
     * @return Un String como mensaje, que informa cuales son los campos que
     * están vaciós.
     */
    private String hayCamposVacios() {
        JTextField[] jTextFields = {this.jTxtMonto};
        String avisoDeCorreccion = "";
        if (Vistas.Util.Util.isEmpty(jTextFields)) {
            avisoDeCorreccion = "Ingrese el monto del ingreso \n";
        }
        if (datePicker.getDate() == null) {
            avisoDeCorreccion += "Ingrese la fecha del ingreso\n";
        }
        if (jComboTipoCuenta.getSelectedIndex() == -1) {
            avisoDeCorreccion += "Seleccione el tipo de cuenta\n";
        }
        return avisoDeCorreccion;
    }

    public ArrayList<CuentaIngreso> getCuentas() {
        return cuentas;
    }

    public void setCuentas(ArrayList<CuentaIngreso> cuentas) {
        this.cuentas = cuentas;
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    private Miembro getMiembroAnonimo() {
        if (miembroAnonimo == null) {
            miembroAnonimo = institucion.getMiembroAnonimo();
        }
        return miembroAnonimo;
    }

    private void addDiezmoAMiembroAnonimo(Diezmo diezmo) {
        Miembro anonimo = this.getMiembroAnonimo();
        anonimo.agregarDiezmo(diezmo);
        if (miembrosAActualizar.contains(anonimo)) {
            miembrosAActualizar.remove(anonimo);
            miembrosAActualizar.add(anonimo);
        } else {
            miembrosAActualizar.add(anonimo);
        }
    }

    public void setMiembrosConDiezmo(ArrayList<Entry<Miembro, Ingreso>> miembrosConDiezmo) {
        this.miembrosConDiezmo = miembrosConDiezmo;
    }

    public ArrayList<Entry<Miembro, Ingreso>> getMiembrosConDiezmo() {
        return miembrosConDiezmo;
    }

    public Donante getDonante() {
        return donante;
    }

    public void setDonante(Donante donante) {
        this.donante = donante;
    }

    private void cargarIngresoYActulizarIngresoEnCultoSiCorresponde(Ingreso ingreso) {
        if (llamadoParaRegistarIngresosDeCulto) {
            ingresoEnCulto.addIngreso(ingreso);
            ingresosCargados.add(ingreso);
        } else {
            ingresosCargados.add(ingreso);
            donante = null;
        }
    }

    private void cargarMiembroYActulizarIngresoEnCulto(Miembro miembro, Ingreso diezmo) {
        miembrosConDiezmo.add(new SimpleEntry<Miembro, Ingreso>(miembro, diezmo));
        ingresoEnCulto.addIngreso(diezmo);
    }

    private void agregarPopupMenuAJXDateFecha() {
        JMenuItem menuEditar = new JMenuItem("Editar");
        jPopupMenuJXDatePicker.add(menuEditar);
        this.datePicker.setComponentPopupMenu(jPopupMenuJXDatePicker);
    }

    private void jBtnVolverAlInicioActionPerformed(java.awt.event.ActionEvent evt) {
        if (llamadoParaRegistarIngresosDeCulto) {
            if (!((ingresosCargados == null && miembrosConDiezmo == null) || (ingresosCargados.isEmpty() && miembrosConDiezmo.isEmpty()))) {
                int respuesta = JOptionPane.showConfirmDialog(this, "Hay ingresos cargados. Si sale, estos no serán guardados. \n "
                        + "¿Salir de todos modos?", "Aviso", JOptionPane.YES_NO_OPTION);
                if ((respuesta == JOptionPane.YES_NO_OPTION)) {
                    int respuesta2 = JOptionPane.showConfirmDialog(this, "Si sale, ya no podrá registrar ingresos para este culto. \n ¿Está seguro"
                            + "de salir?. ", "Aviso", JOptionPane.YES_NO_OPTION);
                    if (JOptionPane.YES_OPTION == respuesta2) {
                        this.dispose();
                        this.getParent().setVisible(true);
                    } else {
                        return;
                    }
                }
            }
        } else {
            if ((ingresosCargados != null && !ingresosCargados.isEmpty())) {
                int respuesta = JOptionPane.showConfirmDialog(this, "Hay ingresos cargados (en la carga temporal). Si sale, estos no serán guardados. \n "
                        + "¿Salir de todos modos?", "Aviso", JOptionPane.YES_NO_OPTION);
                if (respuesta == JOptionPane.YES_OPTION) {
                    this.dispose();
                    this.getParent().setVisible(true);
                } else {
                    return;
                }
            } else {
                this.dispose();
                this.getParent().setVisible(true);
            }
        }
        if (ingresosCargados == null || ingresosCargados.isEmpty()) {
            this.dispose();
            this.getParent().setVisible(true);
        }
    }

    private void modificarVistaParaRegistrarIngresosQueNoSonDeCulto() {
        Color colorIgualQueElFondoDelPanel = new Color(204, 204, 204);
        this.jLblDiezmoPastoral.setForeground(colorIgualQueElFondoDelPanel);
        this.jLblDiezmoPastoralDato.setForeground(colorIgualQueElFondoDelPanel);
        this.jLblIngresoEnCulto.setForeground(colorIgualQueElFondoDelPanel);
        this.jLblIngresoEnCultoDato.setForeground(colorIgualQueElFondoDelPanel);
        this.jLblOficioDeCulto.setForeground(colorIgualQueElFondoDelPanel);
        this.jLblOficioDeCultoDato.setForeground(colorIgualQueElFondoDelPanel);
    }

    private void modificarVistaParaRegistrarIngresosQueSonDeCulto() {
        this.jLblDiezmoPastoral.setVisible(true);
        this.jLblDiezmoPastoralDato.setVisible(true);
        this.jLblIngresoEnCulto.setVisible(true);
        this.jLblIngresoEnCultoDato.setVisible(true);
        this.jLblOficioDeCulto.setVisible(true);
        this.jLblOficioDeCultoDato.setVisible(true);
    }

    private void cargarIngresoEnCultoEnLabel(IngresoEnCulto ingresoEnCulto) {
        this.jLblIngresoEnCultoDato.setText(String.valueOf(ingresoEnCulto.getMonto()));
    }

    private void modificarLabelsConIngresos(ArrayList<Ingreso> ingresosCargados, ArrayList<Entry<Miembro, Ingreso>> miembrosConDiezmo) {
        double totalIngresos = 0;
        if (ingresosCargados != null && !ingresosCargados.isEmpty()) {
            Iterator<Ingreso> it = ingresosCargados.iterator();
            while (it.hasNext()) {
                Ingreso ingreso = it.next();
                totalIngresos += ingreso.getMonto();
            }
        }

        if (miembrosConDiezmo != null && !miembrosConDiezmo.isEmpty()) {
            Iterator<Entry<Miembro, Ingreso>> itMiembros = miembrosConDiezmo.iterator();
            while (itMiembros.hasNext()) {
                Entry<Miembro, Ingreso> entry = itMiembros.next();
                totalIngresos += entry.getValue().getMonto();
            }
        }
        jLblDiezmoPastoralDato.setText(Vistas.Util.Util.formatearTipoMoneda(institucion.getDiezmoPastoral(totalIngresos)));
        jLblIngresoEnCultoDato.setText(Vistas.Util.Util.formatearTipoMoneda(totalIngresos));
        jLblOficioDeCultoDato.setText(Vistas.Util.Util.formatearTipoMoneda(totalIngresos * institucion.getConfiguracion().getPorcentajeParaElPastorSobreLosDiezmosYOfrendas()));
        jLblSaldoCajaDato.setText(Vistas.Util.Util.formatearTipoMoneda(institucion.consultarCajaDisponible()));
    }

    private void registrarIngresosDiezmos(ArrayList<Entry<Miembro, Ingreso>> miembrosConDiezmo) throws DAOProblemaConTransaccionException {
        Iterator<Entry<Miembro, Ingreso>> itMiembrosConDiezmo = miembrosConDiezmo.iterator();
        ArrayList<Miembro> miembrosArrayList = new ArrayList<Miembro>();
        ArrayList<Ingreso> ingresos = new ArrayList<Ingreso>();
        Miembro miembroIt;
        Ingreso ingreso;
        while (itMiembrosConDiezmo.hasNext()) {
            Entry<Miembro, Ingreso> entry = itMiembrosConDiezmo.next();
            miembroIt = entry.getKey();
            ingreso = entry.getValue();
            ingresos.add(ingreso);
            if (miembroIt.getApellidos().equalsIgnoreCase("Anonimo")) {
                if (miembrosArrayList.contains(getMiembroAnonimo())) {
                    if (ingreso instanceof Diezmo) {
                        miembrosArrayList.get(miembrosArrayList.indexOf(getMiembroAnonimo())).agregarDiezmo((Diezmo) entry.getValue());
                    } else {
                        miembrosArrayList.get(miembrosArrayList.indexOf(getMiembroAnonimo())).addOfrenda((Ofrenda) entry.getValue());
                    }
                } else {
                    if (ingreso instanceof Diezmo) {
                        getMiembroAnonimo().agregarDiezmo((Diezmo) entry.getValue());
                    } else {
                        getMiembroAnonimo().addOfrenda((Ofrenda) entry.getValue());
                    }
                    miembrosArrayList.add(getMiembroAnonimo());
                }
            } else {
                if (ingreso instanceof Diezmo) {
                    miembroIt.agregarDiezmo((Diezmo) entry.getValue());
                } else {
                    miembroIt.addOfrenda((Ofrenda) entry.getValue());
                }
                miembrosArrayList.add(miembro);
            }
        }
        institucion.guardarIngresos(ingresos);
        institucion.actualizarDatosDeMiembro(miembro);
        jLabelAvisoDeGuardar.setVisible(false);
    }

    private void deshabilitarEdicion() {
        jTxtMonto.setEnabled(false);
        datePicker.setEditable(false);
        jComboTipoCuenta.setEnabled(false);
        jBtnCargarUnIngreso.setEnabled(false);
        jBtnGuardar.setEnabled(false);
        jBtnVerCarga.setEnabled(false);
        jCheckCambiarFecha.setEnabled(false);
        jLinkBuscarMiembro.setEnabled(false);
        jBtnLimpiarCampos.setEnabled(false);
        jCheckAnonimo.setEnabled(false);
    }

    private boolean esDiezmoUOfrenda(CuentaIngreso cuentaIngreso) {
        return cuentaIngreso.sosDiezmo() || cuentaIngreso.sosOfrenda();
    }
}