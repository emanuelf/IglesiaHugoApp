/*
 * JDialogModificarAsientoIngreso.java
 * Created on 22/08/2011, 18:50:27
 */
package Vistas;

import Modelo.Cuenta;
import Modelo.CuentaIngreso;
import Modelo.Ingreso;
import Modelo.InstitucionCentralTesoreria;
import Util.UtilFechas;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JOptionPane;

/**
 * @author emanuel
 */
public class JDialogModificarAsientoIngreso extends javax.swing.JDialog {

    JDialogConsultaCargaIngreso padre;
    Ingreso ingresoAModificar;
    boolean ingresoModificado;
    InstitucionCentralTesoreria institucion;
    ArrayList<CuentaIngreso> cuentasIngreso;
    private boolean llamadoParaModificarCarga;

    public JDialogModificarAsientoIngreso(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
    }

    public JDialogModificarAsientoIngreso(JDialogConsultaCargaIngreso padre, boolean modal, Ingreso ingresoAModificar,
            InstitucionCentralTesoreria institucion, boolean llamadoParaModificarCargaSolamente) {
        super(padre, modal);
        this.padre = padre;
        ingresoModificado = false;
        initComponents();
        this.ingresoAModificar = ingresoAModificar;
        this.institucion = padre.getInstitucion();
        if (this.institucion == null) {
            JOptionPane.showMessageDialog(null, "es null que cosa");
        }
        llamadoParaModificarCarga = llamadoParaModificarCargaSolamente;
        cuentasIngreso = this.institucion.getCuentasIngreso();
        cargarCuentasIngresoEnJCombo(cuentasIngreso);
        jComboTiposDeCuentas.setSelectedItem(ingresoAModificar.getCuenta().getNombre());
        cargarDatosDeIngresoEnVista(ingresoAModificar);
        modificarVistaParaElTipoDeIngreso(ingresoAModificar);
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelCuerpo = new javax.swing.JPanel();
        jXSeparadorDeTituloModificarAsiento = new org.jdesktop.swingx.JXTitledSeparator();
        jPnlButonera = new javax.swing.JPanel();
        jBtnGuardarCambios = new javax.swing.JButton();
        jBtnVolver = new javax.swing.JButton();
        jPnlDatosGenerales = new javax.swing.JPanel();
        jLabelFecha = new javax.swing.JLabel();
        jXDatePickerFechaAsiento = new org.jdesktop.swingx.JXDatePicker();
        jComboTiposDeCuentas = new javax.swing.JComboBox();
        jLabelTipoDeCuenta = new javax.swing.JLabel();
        jPnlMonto = new javax.swing.JPanel();
        jLabelMonto2 = new javax.swing.JLabel();
        jTxtMonto = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanelCuerpo.setPreferredSize(new java.awt.Dimension(500, 600));

        jXSeparadorDeTituloModificarAsiento.setFont(new java.awt.Font("Tahoma", 0, 12));
        jXSeparadorDeTituloModificarAsiento.setHorizontalAlignment(0);
        jXSeparadorDeTituloModificarAsiento.setTitle("Modificar asiento");

        jPnlButonera.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        jBtnGuardarCambios.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnGuardarCambios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Guardar 30x30.png"))); // NOI18N
        jBtnGuardarCambios.setText("Guardar cambios");
        jBtnGuardarCambios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnGuardarCambiosActionPerformed(evt);
            }
        });
        jPnlButonera.add(jBtnGuardarCambios);

        jBtnVolver.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolver.setText("Volver");
        jBtnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverActionPerformed(evt);
            }
        });
        jPnlButonera.add(jBtnVolver);

        jPnlDatosGenerales.setForeground(new java.awt.Color(204, 204, 204));

        jLabelFecha.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelFecha.setText("Fecha:");

        jComboTiposDeCuentas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jComboTiposDeCuentasMouseClicked(evt);
            }
        });
        jComboTiposDeCuentas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboTiposDeCuentasActionPerformed(evt);
            }
        });
        jComboTiposDeCuentas.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jComboTiposDeCuentasPropertyChange(evt);
            }
        });

        jLabelTipoDeCuenta.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelTipoDeCuenta.setText("Tipo cuenta:");

        javax.swing.GroupLayout jPnlDatosGeneralesLayout = new javax.swing.GroupLayout(jPnlDatosGenerales);
        jPnlDatosGenerales.setLayout(jPnlDatosGeneralesLayout);
        jPnlDatosGeneralesLayout.setHorizontalGroup(
            jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPnlDatosGeneralesLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jXDatePickerFechaAsiento, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE))
                    .addComponent(jLabelFecha)
                    .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                        .addComponent(jLabelTipoDeCuenta)
                        .addGap(29, 29, 29))
                    .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jComboTiposDeCuentas, 0, 164, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPnlDatosGeneralesLayout.setVerticalGroup(
            jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                .addComponent(jLabelFecha)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jXDatePickerFechaAsiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelTipoDeCuenta)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboTiposDeCuentas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabelMonto2.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelMonto2.setText("Monto:");

        jTxtMonto.setFont(new java.awt.Font("Dialog", 1, 24));
        jTxtMonto.setForeground(new java.awt.Color(0, 204, 51));
        jTxtMonto.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtMonto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTxtMontoMouseClicked(evt);
            }
        });
        jTxtMonto.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTxtMontoCaretUpdate(evt);
            }
        });
        jTxtMonto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtMontoActionPerformed(evt);
            }
        });
        jTxtMonto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTxtMontoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTxtMontoFocusLost(evt);
            }
        });
        jTxtMonto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtMontoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxtMontoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtMontoKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPnlMontoLayout = new javax.swing.GroupLayout(jPnlMonto);
        jPnlMonto.setLayout(jPnlMontoLayout);
        jPnlMontoLayout.setHorizontalGroup(
            jPnlMontoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlMontoLayout.createSequentialGroup()
                .addGroup(jPnlMontoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlMontoLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jTxtMonto, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE))
                    .addComponent(jLabelMonto2))
                .addContainerGap())
        );
        jPnlMontoLayout.setVerticalGroup(
            jPnlMontoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlMontoLayout.createSequentialGroup()
                .addComponent(jLabelMonto2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtMonto, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelCuerpoLayout = new javax.swing.GroupLayout(jPanelCuerpo);
        jPanelCuerpo.setLayout(jPanelCuerpoLayout);
        jPanelCuerpoLayout.setHorizontalGroup(
            jPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jXSeparadorDeTituloModificarAsiento, javax.swing.GroupLayout.DEFAULT_SIZE, 354, Short.MAX_VALUE)
                    .addGroup(jPanelCuerpoLayout.createSequentialGroup()
                        .addComponent(jPnlDatosGenerales, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPnlMonto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPnlButonera, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanelCuerpoLayout.setVerticalGroup(
            jPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXSeparadorDeTituloModificarAsiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPnlDatosGenerales, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPnlMonto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPnlButonera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, 374, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanelCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnGuardarCambiosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnGuardarCambiosActionPerformed
        if (hayDatosNoCargados()) {
            JOptionPane.showMessageDialog(this, "Hay datos sin ingresar.");
            return;
        }
        int respuesta = JOptionPane.showConfirmDialog(this, "¿Guardar los cambios en el ingreso?", "Aviso", JOptionPane.YES_NO_OPTION);
        if (respuesta == JOptionPane.YES_OPTION) {
            guardarCambiosEnIngreso();
            if (llamadoParaModificarCarga) {
                padre.guardarModificacionEnColeccionYEnJTable(ingresoAModificar);
            } else {
                padre.guardarModificacionDeIngresoEnColeccionYEnJTableYEnBd(ingresoAModificar);
            }
            dispose();
            padre.setVisible(true);
        } else {
            return;
        }


}//GEN-LAST:event_jBtnGuardarCambiosActionPerformed

    private void jBtnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVolverActionPerformed
        if (!ingresoModificado) {
            int respuesta = JOptionPane.showConfirmDialog(this, "No se modificaron los datos del egreso. ¿Salir de todos modos?", "Aviso",
                    JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_OPTION) {
                dispose();
                padre.setVisible(true);
            } else {
                return;
            }
        }
}//GEN-LAST:event_jBtnVolverActionPerformed

    private void jTxtMontoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTxtMontoMouseClicked
}//GEN-LAST:event_jTxtMontoMouseClicked

    private void jTxtMontoCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTxtMontoCaretUpdate
}//GEN-LAST:event_jTxtMontoCaretUpdate

    private void jTxtMontoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtMontoActionPerformed
}//GEN-LAST:event_jTxtMontoActionPerformed

    private void jTxtMontoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtMontoFocusGained
}//GEN-LAST:event_jTxtMontoFocusGained

    private void jTxtMontoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtMontoFocusLost
}//GEN-LAST:event_jTxtMontoFocusLost

    private void jTxtMontoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoKeyPressed
}//GEN-LAST:event_jTxtMontoKeyPressed

    private void jTxtMontoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoKeyReleased
        Vistas.Util.Util.limpiarTextoSiSoloHaySigno(jTxtMonto);
}//GEN-LAST:event_jTxtMontoKeyReleased

    private void jTxtMontoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoKeyTyped
        Vistas.Util.Util.formatearJTextFieldTipoMonedaLuegoDeUnKeyEvent(evt, jTxtMonto);
}//GEN-LAST:event_jTxtMontoKeyTyped

    private void jComboTiposDeCuentasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboTiposDeCuentasMouseClicked
    }//GEN-LAST:event_jComboTiposDeCuentasMouseClicked

    private void jComboTiposDeCuentasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboTiposDeCuentasActionPerformed
    }//GEN-LAST:event_jComboTiposDeCuentasActionPerformed

    private void jComboTiposDeCuentasPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jComboTiposDeCuentasPropertyChange
    }//GEN-LAST:event_jComboTiposDeCuentasPropertyChange

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogModificarAsientoIngreso dialog = new JDialogModificarAsientoIngreso(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnGuardarCambios;
    private javax.swing.JButton jBtnVolver;
    private javax.swing.JComboBox jComboTiposDeCuentas;
    private javax.swing.JLabel jLabelFecha;
    private javax.swing.JLabel jLabelMonto2;
    private javax.swing.JLabel jLabelTipoDeCuenta;
    private javax.swing.JPanel jPanelCuerpo;
    private javax.swing.JPanel jPnlButonera;
    private javax.swing.JPanel jPnlDatosGenerales;
    private javax.swing.JPanel jPnlMonto;
    private javax.swing.JTextField jTxtMonto;
    private org.jdesktop.swingx.JXDatePicker jXDatePickerFechaAsiento;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloModificarAsiento;
    // End of variables declaration//GEN-END:variables

    private void guardarCambiosEnIngreso() {
        java.util.Date fechaDelIngresoAModificar = jXDatePickerFechaAsiento.getDate();
        ingresoAModificar.getFecha().setTime(fechaDelIngresoAModificar);
        ingresoAModificar.setMonto(Vistas.Util.Util.getMontoSinSimbolo(jTxtMonto.getText()));
        ingresoAModificar.setCuenta(buscarCuenta(jComboTiposDeCuentas.getSelectedItem().toString()));
    }

    private void cargarCuentasIngresoEnJCombo(ArrayList<CuentaIngreso> cuentasIngreso) {
        Iterator<CuentaIngreso> it = cuentasIngreso.iterator();
        while (it.hasNext()) {
            CuentaIngreso cuentaIngreso = it.next();
            if (cuentaIngreso.sosDonacion()) {
                continue;
            }
            if (ingresoAModificar.sosIngresoDeCulto() && (cuentaIngreso.sosDiezmo() || cuentaIngreso.sosOfrenda())) {
                jComboTiposDeCuentas.addItem(cuentaIngreso.getNombre());
            }
            if (!ingresoAModificar.sosIngresoDeCulto() && !(cuentaIngreso.sosDiezmo() || cuentaIngreso.sosOfrenda())) {
                jComboTiposDeCuentas.addItem(cuentaIngreso.getNombre());
            }
        }
    }

    private Cuenta buscarCuenta(String toString) {
        CuentaIngreso retorno = null;
        for (CuentaIngreso cuentaIngreso : cuentasIngreso) {
            if (cuentaIngreso.getNombre().equals(toString)) {
                retorno = cuentaIngreso;
            }
        }
        return retorno;
    }

    private void cargarDatosDeIngresoEnVista(Ingreso ingresoAModificar) {
        jXDatePickerFechaAsiento.setDate(UtilFechas.parseDate(ingresoAModificar.getFecha()));
        jTxtMonto.setText("$" + String.valueOf(ingresoAModificar.getMonto()));
        jComboTiposDeCuentas.setSelectedItem(ingresoAModificar.getCuenta().getNombre());
    }

    private boolean hayDatosNoCargados() {
        return jTxtMonto.getText().isEmpty() || jComboTiposDeCuentas.getSelectedIndex() == -1 || jXDatePickerFechaAsiento.getDate() == null;
    }

    private void modificarVistaParaElTipoDeIngreso(Ingreso ingresoAModificar) {
        if (ingresoAModificar.sosDiezmo()) {
            jComboTiposDeCuentas.setEnabled(false);
        } else {
            jComboTiposDeCuentas.removeItem("Diezmo");
        }
    }
}
