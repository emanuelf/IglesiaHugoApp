package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Excepciones.FilaNoSelecionadaException;
import Modelo.*;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import Util.UtilFechas;
import Vistas.Util.ImporteCellRenderer;
import Vistas.Util.Util;
import java.awt.BorderLayout;
import java.util.Date;
import java.util.Map.Entry;

/**
 *
 * @author emanuel
 */
public class JDialogConsultaCargaIngreso extends javax.swing.JDialog {

    private boolean hayModificacionesEnIngresosCargadosSinGuardar = false;
    private JDialogAltaIngreso jDialogAltaIngreso;
    private ArrayList<Ingreso> ingresosCargados = new ArrayList<Ingreso>();
    private InstitucionCentralTesoreria institucion;
    private ArrayList< Entry<Miembro, Ingreso>> miembrosYDiezmos = new ArrayList<Entry<Miembro, Ingreso>>(); //solo usado para la consulta desde JDialogAltaIngreso
    private Date fechaDeCulto; //TODO: puede ser utilizado para mostrar la fecha de los diezmos si es pertinente

    public JDialogConsultaCargaIngreso(JFrameSistema parent, boolean modal, InstitucionCentralTesoreria institucionCentralTesoreria) {
        super(parent, modal);
        initComponents();
        jTableIngresos.setDefaultRenderer(Double.class, new ImporteCellRenderer());
        ordenarComponentesConBorderLayout();
        institucion = institucionCentralTesoreria;
        jDialogAltaIngreso = null;
        Vistas.Util.Util.cargaMesesEnCombo(jComboMes);
        Vistas.Util.Util.cargaSecuenciaEnCombo(2010, 2311, jComboAnio);
        Vistas.Util.Util.ocultarAnimacion(jXAnimacionEspera);
        setLocationRelativeTo(null);
    }

    public JDialogConsultaCargaIngreso(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        jTableIngresos.setDefaultRenderer(Double.class, new ImporteCellRenderer());
        ordenarComponentesConBorderLayout();
        Vistas.Util.Util.cargaMesesEnCombo(jComboMes);
        Vistas.Util.Util.cargaSecuenciaEnCombo(2010, 2300, jComboAnio);
        setLocationRelativeTo(null);
    }

    public JDialogConsultaCargaIngreso(JDialogAltaIngreso padre, boolean modal, InstitucionCentralTesoreria institucion,
            ArrayList<Ingreso> ingresos, ArrayList<Entry<Miembro, Ingreso>> miembrosAActualizarParametro) {
        initComponents();
        jTableIngresos.setDefaultRenderer(Double.class, new ImporteCellRenderer());
        jDialogAltaIngreso = padre;
        setModal(modal);
        this.institucion = institucion;
        ingresosCargados = ingresos;
        miembrosYDiezmos = miembrosAActualizarParametro;
        cargarDatosEnTabla(ingresos);
        cargarDiezmosEnLaTabla();
        //aca falta la llamada al nuevo metodo que cargue los diezmos... jejeje. te quero. falta el método. a refactorizar!!!
        jPanelCuerpo.remove(jPnlDatosParaBusqueda);
        jPanelCuerpo.update(jPanelCuerpo.getGraphics());
        pack();
        setLocationRelativeTo(null);
    }

    private void ordenarComponentesConBorderLayout() {
        jPanelCuerpo.add(jPnlDatosParaBusqueda, BorderLayout.NORTH);
        jPanelCuerpo.add(jScroll, BorderLayout.CENTER);
        jPanelCuerpo.add(jPnlButtonera, BorderLayout.SOUTH);
    }

    private boolean hayQueEliminarIngresoEnBaseDeDatos() {
        return jDialogAltaIngreso == null;
    }

    private boolean llamadoParaModificarCarga() {
        return jDialogAltaIngreso != null;
    }

    /**
     *
     * @return <strong> true </strong>si se encontraron ingresos entre el
     * período indicado o <strong> false </strong>si no
     */
    public void buscarAsientosYCargarJTable() {
        int mes = UtilFechas.mesEnNumero(jComboMes.getSelectedItem().toString());
        Integer anio = (Integer) jComboAnio.getSelectedItem();
        ingresosCargados = institucion.buscarIngresos(anio.intValue(), mes);
        DefaultTableModel modelo = getModeloTabla();
        if (modelo.getRowCount() > 0) {
            for (int i = modelo.getRowCount(); i > 0; i--) {
                modelo.removeRow(i - 1);
            }
        }

        if (ingresosCargados == null) {
            JOptionPane.showMessageDialog(this, "Ningún ingreso coincide con el período.", "Aviso", JOptionPane.INFORMATION_MESSAGE);
            return;
        } else {
            try {
                cargarDatosEnTabla(ingresosCargados);
                cargarDiezmosEnLaTabla();
            } catch (NullPointerException ex) {
                JOptionPane.showMessageDialog(this, "Ningún ingreso coincide con el período.", "Aviso", JOptionPane.INFORMATION_MESSAGE);
                removerAllDatosEnTabla();
                return;
            }
        }
    }

    /**
     * @throws NullPointerException Si el ArrayList se encuentra vacío
     */
    private void cargarDatosEnTabla(ArrayList<Ingreso> ingresos) {
        if (ingresos.isEmpty()) {
            return;
        }
        ListIterator<Ingreso> itIngresos = ingresos.listIterator();
        DefaultTableModel modeloTabla = (DefaultTableModel) jTableIngresos.getModel();
        while (itIngresos.hasNext()) {
            Ingreso ingreso = itIngresos.next();
            Vector<String> vector = new Vector<String>(5);
            if (llamadoParaModificarCarga()) {
                vector.add(String.valueOf(itIngresos.previousIndex()));
            } else {
                vector.add(String.valueOf(ingreso.getNroAsiento()));
            }
            vector.add((ingreso.getCuenta().getNombre()));
            vector.add(UtilFechas.getFechaEnFormatoDD_MM_AA(ingreso.getFecha()));
            vector.add(Double.toString(ingreso.getMonto()));
            if (!ingreso.sosDiezmo()) {
                if (ingreso.sosDonacion()) {
                    Donacion donacion = (Donacion) ingreso;
                    if (donacion.tieneDonanteRegistrado()) {
                        vector.add(donacion.getApellidoYNombreDeDonante() + ". Cuit: " + donacion.getDonante().getCuitEnString());
                    } else {
                        vector.add("No registrado");
                    }
                }
            }
            vector.add("-");
            modeloTabla.addRow(vector);
        }

    }

    private void cargarDiezmosEnLaTabla() {
        DefaultTableModel modeloTabla = getModeloTabla();
        GregorianCalendar g = null;
        if (miembrosYDiezmos.isEmpty()) {
            return;
        }
        Iterator<Entry<Miembro, Ingreso>> itMiembros = miembrosYDiezmos.iterator();
        while (itMiembros.hasNext()) {
            Entry<Miembro, Ingreso> miembroYDiezmo = itMiembros.next();
            Vector<String> vector = new Vector<String>(5);
            vector.add(String.valueOf(miembroYDiezmo.getValue().getNroAsiento()));
            vector.add(miembroYDiezmo.getValue().getCuenta().toString());
            vector.add(UtilFechas.getFechaEnFormatoDD_MM_AA(miembroYDiezmo.getValue().getFecha()));
            vector.add(Double.toString(miembroYDiezmo.getValue().getMonto()));
            if (miembroYDiezmo.getKey().esAnonimo()) {
                vector.add("Anonimo");
            } else {
                vector.add(miembroYDiezmo.getKey().getApellidos() + ", " + miembroYDiezmo.getKey().getNombres() + "- identificador: "
                        + miembroYDiezmo.getKey().getIdentificadorPersona());
            }
            modeloTabla.addRow(vector);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXBusyLabel1 = new org.jdesktop.swingx.JXBusyLabel();
        jPanelCuerpo = new javax.swing.JPanel();
        jPnlButtonera = new javax.swing.JPanel();
        jBtnEliminarAsiento = new javax.swing.JButton();
        jBtnModificarAsiento = new javax.swing.JButton();
        jBtnVolver = new javax.swing.JButton();
        jScroll = new javax.swing.JScrollPane();
        jTableIngresos = new javax.swing.JTable();
        jPnlDatosParaBusqueda = new javax.swing.JPanel();
        jXSeparadorDeTitulosBuscarAsiento = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelMes = new javax.swing.JLabel();
        jComboMes = new javax.swing.JComboBox();
        jLabelAnio = new javax.swing.JLabel();
        jComboAnio = new javax.swing.JComboBox();
        jXAnimacionEspera = new org.jdesktop.swingx.JXBusyLabel();
        jBtnBuscar = new javax.swing.JButton();
        jXSeparadorDeTituloAsientos = new org.jdesktop.swingx.JXTitledSeparator();

        jXBusyLabel1.setText("jXBusyLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanelCuerpo.setPreferredSize(new java.awt.Dimension(1130, 548));
        jPanelCuerpo.setLayout(new java.awt.BorderLayout());

        jPnlButtonera.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        jBtnEliminarAsiento.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnEliminarAsiento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Eliminar egreso 20x20.png"))); // NOI18N
        jBtnEliminarAsiento.setText("Eliminar asiento");
        jBtnEliminarAsiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnEliminarAsientoActionPerformed(evt);
            }
        });
        jPnlButtonera.add(jBtnEliminarAsiento);

        jBtnModificarAsiento.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnModificarAsiento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Modificar egreso 20x20.png"))); // NOI18N
        jBtnModificarAsiento.setText("Modificar asiento");
        jBtnModificarAsiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnModificarAsientoActionPerformed(evt);
            }
        });
        jPnlButtonera.add(jBtnModificarAsiento);

        jBtnVolver.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolver.setText("Volver");
        jBtnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverActionPerformed(evt);
            }
        });
        jPnlButtonera.add(jBtnVolver);

        jPanelCuerpo.add(jPnlButtonera, java.awt.BorderLayout.CENTER);

        jScroll.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jScrollFocusGained(evt);
            }
        });

        jTableIngresos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nro. Ingreso", "Cuenta", "Fecha", "Monto", "Miembro/Donante"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableIngresos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTableIngresos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableIngresosMouseClicked(evt);
            }
        });
        jScroll.setViewportView(jTableIngresos);
        jTableIngresos.getColumnModel().getColumn(0).setMinWidth(80);
        jTableIngresos.getColumnModel().getColumn(0).setPreferredWidth(80);
        jTableIngresos.getColumnModel().getColumn(0).setMaxWidth(80);
        jTableIngresos.getColumnModel().getColumn(1).setMinWidth(200);
        jTableIngresos.getColumnModel().getColumn(1).setPreferredWidth(200);
        jTableIngresos.getColumnModel().getColumn(1).setMaxWidth(200);
        jTableIngresos.getColumnModel().getColumn(2).setMinWidth(125);
        jTableIngresos.getColumnModel().getColumn(2).setPreferredWidth(125);
        jTableIngresos.getColumnModel().getColumn(2).setMaxWidth(125);
        jTableIngresos.getColumnModel().getColumn(3).setMinWidth(125);
        jTableIngresos.getColumnModel().getColumn(3).setPreferredWidth(125);
        jTableIngresos.getColumnModel().getColumn(3).setMaxWidth(125);
        jTableIngresos.getColumnModel().getColumn(4).setMinWidth(300);
        jTableIngresos.getColumnModel().getColumn(4).setPreferredWidth(300);
        jTableIngresos.getColumnModel().getColumn(4).setMaxWidth(3000);

        jPanelCuerpo.add(jScroll, java.awt.BorderLayout.PAGE_START);

        jXSeparadorDeTitulosBuscarAsiento.setFont(new java.awt.Font("Tahoma", 0, 14));
        jXSeparadorDeTitulosBuscarAsiento.setHorizontalAlignment(0);
        jXSeparadorDeTitulosBuscarAsiento.setHorizontalTextPosition(0);
        jXSeparadorDeTitulosBuscarAsiento.setTitle("Buscar asiento");

        jLabelMes.setFont(new java.awt.Font("Dialog", 0, 12));
        jLabelMes.setText("Mes:");

        jComboMes.setFont(new java.awt.Font("Dialog", 0, 12));

        jLabelAnio.setFont(new java.awt.Font("Dialog", 0, 12));
        jLabelAnio.setText("Año:");

        jComboAnio.setFont(new java.awt.Font("Dialog", 0, 12));

        jXAnimacionEspera.setText("Espere...");
        jXAnimacionEspera.setFont(new java.awt.Font("Dialog", 0, 12));

        jBtnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Buscar.png"))); // NOI18N
        jBtnBuscar.setText("Buscar");
        jBtnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnBuscarActionPerformed(evt);
            }
        });

        jXSeparadorDeTituloAsientos.setFont(new java.awt.Font("Tahoma", 0, 14));
        jXSeparadorDeTituloAsientos.setHorizontalAlignment(0);
        jXSeparadorDeTituloAsientos.setHorizontalTextPosition(0);
        jXSeparadorDeTituloAsientos.setTitle("Asientos");

        javax.swing.GroupLayout jPnlDatosParaBusquedaLayout = new javax.swing.GroupLayout(jPnlDatosParaBusqueda);
        jPnlDatosParaBusqueda.setLayout(jPnlDatosParaBusquedaLayout);
        jPnlDatosParaBusquedaLayout.setHorizontalGroup(
            jPnlDatosParaBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlDatosParaBusquedaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPnlDatosParaBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelAnio)
                    .addGroup(jPnlDatosParaBusquedaLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jComboAnio, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPnlDatosParaBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlDatosParaBusquedaLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jComboMes, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabelMes))
                .addGap(18, 18, 18)
                .addComponent(jBtnBuscar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jXAnimacionEspera, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(420, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPnlDatosParaBusquedaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXSeparadorDeTituloAsientos, javax.swing.GroupLayout.DEFAULT_SIZE, 955, Short.MAX_VALUE))
            .addComponent(jXSeparadorDeTitulosBuscarAsiento, javax.swing.GroupLayout.DEFAULT_SIZE, 965, Short.MAX_VALUE)
        );
        jPnlDatosParaBusquedaLayout.setVerticalGroup(
            jPnlDatosParaBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlDatosParaBusquedaLayout.createSequentialGroup()
                .addComponent(jXSeparadorDeTitulosBuscarAsiento, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPnlDatosParaBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlDatosParaBusquedaLayout.createSequentialGroup()
                        .addGroup(jPnlDatosParaBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelMes)
                            .addComponent(jLabelAnio))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPnlDatosParaBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboAnio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPnlDatosParaBusquedaLayout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(jPnlDatosParaBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jBtnBuscar)
                            .addComponent(jXAnimacionEspera, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jXSeparadorDeTituloAsientos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanelCuerpo.add(jPnlDatosParaBusqueda, java.awt.BorderLayout.PAGE_END);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelCuerpo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 965, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, 480, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTableIngresosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableIngresosMouseClicked
}//GEN-LAST:event_jTableIngresosMouseClicked

    private void jScrollFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jScrollFocusGained
}//GEN-LAST:event_jScrollFocusGained

    private void jBtnEliminarAsientoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnEliminarAsientoActionPerformed
        Ingreso ingresoAEliminar = null;
        try {
            if (ingresoDeCultoSeleccionado()) {
                ingresoAEliminar = buscarIngresoEnArrayList(jTableIngresos.getSelectedRow());
                if (JOptionPane.showConfirmDialog(this, "¿Esta seguro? Se eliminará (de la carga temporal) el asiento: \n "
                        + ingresoAEliminar.getCuenta().getNombre() + "\n Monto: "
                        + ingresoAEliminar.getMonto(), "Pregunta", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
                    if (hayQueEliminarIngresoEnBaseDeDatos()) {
                        try {
                            institucion.eliminarIngreso(ingresoAEliminar);
                            eliminarIngresoDeColeccionYJTable(ingresoAEliminar);
                        } catch (DAOProblemaConTransaccionException ex) {
                            Util.showMessageErrorEnTransaccion(ex);
                        }
                    }
                    setHayModificacionesEnIngresosCargadosSinGuardar(true);
                } else {
                    Iterator it = miembrosYDiezmos.iterator();
                    while (it.hasNext()) {
                        Entry<Miembro, Ingreso> entry = miembrosYDiezmos.iterator().next();
                        if(entry.getValue().equals(ingresoAEliminar)){
                            it.remove();
                        }
                    }
                }
            } else {
                try {
                    ingresoAEliminar = buscarIngresoEnArrayList(jTableIngresos.getSelectedRow());
                    if (JOptionPane.showConfirmDialog(this, "¿Esta seguro? Se eliminará (de la carga temporal) el asiento: \n "
                            + ingresoAEliminar.getCuenta().getNombre() + "\n Monto: "
                            + ingresoAEliminar.getMonto(), "Pregunta", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
                        if (hayQueEliminarIngresoEnBaseDeDatos()) {
                            try {
                                institucion.eliminarIngreso(ingresoAEliminar);
                            } catch (DAOProblemaConTransaccionException ex) {
                                Util.showMessageErrorEnTransaccion(ex);
                            }
                        }
                        eliminarIngresoDeColeccionYJTable(ingresoAEliminar);
                        setHayModificacionesEnIngresosCargadosSinGuardar(true);
                    }
                } catch (FilaNoSelecionadaException ex) {
                    JOptionPane.showMessageDialog(this, "No ha seleccionado ningun ingreso.");
                    return;
                }
            } //si es un diezmo, borrar para el miembro cargado, el diezmo de la fecha seleccionada (deberá tener de todos modos, un diezmo por culto
        } catch (FilaNoSelecionadaException ex) {
            JOptionPane.showMessageDialog(this, "No ha seleccionado ningun ingreso.");
        }


    }//GEN-LAST:event_jBtnEliminarAsientoActionPerformed

    private void jBtnModificarAsientoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnModificarAsientoActionPerformed
        int filaSeleccionada = jTableIngresos.getSelectedRow();
        Ingreso ingresoAModificar = null;
        if (llamadoParaModificarCarga()) {
            try {
                ingresoAModificar = buscarIngresoEnArrayList(filaSeleccionada);
            } catch (FilaNoSelecionadaException ex) {
                JOptionPane.showMessageDialog(this, "No ha seleccionado ningun ingreso.");
            }
        } else {
            Entry<Miembro, Diezmo> miembroAModificar = null;
            boolean miembroSeleccionado = false;
            try {
                miembroSeleccionado = ingresoDeCultoSeleccionado();
            } catch (FilaNoSelecionadaException ex) {
                JOptionPane.showMessageDialog(this, "No ha seleccionado ningun ingreso.");
                return;
            }
            try {
                ingresoAModificar = buscarIngresoEnArrayList(filaSeleccionada);//aca
            } catch (FilaNoSelecionadaException ex) {
                JOptionPane.showMessageDialog(this, "No ha seleccionado ninguna fila.");
                return;
            }
        }
        JDialogModificarAsientoIngreso jDialogModificarAsientoIngreso;
        if (llamadoParaModificarCarga()) {
            jDialogModificarAsientoIngreso = new JDialogModificarAsientoIngreso(this, true,
                    ingresoAModificar, institucion, true);
            setHayModificacionesEnIngresosCargadosSinGuardar(true);
        } else {
            jDialogModificarAsientoIngreso = new JDialogModificarAsientoIngreso(this, true,
                    ingresoAModificar, institucion, false);
        }
        jDialogModificarAsientoIngreso.setVisible(true);

    }//GEN-LAST:event_jBtnModificarAsientoActionPerformed

    private void jBtnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVolverActionPerformed
        if (!llamadoDesdeJFrameSistema()) {
            if (ingresosCargados != null) {
                jDialogAltaIngreso.setIngresosCargados(ingresosCargados);
            }
            if (miembrosYDiezmos != null && !miembrosYDiezmos.isEmpty()) {
                jDialogAltaIngreso.setMiembrosConDiezmo(miembrosYDiezmos);
            }
        }


        dispose();
    }//GEN-LAST:event_jBtnVolverActionPerformed

    private void jBtnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnBuscarActionPerformed
        Vistas.Util.Util.mostrarAnimacion(jXAnimacionEspera);
        buscarAsientosYCargarJTable();
        Vistas.Util.Util.ocultarAnimacion(jXAnimacionEspera);
    }//GEN-LAST:event_jBtnBuscarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogConsultaCargaIngreso dialog = new JDialogConsultaCargaIngreso(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnBuscar;
    private javax.swing.JButton jBtnEliminarAsiento;
    private javax.swing.JButton jBtnModificarAsiento;
    private javax.swing.JButton jBtnVolver;
    private javax.swing.JComboBox jComboAnio;
    private javax.swing.JComboBox jComboMes;
    private javax.swing.JLabel jLabelAnio;
    private javax.swing.JLabel jLabelMes;
    private javax.swing.JPanel jPanelCuerpo;
    private javax.swing.JPanel jPnlButtonera;
    private javax.swing.JPanel jPnlDatosParaBusqueda;
    private javax.swing.JScrollPane jScroll;
    private javax.swing.JTable jTableIngresos;
    private org.jdesktop.swingx.JXBusyLabel jXAnimacionEspera;
    private org.jdesktop.swingx.JXBusyLabel jXBusyLabel1;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloAsientos;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTitulosBuscarAsiento;
    // End of variables declaration//GEN-END:variables

    private Ingreso buscarIngresoEnArrayList(int filaSeleccionada) throws FilaNoSelecionadaException {
        boolean esDiezmoUOfrenda = getModeloTabla().getValueAt(filaSeleccionada, 1).toString().contains("Diezmo") ||
                getModeloTabla().getValueAt(filaSeleccionada, 1).toString().contains("Ofrenda");
        if (filaSeleccionada == -1) {
            throw new Excepciones.FilaNoSelecionadaException();
        }
        if (llamadoParaModificarCarga() && !esDiezmoUOfrenda) {
            return ingresosCargados.get(filaSeleccionada);
            //TODO faltaria arreglar el problema de agregar diezmos y ofrendas en diferentes colecciones
        } else if (llamadoParaModificarCarga() && esDiezmoUOfrenda) {
            return getIngresoDeCultoSeleccionado();
        }
        Ingreso retorno = null;
        long nroAsiento = Long.valueOf(getModeloTabla().getValueAt(filaSeleccionada, 0).toString());
        Iterator<Ingreso> it = ingresosCargados.iterator();
        while (it.hasNext()) {
            Ingreso ingreso = it.next();
            if (nroAsiento == ingreso.getNroAsiento()) {
                retorno = ingreso;
            }
        }

        /*
         * if (tipoCuenta.contains("Donacion") ||
         * tipoCuenta.contains("donacion")) { //es una donacion
         * Iterator<Ingreso> it = ingresosCargados.iterator(); while
         * (it.hasNext()) { Ingreso ingreso = it.next(); if
         * (!ingreso.sosDonacion()) { continue; } Donacion donacion = (Donacion)
         * ingreso; if (campoMiembroODonante.equals("No registrado")) { if
         * (nroAsiento == donacion.getNroAsiento()) { return donacion; } } } }
         * else {
         */
        return retorno;
    }

    public boolean isHayModificacionesEnIngresosCargadosSinGuardar() {
        return hayModificacionesEnIngresosCargadosSinGuardar;
    }

    public void setHayModificacionesEnIngresosCargadosSinGuardar(boolean hayModificacionesEnIngresosCargadosSinGuardar) {
        this.hayModificacionesEnIngresosCargadosSinGuardar = hayModificacionesEnIngresosCargadosSinGuardar;
    }

    public JDialogAltaIngreso getPadre() {
        return jDialogAltaIngreso;
    }

    public void setPadre(JDialogAltaIngreso padre) {
        jDialogAltaIngreso = padre;
    }

    public ArrayList<Ingreso> getIngresosCargados() {
        return ingresosCargados;
    }

    public void setIngresosCargados(ArrayList<Ingreso> ingresosCargados) {
        this.ingresosCargados = ingresosCargados;
    }

    public InstitucionCentralTesoreria getInstitucion() {
        return institucion;
    }

    public void setInstitucion(InstitucionCentralTesoreria institucion) {
        this.institucion = institucion;
    }

    private void guardarCambios() {
        Ingreso ingresoQueModificara = null;
        Iterator it = ingresosCargados.iterator();
        int fila = 0;
        int columna = 0;
        DefaultTableModel d = (DefaultTableModel) jTableIngresos.getModel();
        for (fila = 0; fila <= jTableIngresos.getModel().getRowCount(); fila++) {
            while (it.hasNext()) {
                Ingreso ingreso = (Ingreso) it.next();
                it.remove();
                if (ingreso.getNroAsiento() == Long.valueOf(String.valueOf(d.getValueAt(fila, columna)))) {
                    ingresoQueModificara = ingreso;
                    break;
                }
            }
            for (columna = 0; columna <= jTableIngresos.getModel().getColumnCount(); fila++) {
                Object dato = d.getValueAt(fila, columna);
                agregarDatoAEgreso(jTableIngresos.getColumnName(columna), dato, ingresoQueModificara);

            }
            ingresosCargados.add(ingresoQueModificara);
        }
    }

    private void agregarDatoAEgreso(String columna, Object dato, Ingreso ingresoQueModificara) {
        if (columna.equalsIgnoreCase("Codigo")) {
            ingresoQueModificara.setNroAsiento((Integer) dato);
        }
        if (columna.equalsIgnoreCase("Cuenta")) {
            ingresoQueModificara.getCuenta().setNombre(String.valueOf(dato));
            ingresoQueModificara.getCuenta().setCodigo(Integer.parseInt(String.valueOf(dato)));
        }
        if (columna.equalsIgnoreCase("Fecha")) {
            ingresoQueModificara.setFecha((GregorianCalendar) dato);
        }
        if (columna.equalsIgnoreCase("Monto")) {
            ingresoQueModificara.setMonto(Double.valueOf(String.valueOf(dato)));
        }
    }

    void guardarModificacionEnColeccionYEnJTable(Ingreso ingresoModificado) {
        DefaultTableModel modelo = getModeloTabla();
        int filaSeleccionada = jTableIngresos.getSelectedRow();
        modelo.setValueAt(ingresoModificado.getNroAsiento(), filaSeleccionada, 0);
        modelo.setValueAt(ingresoModificado.getCuenta().getNombre(), filaSeleccionada, 1);
        modelo.setValueAt(ingresoModificado.getMonto(), filaSeleccionada, 3);
        modelo.setValueAt(UtilFechas.getFechaEnFormatoDD_MM_AA(ingresoModificado.getFecha()),
                filaSeleccionada, 2);
        boolean miembroSeleccionado = false;
        try {
            miembroSeleccionado = ingresoDeCultoSeleccionado();
        } catch (FilaNoSelecionadaException e) {
        }
        if (!miembroSeleccionado) {
            if (ingresosCargados.contains(ingresoModificado)) {
                ingresosCargados.remove(ingresoModificado);
                ingresosCargados.add(ingresoModificado);
            } else {
                ingresosCargados.add(ingresoModificado);
            }
        } else {
            miembrosYDiezmos.get(filaSeleccionada).setValue((Diezmo) ingresoModificado);
        }
    }

    private boolean llamadoDesdeJFrameSistema() {
        if (jDialogAltaIngreso == null) {
            return true;
        } else {
            return false;
        }
    }

    private void eliminarIngresoDeColeccionYJTable(Ingreso ingresoAEliminar) {
        int filaSeleccionada = jTableIngresos.getSelectedRow();
        DefaultTableModel modeloTabla = getModeloTabla();
        modeloTabla.removeRow(filaSeleccionada);
        if(ingresoAEliminar instanceof Ofrenda || ingresoAEliminar instanceof Diezmo){
            miembrosYDiezmos.remove(filaSeleccionada);
        }else{
            ingresosCargados.remove(filaSeleccionada);
        }
    }

    private DefaultTableModel getModeloTabla() {
        DefaultTableModel modeloTabla = (DefaultTableModel) jTableIngresos.getModel();
        return modeloTabla;
    }

    void guardarModificacionDeIngresoEnColeccionYEnJTableYEnBd(Ingreso ingresoModificado) {
        try {
            institucion.actualizarIngreso(ingresoModificado);
            guardarModificacionEnColeccionYEnJTable(ingresoModificado);
        } catch (DAOProblemaConTransaccionException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private boolean ingresoDeCultoSeleccionado() throws FilaNoSelecionadaException {
        boolean retorno = false;
        if (jTableIngresos.getSelectedRow() == -1) {
            throw new FilaNoSelecionadaException();
        }
        String tipoCuenta = getModeloTabla().getValueAt(jTableIngresos.getSelectedRow(), 1).toString();
        if (!tipoCuenta.contains("Diezmo".toLowerCase()) || !tipoCuenta.contains("Ofrenda".toLowerCase())) {
            return retorno;
        } else {
            return true;
        }
    }

    private Entry<Miembro, Ingreso> getMiembroSeleccionado(int filaSeleccionada) throws FilaNoSelecionadaException {
        Entry<Miembro, Ingreso> retorno = null;
        if (filaSeleccionada == -1) {
            throw new FilaNoSelecionadaException();
        }
        long idDeMiembro = getIdDeMiembroSeleccionado();
        Iterator<Entry<Miembro, Ingreso>> it = miembrosYDiezmos.iterator();
        while (it.hasNext()) {
            Entry<Miembro, Ingreso> miembroYDiezmo = it.next();
            if (miembroYDiezmo.getKey().getIdentificadorPersona() == idDeMiembro) {
                if (miembroYDiezmo.getKey().esAnonimo()) {
                    if (miembroYDiezmo.getValue().getMonto()
                            == Double.parseDouble(getModeloTabla().getValueAt(jTableIngresos.getSelectedRow(), 3).toString())) {
                        retorno = miembroYDiezmo;
                        break;
                    }
                } else {
                    retorno = miembroYDiezmo;
                    break;
                }
            }
        }
        return retorno;
    }

    private void eliminarMiembroYSuDiezmoDeArrayListYJTable(Entry<Miembro, Diezmo> miembroAEliminar) {
        miembrosYDiezmos.remove(miembroAEliminar);
        getModeloTabla().removeRow(jTableIngresos.getSelectedRow());
    }

    private void removerAllDatosEnTabla() {
        Util.removeAllFilasEnTabla((DefaultTableModel) jTableIngresos.getModel());
    }

    private Ingreso getIngresoDeCultoSeleccionado() throws FilaNoSelecionadaException {
        Iterator<Entry<Miembro, Ingreso>> it = miembrosYDiezmos.iterator();
        Ingreso retorno = null;
        if (!llamadoParaModificarCarga()) {
            while (it.hasNext()) {
                Entry<Miembro, Ingreso> entry = it.next();
                if (entry.getValue().getNroAsiento() == getNroAsientoSeleccionado()) {
                    retorno = entry.getValue();
                }
            }
        } else {
            Entry<Miembro, Ingreso> miembroYDiezmoSeleccionado = getMiembroSeleccionado(jTableIngresos.getSelectedRow());
            retorno = miembroYDiezmoSeleccionado.getValue();
        }
        return retorno;
    }

    public Long getNroAsientoSeleccionado() {
        return Long.valueOf(getModeloTabla().getValueAt(jTableIngresos.getSelectedRow(), 0).toString());
    }

    private long getIdDeMiembroSeleccionado() {
        long idDeMiembro = 0;
        String campoMiembro = getCampoMiembro();
        if (isMiembroAnonimoSeleccionado(campoMiembro)) {
            idDeMiembro = institucion.getMiembroAnonimo().getIdentificadorPersona();
        } else {
            idDeMiembro = Long.parseLong(campoMiembro.substring(campoMiembro.indexOf(": "), campoMiembro.length()).replaceAll(": ", "")); //TODO: horroroso!
        }
        return idDeMiembro;
    }

    private String getCampoMiembro() {
        return getModeloTabla().getValueAt(jTableIngresos.getSelectedRow(), 4).toString();
    }

    private boolean isMiembroAnonimoSeleccionado(String campoMiembro) {
        return campoMiembro.toLowerCase().contains("anonimo");
    }
}
