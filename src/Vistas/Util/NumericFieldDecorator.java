package Vistas.Util;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class NumericFieldDecorator extends KeyAdapter {

    private boolean conDecimales;
    private JTextField componente;

    public NumericFieldDecorator(boolean conDecimales, JTextField componente) {
        this.conDecimales = conDecimales;
        this.componente = componente;
    }

    @Override
    public void keyTyped(KeyEvent evt) {
        if ((evt.isControlDown()) || (evt.isAltDown())) {
            return;
        }
        int keyChar = (int) evt.getKeyChar();
        if (!isValidKey(keyChar)) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            return;
        }
        if (isPeriodKey(keyChar)) {
            if (conDecimales) {
                checkOneDecimalSeparatorOnly(evt);
            } else {
                evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            }
        }
    }

    private void checkOneDecimalSeparatorOnly(KeyEvent evt) {
        JTextField numericTextField = (JTextField) evt.getSource();
        if (numericTextField.getText().contains(".")) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
        }
    }

    private boolean isPeriodKey(int keyChar) {
        return keyChar == KeyEvent.VK_PERIOD;
    }

    private boolean isValidKey(int keyChar) {
        return isNumberKey(keyChar) || isControlKey(keyChar);
    }

    private boolean isNumberKey(int keyChar) {
        return (keyChar >= KeyEvent.VK_0 && keyChar <= KeyEvent.VK_9) || 
                ((keyChar == KeyEvent.VK_MINUS ||keyChar ==  KeyEvent.VK_SUBTRACT) && 
                componente.getText().length()>=1 ? !componente.getText().substring(0,1).contains("-") : true);
    }

    private boolean isControlKey(int keyChar) {
        return keyChar == KeyEvent.VK_BACK_SPACE || keyChar == KeyEvent.VK_DELETE || keyChar == KeyEvent.VK_ENTER || keyChar == KeyEvent.VK_ESCAPE;
    }
}