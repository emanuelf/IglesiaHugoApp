package Vistas.Util;

import java.text.Format;
import java.text.NumberFormat;
import javax.swing.table.DefaultTableCellRenderer;

public class ImporteCellRenderer extends DefaultTableCellRenderer {

    private Format formatter = NumberFormat.getCurrencyInstance();

    @Override
    protected void setValue(Object value) {
        if(value.toString().equals("-")) return; //por requerimientos de este sistema
        setText((value == null) ? "" : getFormatter().format(Double.valueOf(value.toString())));
    }

    private Format getFormatter() {
        return formatter;
    }
}