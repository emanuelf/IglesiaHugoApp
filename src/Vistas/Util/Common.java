package Vistas.Util;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.Filial;
import Modelo.InstitucionCentralTesoreria;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JComboBox;

/**
 *
 * @author Emanuel
 */
public class Common {
    
  public static void cargarFilialesEnCombo(InstitucionCentralTesoreria institucion, JComboBox jCombo) throws DAOProblemaConTransaccionException {
      ArrayList<Filial> filiales = institucion.getFiliales();
      Iterator <Filial> itFiliales = filiales.iterator();
        while (itFiliales.hasNext()) {
            Filial filial = itFiliales.next();
            jCombo.addItem(filial);
        }
    }
    
}
