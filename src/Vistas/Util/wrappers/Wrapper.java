package Vistas.Util.wrappers;

import Vistas.Util.Vectorizable;

public abstract class Wrapper<T> implements Vectorizable{

    protected T objetoWrapeado;
            
    public Wrapper(T objetoAWrapear) {
        this.objetoWrapeado = objetoAWrapear;
    }
    
    public int getColumnCount(){
        return toRow().length;
    }

    public T getObjetoWrapeado() {
        return objetoWrapeado;
    }
}