package Vistas.Util;

import Excepciones.DAOProblemaConTransaccionException;
import Util.UtilFechas;
import java.awt.Component;
import java.awt.event.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import org.jdesktop.swingx.JXBusyLabel;

/**
 *
 * @author Emanuel
 */
public class Util {

    public static void mostrarAnimacion(JXBusyLabel animacion) {
        animacion.setVisible(true);
        animacion.setBusy(true);
    }

    private static boolean isUnPunto(char keyChar) {
        return keyChar == '.';
    }

    private static boolean isUnMinus(char keyChar) {
        return keyChar == '-';
    }

    private void configurarEscapeParaCerrar() {
        KeyStroke esc = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
        // rootPane.registerKeyboardAction(this, "CERRAR_SI_ESCAPE", esc, JComponent.WHEN_IN_FOCUSED_WINDOW);
    }

    /**
     *
     * @param evt <p> El KeyEvent necesario para conocer que tecla fue pulsada
     * </p>
     * @param jTextFieldAFormatear <p> El JTextField que formateará al estilo
     * moneda
     *
     * <p> Este evento formatea un JTextField para que al ingresarse datos,
     * rechaze los caracteres indeseados, y presente solo números y un signo '$'
     * al comienzo </p>
     *
     * <strong> Utilice este metodo en eventos KeyTiped y KeyReleased (en los
     * dos sino en caso de que le interese el efecto de borrar el signo peso al
     * borrar el último número) y el hará todo lo que usted necesita <strong>
     */
    public static void formatearJTextFieldTipoMonedaLuegoDeUnKeyEvent(java.awt.event.KeyEvent evt, JTextField jTextFieldAFormatear) {
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE && jTextFieldAFormatear.getText().length() <= 1) {
            jTextFieldAFormatear.setText("");
            return;
        }
        if ((jTextFieldAFormatear.getText().isEmpty()) && evt.getKeyChar() == '$') {
            return;
        }
        if (Vistas.Util.Util.esNumero(evt.getKeyChar()) && jTextFieldAFormatear.getText().isEmpty()) {
            jTextFieldAFormatear.setText("$");
            jTextFieldAFormatear.setCaretPosition(jTextFieldAFormatear.getText().length());
        }
        ignorarSiNoEsDouble(evt, jTextFieldAFormatear.getText());
    }

    public static String formatearTipoMoneda(double monto) {
        if (monto == 0) {
            return "0";
        }
        return java.text.DecimalFormat.getCurrencyInstance(Locale.US).format(monto);
    }

    /**
     *
     * @param jTextField
     *
     * <p> Este método debe ser utilizado en conjunto con
     * formatearJTextFieldTipoMonedaLuegoDeUnKeyEvent. </p> <p> Es método
     * formatea el texto que quizás tiene más de los decimales permitidos para
     * monedas (dos decimales) </p>
     */
    public static void redondearUnMontoADosCifras(JTextField jTextField) {
        if (jTextField.getText().isEmpty() == true) {
            return;
        } else {
            String textoFormateado = jTextField.getText().replace("$", "");
            DecimalFormat hola = new DecimalFormat("0.00");
            double doble = Double.parseDouble(textoFormateado);
            textoFormateado = hola.format(doble);
            textoFormateado = "$" + textoFormateado;
            jTextField.setText(textoFormateado);
            jTextField.setText(jTextField.getText().replace(",", "."));
        }
    }

    public static boolean isEmpty(JTextField[] campos) {
        boolean retorno = false;
        for (JTextField i : campos) {
            if (i == null || i.getText().isEmpty()) {
                retorno = true;
                break;
            }
        }
        return retorno;
    }

    public static double getMontoSinSimbolo(String montoConSimbolo) {
        if (montoConSimbolo.contains(",")) {
            montoConSimbolo.replace(",", ".");
        }
        return Double.parseDouble(montoConSimbolo.replace("$", ""));
    }

    public static boolean hayJComboVacios(JComboBox[] jCombos) {
        boolean retorno = false;
        for (JComboBox i : jCombos) {
            if (i != null && i.getSelectedIndex() == -1) {
                retorno = true;
                break;
            }
        }
        return retorno;

    }

    public static void ocultarAnimacion(JXBusyLabel animacion) {
        animacion.setVisible(false);
        animacion.setBusy(false);
    }

    public static boolean hayUnJRadioButtonSeleccionado(JRadioButton[] jRadioButtons) {
        boolean retorno = false;
        for (JRadioButton i : jRadioButtons) {
            if (i.isSelected()) {
                retorno = true;
                break;
            }
        }
        return retorno;
    }

    public static void cargaMesesEnCombo(JComboBox jComboBox) {
        jComboBox.removeAllItems();
        int mes = 0;
        String mesString = null;
        for (mes = 0; mes < 12; mes++) {
            mesString = UtilFechas.getMesEnString(mes);
            jComboBox.addItem(mesString);
        }
    }

    /**
     * Este metodo carga una secuencia de numeros en el jComboBox pasado como
     * parámetro. Si
     * <code> desde </code> >
     * <code> hasta</hasta> el orden se invierte, y la secuencia pasa a ser hasta-desde.
     *
     * @param desde
     * @param hasta
     * @param jComboBox
     */
    public static void cargaSecuenciaEnCombo(int desde, int hasta, JComboBox jComboBox) {
        jComboBox.removeAllItems();
        int i;
        if (desde > hasta) {
            int auxiliar = desde;
            desde = hasta;
            hasta = auxiliar;
        }
        for (i = desde; i < hasta + 1; i++) {
            jComboBox.addItem(i);
        }
    }

    public static void ignorarSiNoEsEntero(KeyEvent evt) {
        if (!(esNumero(Character.valueOf(evt.getKeyChar())))) {
            evt.consume();
            if (evt.getSource() instanceof JTextField) {
                JTextField jText = (JTextField) evt.getSource();
                try {
                    long numeroParseado = Long.parseLong(jText.getText());
                    if (numeroParseado > Integer.MAX_VALUE) {
                        evt.consume();
                    }
                } catch (NumberFormatException ex) {
                    evt.consume();
                }
            }
        }
    }

    public static boolean esNumero(Character caracter) {
        if (Character.isDigit(caracter)) {
            return true;
        } else {
            return false;
        }
    }

    public static void ignorarSiNoEsDouble(KeyEvent evt, String numero) {
        char caracter = evt.getKeyChar();
        if(esLetra(caracter) && caracter != '-' && caracter != '.') evt.consume();
        if(!esNumero(Character.valueOf(evt.getKeyChar()))){
            if(isUnPunto(caracter)){
                if(numero.contains(".")) evt.consume();
            }
            if(isUnMinus(caracter)){
                if(!numero.isEmpty()) evt.consume();
            }
        }
    }

    public static void siempreCursorAlFinal(JTextField campo) {
        campo.setCaretPosition(campo.getText().length());
    }

    public static boolean masDeUnPunto(String cadena) {
        char[] transformadaEnVector = cadena.toCharArray();
        int i;
        boolean retorno = false;
        int contador = 0;
        for (i = 0; i < transformadaEnVector.length; i++) {
            if (transformadaEnVector[i] == '.') {
                contador++;
                if (contador >= 1) {
                    retorno = true;
                    break;
                }
            }
        }
        return retorno;
    }

    public static boolean esLetra(char caracter) {
        if (!(esNumero(caracter) || caracter == '.')) {
            return true;
        } else {
            return false;
        }
    }

    public static void ignorarSiNoEsLetra(KeyEvent evt) {
        if (!esLetra(evt.getKeyChar())) {
            evt.consume();
        }
    }

    public static void limpiarCamposJText(ArrayList<JTextField> arrayListJTextField) {
        JTextField auxiliar = null;
        Iterator it = arrayListJTextField.iterator();
        while (it.hasNext()) {
            JTextField jTextField = (JTextField) it.next();
            jTextField.setText("");
        }
    }

    public static void cargarItemsDeComboBox(JComboBox jComboBox, Collection<String> items) {
        // Codificar
    }
    
    public static void limpiar(JTextComponent[] components){
        for (JTextComponent jTextComponent : components) {
            jTextComponent.setText("");
        }
    }
    
    public static void setEnabled(java.awt.Component[] components, boolean deshabilitarOHabilitar) {
        for (java.awt.Component component : components) {
            component.setEnabled(deshabilitarOHabilitar);
        }
    }

    public static void salirSiPresionaEscape(JDialog contenedor, KeyEvent evt) {
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            contenedor.dispose();
        }
    }

    public static void salirSiPresionaEscape(JFrame contenedor, KeyEvent evt) {
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            contenedor.dispose();
        }
    }

    public static void limitarCaracteres(KeyEvent evt, String jTxtDireccionCalle, int i) {
        if (jTxtDireccionCalle.length() >= i) {
            evt.consume();
        }
    }

    public static boolean hayCamposEnCero(String text, String text0) {
        if (Double.valueOf(text) == 0 || Double.valueOf(text0) == 0) {
            return true;
        }
        return false;

    }

    public static void limpiarTextoSiSoloHaySigno(JTextField jTxtMonto) {
        if (jTxtMonto.getText().length() <= 1 && jTxtMonto.getText().equals("$")) {
            jTxtMonto.setText("");
        }
    }

    public static void deshabilitarComponentes(Component[] componentes) {
        for (Component i : componentes) {
            i.setEnabled(false);
        }
    }

    public static void removeAllFilasEnTabla(DefaultTableModel modeloTabla) {
        for (int i = modeloTabla.getRowCount(); i < 0; i--) {
            modeloTabla.removeRow(i);
        }
    }

    public static void showMessageErrorEnTransaccion(DAOProblemaConTransaccionException ex) {
        JOptionPane.showMessageDialog(null, ex.getMessage(), "Error en base de datos", JOptionPane.ERROR_MESSAGE);
    }
}
