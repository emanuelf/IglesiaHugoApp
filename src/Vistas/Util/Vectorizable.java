package Vistas.Util;

public interface Vectorizable{
   
    public Object[] toRow();
    public void setValueAt(int column, Object aValue);
    public boolean isCellEditable(int column);
}
