package Vistas.Util;

import Vistas.Util.wrappers.Wrapper;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class CustomTableModel<T extends Wrapper> extends AbstractTableModel {

    private boolean editable = true;
    private String[] columnas; // el nombre de las columnas de las tablas de las vistas
    private final List<T> datos; //Las instancias de T (Vectorizables en nuestro caso) que se cargan en las tablas de las vistas

    /**
     * usa un ArrayList como lista de Datos
     *
     * @param columnas los labels de la cabecera de la tabla
     */
    public CustomTableModel(String[] columnas) {
        this.columnas = columnas;
        datos = new ArrayList<T>();
    }

    //<editor-fold defaultstate="collapsed" desc="Standard Getters & Setters">
    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    protected boolean isEditable() {
        return editable;
    }
    //</editor-fold>

    @Override
    public int getRowCount() {
        return datos.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnas[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (datos.isEmpty())
            return Object.class;
        else {
            Object data = datos.get(0).toRow()[columnIndex];
            return data == null ? Object.class : data.getClass();
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return editable && datos.get(rowIndex).isCellEditable(columnIndex);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        T get = datos.get(rowIndex);
        return get.toRow()[columnIndex];
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        datos.get(rowIndex).setValueAt(columnIndex, aValue);
        fireTableRowsUpdated(rowIndex, rowIndex);
    }

    public boolean addRow(T value) {
        datos.add(value);
        if (value.toRow().length != columnas.length)
            throw new RuntimeException("Hay diferente numero de columnas");
        fireTableRowsInserted(datos.size() - 1, datos.size() - 1);
        return true;
    }

    public boolean addRow(int index, T value) {
        datos.add(index, value);
        fireTableRowsInserted(index, index);
        return true;
    }

    public boolean addRows(Collection<T> values) {
        int lastRow = datos.size() - 1;
        if (datos.addAll(values)) {
            fireTableRowsInserted(lastRow, datos.size() - 1);
            return true;
        } else
            return false;
    }

    public void setRow(int index, T value) {
        datos.set(index, value);
    }

    public T removeRow(int row) {
        T removed = datos.remove(row);
        fireTableRowsDeleted(row, row);
        return removed;
    }

    public void clear() {
        if (!datos.isEmpty()) {
            int lastRow = datos.size() - 1;
            datos.clear();
            fireTableRowsDeleted(0, lastRow);
        }
    }

    public void setDatos(Collection<T> datos) {
        clear();
        if (!datos.isEmpty())
            this.datos.addAll(datos);
        fireTableDataChanged();
    }

    public final List<T> getDatos() {
        return Collections.unmodifiableList(datos);
    }
}