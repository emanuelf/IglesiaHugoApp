package Vistas.presentadores;

import Modelo.Miembro;
import Vistas.JDialogAltaMiembro;
import Vistas.JPnlAbmMiembros;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Emanuel
 */
public class MiembroPresenter extends Presenter {

    private final JPnlAbmMiembros abmMiembros = new JPnlAbmMiembros();
    private static MiembroPresenter INSTANCE;
    private JDialogAltaMiembro altaMiembro = new JDialogAltaMiembro();


    private MiembroPresenter() {
        super();
        window.setContentPane(abmMiembros);
    }

    public static MiembroPresenter getInstance(){
        if(INSTANCE == null){
            INSTANCE = new MiembroPresenter();
        }
        return INSTANCE;
    }

    public void presentarAbm() {
        abmMiembros.setModo(PRESENTACION.ABM);
        presentar();
    }

    /**
     * @return el miembro agregado o null en caso de que no se haya agregado ningun miembro
     */
    public Miembro presentarAlta(){
        altaMiembro.setModal(true);
        altaMiembro.setVisible(true);
        altaMiembro.setLlamadoParaModificarUnMiembro(false);
        return altaMiembro.getUltimoAgregado();
    }

    public Miembro presentarSeleccionMiembro(){
        abmMiembros.setModo(PRESENTACION.SELECCION_MIEMBRO);
        window.setModal(true);
        presentar();
        return miembroSeleccionado();
    }

    public Miembro presentarModificacion(Miembro miembroAModificar){
        altaMiembro.setModal(true);
        altaMiembro.setLlamadoParaModificarUnMiembro(true);
        altaMiembro.setAModificar(miembroAModificar);
        altaMiembro.setVisible(true);
        return altaMiembro.getModificado();
    }

    public Miembro miembroSeleccionado(){
        return abmMiembros.getMiembroSeleccionado();
    }

    public enum PRESENTACION {

        ABM,
        SELECCION_MIEMBRO,
    }
}
