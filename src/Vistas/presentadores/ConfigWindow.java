package Vistas.presentadores;

import java.awt.Panel;

import javax.swing.JPanel;

/**
 *
 * @author Emanuel
 */
public abstract class ConfigWindow {

    private String titulo;

    public ConfigWindow(String titulo) {
        this.titulo = titulo;
    }

    public String getTitulo() {
        return titulo;
    }
}
