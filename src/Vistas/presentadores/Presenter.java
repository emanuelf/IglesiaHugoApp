package Vistas.presentadores;


import javax.swing.JDialog;

/**
 * @author Emanuel
 */
public abstract class Presenter {

    protected JDialog window;

    public Presenter() {
        window = new JDialog();
    }

    protected void presentar() {
        window.pack();
        window.setLocationRelativeTo(null);
        window.setVisible(true);
    }
}
