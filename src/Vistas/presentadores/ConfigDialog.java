package Vistas.presentadores;

import java.awt.Window;

/**
 *
 * @author Emanuel
 */
public class ConfigDialog extends ConfigWindow{

    private Window owner;
    private boolean modal;

    public ConfigDialog(String titulo, Window owner, boolean modal) {
        super(titulo);
        this.owner = owner;
        this.modal = modal;
    }

    public Window getOwner() {
        return owner;
    }

    public boolean modal() {
        return modal;
    }
}
