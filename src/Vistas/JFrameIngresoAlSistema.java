package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Excepciones.ObjetoNoEncontradoException;
import Modelo.AdministradorDeInicio;
import Modelo.InstitucionCentral;
import Modelo.InstitucionCentralSecretaria;
import Modelo.InstitucionCentralTesoreria;
import Modelo.Usuario;
import java.awt.Color;
import javax.swing.JOptionPane;
import org.postgresql.util.PSQLException;

/**
 *
 * @author emanuel
 */
public class JFrameIngresoAlSistema extends javax.swing.JFrame {

    AdministradorDeInicio admin;

    public JFrameIngresoAlSistema(InstitucionCentral institucionCentral) {
        initComponents();
        admin = institucionCentral;

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXPnlLoguin = new org.jdesktop.swingx.JXLoginPane();
        jXButtonIngresar = new org.jdesktop.swingx.JXButton();
        jXLblAviso = new org.jdesktop.swingx.JXLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(204, 204, 204));
        setResizable(false);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        jXPnlLoguin.setBannerText("Ingreso al sistema");
        jXPnlLoguin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jXPnlLoguinKeyPressed(evt);
            }
        });

        jXButtonIngresar.setText("Ingresar");
        jXButtonIngresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jXButtonIngresarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jXPnlLoguin, javax.swing.GroupLayout.DEFAULT_SIZE, 477, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addComponent(jXLblAviso, javax.swing.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)
                .addGap(150, 150, 150))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(394, Short.MAX_VALUE)
                .addComponent(jXButtonIngresar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jXPnlLoguin, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jXLblAviso, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jXButtonIngresar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private boolean hayCamposVacios() {
        boolean retorno = false;
        if (jXPnlLoguin.getUserName().isEmpty() || jXPnlLoguin.getPassword().length == 0) {
            retorno = true;
        }
        return retorno;
    }

    private void avisarQueHayCamposVacios() {
        this.jXLblAviso.setForeground(Color.red);
        this.jXLblAviso.setText("Ingre el nombre de usuario y contraseña");
    }
    
    private void jXButtonIngresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jXButtonIngresarActionPerformed
        if (hayCamposVacios()) {
            avisarQueHayCamposVacios();
            return;
        } else {
            jXLblAviso.setText(null);
            try {
                Usuario usuario =
                        admin.ingresarAlSistema(jXPnlLoguin.getUserName(),
                        String.copyValueOf(jXPnlLoguin.getPassword()));
                if(!usuario.esMiContrasenia(String.valueOf(jXPnlLoguin.getPassword()))){
                    setAvisoDeUsuarioYOContraseniaIncorrectos();
                    return;
                }
                InstitucionCentral institucionCentral = null;
                if (usuario.sosTesorero()) {
                    try {
                        institucionCentral = new InstitucionCentralTesoreria();
                        institucionCentral.setUsuarioConectado(usuario);
                    } catch (DAOProblemaConTransaccionException ex) {
                        JOptionPane.showMessageDialog(this, ex.getMessage());
                    }catch(PSQLException ex){
                        ex.printStackTrace();
                    }
                }
                if (!usuario.sosTesorero()) {
                    try {
                        institucionCentral = new InstitucionCentralSecretaria();
                    } catch (DAOProblemaConTransaccionException ex) {
                        JOptionPane.showMessageDialog(this, ex.getMessage());
                    }catch(PSQLException exception){
                        JOptionPane.showMessageDialog(this, exception.getMessage());
                    }
                    institucionCentral.setUsuarioConectado(usuario);
                }
                abrirVentanaPrincipalDelSistema(institucionCentral);
                this.dispose();
            } catch (ObjetoNoEncontradoException ex) {
                setAvisoDeUsuarioYOContraseniaIncorrectos();
            }
        }

    }//GEN-LAST:event_jXButtonIngresarActionPerformed

    public void abrirVentanaPrincipalDelSistema(InstitucionCentral institucionCentral) {
        JFrameSistema ventanaPrincipalDelSistema = new JFrameSistema(institucionCentral);
        ventanaPrincipalDelSistema.setVisible(true);
        ventanaPrincipalDelSistema.setLocationRelativeTo(null);
    }

    private void setAvisoDeUsuarioYOContraseniaIncorrectos() {
        jXLblAviso.setForeground(Color.red);
        jXLblAviso.setText("Usuario y/o contraseña incorrectos.");
    }

    //TODO: fijarse si este metodo tiene sentido y se usa
    /*private InstitucionCentral getTipoDeInstitucion(InstitucionCentral institucion) {
        InstitucionCentral retorno = null;
        if (institucion.sosInstitucionCentralTesoreria()) {
            try {
                retorno = new InstitucionCentralTesoreria();
            } catch (DAOProblemaConTransaccionException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage());
            }
        }
        if (!institucion.sosInstitucionCentralTesoreria()) {
            try {
                retorno = new InstitucionCentralSecretaria();
            } catch (DAOProblemaConTransaccionException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage());
            }
        }
        return retorno;
    }*/
    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        if (evt.getKeyCode() == evt.VK_ESCAPE) {
            this.dispose();
        }
    }//GEN-LAST:event_formKeyPressed

    private void jXPnlLoguinKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jXPnlLoguinKeyPressed
        if (evt.getKeyCode() == evt.VK_ESCAPE) {
            this.dispose();

        }
    }//GEN-LAST:event_jXPnlLoguinKeyPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.jdesktop.swingx.JXButton jXButtonIngresar;
    private org.jdesktop.swingx.JXLabel jXLblAviso;
    private org.jdesktop.swingx.JXLoginPane jXPnlLoguin;
    // End of variables declaration//GEN-END:variables
}
