/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * JDialogABMCuentas.java
 *
 * Created on 24/05/2011, 10:29:15
 */
package Vistas;

import Excepciones.FilaNoSelecionadaException;
import Excepciones.DAOProblemaConTransaccionException;
import Modelo.Cuenta;
import Modelo.CuentaEgreso;
import Modelo.InstitucionCentralTesoreria;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author emanuel
 */
public class JDialogABMCuentas extends javax.swing.JDialog {

    InstitucionCentralTesoreria institucionCentralTesoreria;
    java.util.ArrayList<Cuenta> cuentas = new java.util.ArrayList<Cuenta>();

    public JDialogABMCuentas(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
    }

    JDialogABMCuentas(JFrameSistema padre, boolean modal, InstitucionCentralTesoreria institucionCentralTesoreria) {
        this(padre, modal);
        this.institucionCentralTesoreria = institucionCentralTesoreria;
        try {
            this.cuentas = this.institucionCentralTesoreria.getCuentas();
        } catch (DAOProblemaConTransaccionException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.INFORMATION_MESSAGE);
        }
        this.agregarCuentasAJTable(cuentas);
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXPanelCuerpo = new org.jdesktop.swingx.JXPanel();
        jXSeparadorDeTituloDatosDeCuentas = new org.jdesktop.swingx.JXTitledSeparator();
        jScrollPane2 = new javax.swing.JScrollPane();
        jXTableCuentas = new org.jdesktop.swingx.JXTable();
        jPanel8 = new javax.swing.JPanel();
        jBtnEliminarCuenta = new javax.swing.JButton();
        jBtnModificarCuenta = new javax.swing.JButton();
        jBtnAgregarCuenta = new javax.swing.JButton();
        jBtnInicio_seleccionarMiembro = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jXPanelCuerpo.setMinimumSize(new java.awt.Dimension(1130, 548));

        jXSeparadorDeTituloDatosDeCuentas.setFont(new java.awt.Font("Tahoma", 0, 14));
        jXSeparadorDeTituloDatosDeCuentas.setHorizontalAlignment(0);
        jXSeparadorDeTituloDatosDeCuentas.setTitle("Cuentas");

        jXTableCuentas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Tipo ", "Código de cuenta", "Nombre de cuenta"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jXTableCuentas);
        jXTableCuentas.getColumnModel().getColumn(0).setResizable(false);
        jXTableCuentas.getColumnModel().getColumn(1).setResizable(false);
        jXTableCuentas.getColumnModel().getColumn(2).setResizable(false);

        jPanel8.setBackground(new java.awt.Color(204, 204, 204));

        jBtnEliminarCuenta.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnEliminarCuenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Iglesia-delete.png"))); // NOI18N
        jBtnEliminarCuenta.setText("Eliminar");
        jBtnEliminarCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnEliminarCuentaActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnEliminarCuenta);

        jBtnModificarCuenta.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnModificarCuenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Iglesia-modificar.png"))); // NOI18N
        jBtnModificarCuenta.setText("Modificar");
        jBtnModificarCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnModificarCuentaActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnModificarCuenta);

        jBtnAgregarCuenta.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnAgregarCuenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Iglesia-add.png"))); // NOI18N
        jBtnAgregarCuenta.setText("Agregar");
        jBtnAgregarCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnAgregarCuentaActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnAgregarCuenta);

        jBtnInicio_seleccionarMiembro.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnInicio_seleccionarMiembro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnInicio_seleccionarMiembro.setText("Inicio");
        jBtnInicio_seleccionarMiembro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnInicio_seleccionarMiembroActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnInicio_seleccionarMiembro);

        javax.swing.GroupLayout jXPanelCuerpoLayout = new javax.swing.GroupLayout(jXPanelCuerpo);
        jXPanelCuerpo.setLayout(jXPanelCuerpoLayout);
        jXPanelCuerpoLayout.setHorizontalGroup(
            jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                    .addComponent(jXSeparadorDeTituloDatosDeCuentas, javax.swing.GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jXPanelCuerpoLayout.setVerticalGroup(
            jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                .addContainerGap(18, Short.MAX_VALUE)
                .addComponent(jXSeparadorDeTituloDatosDeCuentas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 431, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jXPanelCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, 472, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jXPanelCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, 540, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnModificarCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnModificarCuentaActionPerformed
        Cuenta cuentaSeleccionada = null;
        try {
            try {
                cuentaSeleccionada = getCuentaSeleccionada();
            } catch (DAOProblemaConTransaccionException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.INFORMATION_MESSAGE);
            }
            if (cuentaSeleccionada.sosCuentaPrincipal()) {
                JOptionPane.showMessageDialog(this, "No puede modificar esta cuenta");
                return;
            }
            JDialogAltaCuenta jDialogAltaCuenta = new JDialogAltaCuenta(this, true, institucionCentralTesoreria, cuentaSeleccionada);
            jDialogAltaCuenta.setVisible(true);
        } catch (FilaNoSelecionadaException ex) {
            JOptionPane.showMessageDialog(this, "Ninguna cuenta seleccionada.", "Aviso", JOptionPane.INFORMATION_MESSAGE);
        }


}//GEN-LAST:event_jBtnModificarCuentaActionPerformed

    private void jBtnEliminarCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnEliminarCuentaActionPerformed
        Cuenta cuentaSeleccionada = null;
        try {
            cuentaSeleccionada = this.getCuentaSeleccionada();
        } catch (FilaNoSelecionadaException ex) {
            JOptionPane.showMessageDialog(this, "Ninguna cuenta seleccionada.", "Aviso", JOptionPane.INFORMATION_MESSAGE);
            return;
        } catch (DAOProblemaConTransaccionException ex1) {
            JOptionPane.showMessageDialog(this, ex1.getMessage(), "Aviso", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        if (cuentaSeleccionada.sosCuentaPrincipal()) {
            JOptionPane.showMessageDialog(this, "No puede eliminar esta cuenta");
            return;
        }
        int respuesta = JOptionPane.showConfirmDialog(this,
                "¿Desea eliminar los datos de la cuenta seleccionada? \n", "Aviso", JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
        if (respuesta == JOptionPane.YES_OPTION) {
            try {
                eliminarCuentaEnBdYEnTablaYEnColeccion(cuentaSeleccionada);
            } catch (DAOProblemaConTransaccionException ex) {
                JOptionPane.showMessageDialog(this, "No existe la cuenta a eliminar", "Aviso", JOptionPane.INFORMATION_MESSAGE);
            }
        }
}//GEN-LAST:event_jBtnEliminarCuentaActionPerformed

    private void jBtnAgregarCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnAgregarCuentaActionPerformed
        JDialogAltaCuenta altaCuenta = new JDialogAltaCuenta(this, true, this.institucionCentralTesoreria);
        altaCuenta.setVisible(true);
}//GEN-LAST:event_jBtnAgregarCuentaActionPerformed

    private void jBtnInicio_seleccionarMiembroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnInicio_seleccionarMiembroActionPerformed
        this.dispose();
        try {
            this.finalize();
        } catch (Throwable ex) {
            Logger.getLogger(JDialogABMCuentas.class.getName()).log(Level.SEVERE, null, ex);
        }
}//GEN-LAST:event_jBtnInicio_seleccionarMiembroActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnAgregarCuenta;
    private javax.swing.JButton jBtnEliminarCuenta;
    private javax.swing.JButton jBtnInicio_seleccionarMiembro;
    private javax.swing.JButton jBtnModificarCuenta;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane2;
    private org.jdesktop.swingx.JXPanel jXPanelCuerpo;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloDatosDeCuentas;
    private org.jdesktop.swingx.JXTable jXTableCuentas;
    // End of variables declaration//GEN-END:variables

    private Cuenta getCuentaSeleccionada() throws FilaNoSelecionadaException, DAOProblemaConTransaccionException {
        if (jXTableCuentas.getSelectedRow() == -1) {
            throw new FilaNoSelecionadaException();
        }
        DefaultTableModel modelo = (DefaultTableModel) jXTableCuentas.getModel();
        int codigoCuenta = Integer.parseInt((modelo.getValueAt(jXTableCuentas.getSelectedRow(), 1)).toString());
        Cuenta cuentaSeleccionada = null;
        Iterator<Cuenta> it = cuentas.iterator();
        while (it.hasNext()) {
            Cuenta cuenta = it.next();
            if (cuenta.getCodigo() == codigoCuenta) {
                cuentaSeleccionada = cuenta;
                break;
            }
        }
        return cuentaSeleccionada;
    }

    private void agregarCuentasAJTable(ArrayList<Cuenta> cuentas) {
        if (cuentas != null) {
            Iterator it = cuentas.iterator();
            while (it.hasNext()) {
                Cuenta cuenta = (Cuenta) it.next();
                Object[] filaCuenta = {cuenta instanceof CuentaEgreso ? "Egreso" : "Ingreso", cuenta.getCodigo(), cuenta.getNombre()};
                DefaultTableModel modelo = (DefaultTableModel) this.jXTableCuentas.getModel();
                modelo.addRow(filaCuenta);
            }
        }

    }

    void agregarCuentaATablaYAColeccion(Cuenta cuenta) {
        DefaultTableModel modelo = (DefaultTableModel) this.jXTableCuentas.getModel();
        modelo.addRow(new Object[]{cuenta instanceof CuentaEgreso ? "Egreso" : "Ingreso", cuenta.getCodigo(), cuenta.getNombre()});
        cuentas.add(cuenta);
    }

    private void eliminarFilaSeleccionada() {
        DefaultTableModel modelo = (DefaultTableModel) this.jXTableCuentas.getModel();
        modelo.removeRow(jXTableCuentas.getSelectedRow());
    }

    private void eliminarCuentaEnBdYEnTablaYEnColeccion(Cuenta cuentaSeleccionada) throws DAOProblemaConTransaccionException {
        this.institucionCentralTesoreria.eliminarCuenta(cuentaSeleccionada);
        eliminarFilaSeleccionada();
        this.cuentas.remove(cuentaSeleccionada);
    }

    void actualizarDatosDeCuenta(Cuenta cuentaAModificar) {
        DefaultTableModel modelo = (DefaultTableModel) this.jXTableCuentas.getModel();
        modelo.setValueAt(cuentaAModificar.getCodigo(), jXTableCuentas.getSelectedRow(), jXTableCuentas.getSelectedColumn());
    }
}
