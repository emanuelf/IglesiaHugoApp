/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * JDialogAltaCuenta.java
 *
 * Created on 24/05/2011, 10:49:27
 */
package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Excepciones.DAOYaExisteRegistroException;
import Modelo.Cuenta;
import Modelo.InstitucionCentralTesoreria;
import Vistas.Util.Util;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author emanuel
 */
public class JDialogAltaCuenta extends javax.swing.JDialog {

    private InstitucionCentralTesoreria institucionCentralTesoreria;
    private JDialogABMCuentas padre = null;
    private Cuenta cuentaAModificar = null;
    private boolean cuentaModificada;

    /** Creates new form JDialogAltaCuenta */
    public JDialogAltaCuenta(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
    }

    public JDialogAltaCuenta(JDialogABMCuentas aBMCuentas, boolean modal, InstitucionCentralTesoreria institucionCentralTesoreria) {
        initComponents();
        this.padre = aBMCuentas;
        this.setModal(modal);
        this.institucionCentralTesoreria = institucionCentralTesoreria;
        this.setLocationRelativeTo(null);

    }

    public JDialogAltaCuenta(JDialogABMCuentas aBMCuentas, boolean modal, InstitucionCentralTesoreria institucionCentralTesoreria,
            Cuenta cuentaAModificar) {
        initComponents();
        this.padre = aBMCuentas;
        this.setModal(modal);
        this.institucionCentralTesoreria = institucionCentralTesoreria;
        this.cuentaAModificar = cuentaAModificar;
        cuentaModificada = false;
        this.cargarDatosDeCuentaParaModificar(cuentaAModificar);
        this.setLocationRelativeTo(null);
    }

    public JDialogAltaCuenta(java.awt.Frame padre, boolean modal,
            InstitucionCentralTesoreria institucionCentralTesoreria) {
        super(padre, modal);
        this.institucionCentralTesoreria = institucionCentralTesoreria;
        this.setLocationRelativeTo(null);

    }

    private boolean llamadoParaModificarCuenta() {
        boolean retorno;
        retorno = cuentaAModificar == null ? false : true;
        return retorno;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        comboBoxRect1 = new org.edisoncor.gui.comboBox.ComboBoxRect();
        jPnlCuerpo = new javax.swing.JPanel();
        jXSeparadorDeTituloNuevaCuenta = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelNroCodigo = new javax.swing.JLabel();
        jTxtNroDeCodigo = new javax.swing.JTextField();
        jLabelNumeroDeCodigo = new javax.swing.JLabel();
        jTxtNombreDeCuenta = new javax.swing.JTextField();
        jBtnGuardarCuenta = new javax.swing.JButton();
        jBtnVolverAlInicio = new javax.swing.JButton();
        jComboTipoCuenta = new javax.swing.JComboBox();
        jLabelNumeroDeCodigo1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jXSeparadorDeTituloNuevaCuenta.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jXSeparadorDeTituloNuevaCuenta.setHorizontalAlignment(0);
        jXSeparadorDeTituloNuevaCuenta.setTitle("Nueva cuenta");

        jLabelNroCodigo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelNroCodigo.setText("Número de código:");

        jTxtNroDeCodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtNroDeCodigoActionPerformed(evt);
            }
        });
        jTxtNroDeCodigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtNroDeCodigoKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtNroDeCodigoKeyTyped(evt);
            }
        });

        jLabelNumeroDeCodigo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelNumeroDeCodigo.setText("Nombre de nueva cuenta:");

        jTxtNombreDeCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtNombreDeCuentaActionPerformed(evt);
            }
        });
        jTxtNombreDeCuenta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtNombreDeCuentaKeyPressed(evt);
            }
        });

        jBtnGuardarCuenta.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnGuardarCuenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Guardar 30x30.png"))); // NOI18N
        jBtnGuardarCuenta.setText("Guardar");
        jBtnGuardarCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnGuardarCuentaActionPerformed(evt);
            }
        });

        jBtnVolverAlInicio.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnVolverAlInicio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolverAlInicio.setText("Volver");
        jBtnVolverAlInicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverAlInicioActionPerformed(evt);
            }
        });

        jComboTipoCuenta.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ingreso", "Egreso" }));

        jLabelNumeroDeCodigo1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelNumeroDeCodigo1.setText("Tipo de cuenta:");

        javax.swing.GroupLayout jPnlCuerpoLayout = new javax.swing.GroupLayout(jPnlCuerpo);
        jPnlCuerpo.setLayout(jPnlCuerpoLayout);
        jPnlCuerpoLayout.setHorizontalGroup(
            jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlCuerpoLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelNumeroDeCodigo)
                            .addComponent(jLabelNroCodigo)
                            .addComponent(jLabelNumeroDeCodigo1)
                            .addGroup(jPnlCuerpoLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTxtNroDeCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jComboTipoCuenta, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jTxtNombreDeCuenta, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)))))
                        .addGap(47, 47, 47))
                    .addComponent(jXSeparadorDeTituloNuevaCuenta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPnlCuerpoLayout.createSequentialGroup()
                        .addComponent(jBtnGuardarCuenta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBtnVolverAlInicio)))
                .addContainerGap())
        );
        jPnlCuerpoLayout.setVerticalGroup(
            jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXSeparadorDeTituloNuevaCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelNroCodigo)
                .addGap(9, 9, 9)
                .addComponent(jTxtNroDeCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabelNumeroDeCodigo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtNombreDeCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelNumeroDeCodigo1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jComboTipoCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPnlCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBtnVolverAlInicio)
                    .addComponent(jBtnGuardarCuenta))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPnlCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPnlCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public boolean isCamposCargados() {
        return true;
    }

    private void jBtnGuardarCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnGuardarCuentaActionPerformed
        JTextField[] jTextFields = {this.jTxtNombreDeCuenta, this.jTxtNroDeCodigo};
        if (Vistas.Util.Util.isEmpty(jTextFields)) {
            JOptionPane.showMessageDialog(this, "Ingrese el codigo y el nombre de la cuenta.", "Aviso", JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        String nombreCuenta = this.jTxtNombreDeCuenta.getText().trim();
        int codigoCuenta = Integer.valueOf(jTxtNroDeCodigo.getText());
        String tipoCuenta = jComboTipoCuenta.getSelectedItem().toString();
        if (!llamadoParaModificarCuenta()) { //Entonces es llamado para guardar una nueva cuenta!! guardar y salir ;)
            int decision = JOptionPane.showConfirmDialog(this, "Desea agregar la cuenta: \n * " + nombreCuenta
                    + "\n Número de código: \n * " + String.valueOf(codigoCuenta), "Pregunta", JOptionPane.YES_NO_OPTION);
            if (decision == JOptionPane.YES_OPTION) {
                try {
                    try {
                        Cuenta cuentaAgregada = this.institucionCentralTesoreria.registrarNuevaCuenta(nombreCuenta, codigoCuenta, tipoCuenta);
                        this.padre.agregarCuentaATablaYAColeccion(cuentaAgregada);
                        this.dispose();
                    } catch (DAOYaExisteRegistroException ex) {
                        JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.ERROR_MESSAGE);
                    }

                    try {
                        this.finalize();
                    } catch (Throwable ex) {
                        Logger.getLogger(JDialogAltaCuenta.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (DAOProblemaConTransaccionException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                return;
            }
        } else {
            int decision = JOptionPane.showConfirmDialog(this, "Desea modificar la cuenta, con los siguientes datos: \n * " + nombreCuenta
                    + "\n Número de código: \n * " + codigoCuenta, "Pregunta", JOptionPane.YES_NO_OPTION);
            if (decision == JOptionPane.YES_OPTION) {
                try {
                    this.institucionCentralTesoreria.modificarDatosDeCuenta(cuentaAModificar, nombreCuenta, codigoCuenta,
                            tipoCuenta);
                    this.padre.actualizarDatosDeCuenta(cuentaAModificar);
                    this.dispose();

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }

    }//GEN-LAST:event_jBtnGuardarCuentaActionPerformed

    private void jBtnVolverAlInicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVolverAlInicioActionPerformed
        if (llamadoParaModificarCuenta()) {
            if (this.cuentaModificada) {
                this.dispose();
                this.padre.setVisible(true);
            } else {
                int respuesta = JOptionPane.showConfirmDialog(this, "No ha guardado cambios en los datos de la cuenta"
                        + " ¿Salir de todos modos?", "Aviso", JOptionPane.YES_NO_OPTION);
                if (respuesta == JOptionPane.YES_OPTION) {
                    this.dispose();
                } else {
                    return;
                }
            }
        } else {
            this.dispose();
            this.getParent().setVisible(true);
        }

    }//GEN-LAST:event_jBtnVolverAlInicioActionPerformed

    private void jTxtNroDeCodigoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtNroDeCodigoKeyPressed

    }//GEN-LAST:event_jTxtNroDeCodigoKeyPressed

    private void jTxtNombreDeCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtNombreDeCuentaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTxtNombreDeCuentaActionPerformed

    private void jTxtNombreDeCuentaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtNombreDeCuentaKeyPressed
        Vistas.Util.Util.ignorarSiNoEsLetra(evt);
    }//GEN-LAST:event_jTxtNombreDeCuentaKeyPressed

private void jTxtNroDeCodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtNroDeCodigoActionPerformed

}//GEN-LAST:event_jTxtNroDeCodigoActionPerformed

private void jTxtNroDeCodigoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtNroDeCodigoKeyTyped
    Util.ignorarSiNoEsEntero(evt);
}//GEN-LAST:event_jTxtNroDeCodigoKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogAltaCuenta dialog = new JDialogAltaCuenta(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.edisoncor.gui.comboBox.ComboBoxRect comboBoxRect1;
    private javax.swing.JButton jBtnGuardarCuenta;
    private javax.swing.JButton jBtnVolverAlInicio;
    private javax.swing.JComboBox jComboTipoCuenta;
    private javax.swing.JLabel jLabelNroCodigo;
    private javax.swing.JLabel jLabelNumeroDeCodigo;
    private javax.swing.JLabel jLabelNumeroDeCodigo1;
    private javax.swing.JPanel jPnlCuerpo;
    private javax.swing.JTextField jTxtNombreDeCuenta;
    private javax.swing.JTextField jTxtNroDeCodigo;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloNuevaCuenta;
    // End of variables declaration//GEN-END:variables

    private void cargarDatosDeCuentaParaModificar(Cuenta cuentaAModificar) {
        jTxtNombreDeCuenta.setText(cuentaAModificar.getNombre());
        jTxtNroDeCodigo.setText(String.valueOf(cuentaAModificar.getCodigo()));
    }
}
