/*
 * JDialogAltaUsuario.java
 *
 * Created on 02/06/2011, 19:20:42
 */
package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Excepciones.DAOYaExisteRegistroException;
import Modelo.InstitucionCentralTesoreria;
import Modelo.Usuario;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author emanuel
 */
public class JDialogAltaUsuario extends javax.swing.JDialog {

    InstitucionCentralTesoreria institucionCentralTesoreria;
    private boolean llamadoParaModificarUsuario;
    private Usuario usuarioAModificar;
    private JDialogUsuarios padre;

    public JDialogAltaUsuario(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
    }

    public JDialogAltaUsuario(JDialogUsuarios jDialogABMUsuarios, boolean modal, InstitucionCentralTesoreria institucionCentralTesoreria, Usuario usuario) {
        super(jDialogABMUsuarios, modal);
        this.institucionCentralTesoreria = institucionCentralTesoreria;
        initComponents();
        padre = jDialogABMUsuarios;
        this.setLocationRelativeTo(null);
        if (usuario == null){
            llamadoParaModificarUsuario= false;
        }else{
            llamadoParaModificarUsuario= true;
            this.usuarioAModificar= usuario;
            cargarDatosDeUsuario(usuarioAModificar);
            jBtnAgregarUsuario.setText("Guardar modificación");
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jXSeparadorDeTituloNuevoUsuario = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelContrasenia = new javax.swing.JLabel();
        jLabelUsuariosRegistrados = new javax.swing.JLabel();
        jTxtNombreDeUsuario = new javax.swing.JTextField();
        jXLblAviso = new org.jdesktop.swingx.JXLabel();
        jPasswordFieldUsuario = new javax.swing.JPasswordField();
        jPanel2 = new javax.swing.JPanel();
        jBtnAgregarUsuario = new javax.swing.JButton();
        jBtnVolverAlInicio = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setLocationByPlatform(true);
        setResizable(false);

        jXSeparadorDeTituloNuevoUsuario.setFont(new java.awt.Font("Tahoma", 0, 12));
        jXSeparadorDeTituloNuevoUsuario.setHorizontalAlignment(0);
        jXSeparadorDeTituloNuevoUsuario.setTitle("Nuevo usuario");

        jLabelContrasenia.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelContrasenia.setText("Contraseña:");

        jLabelUsuariosRegistrados.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelUsuariosRegistrados.setText("Nombre de usuario:");

        jTxtNombreDeUsuario.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTxtNombreDeUsuarioFocusGained(evt);
            }
        });
        jTxtNombreDeUsuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtNombreDeUsuarioKeyTyped(evt);
            }
        });

        jXLblAviso.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jXLblAviso.setText("Asegúrese de recordar el nuevo usuario y contraseña. ");
        jXLblAviso.setFont(new java.awt.Font("SansSerif", 1, 18));
        jXLblAviso.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jXLblAviso.setLineWrap(true);

        jPasswordFieldUsuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jPasswordFieldUsuarioKeyTyped(evt);
            }
        });

        jBtnAgregarUsuario.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnAgregarUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Add usuario.png"))); // NOI18N
        jBtnAgregarUsuario.setText("Agregar");
        jBtnAgregarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnAgregarUsuarioActionPerformed(evt);
            }
        });
        jPanel2.add(jBtnAgregarUsuario);

        jBtnVolverAlInicio.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnVolverAlInicio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolverAlInicio.setText("Volver");
        jBtnVolverAlInicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverAlInicioActionPerformed(evt);
            }
        });
        jPanel2.add(jBtnVolverAlInicio);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jPasswordFieldUsuario))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jTxtNombreDeUsuario))
                            .addComponent(jLabelUsuariosRegistrados, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                            .addComponent(jLabelContrasenia, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jXSeparadorDeTituloNuevoUsuario, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jXLblAviso, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 302, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(15, 15, 15))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXSeparadorDeTituloNuevoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jXLblAviso, javax.swing.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelUsuariosRegistrados)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtNombreDeUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelContrasenia)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPasswordFieldUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnAgregarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnAgregarUsuarioActionPerformed
        if (formataDeNombreDeUsuarioCorrecto()) {
            if (!formatoDeContraseniaCorrecto()) {
                JOptionPane.showMessageDialog(this, "La contraseña debe tener por lo mínimo 6 caracteres.", null, JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            String nombreUsuario = this.jTxtNombreDeUsuario.getText();
            String contrasenia = String.copyValueOf(this.jPasswordFieldUsuario.getPassword());
            if (!llamadoParaModificarUsuario) {
                try {
                    Modelo.Secretario nuevoUsuario = this.institucionCentralTesoreria.registrarNuevoUsuarioSecretario(nombreUsuario, contrasenia);
                    padre.agregarOModificarUsuarioEnVista(nuevoUsuario);
                    JOptionPane.showMessageDialog(this, "¡Usuario agregado!", "Aviso", JOptionPane.PLAIN_MESSAGE);
                    this.dispose();

                } catch (DAOProblemaConTransaccionException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "Error en transacción", JOptionPane.INFORMATION_MESSAGE);
                    return;
                } catch (DAOYaExisteRegistroException ex) { //el nombre de usuario ya está usado
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "Usuario ya registrado", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
            } else {
                Usuario usuarioModificado = this.institucionCentralTesoreria.modificarUsuario(usuarioAModificar, nombreUsuario, contrasenia);
                showPadre();
            }

        } else {
            JOptionPane.showMessageDialog(this, "El nombre de usuario debe tener por lo mínimo 6 caracteres.", null, JOptionPane.INFORMATION_MESSAGE);
            return;
        }

    }//GEN-LAST:event_jBtnAgregarUsuarioActionPerformed

    private void jBtnVolverAlInicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVolverAlInicioActionPerformed
        this.dispose();
    }//GEN-LAST:event_jBtnVolverAlInicioActionPerformed

    private void jTxtNombreDeUsuarioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtNombreDeUsuarioKeyTyped
        JTextField source = (JTextField) evt.getSource();
        if (evt.getKeyChar() == evt.VK_SPACE || source.getText().length() > 15) {
            evt.consume();
        }
    }//GEN-LAST:event_jTxtNombreDeUsuarioKeyTyped

    private void jPasswordFieldUsuarioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPasswordFieldUsuarioKeyTyped
        JTextField source = (JTextField) evt.getSource();
        if (evt.getKeyChar() == evt.VK_SPACE || source.getText().length() > 15) {
            evt.consume();
        }
    }//GEN-LAST:event_jPasswordFieldUsuarioKeyTyped

    private void jTxtNombreDeUsuarioFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtNombreDeUsuarioFocusGained
        jTxtNombreDeUsuario.selectAll();
    }//GEN-LAST:event_jTxtNombreDeUsuarioFocusGained

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogAltaUsuario dialog = new JDialogAltaUsuario(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnAgregarUsuario;
    private javax.swing.JButton jBtnVolverAlInicio;
    private javax.swing.JLabel jLabelContrasenia;
    private javax.swing.JLabel jLabelUsuariosRegistrados;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPasswordField jPasswordFieldUsuario;
    private javax.swing.JTextField jTxtNombreDeUsuario;
    private org.jdesktop.swingx.JXLabel jXLblAviso;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloNuevoUsuario;
    // End of variables declaration//GEN-END:variables

    private boolean formatoDeContraseniaCorrecto() {
        boolean retorno = (jPasswordFieldUsuario.getPassword().length < 6) ? false : true;
        return retorno;
    }

    private boolean formataDeNombreDeUsuarioCorrecto() {
        boolean retorno = (jTxtNombreDeUsuario.getText().length() < 6 || jTxtNombreDeUsuario.getText().contains(" ")) ? false : true;
        return retorno;
    }

    private void showPadre() {
        padre.dispose();
        this.dispose();
        new JDialogUsuarios(null, true, institucionCentralTesoreria).setVisible(true);
    }

    private void cargarDatosDeUsuario(Usuario usuarioAModificar) {
        jTxtNombreDeUsuario.setText(usuarioAModificar.getNombre());
        jPasswordFieldUsuario.setText(usuarioAModificar.getContrasenia());
    }
}
