package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.Balance;
import Modelo.BalanceAnual;
import Modelo.BalanceMensual;
import Modelo.Egreso;
import Modelo.Ingreso;
import Modelo.InstitucionCentralTesoreria;
import Util.UtilFechas;
import Vistas.Util.Util;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.Beans;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTitledSeparator;

/**
 *
 * @author emanuel
 */
public class JDialogConsultaBalance extends javax.swing.JDialog {

    InstitucionCentralTesoreria institucionCentralTesoreria;

    public JDialogConsultaBalance(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        if(Beans.isDesignTime()){
            jButtonRadioPeriodo.add(jRadioButtonMensual);
            jButtonRadioPeriodo.add(jRadioButtonAnual);
            Util.cargaMesesEnCombo(jComboBoxMes);
            Util.cargaSecuenciaEnCombo(2000, Calendar.getInstance().get(Calendar.YEAR), jComboBoxAnio);
            setLocationRelativeTo(null);
        }
    }

    JDialogConsultaBalance(JFrameSistema padre, boolean modal, InstitucionCentralTesoreria institucionCentralTesoreria) {
        this(padre, modal);
        this.institucionCentralTesoreria = institucionCentralTesoreria;
        this.setLocationRelativeTo(null);
    }

    public void mostrarAnimacionDeEspera(JXBusyLabel animacion) {
        Vistas.Util.Util.mostrarAnimacion(animacion);
    }

    public void ocultarAnimacionEspera(JXBusyLabel animacion) {
        Vistas.Util.Util.ocultarAnimacion(animacion);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonRadioPeriodo = new ButtonGroup();
        jXPanelCuerpo = new JXPanel();
        jXSeparadorDeTituloBuscarBalance = new JXTitledSeparator();
        jScrollPane1 = new JScrollPane();
        jXTablaAsientos = new JXTable();
        jXSeparadorDeTituloDetallesDeAsientos = new JXTitledSeparator();
        jPnlTipoBusqueda = new JPanel();
        jRadioButtonMensual = new JRadioButton();
        jRadioButtonAnual = new JRadioButton();
        jPnlBusqueda = new JPanel();
        jXLabelAnio = new JXLabel();
        jComboBoxAnio = new JComboBox();
        jXLabeMes = new JXLabel();
        jComboBoxMes = new JComboBox();
        jBtnBuscar = new JButton();
        jXAnimacionEspera = new JXBusyLabel();
        jPnlDatosYTotales = new JPanel();
        jLabelNroDeBalance = new JLabel();
        jLabelNroBalanceDato = new JLabel();
        jLabelFechaDeEmision = new JLabel();
        jLabelPeriodoRelatorio = new JLabel();
        jLabelFechaDeEmisionDato = new JLabel();
        jLabelPeriodoDato = new JLabel();
        jLabelTotalNeto = new JLabel();
        jLabelTotalNetoDato = new JLabel();
        jLabelTotalEgresosDato = new JLabel();
        jLabelTotalEgresos = new JLabel();
        jLabelTotalIngresos = new JLabel();
        jLabelTotalIngresosDato = new JLabel();
        jPnlBotonera = new JPanel();
        jBtnImprimir = new JButton();
        jBtnVolverAlInicio = new JButton();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        jXPanelCuerpo.setMinimumSize(new Dimension(1130, 548));

        jScrollPane1.setViewportView(jXTablaAsientos);
        jXTablaAsientos.getColumnModel().getColumn(0).setMinWidth(100);
        jXTablaAsientos.getColumnModel().getColumn(0).setPreferredWidth(125);
        jXTablaAsientos.getColumnModel().getColumn(0).setMaxWidth(175);
        jXTablaAsientos.getColumnModel().getColumn(2).setMinWidth(100);
        jXTablaAsientos.getColumnModel().getColumn(2).setPreferredWidth(125);
        jXTablaAsientos.getColumnModel().getColumn(2).setMaxWidth(175);
        jXTablaAsientos.getColumnModel().getColumn(3).setMinWidth(75);
        jXTablaAsientos.getColumnModel().getColumn(3).setPreferredWidth(100);
        jXTablaAsientos.getColumnModel().getColumn(3).setMaxWidth(120);
        jXTablaAsientos.getColumnModel().getColumn(4).setMinWidth(75);
        jXTablaAsientos.getColumnModel().getColumn(4).setPreferredWidth(100);
        jXTablaAsientos.getColumnModel().getColumn(4).setMaxWidth(120);

        jPnlTipoBusqueda.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153)), "Tipo:", TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION));
        jPnlTipoBusqueda.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                jPnlTipoBusquedaMouseClicked(evt);
            }
        });
        jPnlTipoBusqueda.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent evt) {
                jPnlTipoBusquedaPropertyChange(evt);
            }
        });

        jRadioButtonMensual.setText("Mensual");
        jRadioButtonMensual.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jRadioButtonMensualActionPerformed(evt);
            }
        });

        jRadioButtonAnual.setText("Anual");
        jRadioButtonAnual.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jRadioButtonAnualActionPerformed(evt);
            }
        });
        jRadioButtonAnual.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent evt) {
                jRadioButtonAnualPropertyChange(evt);
            }
        });

        GroupLayout jPnlTipoBusquedaLayout = new GroupLayout(jPnlTipoBusqueda);
        jPnlTipoBusqueda.setLayout(jPnlTipoBusquedaLayout);
        jPnlTipoBusquedaLayout.setHorizontalGroup(
            jPnlTipoBusquedaLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPnlTipoBusquedaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPnlTipoBusquedaLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jRadioButtonMensual)
                    .addComponent(jRadioButtonAnual))
                .addContainerGap(49, Short.MAX_VALUE))
        );
        jPnlTipoBusquedaLayout.setVerticalGroup(
            jPnlTipoBusquedaLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPnlTipoBusquedaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jRadioButtonAnual)
                .addGap(18, 18, 18)
                .addComponent(jRadioButtonMensual)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPnlBusqueda.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153)), "Período:", TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION));

        jXLabelAnio.setText("Año:");
        jXLabelAnio.setFont(new Font("Tahoma", 0, 12)); // NOI18N

        jXLabeMes.setText("Mes:");
        jXLabeMes.setFont(new Font("Tahoma", 0, 12)); // NOI18N

        jBtnBuscar.setFont(new Font("Dialog", 0, 12)); // NOI18N
        jBtnBuscar.setIcon(new ImageIcon(getClass().getResource("/Iconos/Buscar.png"))); // NOI18N
        jBtnBuscar.setText("Buscar");
        jBtnBuscar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jBtnBuscarActionPerformed(evt);
            }
        });

        jXAnimacionEspera.setText("Espere...");
        jXAnimacionEspera.setFont(new Font("Dialog", 0, 12)); // NOI18N

        GroupLayout jPnlBusquedaLayout = new GroupLayout(jPnlBusqueda);
        jPnlBusqueda.setLayout(jPnlBusquedaLayout);
        jPnlBusquedaLayout.setHorizontalGroup(
            jPnlBusquedaLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPnlBusquedaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPnlBusquedaLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jXLabelAnio, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPnlBusquedaLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jComboBoxAnio, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)))
                .addGap(17, 17, 17)
                .addGroup(jPnlBusquedaLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jXLabeMes, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPnlBusquedaLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jComboBoxMes, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jBtnBuscar)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jXAnimacionEspera, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPnlBusquedaLayout.setVerticalGroup(
            jPnlBusquedaLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPnlBusquedaLayout.createSequentialGroup()
                .addGroup(jPnlBusquedaLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlBusquedaLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPnlBusquedaLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(jXLabelAnio, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addComponent(jXLabeMes, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPnlBusquedaLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboBoxMes, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBoxAnio, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPnlBusquedaLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(jPnlBusquedaLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(jBtnBuscar)
                            .addComponent(jXAnimacionEspera, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        jLabelNroDeBalance.setFont(new Font("Tahoma", 0, 12)); // NOI18N
        jLabelNroDeBalance.setText("Número de balance:");

        jLabelNroBalanceDato.setText("                            ");

        jLabelFechaDeEmision.setFont(new Font("Tahoma", 0, 12)); // NOI18N
        jLabelFechaDeEmision.setText("Fecha de emisión:");

        jLabelPeriodoRelatorio.setFont(new Font("Tahoma", 0, 12)); // NOI18N
        jLabelPeriodoRelatorio.setText("Período");

        jLabelFechaDeEmisionDato.setText("                               ");

        jLabelPeriodoDato.setText("                               ");

        jLabelTotalNeto.setFont(new Font("Tahoma", 0, 12)); // NOI18N
        jLabelTotalNeto.setText("Total neto: ");

        jLabelTotalNetoDato.setText("                               ");

        jLabelTotalEgresosDato.setText("                               ");

        jLabelTotalEgresos.setFont(new Font("Tahoma", 0, 12)); // NOI18N
        jLabelTotalEgresos.setText("Total egresos:");

        jLabelTotalIngresos.setFont(new Font("Tahoma", 0, 12)); // NOI18N
        jLabelTotalIngresos.setText("Total ingresos:");

        jLabelTotalIngresosDato.setText("                               ");

        GroupLayout jPnlDatosYTotalesLayout = new GroupLayout(jPnlDatosYTotales);
        jPnlDatosYTotales.setLayout(jPnlDatosYTotalesLayout);
        jPnlDatosYTotalesLayout.setHorizontalGroup(
            jPnlDatosYTotalesLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPnlDatosYTotalesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPnlDatosYTotalesLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelTotalIngresos)
                    .addComponent(jLabelNroDeBalance)
                    .addGroup(jPnlDatosYTotalesLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPnlDatosYTotalesLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelNroBalanceDato, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelTotalIngresosDato, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE))))
                .addGap(86, 86, 86)
                .addGroup(jPnlDatosYTotalesLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelFechaDeEmision)
                    .addComponent(jLabelTotalEgresos)
                    .addGroup(jPnlDatosYTotalesLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPnlDatosYTotalesLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelFechaDeEmisionDato, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelTotalEgresosDato, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE))))
                .addGap(108, 108, 108)
                .addGroup(jPnlDatosYTotalesLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelTotalNeto)
                    .addComponent(jLabelPeriodoRelatorio)
                    .addGroup(jPnlDatosYTotalesLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPnlDatosYTotalesLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelTotalNetoDato, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelPeriodoDato, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(234, Short.MAX_VALUE))
        );
        jPnlDatosYTotalesLayout.setVerticalGroup(
            jPnlDatosYTotalesLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPnlDatosYTotalesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPnlDatosYTotalesLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNroDeBalance)
                    .addComponent(jLabelFechaDeEmision)
                    .addComponent(jLabelPeriodoRelatorio, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPnlDatosYTotalesLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNroBalanceDato)
                    .addComponent(jLabelFechaDeEmisionDato)
                    .addComponent(jLabelPeriodoDato))
                .addGap(18, 18, 18)
                .addGroup(jPnlDatosYTotalesLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelTotalIngresos)
                    .addComponent(jLabelTotalEgresos)
                    .addComponent(jLabelTotalNeto))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPnlDatosYTotalesLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelTotalIngresosDato)
                    .addComponent(jLabelTotalEgresosDato)
                    .addComponent(jLabelTotalNetoDato))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jBtnImprimir.setFont(new Font("Dialog", 0, 12)); // NOI18N
        jBtnImprimir.setIcon(new ImageIcon(getClass().getResource("/Iconos/Imprimir 20x20.png"))); // NOI18N
        jBtnImprimir.setText("Imprimir");
        jBtnImprimir.setEnabled(false);
        jPnlBotonera.add(jBtnImprimir);

        jBtnVolverAlInicio.setFont(new Font("Dialog", 0, 12)); // NOI18N
        jBtnVolverAlInicio.setIcon(new ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolverAlInicio.setText("Volver al inicio");
        jPnlBotonera.add(jBtnVolverAlInicio);

        GroupLayout jXPanelCuerpoLayout = new GroupLayout(jXPanelCuerpo);
        jXPanelCuerpo.setLayout(jXPanelCuerpoLayout);
        jXPanelCuerpoLayout.setHorizontalGroup(
            jXPanelCuerpoLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jXPanelCuerpoLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jXSeparadorDeTituloBuscarBalance, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                        .addComponent(jPnlTipoBusqueda, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPnlBusqueda, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jXSeparadorDeTituloDetallesDeAsientos, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addComponent(jPnlDatosYTotales, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPnlBotonera, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jXPanelCuerpoLayout.setVerticalGroup(
            jXPanelCuerpoLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXSeparadorDeTituloBuscarBalance, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jXPanelCuerpoLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jPnlTipoBusqueda, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPnlBusqueda, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jXSeparadorDeTituloDetallesDeAsientos, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPnlDatosYTotales, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPnlBotonera, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jXPanelCuerpo, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 841, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jXPanelCuerpo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnBuscarActionPerformed(ActionEvent evt) {//GEN-FIRST:event_jBtnBuscarActionPerformed
        if (!estanLosCamposSeleccionadosParaBuscar()) {
            JOptionPane.showMessageDialog(this, "Seleccione los campos para la búsqueda.", "Aviso", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        //si están los campos seleccionados como para empezar la búsqueda, sea de balance mensual o anual, seguimos
        int anio = Integer.valueOf(this.jComboBoxAnio.getSelectedItem().toString());
        if (queremosBuscarUnBalanceAnual()) {
            try {
                BalanceAnual balance = this.institucionCentralTesoreria.generarBalanceAnual(anio);
                cargarDatosDeBalanceEnVista(balance);
            } catch (DAOProblemaConTransaccionException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.ERROR_MESSAGE);
            }
        } else { //queremos buscar un balance mensual, entonces.
            int mes = Integer.valueOf(this.jComboBoxMes.getSelectedItem().toString());
            try {
                BalanceMensual balance = this.institucionCentralTesoreria.generarBalanceMensual(mes, anio);
                cargarDatosDeBalanceEnVista(balance);
            } catch (DAOProblemaConTransaccionException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.ERROR_MESSAGE);
            }
        }
}//GEN-LAST:event_jBtnBuscarActionPerformed

    private void jRadioButtonAnualPropertyChange(PropertyChangeEvent evt) {//GEN-FIRST:event_jRadioButtonAnualPropertyChange
    }//GEN-LAST:event_jRadioButtonAnualPropertyChange

    private void jRadioButtonAnualActionPerformed(ActionEvent evt) {//GEN-FIRST:event_jRadioButtonAnualActionPerformed
        habilitarCamposParaBusquedaAnual();
    }//GEN-LAST:event_jRadioButtonAnualActionPerformed

    private void jRadioButtonMensualActionPerformed(ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMensualActionPerformed
        habilitarCamposParaBusquedaMensual();
    }//GEN-LAST:event_jRadioButtonMensualActionPerformed

    private void jPnlTipoBusquedaPropertyChange(PropertyChangeEvent evt) {//GEN-FIRST:event_jPnlTipoBusquedaPropertyChange
    }//GEN-LAST:event_jPnlTipoBusquedaPropertyChange

    private void jPnlTipoBusquedaMouseClicked(MouseEvent evt) {//GEN-FIRST:event_jPnlTipoBusquedaMouseClicked
    }//GEN-LAST:event_jPnlTipoBusquedaMouseClicked
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton jBtnBuscar;
    private JButton jBtnImprimir;
    private JButton jBtnVolverAlInicio;
    private ButtonGroup jButtonRadioPeriodo;
    private JComboBox jComboBoxAnio;
    private JComboBox jComboBoxMes;
    private JLabel jLabelFechaDeEmision;
    private JLabel jLabelFechaDeEmisionDato;
    private JLabel jLabelNroBalanceDato;
    private JLabel jLabelNroDeBalance;
    private JLabel jLabelPeriodoDato;
    private JLabel jLabelPeriodoRelatorio;
    private JLabel jLabelTotalEgresos;
    private JLabel jLabelTotalEgresosDato;
    private JLabel jLabelTotalIngresos;
    private JLabel jLabelTotalIngresosDato;
    private JLabel jLabelTotalNeto;
    private JLabel jLabelTotalNetoDato;
    private JPanel jPnlBotonera;
    private JPanel jPnlBusqueda;
    private JPanel jPnlDatosYTotales;
    private JPanel jPnlTipoBusqueda;
    private JRadioButton jRadioButtonAnual;
    private JRadioButton jRadioButtonMensual;
    private JScrollPane jScrollPane1;
    private JXBusyLabel jXAnimacionEspera;
    private JXLabel jXLabeMes;
    private JXLabel jXLabelAnio;
    private JXPanel jXPanelCuerpo;
    private JXTitledSeparator jXSeparadorDeTituloBuscarBalance;
    private JXTitledSeparator jXSeparadorDeTituloDetallesDeAsientos;
    private JXTable jXTablaAsientos;
    // End of variables declaration//GEN-END:variables

    private void habilitarCamposParaBusquedaAnual() {
        this.jComboBoxMes.setEnabled(false);
    }

    private void habilitarCamposParaBusquedaMensual() {
        this.jComboBoxMes.setEnabled(true);
    }

    private boolean queremosBuscarUnBalanceAnual() {
        if (jRadioButtonAnual.isSelected()) {
            return true;
        } else {
            return false;
        }
    }

    private void cargarDatosDeBalanceEnVista(BalanceAnual balance) {
        cargarIngresosEnTabla(balance.getIngresos());
        cargarEgresosEnTabla(balance.getEgresos());
        cargarBalanceEnLabels(balance);

    }

    private void cargarDatosDeBalanceEnVista(BalanceMensual balance) {
        cargarIngresosEnTabla(balance.getIngresos());
        cargarEgresosEnTabla(balance.getEgresos());
        cargarBalanceEnLabels(balance);
    }

    private boolean estanLosCamposSeleccionadosParaBuscar() {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void cargarEgresosEnTabla(Set<Egreso> egresos) {
        Iterator<Egreso> it = egresos.iterator();
        DefaultTableModel modelo = (DefaultTableModel) this.jXTablaAsientos.getModel();
        String[] filaEgresoTabla = new String[5];
        while (it.hasNext()) {
            Egreso egreso = it.next();
            filaEgresoTabla[0] = "Ingreso";
            filaEgresoTabla[1] = egreso.getCuenta().getNombre();
            filaEgresoTabla[2] = UtilFechas.getFechaEnFormatoDD_MM_AA(egreso.getFecha());
            filaEgresoTabla[3] = String.valueOf(egreso.getMonto());
            filaEgresoTabla[4] = String.valueOf(egreso.getPagado());
            modelo.addRow(filaEgresoTabla);
        }
    }

    private void cargarIngresosEnTabla(Set<Ingreso> ingresos) {
        Iterator<Ingreso> it = ingresos.iterator();
        DefaultTableModel modelo = (DefaultTableModel) this.jXTablaAsientos.getModel();
        String[] filaIngresoTabla = new String[5];
        while (it.hasNext()) {
            Ingreso ingreso = it.next();
            filaIngresoTabla[0] = "Ingreso";
            filaIngresoTabla[1] = ingreso.getCuenta().getNombre();
            filaIngresoTabla[2] = UtilFechas.getFechaEnFormatoDD_MM_AA(ingreso.getFecha());
            filaIngresoTabla[3] = String.valueOf(ingreso.getMonto());
            filaIngresoTabla[4] = "-";
            modelo.addRow(filaIngresoTabla);
        }
    }

    private void cargarBalanceEnLabels(BalanceAnual balance) {
        cargarBalanceEnLabels(balance);
        this.jLabelPeriodoDato.setText(String.valueOf(balance.getAnio()));       
        
    }
    
    private void CargarDatosDeBalanceEnComunEnLabels(Balance balance){
        this.jLabelNroBalanceDato.setText(String.valueOf(balance.getNroBalance()));
        this.jLabelFechaDeEmisionDato.setText(UtilFechas.getFechaEnFormatoDD_MM_AA(balance.getFechaDeEmision()));
        this.jLabelTotalEgresosDato.setText(String.valueOf(balance.getTotalEgresos()));
        this.jLabelTotalNetoDato.setText(String.valueOf(balance.getTotalNeto()));
        this.jLabelTotalIngresosDato.setText(String.valueOf(balance.getTotalIngresos()));
    }

    private void cargarBalanceEnLabels(BalanceMensual balance) {
        CargarDatosDeBalanceEnComunEnLabels(balance);
        this.jLabelPeriodoDato.setText(UtilFechas.getFechaEnFormatoMM_AA(new GregorianCalendar (balance.getAnio(), balance.getMes(), 2)));
    }
}
