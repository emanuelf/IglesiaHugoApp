package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Excepciones.FilaNoSelecionadaException;
import Modelo.CuentaFilial;
import Modelo.CuentaFilialDeuda;
import Modelo.CuentaFilialEgreso;
import Modelo.CuentaFilialIngreso;
import Modelo.DeudaRelatorio;
import Modelo.EgresoRelatorio;
import Modelo.Filial;
import Modelo.IngresoRelatorio;
import Modelo.InstitucionCentralTesoreria;
import Util.UtilFechas;
import Vistas.Util.Util;
import java.awt.Component;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author emanuel
 */
public class JDialogAltaRelatorioDeFilial extends javax.swing.JDialog {

    private InstitucionCentralTesoreria institucionCentralTesoreria;
    private Filial filial;
    private boolean datosCargadosSinGuardar;
    private Set<IngresoRelatorio> ingresos = new HashSet<IngresoRelatorio>();
    private Set<EgresoRelatorio> egresos = new HashSet<EgresoRelatorio>();
    private Set<DeudaRelatorio> deudas = new HashSet<DeudaRelatorio>();
    private double totalEgresos;
    private double totalIngresos;
    private double totalDeuda;

    public JDialogAltaRelatorioDeFilial(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
    }

    JDialogAltaRelatorioDeFilial(JFrameSistema padre, boolean modal, InstitucionCentralTesoreria institucionCentralTesoreria, Filial filial) {
        this(padre, modal);
        this.institucionCentralTesoreria = institucionCentralTesoreria;
        this.filial = filial;
        this.jLblNombreFilial.setText(filial.getNombre());
        this.datosCargadosSinGuardar = false;
        Util.cargaMesesEnCombo(jComboPeriodoMes);
        try {
            cargarCuentasEnJCombo();
        } catch (DAOProblemaConTransaccionException ex) {
            Logger.getLogger(JDialogAltaRelatorioDeFilial.class.getName()).log(Level.SEVERE, null, ex);
        }
        Util.cargaSecuenciaEnCombo(Calendar.getInstance().get(Calendar.YEAR) - 3, Calendar.getInstance().get(Calendar.YEAR) + 2,
                jComboPeriodoAnio);
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXPanelCuerpo = new org.jdesktop.swingx.JXPanel();
        jXSeparadorDeTituloDatosRelatorio = new org.jdesktop.swingx.JXTitledSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        jXTableAsientos = new org.jdesktop.swingx.JXTable();
        jXSeparadorDeTituloDetallesDeLasCuentas = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelMesDeRelatorio1 = new javax.swing.JLabel();
        jComboCuentas = new javax.swing.JComboBox();
        jLabelMonto = new javax.swing.JLabel();
        jBtnCargarAsientoCuenta = new javax.swing.JButton();
        jPnlBotonera = new javax.swing.JPanel();
        jBtnGuardarRelatorio = new javax.swing.JButton();
        jBtnVolver = new javax.swing.JButton();
        jTxtMonto = new javax.swing.JTextField();
        jBtnDeshacerUltimoAsiento = new javax.swing.JButton();
        jPnlDatosGenerales = new javax.swing.JPanel();
        jLabelNroDeRelatorio1 = new javax.swing.JLabel();
        jComboPeriodoMes = new javax.swing.JComboBox();
        jComboPeriodoAnio = new javax.swing.JComboBox();
        jLabelPeriodoRelatorio = new javax.swing.JLabel();
        jTxtTotalNeto = new javax.swing.JTextField();
        jLabelNroDeRelatorio5 = new javax.swing.JLabel();
        jLabelCajaInicial = new javax.swing.JLabel();
        jTxtCajaInicial = new javax.swing.JTextField();
        jLabelCajaFinal = new javax.swing.JLabel();
        jTxtCajaFinal = new javax.swing.JTextField();
        jTxtIngresos = new javax.swing.JTextField();
        jLabelNroDeRelatorio8 = new javax.swing.JLabel();
        jLabelNroDeRelatorio6 = new javax.swing.JLabel();
        jTxtEgresos = new javax.swing.JTextField();
        jLabelNroDeRelatorio9 = new javax.swing.JLabel();
        jTxtDeuda = new javax.swing.JTextField();
        jLabelNroDeRelatorio2 = new javax.swing.JLabel();
        jTxtDiezmoPastoral = new javax.swing.JTextField();
        jLabelNroDeRelatorio3 = new javax.swing.JLabel();
        jTxtDiezmoFilial = new javax.swing.JTextField();
        jLblNombreFilial = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Ingreso de nuevo relatorio de filial");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jXPanelCuerpo.setMinimumSize(new java.awt.Dimension(1130, 548));

        jXSeparadorDeTituloDatosRelatorio.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jXSeparadorDeTituloDatosRelatorio.setHorizontalAlignment(0);
        jXSeparadorDeTituloDatosRelatorio.setTitle("Datos generales");

        jXTableAsientos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Tipo", "Cuenta", "Monto"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jXTableAsientos);
        jXTableAsientos.getColumnModel().getColumn(0).setResizable(false);
        jXTableAsientos.getColumnModel().getColumn(1).setResizable(false);
        jXTableAsientos.getColumnModel().getColumn(2).setResizable(false);

        jXSeparadorDeTituloDetallesDeLasCuentas.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jXSeparadorDeTituloDetallesDeLasCuentas.setHorizontalAlignment(0);
        jXSeparadorDeTituloDetallesDeLasCuentas.setTitle("Detalle del relatorio");

        jLabelMesDeRelatorio1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelMesDeRelatorio1.setText("Nombre de la cuenta:");

        jComboCuentas.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jComboCuentas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jComboCuentasMouseReleased(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jComboCuentasMouseClicked(evt);
            }
        });
        jComboCuentas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboCuentasActionPerformed(evt);
            }
        });

        jLabelMonto.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelMonto.setText("Monto:");

        jBtnCargarAsientoCuenta.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnCargarAsientoCuenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Guardar 30x30.png"))); // NOI18N
        jBtnCargarAsientoCuenta.setText("Cargar");
        jBtnCargarAsientoCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnCargarAsientoCuentaActionPerformed(evt);
            }
        });

        jBtnGuardarRelatorio.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnGuardarRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Guardar 30x30.png"))); // NOI18N
        jBtnGuardarRelatorio.setText("Guardar");
        jBtnGuardarRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnGuardarRelatorioActionPerformed(evt);
            }
        });
        jPnlBotonera.add(jBtnGuardarRelatorio);

        jBtnVolver.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolver.setText("Volver al inicio");
        jBtnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverActionPerformed(evt);
            }
        });
        jPnlBotonera.add(jBtnVolver);

        jTxtMonto.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jTxtMonto.setForeground(new java.awt.Color(0, 204, 51));
        jTxtMonto.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtMonto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTxtMontoMouseClicked(evt);
            }
        });
        jTxtMonto.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTxtMontoCaretUpdate(evt);
            }
        });
        jTxtMonto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtMontoActionPerformed(evt);
            }
        });
        jTxtMonto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTxtMontoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTxtMontoFocusLost(evt);
            }
        });
        jTxtMonto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtMontoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxtMontoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtMontoKeyTyped(evt);
            }
        });

        jBtnDeshacerUltimoAsiento.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnDeshacerUltimoAsiento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Eliminar egreso 20x20.png"))); // NOI18N
        jBtnDeshacerUltimoAsiento.setText("Deshacer");
        jBtnDeshacerUltimoAsiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnDeshacerUltimoAsientoActionPerformed(evt);
            }
        });

        jPnlDatosGenerales.setMaximumSize(new java.awt.Dimension(901, 32767));
        jPnlDatosGenerales.setPreferredSize(new java.awt.Dimension(700, 116));

        jLabelNroDeRelatorio1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelNroDeRelatorio1.setText("Nombre de filial:");

        jComboPeriodoMes.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jComboPeriodoMes.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Noviembre" }));

        jComboPeriodoAnio.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N

        jLabelPeriodoRelatorio.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelPeriodoRelatorio.setText("Periodo:");

        jTxtTotalNeto.setOpaque(false);
        jTxtTotalNeto.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jTxtTotalNetoPropertyChange(evt);
            }
        });
        jTxtTotalNeto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtTotalNetoKeyTyped(evt);
            }
        });

        jLabelNroDeRelatorio5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelNroDeRelatorio5.setText("Total neto:");

        jLabelCajaInicial.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelCajaInicial.setText("Caja inicial:");

        jTxtCajaInicial.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTxtCajaInicialFocusLost(evt);
            }
        });
        jTxtCajaInicial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtCajaInicialKeyTyped(evt);
            }
        });

        jLabelCajaFinal.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelCajaFinal.setText("Caja final:");

        jTxtCajaFinal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtCajaFinalKeyTyped(evt);
            }
        });

        jTxtIngresos.setEditable(false);

        jLabelNroDeRelatorio8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelNroDeRelatorio8.setText("Ingresos:");

        jLabelNroDeRelatorio6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelNroDeRelatorio6.setText("Egresos:");

        jTxtEgresos.setEditable(false);

        jLabelNroDeRelatorio9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelNroDeRelatorio9.setText("Deuda:");

        jTxtDeuda.setEditable(false);

        jLabelNroDeRelatorio2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelNroDeRelatorio2.setText("Diezmo pastoral:");

        jLabelNroDeRelatorio3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelNroDeRelatorio3.setText("Diezmo filial:");

        jLblNombreFilial.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLblNombreFilial.setText(" ");

        javax.swing.GroupLayout jPnlDatosGeneralesLayout = new javax.swing.GroupLayout(jPnlDatosGenerales);
        jPnlDatosGenerales.setLayout(jPnlDatosGeneralesLayout);
        jPnlDatosGeneralesLayout.setHorizontalGroup(
            jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                        .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelNroDeRelatorio8)
                            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jTxtIngresos, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jTxtEgresos, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabelNroDeRelatorio6)))
                    .addComponent(jLabelNroDeRelatorio1)
                    .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLblNombreFilial, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelPeriodoRelatorio)
                    .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                        .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelNroDeRelatorio9)
                            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jComboPeriodoAnio, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTxtDeuda, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(8, 8, 8)
                        .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jComboPeriodoMes, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelNroDeRelatorio2, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTxtDiezmoPastoral, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(30, 30, 30)
                .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jTxtCajaInicial))
                            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                                .addComponent(jLabelCajaInicial)
                                .addGap(35, 35, 35))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                            .addGap(10, 10, 10)
                            .addComponent(jTxtDiezmoFilial))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPnlDatosGeneralesLayout.createSequentialGroup()
                            .addComponent(jLabelNroDeRelatorio3)
                            .addGap(30, 30, 30))))
                .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelNroDeRelatorio5)
                            .addComponent(jLabelCajaFinal)))
                    .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPnlDatosGeneralesLayout.createSequentialGroup()
                            .addGap(26, 26, 26)
                            .addComponent(jTxtTotalNeto))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPnlDatosGeneralesLayout.createSequentialGroup()
                            .addGap(28, 28, 28)
                            .addComponent(jTxtCajaFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(66, 66, 66))
        );
        jPnlDatosGeneralesLayout.setVerticalGroup(
            jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                        .addComponent(jLabelPeriodoRelatorio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboPeriodoAnio, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboPeriodoMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTxtDeuda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                                .addComponent(jLabelNroDeRelatorio9)
                                .addGap(25, 25, 25))
                            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                                .addComponent(jLabelNroDeRelatorio2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTxtDiezmoPastoral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                            .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                                    .addComponent(jLabelCajaFinal)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jTxtCajaFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                                    .addGap(2, 2, 2)
                                    .addComponent(jLabelCajaInicial)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jTxtCajaInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jLabelNroDeRelatorio3)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jTxtTotalNeto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTxtDiezmoFilial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                            .addGap(52, 52, 52)
                            .addComponent(jLabelNroDeRelatorio5)))
                    .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                        .addComponent(jLabelNroDeRelatorio1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblNombreFilial)
                        .addGap(18, 18, 18)
                        .addGroup(jPnlDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                                .addComponent(jLabelNroDeRelatorio8)
                                .addGap(5, 5, 5)
                                .addComponent(jTxtIngresos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPnlDatosGeneralesLayout.createSequentialGroup()
                                .addComponent(jLabelNroDeRelatorio6)
                                .addGap(5, 5, 5)
                                .addComponent(jTxtEgresos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jXPanelCuerpoLayout = new javax.swing.GroupLayout(jXPanelCuerpo);
        jXPanelCuerpo.setLayout(jXPanelCuerpoLayout);
        jXPanelCuerpoLayout.setHorizontalGroup(
            jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jXSeparadorDeTituloDatosRelatorio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jXSeparadorDeTituloDetallesDeLasCuentas, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jXPanelCuerpoLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPnlBotonera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                        .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jBtnDeshacerUltimoAsiento, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                                    .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                                            .addGap(12, 12, 12)
                                            .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabelMesDeRelatorio1)
                                                .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                                                    .addGap(12, 12, 12)
                                                    .addComponent(jComboCuentas, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(jLabelMonto)))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jXPanelCuerpoLayout.createSequentialGroup()
                                            .addGap(22, 22, 22)
                                            .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jTxtMonto, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jBtnCargarAsientoCuenta, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addGap(18, 18, 18)
                                    .addComponent(jScrollPane1))
                                .addComponent(jPnlDatosGenerales, javax.swing.GroupLayout.PREFERRED_SIZE, 741, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jXPanelCuerpoLayout.setVerticalGroup(
            jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXSeparadorDeTituloDatosRelatorio, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(jPnlDatosGenerales, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jXSeparadorDeTituloDetallesDeLasCuentas, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                        .addComponent(jLabelMesDeRelatorio1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboCuentas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(jLabelMonto)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxtMonto, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBtnCargarAsientoCuenta)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBtnDeshacerUltimoAsiento)
                .addGap(18, 18, 18)
                .addComponent(jPnlBotonera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jXPanelCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jXPanelCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, 471, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTxtMontoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTxtMontoMouseClicked
        if (jTxtMonto.getText().isEmpty()) {
            return;
        }
        jTxtMonto.setCaretPosition(jTxtMonto.getText().length());
}//GEN-LAST:event_jTxtMontoMouseClicked

    private void jTxtMontoCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTxtMontoCaretUpdate
        if (evt.getDot() == 0 && !this.jTxtMonto.getText().isEmpty()) {
            this.jTxtMonto.setCaretPosition(1);
        }
}//GEN-LAST:event_jTxtMontoCaretUpdate

    private void jTxtMontoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtMontoActionPerformed
}//GEN-LAST:event_jTxtMontoActionPerformed

    private void jTxtMontoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtMontoFocusGained
        Vistas.Util.Util.siempreCursorAlFinal(jTxtMonto);
}//GEN-LAST:event_jTxtMontoFocusGained

    private void jTxtMontoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtMontoFocusLost
        Vistas.Util.Util.redondearUnMontoADosCifras(jTxtMonto);
}//GEN-LAST:event_jTxtMontoFocusLost

    private void jTxtMontoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoKeyPressed
}//GEN-LAST:event_jTxtMontoKeyPressed

    private void jTxtMontoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoKeyReleased
}//GEN-LAST:event_jTxtMontoKeyReleased

    private void jTxtMontoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoKeyTyped
        Vistas.Util.Util.formatearJTextFieldTipoMonedaLuegoDeUnKeyEvent(evt, jTxtMonto);

}//GEN-LAST:event_jTxtMontoKeyTyped

    private void jBtnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVolverActionPerformed
        if (hayAsientosCargados()) {
            int respuesta = JOptionPane.showConfirmDialog(this, "Hay datos cargados sin guardar. ¿Desea salir realmente?", "Aviso", JOptionPane.YES_OPTION);
            if (respuesta == JOptionPane.YES_OPTION) {
                dispose();
                getParent().setVisible(true);
            } else {
                return;
            }
        } else {
            dispose();
            getParent().setVisible(true);
        }
    }//GEN-LAST:event_jBtnVolverActionPerformed

    private void jBtnCargarAsientoCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnCargarAsientoCuentaActionPerformed
        //ya me asegure con el método addNuevaCuentaAJComboYAColeccion que la la cuenta esté actualizada en
        //la colección que mantengo en está clase, por esto no hago verificaciones sobre existencia de la cuenta o no
        if (!datosCargados()) {
            JOptionPane.showMessageDialog(this, "Cargue el monto del Ingreso, Egreso o Deuda.", "Aviso", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        try {
            String nombreDeLaCuenta = getStringTipoDeCuenta(jComboCuentas.getSelectedItem().toString());
            if (hayIngresosParaEstaCuenta(nombreDeLaCuenta)) {
                JOptionPane.showMessageDialog(this, "Ya se hizo un asiento para esta cuenta", "Aviso", JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            double monto = Vistas.Util.Util.getMontoSinSimbolo(jTxtMonto.getText());
            CuentaFilial cuenta = institucionCentralTesoreria.getCuentaFilial(getStringTipoDeCuenta(nombreDeLaCuenta));
            String[] filaNuevaParaTabla = null;
            if (cuenta.sosDeuda()) {
                DeudaRelatorio deudaRelatorio = new DeudaRelatorio((CuentaFilialDeuda) cuenta, monto);
                deudas.add(deudaRelatorio);
                filaNuevaParaTabla = new String[]{"Deuda", deudaRelatorio.getCuentaFilialDeuda().
                    getNombre(), Double.toString(deudaRelatorio.getMonto())};
                totalDeuda += monto;
            }
            if (cuenta.sosEgreso()) {
                EgresoRelatorio egresoRelatorio = new EgresoRelatorio((CuentaFilialEgreso) cuenta, monto);
                egresos.add(egresoRelatorio);
                filaNuevaParaTabla = new String[]{"Egreso", egresoRelatorio.getCuentaFilialEgreso().getNombre(), Double.toString(egresoRelatorio.getMonto())};
                totalEgresos += monto;
            }
            if (cuenta.sosIngreso()) {
                IngresoRelatorio ingresoRelatorio = new IngresoRelatorio((CuentaFilialIngreso) cuenta, monto);
                ingresos.add(ingresoRelatorio);
                filaNuevaParaTabla = new String[]{"Ingreso", ingresoRelatorio.getCuenta().getNombre(), Double.toString(ingresoRelatorio.getMonto())};
                totalIngresos += monto;
            }
            getModeloTabla().addRow(filaNuevaParaTabla);
            this.datosCargadosSinGuardar = true;
            actualizarMontosEnTxts();
        } catch (DAOProblemaConTransaccionException ex) {
            JOptionPane.showMessageDialog(this, "No existe la cuenta especificada."); // nunca debería ejecurtarse (poor ahora lo dejo, aunque se que es muy probable que este mal dejarlo)
        }
        this.jTxtMonto.setText("");
    }//GEN-LAST:event_jBtnCargarAsientoCuentaActionPerformed

    private void jComboCuentasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboCuentasActionPerformed
    }//GEN-LAST:event_jComboCuentasActionPerformed

    private void jBtnGuardarRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnGuardarRelatorioActionPerformed
        if (!hayAsientosCargados()) {
            JOptionPane.showMessageDialog(this, "No hay datos que guardar.", "Aviso", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        if (faltanDatos()) {
            JOptionPane.showMessageDialog(this, "Ingrese la caja inicial, final, el diezmo pastoral y filial.", "Aviso", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        if (hayDatosNoObligatoriosSinCargar()) {
            int respuesta = JOptionPane.showConfirmDialog(this, "Los siguientes datos no fueron cargados: "
                    + getDatosObligatoriosSinCargar() + ".\n ¿Guardar el relatorio de todos modos?", "Aviso",
                    JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.NO_OPTION) {
                return;
            }
        }
        int mes = UtilFechas.mesEnNumero(jComboPeriodoMes.getSelectedItem().toString());
        int anio = Integer.parseInt(jComboPeriodoAnio.getSelectedItem().toString());
        double cajaInicial = jTxtCajaInicial.getText().isEmpty() ? 0 : Double.valueOf(jTxtCajaInicial.getText());
        double cajaFinal = jTxtCajaFinal.getText().isEmpty() ? 0 : Double.valueOf(jTxtCajaFinal.getText());
        double diezmoFilial = jTxtDiezmoFilial.getText().isEmpty() ? 0 : Double.valueOf(jTxtCajaInicial.getText());
        double diezmoPastoral = jTxtDiezmoPastoral.getText().isEmpty() ? 0 : Double.valueOf(jTxtCajaFinal.getText());
        GregorianCalendar periodoRelatorio = new GregorianCalendar(anio, mes, 1);
        if (JOptionPane.showConfirmDialog(this, "¿Desea realmente guardar el relatorio cargado?", "Aviso", JOptionPane.YES_NO_OPTION)
                == JOptionPane.YES_OPTION) {
            try {
                institucionCentralTesoreria.saveRelatorio(periodoRelatorio,
                        filial, ingresos, egresos, deudas, cajaInicial, cajaFinal, diezmoFilial, diezmoPastoral);
                deshabilitarCampos();
                limpiarColecciones();
                JOptionPane.showMessageDialog(this, "¡Relatorio guardado!");

            } catch (DAOProblemaConTransaccionException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage());
            }
        }
    }//GEN-LAST:event_jBtnGuardarRelatorioActionPerformed

    private void jBtnDeshacerUltimoAsientoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnDeshacerUltimoAsientoActionPerformed
        try {
            removeAsiento(jXTableAsientos.getSelectedRow());
        } catch (FilaNoSelecionadaException ex) {
            JOptionPane.showMessageDialog(this, "Ningún asiento fue seleccionado");
            return;
        }
        if (getModeloTabla().getRowCount() == 0) {
            datosCargadosSinGuardar = false;
        }
    }//GEN-LAST:event_jBtnDeshacerUltimoAsientoActionPerformed

    private void jTxtTotalNetoPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jTxtTotalNetoPropertyChange
    }//GEN-LAST:event_jTxtTotalNetoPropertyChange

    private void jTxtTotalNetoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtTotalNetoKeyTyped
    }//GEN-LAST:event_jTxtTotalNetoKeyTyped

    private void jTxtCajaInicialFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtCajaInicialFocusLost
    }//GEN-LAST:event_jTxtCajaInicialFocusLost

    private double getIngresos() {
        if (jTxtIngresos.getText().isEmpty()) {
            return 0;
        }
        return Double.valueOf(jTxtEgresos.getText());
    }

    private double getEgresos() {
        if (jTxtIngresos.getText().isEmpty()) {
            return 0;
        }
        return Double.valueOf(jTxtEgresos.getText());

    }
    private void jTxtCajaInicialKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtCajaInicialKeyTyped
        Util.ignorarSiNoEsDouble(evt, jTxtCajaInicial.getText());
    }//GEN-LAST:event_jTxtCajaInicialKeyTyped

    private void jTxtCajaFinalKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtCajaFinalKeyTyped
        Util.ignorarSiNoEsDouble(evt, jTxtCajaFinal.getText());
        }//GEN-LAST:event_jTxtCajaFinalKeyTyped

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        if (!hayAsientosCargados()) {
            this.dispose();
        } else {
            if (JOptionPane.showConfirmDialog(this,
                    "Hay datos cargados sin guardar.\n ¿Desea salir sin guardarlos?", "Aviso",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                this.dispose();
            }
        }
    }//GEN-LAST:event_formWindowClosing

    private void jComboCuentasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboCuentasMouseClicked
    }//GEN-LAST:event_jComboCuentasMouseClicked

    private void jComboCuentasMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboCuentasMouseReleased
        if (jComboCuentas.getSelectedItem().toString().equals("Nueva cuenta...")) {
            JDialogAltaCuentaFilial altaCuentaFilial = new JDialogAltaCuentaFilial(this, true, institucionCentralTesoreria);
            altaCuentaFilial.setVisible(true);
        }
    }//GEN-LAST:event_jComboCuentasMouseReleased

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogAltaRelatorioDeFilial dialog = new JDialogAltaRelatorioDeFilial(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnCargarAsientoCuenta;
    private javax.swing.JButton jBtnDeshacerUltimoAsiento;
    private javax.swing.JButton jBtnGuardarRelatorio;
    private javax.swing.JButton jBtnVolver;
    private javax.swing.JComboBox jComboCuentas;
    private javax.swing.JComboBox jComboPeriodoAnio;
    private javax.swing.JComboBox jComboPeriodoMes;
    private javax.swing.JLabel jLabelCajaFinal;
    private javax.swing.JLabel jLabelCajaInicial;
    private javax.swing.JLabel jLabelMesDeRelatorio1;
    private javax.swing.JLabel jLabelMonto;
    private javax.swing.JLabel jLabelNroDeRelatorio1;
    private javax.swing.JLabel jLabelNroDeRelatorio2;
    private javax.swing.JLabel jLabelNroDeRelatorio3;
    private javax.swing.JLabel jLabelNroDeRelatorio5;
    private javax.swing.JLabel jLabelNroDeRelatorio6;
    private javax.swing.JLabel jLabelNroDeRelatorio8;
    private javax.swing.JLabel jLabelNroDeRelatorio9;
    private javax.swing.JLabel jLabelPeriodoRelatorio;
    private javax.swing.JLabel jLblNombreFilial;
    private javax.swing.JPanel jPnlBotonera;
    private javax.swing.JPanel jPnlDatosGenerales;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTxtCajaFinal;
    private javax.swing.JTextField jTxtCajaInicial;
    private javax.swing.JTextField jTxtDeuda;
    private javax.swing.JTextField jTxtDiezmoFilial;
    private javax.swing.JTextField jTxtDiezmoPastoral;
    private javax.swing.JTextField jTxtEgresos;
    private javax.swing.JTextField jTxtIngresos;
    private javax.swing.JTextField jTxtMonto;
    private javax.swing.JTextField jTxtTotalNeto;
    private org.jdesktop.swingx.JXPanel jXPanelCuerpo;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloDatosRelatorio;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloDetallesDeLasCuentas;
    private org.jdesktop.swingx.JXTable jXTableAsientos;
    // End of variables declaration//GEN-END:variables

    void addNuevaCuentaAJComboYAColeccion(CuentaFilial cuenta) {
        jComboCuentas.removeItem("Nueva cuenta...");
        String tipoCuenta = cuenta.sosDeuda() ? "Deuda" : cuenta.sosEgreso() ? "Egreso" : "Ingreso";
        jComboCuentas.addItem(cuenta.getNombre() + " / " + tipoCuenta);
        jComboCuentas.addItem("Nueva cuenta...");
        jComboCuentas.setSelectedItem(cuenta.getNombre());
    }

    private boolean hayDatosObligatoriosSinCargar() {
        boolean retorno = false;
        retorno = jComboPeriodoAnio.getSelectedIndex() == -1
                || jComboPeriodoMes.getSelectedIndex() == -1
                || jTxtCajaFinal.getText().isEmpty()
                || jTxtCajaInicial.getText().isEmpty()
                || jTxtDiezmoFilial.getText().isEmpty()
                || jTxtDiezmoPastoral.getText().isEmpty()
                || jTxtTotalNeto.getText().isEmpty();
        return retorno;
    }

    private String getDatosObligatoriosSinCargar() {
        String datos = "";
        datos += jComboPeriodoAnio.getSelectedIndex() == -1 ? "*Año\n" : "";
        datos += jComboPeriodoMes.getSelectedIndex() == -1 ? "*Mes \n" : "";
        datos += jTxtCajaFinal.getText().isEmpty() ? "*Caja final \n" : "";
        return datos;
    }

    private boolean hayDatosNoObligatoriosSinCargar() {
        boolean retorno;
        retorno = jTxtDiezmoFilial.getText().isEmpty() && jTxtDiezmoPastoral.getText().isEmpty();
        return retorno;
    }

    private void limpiarCampos() {
        ArrayList<JTextField> camposALimpiar = new ArrayList<JTextField>();
        camposALimpiar.add(jTxtMonto);
        camposALimpiar.add(jTxtCajaInicial);
        camposALimpiar.add(jTxtDiezmoFilial);
        camposALimpiar.add(jTxtDiezmoPastoral);
        camposALimpiar.add(jTxtEgresos);
        camposALimpiar.add(jTxtIngresos);
        camposALimpiar.add(jTxtDeuda);
        camposALimpiar.add(jTxtTotalNeto);
        Vistas.Util.Util.limpiarCamposJText(camposALimpiar);
    }

    private void actualizarMontosEnTxts() {
        jTxtIngresos.setText(String.valueOf(totalIngresos));
        jTxtEgresos.setText(String.valueOf(totalEgresos));
        jTxtDeuda.setText(String.valueOf(totalDeuda));
    }

    private boolean hayIngresosParaEstaCuenta(String nombreDeLaCuenta) {
        boolean retorno = false;
        if (getModeloTabla().getRowCount() == 0) {
            return retorno;
        }
        for (int i = 0; getModeloTabla().getRowCount() > i; i++) {
            if (nombreDeLaCuenta.equals(getModeloTabla().getValueAt(i, 1).toString())) {
                retorno = true;
            }
        }
        return retorno;
    }

    private DefaultTableModel getModeloTabla() {
        return (DefaultTableModel) jXTableAsientos.getModel();
    }

    private void removeAsiento(int filaSeleccionada) throws FilaNoSelecionadaException {
        if (filaSeleccionada == -1) {
            throw new FilaNoSelecionadaException();
        }
        String cuentaSeleccionada = getModeloTabla().getValueAt(filaSeleccionada, 1).toString();
        Iterator<IngresoRelatorio> itIngresos = ingresos.iterator();
        while (itIngresos.hasNext()) {
            IngresoRelatorio ingresoRelatorio = itIngresos.next();
            if (ingresoRelatorio.getCuenta().getNombre().equals(cuentaSeleccionada)) {
                double montoARestar = ingresoRelatorio.getMonto();
                restarIngreso(montoARestar);
                itIngresos.remove();
            }
        }
        Iterator<EgresoRelatorio> itEgresos = egresos.iterator();
        while (itEgresos.hasNext()) {
            EgresoRelatorio egresoRelatorio = itEgresos.next();
            if (egresoRelatorio.getCuentaFilialEgreso().getNombre().equals(cuentaSeleccionada)) {
                double montoARestar = egresoRelatorio.getMonto();
                restarEgreso(montoARestar);
                egresos.remove(egresoRelatorio);
            }
        }
        Iterator<DeudaRelatorio> itDeudas = deudas.iterator();
        while (itDeudas.hasNext()) {
            DeudaRelatorio deudaRelatorio = itDeudas.next();
            if (deudaRelatorio.getCuentaFilialDeuda().getNombre().equals(cuentaSeleccionada)) {
                double montoARestarDeuda = deudaRelatorio.getMonto();
                restarDeuda(montoARestarDeuda);
                deudas.remove(deudaRelatorio);
            }
        }
        getModeloTabla().removeRow(jXTableAsientos.getSelectedRow());
    }

    @SuppressWarnings("unchecked")
    private void cargarCuentasEnJCombo() throws DAOProblemaConTransaccionException {
        Iterator<CuentaFilial> cuentasFilialesIt = institucionCentralTesoreria.getCuentasFiliales().iterator();
        while (cuentasFilialesIt.hasNext()) {
            CuentaFilial cuentaFilial = cuentasFilialesIt.next();
            jComboCuentas.addItem(getNombreDeCuentaConTipo(cuentaFilial));
        }
        jComboCuentas.addItem("Nueva cuenta...");
    }

    private String getStringTipoDeCuenta(String nombreDeLaCuenta) {
        String aCortar = nombreDeLaCuenta.replaceAll("Egreso|Ingreso|Deuda|/", "").trim();
        return aCortar;
    }

    private String getNombreDeCuentaConTipo(CuentaFilial cuentaFilial) {
        String tipoCuenta = cuentaFilial.sosDeuda() ? "Deuda" : cuentaFilial.sosEgreso() ? "Egreso" : "Ingreso";
        return cuentaFilial.getNombre() + " / " + tipoCuenta;
    }

    private boolean datosCargados() {
        return jComboCuentas.getSelectedIndex() != -1 && !jTxtMonto.getText().isEmpty();
    }

    private boolean faltanDatos() {
        return Util.isEmpty(new JTextField[]{jTxtDiezmoFilial, jTxtDiezmoPastoral, jTxtCajaFinal, jTxtCajaInicial});
    }

    private void restarEgreso(double montoARestar) {
        totalEgresos -= montoARestar;
        jTxtEgresos.setText(Double.toString(totalEgresos));
    }

    private void restarIngreso(double montoARestar) {
        totalIngresos -= montoARestar;
        jTxtIngresos.setText(String.valueOf(totalIngresos));
    }

    private void restarDeuda(double montoARestar) {
        totalDeuda -= montoARestar;
        jTxtDeuda.setText(String.valueOf(totalDeuda));
    }

    private boolean hayAsientosCargados() {
        return (!ingresos.isEmpty() || !egresos.isEmpty() || !deudas.isEmpty());
    }

    private void limpiarColecciones() {
        ingresos.clear();
        egresos.clear();
        deudas.clear();
    }

    private void deshabilitarCampos() {
        Util.setEnabled(new Component[]{jBtnCargarAsientoCuenta,
                    jBtnDeshacerUltimoAsiento,
                    jBtnGuardarRelatorio,
                    jComboCuentas,
                    jComboPeriodoAnio,
                    jComboPeriodoMes,
                    jTxtCajaFinal,
                    jTxtCajaInicial,
                    jTxtDeuda,
                    jTxtDiezmoFilial,
                    jTxtDiezmoPastoral,
                    jTxtMonto,
                    jTxtTotalNeto},
                false);
    }
}
