/*/*
 * JDialogAltaFilial.java
 *
 * Created on 15/09/2011, 09:18:26
 */
package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.Direccion;
import Modelo.Filial;
import Modelo.InstitucionCentral;
import Modelo.PastorFilial;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author emanuel
 */
public class JDialogAltaFilial extends javax.swing.JDialog {

    JDialogABMFiliales padre = null;
    InstitucionCentral institucion;
    private boolean llamadoParaModificarLaFilial;
    private boolean filialModificada = false;
    private Filial filialAModificar;

    /** Creates new form JDialogAltaFilial */
    public JDialogAltaFilial(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
    }

    public JDialogAltaFilial(JDialogABMFiliales padre, boolean modal, InstitucionCentral institucionCentral,
            boolean llamadoParaModificarFilial) {
        this.padre = padre;
        this.initComponents();
        this.setModal(modal);
        this.llamadoParaModificarLaFilial = llamadoParaModificarFilial;
        this.institucion = institucionCentral;
        this.setLocationRelativeTo(null);
        this.setLocationRelativeTo(null);
    }

    public JDialogAltaFilial(JDialogABMFiliales padre, boolean modal, InstitucionCentral institucionCentral,
            boolean llamadoParaModificarMiembro, Filial filialAModificar) {
        this.initComponents();
        this.padre = padre;
        this.setModal(modal);
        this.llamadoParaModificarLaFilial = llamadoParaModificarMiembro;
        this.institucion = institucionCentral;
        this.filialAModificar = filialAModificar;
        cargarDatosDeFilialEnCampos();
        this.setLocationRelativeTo(null);
    }

    public boolean llamadoParaModificarLaFilial() {
        return llamadoParaModificarLaFilial;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jXSeparadorDeTituloDatosDeNuevoMiembro = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelNombreDeFilial = new javax.swing.JLabel();
        jTxtNombreFilial = new javax.swing.JTextField();
        jLabelTelFijo = new javax.swing.JLabel();
        jTxtTelFijoFilial = new javax.swing.JTextField();
        jXSeparadorDeTituloDireccion = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelDireccionCalle = new javax.swing.JLabel();
        jTxtDireccionCalle = new javax.swing.JTextField();
        jLabelDireccionAltura = new javax.swing.JLabel();
        jTxtDireccionAltura = new javax.swing.JTextField();
        jTxtDireccionCiudad = new javax.swing.JTextField();
        jLabelCiudad = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jBtnAgregarAgregarFilial_GuardarCambios = new javax.swing.JButton();
        jBtnVolverInicio_ABMFilial = new javax.swing.JButton();
        jXSeparadorDeTituloDireccion1 = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelNombrePastor = new javax.swing.JLabel();
        jLabelCiudad1 = new javax.swing.JLabel();
        jTxtNombrePastor = new javax.swing.JTextField();
        jTxtApellidoPastor = new javax.swing.JTextField();
        jLabelFechaOrdenacion = new javax.swing.JLabel();
        jLabelTelFijoPastor = new javax.swing.JLabel();
        jLabelTelCelPastor = new javax.swing.JLabel();
        jTxtTelCelPastor = new javax.swing.JTextField();
        jTxtTelFijoPastor = new javax.swing.JTextField();
        jLabelConyuge = new javax.swing.JLabel();
        jTxtConyuge = new javax.swing.JTextField();
        jXDatePickerFechaDeOrdenacionPastor = new org.jdesktop.swingx.JXDatePicker();
        jTxtDireccionCallePastor = new javax.swing.JTextField();
        jLabelDireccionCalle1 = new javax.swing.JLabel();
        jLabelCiudad2 = new javax.swing.JLabel();
        jTxtDireccionCiudadPastor = new javax.swing.JTextField();
        jLabelDireccionAltura1 = new javax.swing.JLabel();
        jTxtDireccionAlturaPastor = new javax.swing.JTextField();
        jXSeparadorDeTituloDireccion2 = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelNombrePastor1 = new javax.swing.JLabel();
        jTxtDniPastor = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setPreferredSize(new java.awt.Dimension(600, 500));

        jXSeparadorDeTituloDatosDeNuevoMiembro.setFont(new java.awt.Font("Tahoma", 0, 12));
        jXSeparadorDeTituloDatosDeNuevoMiembro.setHorizontalAlignment(0
        );
        jXSeparadorDeTituloDatosDeNuevoMiembro.setTitle("Datos de filial");

        jLabelNombreDeFilial.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelNombreDeFilial.setText("Nombre filial:");

        jTxtNombreFilial.setName("Nombre"); // NOI18N
        jTxtNombreFilial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtNombreFilialKeyTyped(evt);
            }
        });

        jLabelTelFijo.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelTelFijo.setText("Teléfono:");

        jTxtTelFijoFilial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtTelFijoFilialKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtTelFijoFilialKeyTyped(evt);
            }
        });

        jXSeparadorDeTituloDireccion.setFont(new java.awt.Font("Tahoma", 0, 12));
        jXSeparadorDeTituloDireccion.setHorizontalAlignment(0);
        jXSeparadorDeTituloDireccion.setHorizontalTextPosition(0);
        jXSeparadorDeTituloDireccion.setTitle("Dirección");

        jLabelDireccionCalle.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelDireccionCalle.setText("Calle:");

        jTxtDireccionCalle.setColumns(5);
        jTxtDireccionCalle.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtDireccionCalleKeyTyped(evt);
            }
        });

        jLabelDireccionAltura.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelDireccionAltura.setText("Número/altura:");

        jTxtDireccionAltura.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtDireccionAlturaKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtDireccionAlturaKeyTyped(evt);
            }
        });

        jTxtDireccionCiudad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtDireccionCiudadKeyTyped(evt);
            }
        });

        jLabelCiudad.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelCiudad.setText("Ciudad:");

        jBtnAgregarAgregarFilial_GuardarCambios.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnAgregarAgregarFilial_GuardarCambios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Iglesia-add.png"))); // NOI18N
        jBtnAgregarAgregarFilial_GuardarCambios.setText("Agregar");
        jBtnAgregarAgregarFilial_GuardarCambios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnAgregarAgregarFilial_GuardarCambiosActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnAgregarAgregarFilial_GuardarCambios);

        jBtnVolverInicio_ABMFilial.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnVolverInicio_ABMFilial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolverInicio_ABMFilial.setText("Volver");
        jBtnVolverInicio_ABMFilial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverInicio_ABMFilialActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnVolverInicio_ABMFilial);

        jXSeparadorDeTituloDireccion1.setFont(new java.awt.Font("Tahoma", 0, 12));
        jXSeparadorDeTituloDireccion1.setHorizontalAlignment(0);
        jXSeparadorDeTituloDireccion1.setHorizontalTextPosition(0);
        jXSeparadorDeTituloDireccion1.setTitle("Datos del pastor responsable");

        jLabelNombrePastor.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelNombrePastor.setText("Nombre:");

        jLabelCiudad1.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelCiudad1.setText("Apellido:");

        jTxtNombrePastor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtNombrePastorKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtNombrePastorKeyTyped(evt);
            }
        });

        jTxtApellidoPastor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtApellidoPastorKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtApellidoPastorKeyTyped(evt);
            }
        });

        jLabelFechaOrdenacion.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelFechaOrdenacion.setText("Fecha de ordenación:");

        jLabelTelFijoPastor.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelTelFijoPastor.setText("Teléfono fijo:");

        jLabelTelCelPastor.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelTelCelPastor.setText("Teléfono celular:");

        jTxtTelCelPastor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtTelCelPastorKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxtTelCelPastorKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtTelCelPastorKeyTyped(evt);
            }
        });

        jTxtTelFijoPastor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtTelFijoPastorKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtTelFijoPastorKeyTyped(evt);
            }
        });

        jLabelConyuge.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelConyuge.setText("Cónyuge:");

        jTxtConyuge.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtConyugeKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxtConyugeKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtConyugeKeyTyped(evt);
            }
        });

        jXDatePickerFechaDeOrdenacionPastor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jXDatePickerFechaDeOrdenacionPastorKeyPressed(evt);
            }
        });

        jTxtDireccionCallePastor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtDireccionCallePastorKeyTyped(evt);
            }
        });

        jLabelDireccionCalle1.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelDireccionCalle1.setText("Calle:");

        jLabelCiudad2.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelCiudad2.setText("Ciudad:");

        jTxtDireccionCiudadPastor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtDireccionCiudadPastorKeyTyped(evt);
            }
        });

        jLabelDireccionAltura1.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelDireccionAltura1.setText("Número/altura:");

        jTxtDireccionAlturaPastor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtDireccionAlturaPastorKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxtDireccionAlturaPastorKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtDireccionAlturaPastorKeyTyped(evt);
            }
        });

        jXSeparadorDeTituloDireccion2.setFont(new java.awt.Font("Tahoma", 0, 12));
        jXSeparadorDeTituloDireccion2.setHorizontalAlignment(0);
        jXSeparadorDeTituloDireccion2.setHorizontalTextPosition(0);
        jXSeparadorDeTituloDireccion2.setTitle("Dirección de pastor");

        jLabelNombrePastor1.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelNombrePastor1.setText("Dni:");

        jTxtDniPastor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtDniPastorKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtDniPastorKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jTxtDireccionAltura, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jTxtDireccionAlturaPastor, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jXSeparadorDeTituloDireccion, javax.swing.GroupLayout.DEFAULT_SIZE, 310, Short.MAX_VALUE)
                        .addGap(76, 76, 76))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jXSeparadorDeTituloDatosDeNuevoMiembro, javax.swing.GroupLayout.PREFERRED_SIZE, 376, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelNombreDeFilial)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addComponent(jTxtNombreFilial, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(30, 30, 30)
                                        .addComponent(jTxtTelFijoFilial, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabelTelFijo)))))
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelDireccionCalle)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jTxtDireccionCalle, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jTxtDireccionCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabelCiudad))
                        .addContainerGap(82, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jXSeparadorDeTituloDireccion1, javax.swing.GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addComponent(jTxtDireccionCallePastor, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabelDireccionCalle1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addComponent(jTxtDireccionCiudadPastor, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabelCiudad2)))
                            .addComponent(jLabelDireccionAltura1))
                        .addContainerGap(90, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelDireccionAltura)
                        .addContainerGap(304, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelConyuge, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(316, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jXSeparadorDeTituloDireccion2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jTxtConyuge))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelNombrePastor1)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addGap(12, 12, 12)
                                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(jTxtTelCelPastor, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jTxtDniPastor, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addComponent(jLabelTelCelPastor))
                                    .addGap(18, 18, 18)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelTelFijoPastor)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addGap(12, 12, 12)
                                            .addComponent(jTxtTelFijoPastor, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jLabelFechaOrdenacion)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addGap(12, 12, 12)
                                            .addComponent(jXDatePickerFechaDeOrdenacionPastor, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addGap(12, 12, 12)
                                            .addComponent(jTxtNombrePastor, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jLabelNombrePastor))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelCiudad1)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addGap(12, 12, 12)
                                            .addComponent(jTxtApellidoPastor, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                        .addContainerGap(82, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jXSeparadorDeTituloDatosDeNuevoMiembro, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelNombreDeFilial)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxtNombreFilial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelTelFijo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxtTelFijoFilial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jXSeparadorDeTituloDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelDireccionCalle))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTxtDireccionCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTxtDireccionCalle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(jLabelDireccionAltura)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtDireccionAltura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jXSeparadorDeTituloDireccion1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTxtNombrePastor, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelNombrePastor)
                        .addGap(25, 25, 25))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelCiudad1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxtApellidoPastor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelFechaOrdenacion)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTxtDniPastor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jXDatePickerFechaDeOrdenacionPastor, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabelNombrePastor1)
                            .addGap(25, 25, 25))))
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelTelCelPastor)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxtTelCelPastor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelTelFijoPastor)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxtTelFijoPastor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(12, 12, 12)
                .addComponent(jLabelConyuge)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtConyuge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jXSeparadorDeTituloDireccion2, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jTxtDireccionCallePastor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelCiudad2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelDireccionCalle1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxtDireccionCiudadPastor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelDireccionAltura1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtDireccionAlturaPastor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 396, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 629, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTxtNombreFilialKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtNombreFilialKeyTyped
        if (!Vistas.Util.Util.esLetra(evt.getKeyChar())) {
            evt.consume();
        }
        Vistas.Util.Util.limitarCaracteres(evt, jTxtNombreFilial.getText(), 100); 
}//GEN-LAST:event_jTxtNombreFilialKeyTyped

    private void jTxtTelFijoFilialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtTelFijoFilialKeyPressed

}//GEN-LAST:event_jTxtTelFijoFilialKeyPressed

    private void jTxtTelFijoFilialKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtTelFijoFilialKeyTyped
        if (Vistas.Util.Util.esLetra(evt.getKeyChar())) {
            evt.consume();
        }
        Vistas.Util.Util.limitarCaracteres(evt, jTxtTelFijoFilial.getText(), 12); 
}//GEN-LAST:event_jTxtTelFijoFilialKeyTyped

    private void jTxtDireccionAlturaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtDireccionAlturaKeyTyped
        Vistas.Util.Util.ignorarSiNoEsEntero(evt);
}//GEN-LAST:event_jTxtDireccionAlturaKeyTyped

    private void jBtnAgregarAgregarFilial_GuardarCambiosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnAgregarAgregarFilial_GuardarCambiosActionPerformed
        /*Pasos a seguir: 1) consulto si está cargado el nombre de la filial, si no está cargado, aviso y volvemos a la pantalla
         * 2)pregunto por el nombre y el apellido del pastor, si no están cargados aviso y volvemos a la pantalla
         */
        String nombreFilial = this.jTxtNombreFilial.getText();
        if (nombreFilial.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Ingrese el nombre de la filial.", "Aviso", JOptionPane.INFORMATION_MESSAGE);
            return;
        } else {
            if (faltanDatosObligatoriosDePastor()) {
                JOptionPane.showMessageDialog(this, "Ingrese al menos nombre y apellido del pastor de la filial.", "Aviso", JOptionPane.INFORMATION_MESSAGE);
                return;
            } else {
                String nombreDePastor = this.jTxtNombrePastor.getText();
                String apellidoDePastor = this.jTxtApellidoPastor.getText();
                long dniPastor = !jTxtDniPastor.getText().isEmpty() ? Long.valueOf(jTxtDniPastor.getText()): 0 ;
                long telefonoFilial = !this.jTxtTelFijoFilial.getText().isEmpty() ? Long.valueOf(this.jTxtTelFijoFilial.getText()) : 0;
                String ciudad = this.jTxtDireccionCiudad.getText();
                String calle = this.jTxtDireccionCalle.getText();
                int altura = Integer.valueOf(this.jTxtDireccionAltura.getText());
                Direccion direccionFilial = new Direccion(calle, altura, ciudad);
                String conyugeDePastor = this.jTxtConyuge.getText();
                long telFijoPastor = !this.jTxtTelFijoPastor.getText().isEmpty() ? Long.valueOf(this.jTxtTelFijoPastor.getText()) : 0;
                long telCelularPastor = !this.jTxtTelCelPastor.getText().isEmpty() ? Long.valueOf(this.jTxtTelCelPastor.getText()) : 0;
                GregorianCalendar fechaDeOrdenacion = null;
                if (jXDatePickerFechaDeOrdenacionPastor.getDate() != null) {
                    fechaDeOrdenacion = new GregorianCalendar(jXDatePickerFechaDeOrdenacionPastor.getDate().getYear(),
                            jXDatePickerFechaDeOrdenacionPastor.getDate().getMonth(), jXDatePickerFechaDeOrdenacionPastor.getDate().getDay());
                }
                String ciudadPastor = this.jTxtDireccionCiudad.getText();
                String callePastor = this.jTxtDireccionCalle.getText();
                int alturaPastor = !this.jTxtDireccionAltura.getText().isEmpty() ? Integer.valueOf(this.jTxtDireccionAltura.getText()) :0;
                Direccion direccionPastor = new Direccion(callePastor, alturaPastor, ciudadPastor);
                PastorFilial pastorFilial = new PastorFilial(calle, apellidoDePastor, direccionPastor, telefonoFilial,
                        telCelularPastor, conyugeDePastor, fechaDeOrdenacion, dniPastor);
                try {
                    //Ya tenemos todos los datos, procedemos a persistirlos y pasarlos ABMFiliales en caso de que sea pertinente
                    if (llamadoParaModificarLaFilial()) {
                        filialAModificar = this.institucion.modificarDatosDeFilial(filialAModificar, nombreFilial, direccionFilial, telefonoFilial, pastorFilial);
                        this.padre.addFilialEnJTableYEnColeccion(filialAModificar);
                        this.dispose();
                        this.padre.setVisible(true);
                    } else {
                        Filial filialNueva = this.institucion.registrarFilial(calle, direccionFilial, telefonoFilial, pastorFilial);
                        this.padre.addFilialEnJTableYEnColeccion(filialNueva);
                        this.padre.setVisible(true);
                        this.dispose();
                    }
                } catch (DAOProblemaConTransaccionException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.ERROR_MESSAGE);
                } catch (Excepciones.DAOYaExisteRegistroException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
}//GEN-LAST:event_jBtnAgregarAgregarFilial_GuardarCambiosActionPerformed

    public boolean faltanDatosObligatoriosDePastor() {
        boolean faltanDatos = false;
        if (jTxtApellidoPastor.getText().isEmpty() || jTxtNombrePastor.getText().isEmpty()) {
            faltanDatos = true;
        }
        return faltanDatos;
    }
    private void jBtnVolverInicio_ABMFilialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVolverInicio_ABMFilialActionPerformed
        if (this.llamadoParaModificarLaFilial && this.filialModificada) {//implica que fue modificada y PERSISTIDA :)
            this.padre.setVisible(true);
            this.dispose();
        } else {
            int respuesta = JOptionPane.showConfirmDialog(this, "No ha guardado cambios en los datos de la filial"
                    + " ¿Salir de todos modos?", "Aviso", JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_OPTION) {
                this.dispose();
                this.padre.setVisible(true);
            } else {
                return;
            }
        }

}//GEN-LAST:event_jBtnVolverInicio_ABMFilialActionPerformed

    private void jTxtTelCelPastorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtTelCelPastorKeyReleased
}//GEN-LAST:event_jTxtTelCelPastorKeyReleased

    private void jTxtTelCelPastorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtTelCelPastorKeyTyped
        Vistas.Util.Util.ignorarSiNoEsEntero(evt);
}//GEN-LAST:event_jTxtTelCelPastorKeyTyped

    private void jTxtConyugeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtConyugeKeyReleased
    }//GEN-LAST:event_jTxtConyugeKeyReleased

    private void jTxtConyugeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtConyugeKeyTyped
        Vistas.Util.Util.limitarCaracteres(evt, jTxtConyuge.getText(), 100); 
    }//GEN-LAST:event_jTxtConyugeKeyTyped

    private void jTxtDireccionAlturaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtDireccionAlturaKeyPressed
        Vistas.Util.Util.ignorarSiNoEsEntero(evt);
    }//GEN-LAST:event_jTxtDireccionAlturaKeyPressed

    private void jTxtNombrePastorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtNombrePastorKeyPressed
    }//GEN-LAST:event_jTxtNombrePastorKeyPressed

    private void jTxtApellidoPastorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtApellidoPastorKeyPressed
        Vistas.Util.Util.ignorarSiNoEsLetra(evt);
    }//GEN-LAST:event_jTxtApellidoPastorKeyPressed

    private void jTxtTelFijoPastorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtTelFijoPastorKeyPressed

    }//GEN-LAST:event_jTxtTelFijoPastorKeyPressed

    private void jTxtTelCelPastorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtTelCelPastorKeyPressed

    }//GEN-LAST:event_jTxtTelCelPastorKeyPressed

    private void jTxtConyugeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtConyugeKeyPressed
    }//GEN-LAST:event_jTxtConyugeKeyPressed

    private void jTxtDireccionAlturaPastorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtDireccionAlturaPastorKeyPressed
    }//GEN-LAST:event_jTxtDireccionAlturaPastorKeyPressed

    private void jTxtDireccionAlturaPastorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtDireccionAlturaPastorKeyTyped
        Vistas.Util.Util.ignorarSiNoEsEntero(evt);
    }//GEN-LAST:event_jTxtDireccionAlturaPastorKeyTyped

    private void jTxtDniPastorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtDniPastorKeyPressed
    }//GEN-LAST:event_jTxtDniPastorKeyPressed

    private void jTxtNombrePastorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtNombrePastorKeyTyped
    }//GEN-LAST:event_jTxtNombrePastorKeyTyped

    private void jTxtApellidoPastorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtApellidoPastorKeyTyped
        Vistas.Util.Util.ignorarSiNoEsLetra(evt);
        Vistas.Util.Util.limitarCaracteres(evt, jTxtApellidoPastor.getText(), 40); 
    }//GEN-LAST:event_jTxtApellidoPastorKeyTyped

    private void jTxtDniPastorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtDniPastorKeyTyped
        Vistas.Util.Util.ignorarSiNoEsEntero(evt);
    }//GEN-LAST:event_jTxtDniPastorKeyTyped

    private void jXDatePickerFechaDeOrdenacionPastorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jXDatePickerFechaDeOrdenacionPastorKeyPressed
        evt.consume();
    }//GEN-LAST:event_jXDatePickerFechaDeOrdenacionPastorKeyPressed

    private void jTxtTelFijoPastorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtTelFijoPastorKeyTyped
        Vistas.Util.Util.ignorarSiNoEsEntero(evt);
    }//GEN-LAST:event_jTxtTelFijoPastorKeyTyped

    private void jTxtDireccionAlturaPastorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtDireccionAlturaPastorKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_jTxtDireccionAlturaPastorKeyReleased

    private void jTxtDireccionCalleKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtDireccionCalleKeyTyped
        Vistas.Util.Util.limitarCaracteres(evt, this.jTxtDireccionCalle.getText(), 40);
    }//GEN-LAST:event_jTxtDireccionCalleKeyTyped

    private void jTxtDireccionCiudadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtDireccionCiudadKeyTyped
        Vistas.Util.Util.limitarCaracteres(evt, jTxtDireccionCiudad.getText(), 40); 
    }//GEN-LAST:event_jTxtDireccionCiudadKeyTyped

    private void jTxtDireccionCallePastorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtDireccionCallePastorKeyTyped
        Vistas.Util.Util.limitarCaracteres(evt, jTxtDireccionCalle.getText(), 100); 
    }//GEN-LAST:event_jTxtDireccionCallePastorKeyTyped

    private void jTxtDireccionCiudadPastorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtDireccionCiudadPastorKeyTyped
        Vistas.Util.Util.limitarCaracteres(evt, jTxtDireccionCiudad.getText(), 100); 
    }//GEN-LAST:event_jTxtDireccionCiudadPastorKeyTyped

    private void modificarAspectoDejBtnAgregarMiembro_GuardarCambios() {
        jBtnAgregarAgregarFilial_GuardarCambios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Guardar 30x30.png")));
        jBtnAgregarAgregarFilial_GuardarCambios.setText("Guardar y seguir");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogAltaFilial dialog = new JDialogAltaFilial(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnAgregarAgregarFilial_GuardarCambios;
    private javax.swing.JButton jBtnVolverInicio_ABMFilial;
    private javax.swing.JLabel jLabelCiudad;
    private javax.swing.JLabel jLabelCiudad1;
    private javax.swing.JLabel jLabelCiudad2;
    private javax.swing.JLabel jLabelConyuge;
    private javax.swing.JLabel jLabelDireccionAltura;
    private javax.swing.JLabel jLabelDireccionAltura1;
    private javax.swing.JLabel jLabelDireccionCalle;
    private javax.swing.JLabel jLabelDireccionCalle1;
    private javax.swing.JLabel jLabelFechaOrdenacion;
    private javax.swing.JLabel jLabelNombreDeFilial;
    private javax.swing.JLabel jLabelNombrePastor;
    private javax.swing.JLabel jLabelNombrePastor1;
    private javax.swing.JLabel jLabelTelCelPastor;
    private javax.swing.JLabel jLabelTelFijo;
    private javax.swing.JLabel jLabelTelFijoPastor;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JTextField jTxtApellidoPastor;
    private javax.swing.JTextField jTxtConyuge;
    private javax.swing.JTextField jTxtDireccionAltura;
    private javax.swing.JTextField jTxtDireccionAlturaPastor;
    private javax.swing.JTextField jTxtDireccionCalle;
    private javax.swing.JTextField jTxtDireccionCallePastor;
    private javax.swing.JTextField jTxtDireccionCiudad;
    private javax.swing.JTextField jTxtDireccionCiudadPastor;
    private javax.swing.JTextField jTxtDniPastor;
    private javax.swing.JTextField jTxtNombreFilial;
    private javax.swing.JTextField jTxtNombrePastor;
    private javax.swing.JTextField jTxtTelCelPastor;
    private javax.swing.JTextField jTxtTelFijoFilial;
    private javax.swing.JTextField jTxtTelFijoPastor;
    private org.jdesktop.swingx.JXDatePicker jXDatePickerFechaDeOrdenacionPastor;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloDatosDeNuevoMiembro;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloDireccion;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloDireccion1;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloDireccion2;
    // End of variables declaration//GEN-END:variables

    /**
     * @return the llamadoParaModificarLaFilial
     */
    public boolean isLlamadoParaModificarLaFilial() {
        return llamadoParaModificarLaFilial;
    }

    /**
     * @param llamadoParaModificarLaFilial the llamadoParaModificarLaFilial to set
     */
    public void setLlamadoParaModificarLaFilial(boolean llamadoParaModificarLaFilial) {
        this.llamadoParaModificarLaFilial = llamadoParaModificarLaFilial;
    }

    /**
     * @return the filialModificada
     */
    public boolean isFilialModificada() {
        return filialModificada;
    }

    /**
     * @param filialModificada the filialModificada to set
     */
    public void setFilialModificada(boolean filialModificada) {
        this.filialModificada = filialModificada;
    }

    private void limpiarTodosLosCampos() {
        java.awt.Component[] components = this.getComponents();
        ArrayList<JTextField> jTextFields = new ArrayList<JTextField>();
        for (java.awt.Component component : components) {
            if (component instanceof JTextField) {
                jTextFields.add((JTextField) component);
            }
        }
        Vistas.Util.Util.limpiarCamposJText(jTextFields);

    }

    private void cargarDatosDeFilialEnCampos() {
        String apellidoPastor = filialAModificar.getPastorFilial().getApellidos();
        this.jTxtApellidoPastor.setText(apellidoPastor);
        this.jTxtConyuge.setText(filialAModificar.getPastorFilial().getConyuge());
        this.jTxtDireccionAltura.setText(String.valueOf(filialAModificar.getDireccion().getAltura()));
        this.jTxtDireccionCalle.setText(String.valueOf(filialAModificar.getDireccion().getCalleOBarrio()));
        this.jTxtDireccionCiudad.setText(String.valueOf(filialAModificar.getDireccion().getCiudad()));
        this.jTxtDireccionAlturaPastor.setText(String.valueOf(filialAModificar.getPastorFilial().getDireccion().getAltura()));
        this.jTxtDireccionCallePastor.setText(String.valueOf(filialAModificar.getPastorFilial().getDireccion().getCalleOBarrio()));
        this.jTxtDireccionCiudadPastor.setText(String.valueOf(filialAModificar.getPastorFilial().getDireccion().getCiudad()));
        this.jTxtDniPastor.setText(String.valueOf(filialAModificar.getPastorFilial().getDni()));
        this.jTxtTelFijoPastor.setText(String.valueOf(filialAModificar.getPastorFilial().getTelefonoFijo()));
        this.jTxtNombreFilial.setText(filialAModificar.getNombre());
        this.jTxtNombrePastor.setText(filialAModificar.getPastorFilial().getNombres());
        this.jTxtTelCelPastor.setText(String.valueOf(filialAModificar.getPastorFilial().getTelefonoCelular()));
        this.jTxtTelFijoFilial.setText(String.valueOf(filialAModificar.getTelefono()));
        Date fechaDeOrdenacion = new Date(filialAModificar.getPastorFilial().getFechaDeOrdenacion().get(Calendar.YEAR),
                filialAModificar.getPastorFilial().getFechaDeOrdenacion().get(Calendar.MONTH),
                filialAModificar.getPastorFilial().getFechaDeOrdenacion().get(Calendar.DAY_OF_MONTH));
        this.jXDatePickerFechaDeOrdenacionPastor.setDate(fechaDeOrdenacion);
    }
}
