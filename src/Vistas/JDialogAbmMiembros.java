package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.InstitucionCentral;
import Modelo.InstitucionCentralTesoreria;
import Modelo.Miembro;
import Util.MessageUtils;
import Util.ReportUtils;
import java.awt.Color;
import java.awt.event.*;
import java.awt.print.PrinterException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import reportes.PrinterUtils;

public class JDialogAbmMiembros extends javax.swing.JDialog {

    private InstitucionCentral institucionCentral;
    private Boolean abiertoParaSeleccionarUnMiembro;
    private JDialogAltaIngreso padre;
    private HashSet<Miembro> miembros;

    public JDialogAbmMiembros(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        addListeners();
    }

    private void addListeners(){
        jBtnImprimirListado.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    try {
                        PrinterUtils.pruebaImpresion();
                        HashMap<String, Object> parametros = new HashMap<String, Object>();
                        parametros.put("solo_activos", true);
                        parametros.put("SUBREPORT_DIR", ReportUtils.getPathOfReports());
                        ReportUtils.printReport("listado_de_miembros", parametros);
                    } catch (PrinterException ex) {
                        MessageUtils.showWarning(JDialogAbmMiembros.this, ex.getMessage());
                    }
                }
            });
        jTxtApellido.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                buscarMiembro();
            }
        });
        jTxtApellido.addKeyListener(new KeyAdapter() {

            @Override
            public void keyTyped(KeyEvent e) {
                if(e.getKeyChar() == KeyEvent.VK_ENTER){
                    buscarMiembro();
                }
            }
        });
        jTxtNombre.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                buscarMiembro();
            }
        });
    }

    public JDialogAbmMiembros(JFrameSistema parent, boolean modal, InstitucionCentral institucionCentralTesoreria) {
        this(parent, modal);
        modificarVistaParaJFrameSistema();
        this.institucionCentral = institucionCentralTesoreria;
        abiertoParaSeleccionarUnMiembro = Boolean.FALSE;
        setLocationRelativeTo(null);
    }

    JDialogAbmMiembros(JDialogAltaIngreso padre, boolean modal, InstitucionCentralTesoreria institucion, boolean abiertoParaSerSeleccionado) {
        super(padre, modal);
        initComponents();
        this.padre = padre;
        institucionCentral = institucion;
        abiertoParaSeleccionarUnMiembro = abiertoParaSerSeleccionado;
        if (abiertoParaSerSeleccionado) {
            modificarVistaParaJDialogAltaIngreso();
        }
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabelApellidoDato1 = new javax.swing.JLabel();
        jLabelTelCelDato = new javax.swing.JLabel();
        jLabelDireccionDato = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jXSeparadorDeTituloBuscarMiembro2 = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelApellido2 = new javax.swing.JLabel();
        jLabelNombre2 = new javax.swing.JLabel();
        jTxtApellido = new javax.swing.JTextField();
        jTxtNombre = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        jLblApellidoDato = new javax.swing.JLabel();
        jLblApellidos = new javax.swing.JLabel();
        jLblFechaBautismo = new javax.swing.JLabel();
        jLblFechaDeBautismoDato = new javax.swing.JLabel();
        jLblActivo = new javax.swing.JLabel();
        jLblTelFijo = new javax.swing.JLabel();
        jLblTelFijoDato = new javax.swing.JLabel();
        jLblNombresDato = new javax.swing.JLabel();
        jLblNombres = new javax.swing.JLabel();
        jLblDireccion = new javax.swing.JLabel();
        jLblTelCelular = new javax.swing.JLabel();
        jLblDireccionDato = new javax.swing.JLabel();
        jLblTelCelDato = new javax.swing.JLabel();
        jLblActivoDato = new javax.swing.JLabel();
        jXSeparadorDeTituloBuscarMiembro3 = new org.jdesktop.swingx.JXTitledSeparator();
        jPanel8 = new javax.swing.JPanel();
        jBtnImprimirListado = new javax.swing.JButton();
        jBtnModificarMiembro = new javax.swing.JButton();
        jBtnEliminarMIembro = new javax.swing.JButton();
        jBtnAgregarMiembro = new javax.swing.JButton();
        jBtnInicio_seleccionarMiembro = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jXTableMiembros = new javax.swing.JTable();
        addListeners();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(204, 204, 204));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelApellidoDato1.setText("                ");

        jLabelTelCelDato.setText("                ");

        jLabelDireccionDato.setText("                ");

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 114, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 54, Short.MAX_VALUE)
        );

        jXSeparadorDeTituloBuscarMiembro2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jXSeparadorDeTituloBuscarMiembro2.setHorizontalAlignment(0);
        jXSeparadorDeTituloBuscarMiembro2.setHorizontalTextPosition(0);
        jXSeparadorDeTituloBuscarMiembro2.setTitle("Buscar miembro");

        jLabelApellido2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelApellido2.setText("Apellido:");

        jLabelNombre2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelNombre2.setText("Nombre:");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jXSeparadorDeTituloBuscarMiembro2, javax.swing.GroupLayout.DEFAULT_SIZE, 790, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelApellido2)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jTxtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(21, 21, 21)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jTxtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabelNombre2))
                .addContainerGap(473, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jXSeparadorDeTituloBuscarMiembro2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabelApellido2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabelNombre2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(17, 17, 17))
        );

        jLblApellidoDato.setText("                ");

        jLblApellidos.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLblApellidos.setText("Apellido/s:");

        jLblFechaBautismo.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLblFechaBautismo.setText("Fecha de bautismo:");

        jLblFechaDeBautismoDato.setText("                ");

        jLblActivo.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLblActivo.setText("Activo:");

        jLblTelFijo.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLblTelFijo.setText("Teléfono fijo:");

        jLblTelFijoDato.setText("                ");

        jLblNombresDato.setText("                ");

        jLblNombres.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLblNombres.setText("Nombre/s:");

        jLblDireccion.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLblDireccion.setText("Dirección:");

        jLblTelCelular.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLblTelCelular.setText("Teléfono celular:");

        jLblDireccionDato.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLblDireccionDato.setText("                        ");

        jLblTelCelDato.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLblTelCelDato.setText("                        ");

        jLblActivoDato.setText("                ");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLblActivoDato, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLblApellidos)
                            .addComponent(jLblFechaBautismo)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLblFechaDeBautismoDato, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLblApellidoDato, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(122, 122, 122)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLblTelFijo)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel7Layout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addComponent(jLblNombresDato, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLblNombres)))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLblTelFijoDato, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(107, 107, 107)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLblTelCelular)
                            .addComponent(jLblDireccion)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLblDireccionDato)
                                    .addComponent(jLblTelCelDato)))))
                    .addComponent(jLblActivo))
                .addContainerGap(150, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLblNombres)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblNombresDato)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLblTelFijo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblTelFijoDato))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLblApellidos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblApellidoDato)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblFechaBautismo, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblFechaDeBautismoDato))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLblDireccion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblDireccionDato)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLblTelCelular)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblTelCelDato)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLblActivo, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLblActivoDato))
        );

        jXSeparadorDeTituloBuscarMiembro3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jXSeparadorDeTituloBuscarMiembro3.setHorizontalAlignment(0);
        jXSeparadorDeTituloBuscarMiembro3.setHorizontalTextPosition(0);
        jXSeparadorDeTituloBuscarMiembro3.setTitle("Buscar miembro");

        jBtnImprimirListado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Imprimir 20x20.png"))); // NOI18N
        jBtnImprimirListado.setText("Imprimir listado");
        jPanel8.add(jBtnImprimirListado);

        jBtnModificarMiembro.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnModificarMiembro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Modificar miembro20x20.png"))); // NOI18N
        jBtnModificarMiembro.setText("Modificar");
        jBtnModificarMiembro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnModificarMiembroActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnModificarMiembro);

        jBtnEliminarMIembro.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnEliminarMIembro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Eliminar miembro20x20.png"))); // NOI18N
        jBtnEliminarMIembro.setText("Eliminar");
        jBtnEliminarMIembro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnEliminarMIembroActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnEliminarMIembro);

        jBtnAgregarMiembro.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnAgregarMiembro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Agregar miembro20x20.png"))); // NOI18N
        jBtnAgregarMiembro.setText("Agregar");
        jBtnAgregarMiembro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnAgregarMiembroActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnAgregarMiembro);

        jBtnInicio_seleccionarMiembro.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnInicio_seleccionarMiembro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnInicio_seleccionarMiembro.setText("Inicio");
        jBtnInicio_seleccionarMiembro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnInicio_seleccionarMiembroActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnInicio_seleccionarMiembro);

        jXTableMiembros.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {},
            new String [] {
                "Id", "Apellido", "Nombre", "Dirección"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jXTableMiembros.addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                Miembro miembroSeleccionadoEnJTable = JDialogAbmMiembros.this.getMiembroSeleccionadoEnJTable();
                JDialogAbmMiembros.this.cargarMiembroEnLabels(miembroSeleccionadoEnJTable);
            }
        });
        jXTableMiembros.setColumnSelectionAllowed(true);
        jScrollPane1.setViewportView(jXTableMiembros);
        jXTableMiembros.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        jXTableMiembros.getColumnModel().setColumnSelectionAllowed(false);

        jXTableMiembros.getColumnModel().getColumn(0).setPreferredWidth(35);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(100, 100, 100)
                .addComponent(jLabelApellidoDato1, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(715, 715, 715)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelDireccionDato, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelTelCelDato, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(303, 303, 303)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1015, 1015, 1015)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane1)
                    .addComponent(jXSeparadorDeTituloBuscarMiembro3, javax.swing.GroupLayout.DEFAULT_SIZE, 791, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(jXSeparadorDeTituloBuscarMiembro3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(63, 63, 63)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelApellidoDato1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabelDireccionDato)
                                .addGap(27, 27, 27)
                                .addComponent(jLabelTelCelDato)))))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 814, 544));

        pack();
    }// </editor-fold>

    private void buscarMiembro() {
        String filtroApellido = jTxtApellido.getText();
        String filtroNombre = jTxtNombre.getText();
        try {
            miembros = (HashSet<Miembro>) institucionCentral.buscarMiembros(filtroNombre, filtroApellido);
            cargarMiembrosEnLaTabla(miembros);
        } catch (DAOProblemaConTransaccionException ex) {
            JOptionPane.showMessageDialog(JDialogAbmMiembros.this, ex.getMessage(), "Aviso", JOptionPane.INFORMATION_MESSAGE);
        }
    }
    private void jBtnInicio_seleccionarMiembroActionPerformed(java.awt.event.ActionEvent evt) {
        if (abiertoParaSeleccionarUnMiembro()) {
            if (!isRowSelected()) {
                if (returnAnonimo()) {
                    padre.setMiembro(institucionCentral.getMiembroAnonimo());
                    dispose();
                }
                return;
            }
            padre.setMiembro(getMiembroSeleccionadoEnJTable());
            dispose();
            padre.setVisible(true);
        } else {
            dispose();
        }
    }

    private boolean returnAnonimo(){
        int respuesta = JOptionPane.showConfirmDialog(this,
                        "<html> No ha seleccionado ningún miembro. ¿Desea que el miembro sea anónimo?"
                        + "\n Presione <strong> no</strong> para volver y seleccionar un miembro.  </html> ", "Aviso",
                        JOptionPane.YES_NO_OPTION);
        return respuesta == JOptionPane.YES_NO_OPTION;
    }

    private boolean isRowSelected() {
        return jXTableMiembros.getSelectedRow() != -1;
    }

    private void jBtnAgregarMiembroActionPerformed(java.awt.event.ActionEvent evt) {
        JDialogAltaMiembro altaMiembro = new JDialogAltaMiembro(this, true, institucionCentral);
        altaMiembro.setVisible(true);
    }

    private void jBtnModificarMiembroActionPerformed(java.awt.event.ActionEvent evt) {
        Miembro miembroSeleccionado = getMiembroSeleccionadoEnJTable();
        if (miembroSeleccionado == null) {
            JOptionPane.showMessageDialog(this, "No ha seleccionado ningún miembro.");
        }
        JDialogAltaMiembro altaMiembro = new JDialogAltaMiembro(this, rootPaneCheckingEnabled, institucionCentral, miembroSeleccionado);
        altaMiembro.setVisible(true);
    }

    private void jBtnEliminarMIembroActionPerformed(java.awt.event.ActionEvent evt) {
        Miembro miembroAEliminar = this.getMiembroSeleccionadoEnJTable();
        int respuesta = JOptionPane.showConfirmDialog(this, "¿Desea eliminar los datos del miembro seleccionado? \n"
                + miembroAEliminar.getApellidos() + ", " + miembroAEliminar.getNombres(), "Aviso", JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
        if (respuesta == JOptionPane.YES_OPTION) {
            eliminarMiembroDeBdYDeJTableYDeColeccion(miembroAEliminar);

        } else {
            return;
        }
    }

    public Miembro getMiembroSeleccionadoEnJTable() {
        if (isRowSelected()) {
            return null;
        }
        Miembro retorno = null;
        Iterator<Miembro> it = this.miembros.iterator();
        DefaultTableModel modeloTabla = (DefaultTableModel) this.jXTableMiembros.getModel();
        while (it.hasNext()) {
            Miembro miembro = it.next();
            if (miembro.getIdentificadorPersona() == Long.parseLong(modeloTabla.getValueAt(this.jXTableMiembros.getSelectedRow(), 0).toString())) {
                retorno = miembro;
            }
        }
        return retorno;
    }

    public boolean abiertoParaSeleccionarUnMiembro() {
        return this.abiertoParaSeleccionarUnMiembro;
    }

    public void addMiembroEnJTableYEnLabels(Miembro unMiembro) {
        String[] nuevaFila = new String[4];
        nuevaFila[0] = String.valueOf(unMiembro.getIdentificadorPersona());
        nuevaFila[1] = unMiembro.getApellidos();
        nuevaFila[2] = unMiembro.getNombres();
        nuevaFila[3] = unMiembro.getDireccion().toString();
        DefaultTableModel modeloDeLaTabla = (DefaultTableModel) this.jXTableMiembros.getModel();
        modeloDeLaTabla.addRow(nuevaFila);
        this.cargarMiembroEnLabels(unMiembro);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogAbmMiembros dialog = new JDialogAbmMiembros(new javax.swing.JFrame(), true);
                dialog.setVisible(true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify
    private javax.swing.JButton jBtnAgregarMiembro;
    private javax.swing.JButton jBtnEliminarMIembro;
    private javax.swing.JButton jBtnImprimirListado;
    private javax.swing.JButton jBtnInicio_seleccionarMiembro;
    private javax.swing.JButton jBtnModificarMiembro;
    private javax.swing.JLabel jLabelApellido2;
    private javax.swing.JLabel jLabelApellidoDato1;
    private javax.swing.JLabel jLabelDireccionDato;
    private javax.swing.JLabel jLabelNombre2;
    private javax.swing.JLabel jLabelTelCelDato;
    private javax.swing.JLabel jLblActivo;
    private javax.swing.JLabel jLblActivoDato;
    private javax.swing.JLabel jLblApellidoDato;
    private javax.swing.JLabel jLblApellidos;
    private javax.swing.JLabel jLblDireccion;
    private javax.swing.JLabel jLblDireccionDato;
    private javax.swing.JLabel jLblFechaBautismo;
    private javax.swing.JLabel jLblFechaDeBautismoDato;
    private javax.swing.JLabel jLblNombres;
    private javax.swing.JLabel jLblNombresDato;
    private javax.swing.JLabel jLblTelCelDato;
    private javax.swing.JLabel jLblTelCelular;
    private javax.swing.JLabel jLblTelFijo;
    private javax.swing.JLabel jLblTelFijoDato;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTxtApellido;
    private javax.swing.JTextField jTxtNombre;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloBuscarMiembro2;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloBuscarMiembro3;
    private javax.swing.JTable jXTableMiembros;
    // End of variables declaration

    /**
     * @return the institucionCentral
     */
    public InstitucionCentral getInstitucionCentral() {
        return institucionCentral;
    }

    /**
     * @param institucionCentral the institucionCentral to set
     */
    public void setInstitucionCentral(InstitucionCentralTesoreria institucionCentral) {
        this.institucionCentral = institucionCentral;
    }

    /**
     * @return the jBtnInicio_seleccionarMiembro
     */
    public javax.swing.JButton getjBtnInicio_seleccionarMiembro() {
        return jBtnInicio_seleccionarMiembro;
    }

    /**
     * @param jBtnInicio_seleccionarMiembro the jBtnInicio_seleccionarMiembro to
     * set
     */
    public void setjBtnInicio_seleccionarMiembro(javax.swing.JButton jBtnInicio_seleccionarMiembro) {
        this.jBtnInicio_seleccionarMiembro = jBtnInicio_seleccionarMiembro;
    }

    /**
     * @return the abiertoParaSeleccionarUnMiembro
     */
    public Boolean getAbiertoParaSeleccionarUnMiembro() {
        return abiertoParaSeleccionarUnMiembro;
    }

    /**
     * @param abiertoParaSeleccionarUnMiembro the
     * abiertoParaSeleccionarUnMiembro to set
     */
    public void setAbiertoParaSeleccionarUnMiembro(Boolean abiertoParaSeleccionarUnMiembro) {
        this.abiertoParaSeleccionarUnMiembro = abiertoParaSeleccionarUnMiembro;
    }

    public JDialogAltaIngreso getPadre() {
        return padre;
    }

    public void setPadre(JDialogAltaIngreso padre) {
        this.padre = padre;
    }

    private void cargarMiembrosEnLaTabla(HashSet<Miembro> miembros) {
        borrarTodosLosElementosDeLaTabla();
        if (miembros == null) {
            return;
        }
        if (miembros.isEmpty()) {
            return;
        }
        Iterator it = miembros.iterator();
        DefaultTableModel modelo = new DefaultTableModel();
        String[] titulosDeColumna = {"Id", "Apellido", "Nombre", "Direccion"};
        String[] nuevaFila = new String[4];
        modelo.setColumnIdentifiers(titulosDeColumna);
        while (it.hasNext()) {
            Miembro miembro = (Miembro) it.next();
            nuevaFila[0] = String.valueOf(miembro.getIdentificadorPersona());
            nuevaFila[1] = miembro.getApellidos();
            nuevaFila[2] = miembro.getNombres();
            if (miembro.getDireccion() != null) {
                nuevaFila[3] = miembro.getDireccion().toString();
            }
            modelo.addRow(nuevaFila);
        }
        jXTableMiembros.setModel(modelo);
        habilitarEliminarYModificar();
    }

    private void cargarMiembroEnLabels(Miembro miembro) {
        if (miembro == null) {
            return;
        }
        this.jLblApellidoDato.setText(miembro.getApellidos());
        this.jLblNombresDato.setText(miembro.getNombres());
        if (miembro.getApellidos().equals("Anonimo")) {
            return;
        }
        if (miembro.getDireccion() != null) {
            this.jLblDireccionDato.setText(miembro.getDireccion().toString());
        }
        SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyy");
        if (miembro.getFechaBautismo() == null) {
            this.jLblFechaDeBautismoDato.setText("");
        } else {
            Date fechaDeBatismo = new Date(miembro.getFechaBautismo().get(Calendar.YEAR), miembro.getFechaBautismo().get(Calendar.MONTH),
                    miembro.getFechaBautismo().get(Calendar.DAY_OF_MONTH));
            this.jLblFechaDeBautismoDato.setText(formateador.format(fechaDeBatismo));
        }
        if (miembro.getTelefonoCelular() == 0) {
            this.jLblTelCelDato.setText("");
        } else {
            this.jLabelTelCelDato.setText(String.valueOf(miembro.getTelefonoCelular()));
        }

        if (miembro.getTelefonoFijo() == 0) {
            this.jLblTelFijoDato.setText("");
        } else {
            this.jLblTelFijoDato.setText(String.valueOf(miembro.getTelefonoFijo()));
        }

        if (miembro.isMiembroActivo()) {
            jLblActivoDato.setText("Activo");
            jLblActivoDato.setForeground(Color.GREEN);
        } else {
            jLblActivoDato.setText("No activo");
            jLblActivoDato.setForeground(Color.red);
        }
    }

    private void deshabilitarEliminarYModificar() {
        jBtnEliminarMIembro.setEnabled(false);
        jBtnModificarMiembro.setEnabled(false);
    }

    private void habilitarEliminarYModificar() {
        jBtnEliminarMIembro.setEnabled(true);
        jBtnModificarMiembro.setEnabled(true);
    }

    private void modificarVistaParaJFrameSistema() {
        jBtnInicio_seleccionarMiembro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png")));
        jBtnInicio_seleccionarMiembro.setText("Volver al inicio");
        deshabilitarEliminarYModificar();
        jBtnImprimirListado.setEnabled(true);
    }

    private void modificarVistaParaJDialogAltaIngreso() {
        getjBtnInicio_seleccionarMiembro().setText("Seleccionar miembro");
        jBtnInicio_seleccionarMiembro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Ok 20x20.png")));
        deshabilitarEliminarYModificar();
        jBtnImprimirListado.setEnabled(false);
    }

    private void eliminarMiembroDeBdYDeJTableYDeColeccion(Miembro miembroAEliminar) {
        institucionCentral.desvincularMiembro(miembroAEliminar);
        miembros.remove(miembroAEliminar);
        DefaultTableModel modelo = (DefaultTableModel) jXTableMiembros.getModel();
        modelo.removeRow(jXTableMiembros.getSelectedRow());
    }

    private void borrarTodosLosElementosDeLaTabla() {
        DefaultTableModel modeloTabla = (DefaultTableModel) this.jXTableMiembros.getModel();
        for (int i = modeloTabla.getRowCount() - 1; i >= 0; i--) {
            modeloTabla.removeRow(i);
        }
    }

    void addMiembroMofificadoEnJTableYEnLabels(Miembro miembroSeleccionadoAModificar) {
        DefaultTableModel modeloTabla = (DefaultTableModel) jXTableMiembros.getModel();
        modeloTabla.setValueAt(miembroSeleccionadoAModificar.getApellidos(), jXTableMiembros.getSelectedRow(), 1);
        modeloTabla.setValueAt(miembroSeleccionadoAModificar.getNombres(), jXTableMiembros.getSelectedRow(), 2);
        modeloTabla.setValueAt(miembroSeleccionadoAModificar.getDireccion().toString(), jXTableMiembros.getSelectedRow(), 3);
        if (miembroSeleccionadoAModificar.isMiembroActivo()) {
            jLblActivoDato.setText("Activo");
            jLblActivoDato.setForeground(Color.GREEN);
        } else {
            jLblActivoDato.setText("Inactivo");
            jLblActivoDato.setForeground(Color.RED);
        }
        jLblApellidoDato.setText(miembroSeleccionadoAModificar.getApellidos());
        jLblTelCelDato.setText(Long.toString(miembroSeleccionadoAModificar.getTelefonoCelular()));
        jLblTelFijoDato.setText(Long.toString(miembroSeleccionadoAModificar.getTelefonoFijo()));
        DateFormat format = DateFormat.getDateInstance(DateFormat.SHORT);
        jLblNombresDato.setText(format.format(new Date(miembroSeleccionadoAModificar.getFechaBautismo().get(Calendar.DAY_OF_MONTH),
                miembroSeleccionadoAModificar.getFechaBautismo().get(Calendar.MONTH),
                miembroSeleccionadoAModificar.getFechaBautismo().get(Calendar.YEAR))));

    }
}
