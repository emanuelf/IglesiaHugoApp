package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Excepciones.FilaNoSelecionadaException;
import Modelo.Filial;
import Modelo.InstitucionCentral;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author emanuel
 *
 */
public class JDialogABMFiliales extends javax.swing.JDialog {

    private InstitucionCentral institucion;
    private Filial filialModificada;
    private ArrayList<Filial> filiales;

    /** Creates new form JDialogFiliales */
    public JDialogABMFiliales(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    JDialogABMFiliales(JFrameSistema padre, boolean modal, InstitucionCentral institucion) {
        this(padre, modal);
        this.institucion = institucion;
        agregarListSelectionListener();
        try {
            this.filiales = this.institucion.getFiliales();
        } catch (DAOProblemaConTransaccionException ex) {
        }
        if (filiales.isEmpty()) {
            deshabilitarModificarYEliminar();
        } else {
            this.cargarFilialesAJTable(filiales);
        }
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelContenedor = new javax.swing.JPanel();
        jXSeparadorDeTituloDatosDeFilial = new org.jdesktop.swingx.JXTitledSeparator();
        jPnlDatosDeFilialExtendido = new javax.swing.JPanel();
        jLblNombreFilialDato = new javax.swing.JLabel();
        jLabelNombreDeFilial = new javax.swing.JLabel();
        jLabelTelefono = new javax.swing.JLabel();
        jLabelTelefonoDato = new javax.swing.JLabel();
        jLabelDireccionDato = new javax.swing.JLabel();
        jLabelDireccion = new javax.swing.JLabel();
        jLabelNombreYApellidoDePastor = new javax.swing.JLabel();
        jLabelNombreYApellidoDePastorDato = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jBtnModificar = new javax.swing.JButton();
        jBtnEliminarMIembro = new javax.swing.JButton();
        jBtnAgregarFilial = new javax.swing.JButton();
        jBtnInicio_seleccionarMiembro = new javax.swing.JButton();
        jScrollPaneFiliales = new javax.swing.JScrollPane();
        jTblFiliales = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Filiales");

        jPanelContenedor.setPreferredSize(new java.awt.Dimension(1130, 548));

        jXSeparadorDeTituloDatosDeFilial.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jXSeparadorDeTituloDatosDeFilial.setHorizontalAlignment(0);
        jXSeparadorDeTituloDatosDeFilial.setHorizontalTextPosition(0);
        jXSeparadorDeTituloDatosDeFilial.setTitle("Datos de filial");

        jLblNombreFilialDato.setText("                ");

        jLabelNombreDeFilial.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabelNombreDeFilial.setText("Nombre de la filial:");

        jLabelTelefono.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabelTelefono.setText("Teléfono:");

        jLabelTelefonoDato.setText("                ");

        jLabelDireccionDato.setText("                ");

        jLabelDireccion.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabelDireccion.setText("Dirección:");

        jLabelNombreYApellidoDePastor.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabelNombreYApellidoDePastor.setText("Pastor:");

        jLabelNombreYApellidoDePastorDato.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabelNombreYApellidoDePastorDato.setText("                        ");

        javax.swing.GroupLayout jPnlDatosDeFilialExtendidoLayout = new javax.swing.GroupLayout(jPnlDatosDeFilialExtendido);
        jPnlDatosDeFilialExtendido.setLayout(jPnlDatosDeFilialExtendidoLayout);
        jPnlDatosDeFilialExtendidoLayout.setHorizontalGroup(
            jPnlDatosDeFilialExtendidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlDatosDeFilialExtendidoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPnlDatosDeFilialExtendidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlDatosDeFilialExtendidoLayout.createSequentialGroup()
                        .addGroup(jPnlDatosDeFilialExtendidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelNombreDeFilial)
                            .addGroup(jPnlDatosDeFilialExtendidoLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(jPnlDatosDeFilialExtendidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelTelefonoDato, javax.swing.GroupLayout.DEFAULT_SIZE, 132, Short.MAX_VALUE)
                                    .addComponent(jLblNombreFilialDato, javax.swing.GroupLayout.DEFAULT_SIZE, 132, Short.MAX_VALUE))))
                        .addGap(122, 122, 122)
                        .addGroup(jPnlDatosDeFilialExtendidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPnlDatosDeFilialExtendidoLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLabelDireccionDato, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE))
                            .addComponent(jLabelDireccion))
                        .addGap(107, 107, 107)
                        .addGroup(jPnlDatosDeFilialExtendidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPnlDatosDeFilialExtendidoLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLabelNombreYApellidoDePastorDato, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE))
                            .addComponent(jLabelNombreYApellidoDePastor)))
                    .addComponent(jLabelTelefono))
                .addContainerGap(172, Short.MAX_VALUE))
        );
        jPnlDatosDeFilialExtendidoLayout.setVerticalGroup(
            jPnlDatosDeFilialExtendidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlDatosDeFilialExtendidoLayout.createSequentialGroup()
                .addGroup(jPnlDatosDeFilialExtendidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlDatosDeFilialExtendidoLayout.createSequentialGroup()
                        .addComponent(jLabelDireccion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelDireccionDato))
                    .addGroup(jPnlDatosDeFilialExtendidoLayout.createSequentialGroup()
                        .addComponent(jLabelNombreDeFilial)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblNombreFilialDato)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelTelefonoDato))
                    .addGroup(jPnlDatosDeFilialExtendidoLayout.createSequentialGroup()
                        .addComponent(jLabelNombreYApellidoDePastor)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelNombreYApellidoDePastorDato)))
                .addGap(48, 48, 48))
        );

        jBtnModificar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Iglesia-modificar.png"))); // NOI18N
        jBtnModificar.setText("Modificar");
        jBtnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnModificarActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnModificar);

        jBtnEliminarMIembro.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnEliminarMIembro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Iglesia-delete.png"))); // NOI18N
        jBtnEliminarMIembro.setText("Eliminar");
        jBtnEliminarMIembro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnEliminarMIembroActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnEliminarMIembro);

        jBtnAgregarFilial.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnAgregarFilial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Iglesia-add.png"))); // NOI18N
        jBtnAgregarFilial.setText("Agregar");
        jBtnAgregarFilial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnAgregarFilialActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnAgregarFilial);

        jBtnInicio_seleccionarMiembro.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnInicio_seleccionarMiembro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnInicio_seleccionarMiembro.setText("Inicio");
        jBtnInicio_seleccionarMiembro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnInicio_seleccionarMiembroActionPerformed(evt);
            }
        });
        jPanel8.add(jBtnInicio_seleccionarMiembro);

        jTblFiliales.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nombre de filial", "Pastor", "Dirección"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPaneFiliales.setViewportView(jTblFiliales);

        javax.swing.GroupLayout jPanelContenedorLayout = new javax.swing.GroupLayout(jPanelContenedor);
        jPanelContenedor.setLayout(jPanelContenedorLayout);
        jPanelContenedorLayout.setHorizontalGroup(
            jPanelContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelContenedorLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPnlDatosDeFilialExtendido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jXSeparadorDeTituloDatosDeFilial, javax.swing.GroupLayout.DEFAULT_SIZE, 792, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelContenedorLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPaneFiliales))
                .addContainerGap())
        );
        jPanelContenedorLayout.setVerticalGroup(
            jPanelContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelContenedorLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXSeparadorDeTituloDatosDeFilial, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPaneFiliales, javax.swing.GroupLayout.PREFERRED_SIZE, 318, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPnlDatosDeFilialExtendido, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelContenedor, javax.swing.GroupLayout.PREFERRED_SIZE, 812, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelContenedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnModificarActionPerformed
        Filial filialSeleccionada = null;
        try {
            filialSeleccionada = this.getFilialSeleccionada();
        } catch (FilaNoSelecionadaException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.ERROR_MESSAGE);
            return;
        }
        JDialogAltaFilial jDialogAltaFilial = new JDialogAltaFilial(this, true, this.institucion, true, filialSeleccionada);
        jDialogAltaFilial.setVisible(true);
        this.setLocationRelativeTo(null);
}//GEN-LAST:event_jBtnModificarActionPerformed

    private void jBtnEliminarMIembroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnEliminarMIembroActionPerformed
        Filial filialSeleccionada = null;
        try {
            filialSeleccionada = this.getFilialSeleccionada();
        } catch (FilaNoSelecionadaException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.ERROR_MESSAGE);
            return;
        }
        int respuesta = JOptionPane.showConfirmDialog(this, "¿Desea eliminar los datos de la filial seleccionada? \n*"
                + filialSeleccionada.getNombre(), "Aviso", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (respuesta == JOptionPane.YES_OPTION) {
            try {

                elimiarFilialDeBdYDJtableYDeColeccion(filialSeleccionada);
            } catch (DAOProblemaConTransaccionException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.ERROR_MESSAGE);
            }
        } 
}//GEN-LAST:event_jBtnEliminarMIembroActionPerformed

    private void jBtnAgregarFilialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnAgregarFilialActionPerformed
        JDialogAltaFilial dialogAltaFilial = new JDialogAltaFilial(this, true, this.institucion, false);
        dialogAltaFilial.setVisible(true);
}//GEN-LAST:event_jBtnAgregarFilialActionPerformed

    private void jBtnInicio_seleccionarMiembroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnInicio_seleccionarMiembroActionPerformed
        this.dispose();
    }//GEN-LAST:event_jBtnInicio_seleccionarMiembroActionPerformed

 
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnAgregarFilial;
    private javax.swing.JButton jBtnEliminarMIembro;
    private javax.swing.JButton jBtnInicio_seleccionarMiembro;
    private javax.swing.JButton jBtnModificar;
    private javax.swing.JLabel jLabelDireccion;
    private javax.swing.JLabel jLabelDireccionDato;
    private javax.swing.JLabel jLabelNombreDeFilial;
    private javax.swing.JLabel jLabelNombreYApellidoDePastor;
    private javax.swing.JLabel jLabelNombreYApellidoDePastorDato;
    private javax.swing.JLabel jLabelTelefono;
    private javax.swing.JLabel jLabelTelefonoDato;
    private javax.swing.JLabel jLblNombreFilialDato;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanelContenedor;
    private javax.swing.JPanel jPnlDatosDeFilialExtendido;
    private javax.swing.JScrollPane jScrollPaneFiliales;
    private javax.swing.JTable jTblFiliales;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloDatosDeFilial;
    // End of variables declaration//GEN-END:variables

    void addFilialEnJTableYEnColeccion(Filial filialNueva) {
        if (!filiales.contains(filialNueva)) {
            filiales.add(filialNueva);
            String[] nuevaFilaFilial = {filialNueva.getNombre(), filialNueva.getPastorFilial().getApellidoYNombre(), filialNueva.getDireccion().toString()};
            DefaultTableModel modelo = (DefaultTableModel) this.jTblFiliales.getModel();
            modelo.addRow(nuevaFilaFilial);
        } else {
            filiales.remove(filiales.indexOf(filialNueva));
            filiales.add(filialNueva);
            String[] nuevaFilaFilial = {filialNueva.getNombre(), filialNueva.getPastorFilial().getApellidoYNombre(), filialNueva.getDireccion().toString()};
            DefaultTableModel modelo = (DefaultTableModel) this.jTblFiliales.getModel();
            modelo.removeRow(jTblFiliales.getSelectedRow());
            modelo.addRow(nuevaFilaFilial);

        }

        habilitarModificarYEliminar();
    }

    private Filial getFilialSeleccionada() throws Excepciones.FilaNoSelecionadaException {
        Filial filialSeleccionada = null;
        if (this.jTblFiliales.getSelectedRow() == -1) {
            throw new FilaNoSelecionadaException();
        } else {
            DefaultTableModel modeloTabla = (DefaultTableModel) this.jTblFiliales.getModel();
            String nombreDeFilialSeleccionada = modeloTabla.getValueAt(jTblFiliales.getSelectedRow(), 0).toString();
            filialSeleccionada = getFilialEnColeccion(nombreDeFilialSeleccionada);
        }
        return filialSeleccionada;
    }

    private Filial getFilialEnColeccion(String nombreDeFilial) {
        Filial filialBuscada = null;
        Iterator<Filial> it = filiales.iterator();
        while (it.hasNext()) {
            Filial filialEnIt = it.next();
            if (filialEnIt.getNombre().equals(nombreDeFilial)) {
                filialBuscada = filialEnIt;
            }
        }
        return filialBuscada;
    }

    private void cargarFilialesAJTable(ArrayList<Filial> filiales) {
        String[] datosFilial = new String[3];
        DefaultTableModel modeloTabla = (DefaultTableModel) this.jTblFiliales.getModel();
        Iterator<Filial> it = filiales.iterator();
        while (it.hasNext()) {
            Filial filial = it.next();
            datosFilial[0] = filial.getNombre();
            datosFilial[1] = filial.getPastorFilial().getApellidoYNombre();
            datosFilial[2] = filial.getDireccion().toString();
            modeloTabla.addRow(datosFilial);
        }
    }

    private void deshabilitarModificarYEliminar() {
        this.jBtnEliminarMIembro.setEnabled(false);
        this.jBtnModificar.setEnabled(false);
    }

    private void habilitarModificarYEliminar() {
        this.jBtnEliminarMIembro.setEnabled(true);
        this.jBtnModificar.setEnabled(true);
    }

    private void elimiarFilialDeBdYDJtableYDeColeccion(Filial filialSeleccionada) throws DAOProblemaConTransaccionException {
        institucion.eliminarFilial(filialSeleccionada);
        DefaultTableModel modeloTabla = (DefaultTableModel) this.jTblFiliales.getModel();
        modeloTabla.removeRow(jTblFiliales.getSelectedRow());
        filiales.remove(filialSeleccionada);
        if (filiales.isEmpty() && modeloTabla.getRowCount() == 0) {
            deshabilitarModificarYEliminar();
        }
    }

    private void cargarDatosDeFilialEnLabels(Filial filial) {
        this.jLblNombreFilialDato.setText(filial.getNombre());
        this.jLabelDireccionDato.setText(filial.getDireccion().toString());
        this.jLabelTelefonoDato.setText(Long.toString(filial.getTelefono()));
        this.jLabelNombreYApellidoDePastorDato.setText(filial.getPastorFilial().getApellidoYNombre());
    }

    private void agregarListSelectionListener() {
        jTblFiliales.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            public void valueChanged(ListSelectionEvent e) {
                try {
                    cargarDatosDeFilialEnLabels(getFilialSeleccionada());
                } catch (FilaNoSelecionadaException ex) {
                    //nothing
                }
            }
        });
    }
}
