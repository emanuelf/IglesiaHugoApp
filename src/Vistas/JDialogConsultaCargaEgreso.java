package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Excepciones.FilaNoSelecionadaException;
import Modelo.Egreso;
import Modelo.InstitucionCentralTesoreria;
import Util.UtilFechas;
import Vistas.Util.ImporteCellRenderer;
import Vistas.Util.Util;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.ListIterator;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class JDialogConsultaCargaEgreso extends javax.swing.JDialog {

    private JDialogAltaEgreso padre;
    private ArrayList<Egreso> egresosCargados = new ArrayList<Egreso>();
    private InstitucionCentralTesoreria institucion;

    public JDialogConsultaCargaEgreso(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
    }

    public JDialogConsultaCargaEgreso(JFrameSistema padre, boolean modal, InstitucionCentralTesoreria institucionCentralTesoreria) {
        super(padre, modal);
        initComponents();
        jTblAsientos.setDefaultRenderer(Double.class, new ImporteCellRenderer());
        this.institucion = institucionCentralTesoreria;
        Vistas.Util.Util.cargaMesesEnCombo(jComboMes);
        Vistas.Util.Util.cargaSecuenciaEnCombo(2010, 2311, jComboAnio);
        Vistas.Util.Util.ocultarAnimacion(jXAnimacionEspera2);
        this.setLocationRelativeTo(null);
    }

    JDialogConsultaCargaEgreso(JDialogAltaEgreso padre, boolean modal, ArrayList<Egreso> egresosCargados, InstitucionCentralTesoreria institucion) {
        super(padre, modal);
        initComponents();
        jTblAsientos.setDefaultRenderer(Double.class, new ImporteCellRenderer());
        this.remove(jPnlBusqueda);
        this.institucion = institucion;
        this.padre = padre;
        this.egresosCargados = egresosCargados;
        try {
            cargarDatosEnTabla(egresosCargados);
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(this, "Ningún egreso coincide con el período");
        }

        deshabilitarOHabilitarCamposSiEsNecesario();
        this.egresosCargados = egresosCargados;
        this.pack();
        this.setLocationRelativeTo(null);
    }

    private void deshabilitarOHabilitarCamposSiEsNecesario() {
        if (jTblAsientos.getSelectedRow() == -1) {
            jBtnEliminarAsiento.setEnabled(false);
            jBtnModificarAsiento.setEnabled(false);

        } else {
            jBtnEliminarAsiento.setEnabled(true);
            jBtnModificarAsiento.setEnabled(true);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel7 = new javax.swing.JPanel();
        jBtnEliminarAsiento = new javax.swing.JButton();
        jBtnModificarAsiento = new javax.swing.JButton();
        jBtnVolver = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTblAsientos = new javax.swing.JTable();
        jPnlBusqueda = new javax.swing.JPanel();
        jXSeparadorDeTitulosBuscarAsiento2 = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelMes2 = new javax.swing.JLabel();
        jComboMes = new javax.swing.JComboBox();
        jLabelAnio2 = new javax.swing.JLabel();
        jComboAnio = new javax.swing.JComboBox();
        jXAnimacionEspera2 = new org.jdesktop.swingx.JXBusyLabel();
        jBtnBuscarAsientos = new javax.swing.JButton();
        jXSeparadorDeTituloAsientos1 = new org.jdesktop.swingx.JXTitledSeparator();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(7, 0), new java.awt.Dimension(7, 0), new java.awt.Dimension(7, 32767));
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(7, 0), new java.awt.Dimension(7, 0), new java.awt.Dimension(7, 32767));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel7.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        jBtnEliminarAsiento.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnEliminarAsiento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Eliminar egreso 20x20.png"))); // NOI18N
        jBtnEliminarAsiento.setText("Eliminar asiento");
        jBtnEliminarAsiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnEliminarAsientoActionPerformed(evt);
            }
        });
        jPanel7.add(jBtnEliminarAsiento);

        jBtnModificarAsiento.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnModificarAsiento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Modificar egreso 20x20.png"))); // NOI18N
        jBtnModificarAsiento.setText("Modificar asiento");
        jBtnModificarAsiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnModificarAsientoActionPerformed(evt);
            }
        });
        jPanel7.add(jBtnModificarAsiento);

        jBtnVolver.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolver.setText("Volver");
        jBtnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverActionPerformed(evt);
            }
        });
        jPanel7.add(jBtnVolver);

        getContentPane().add(jPanel7, java.awt.BorderLayout.PAGE_END);

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        jScrollPane1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jScrollPane1FocusGained(evt);
            }
        });

        jTblAsientos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Egreso", "Cuenta", "Fecha", "Detalle", "Monto", "Pagado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Long.class, java.lang.String.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTblAsientos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTblAsientosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTblAsientos);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jXSeparadorDeTitulosBuscarAsiento2.setFont(new java.awt.Font("Tahoma", 0, 14));
        jXSeparadorDeTitulosBuscarAsiento2.setHorizontalAlignment(0);
        jXSeparadorDeTitulosBuscarAsiento2.setHorizontalTextPosition(0);
        jXSeparadorDeTitulosBuscarAsiento2.setTitle("Buscar asiento");

        jLabelMes2.setFont(new java.awt.Font("Dialog", 0, 12));
        jLabelMes2.setText("Mes:");

        jComboMes.setFont(new java.awt.Font("Dialog", 0, 12));

        jLabelAnio2.setFont(new java.awt.Font("Dialog", 0, 12));
        jLabelAnio2.setText("Año:");

        jComboAnio.setFont(new java.awt.Font("Dialog", 0, 12));

        jXAnimacionEspera2.setText("Cargando");
        jXAnimacionEspera2.setFont(new java.awt.Font("Dialog", 0, 12));

        jBtnBuscarAsientos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Buscar.png"))); // NOI18N
        jBtnBuscarAsientos.setText("Buscar");
        jBtnBuscarAsientos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnBuscarAsientosActionPerformed(evt);
            }
        });

        jXSeparadorDeTituloAsientos1.setFont(new java.awt.Font("Tahoma", 0, 14));
        jXSeparadorDeTituloAsientos1.setHorizontalAlignment(0);
        jXSeparadorDeTituloAsientos1.setHorizontalTextPosition(0);
        jXSeparadorDeTituloAsientos1.setTitle("Asientos");

        javax.swing.GroupLayout jPnlBusquedaLayout = new javax.swing.GroupLayout(jPnlBusqueda);
        jPnlBusqueda.setLayout(jPnlBusquedaLayout);
        jPnlBusquedaLayout.setHorizontalGroup(
            jPnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlBusquedaLayout.createSequentialGroup()
                .addGroup(jPnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlBusquedaLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelAnio2)
                            .addGroup(jPnlBusquedaLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jComboAnio, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPnlBusquedaLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jComboMes, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabelMes2))
                        .addGap(18, 18, 18)
                        .addComponent(jBtnBuscarAsientos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jXAnimacionEspera2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPnlBusquedaLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jXSeparadorDeTituloAsientos1, javax.swing.GroupLayout.DEFAULT_SIZE, 781, Short.MAX_VALUE))
                    .addComponent(jXSeparadorDeTitulosBuscarAsiento2, javax.swing.GroupLayout.DEFAULT_SIZE, 791, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPnlBusquedaLayout.setVerticalGroup(
            jPnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlBusquedaLayout.createSequentialGroup()
                .addComponent(jXSeparadorDeTitulosBuscarAsiento2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlBusquedaLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelMes2)
                            .addComponent(jLabelAnio2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboAnio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPnlBusquedaLayout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(jPnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jBtnBuscarAsientos)
                            .addComponent(jXAnimacionEspera2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(17, 17, 17)
                .addComponent(jXSeparadorDeTituloAsientos1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        getContentPane().add(jPnlBusqueda, java.awt.BorderLayout.PAGE_START);
        getContentPane().add(filler1, java.awt.BorderLayout.LINE_END);
        getContentPane().add(filler2, java.awt.BorderLayout.LINE_START);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnModificarAsientoActionPerformed(java.awt.event.ActionEvent evt) {
        int filaSeleccionada = jTblAsientos.getSelectedRow();
        try {
            comprobarFilaSeleccionada(filaSeleccionada);
        } catch (Exception a) {
            JOptionPane.showMessageDialog(this, "Ningún elemento seleccionado", "Aviso", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        Egreso egresoAModificar = null;
        egresoAModificar = buscarEgresoEnArrayList(Long.valueOf(String.valueOf(jTblAsientos.getValueAt(filaSeleccionada, 0))));
        JDialogModificarAsientoEgreso jDialogModificarAsientoEgreso = new JDialogModificarAsientoEgreso(this, true, egresoAModificar, institucion,
                egresosCargados.indexOf(egresoAModificar), padre != null);
        jDialogModificarAsientoEgreso.setVisible(true);
        jDialogModificarAsientoEgreso.setLocationRelativeTo(null);
    }

    private void jScrollPane1FocusGained(java.awt.event.FocusEvent evt) {
        deshabilitarOHabilitarCamposSiEsNecesario();
    }

    private void jBtnBuscarAsientosActionPerformed(java.awt.event.ActionEvent evt) {
        Vistas.Util.Util.mostrarAnimacion(jXAnimacionEspera2);
        try {
            Util.removeAllFilasEnTabla((DefaultTableModel) jTblAsientos.getModel());
            buscarAsientoYCargarJTable();
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(this, "Ningún egreso coincide con el período");
        }

        if (seBuscaAsientosDelMesCorriente()) {
            deshabilitarOHabilitarBotonesDependeSiEligeElMesCorriente(true);//nombre largo para un metodo;)
        } else {
            deshabilitarOHabilitarBotonesDependeSiEligeElMesCorriente(false);
        }
        Vistas.Util.Util.ocultarAnimacion(jXAnimacionEspera2);
    }

    public void buscarAsientoYCargarJTable() throws NullPointerException {
        int mes = UtilFechas.mesEnNumero(jComboMes.getSelectedItem().toString());
        Integer anio = (Integer) jComboAnio.getSelectedItem();
        if(this.egresosCargados !=null) {
            this.egresosCargados.clear();
        }
        this.egresosCargados = this.institucion.buscarEgresos(anio.intValue(), mes);
        if (egresosCargados == null) {
            JOptionPane.showMessageDialog(this, "Ningún ingreso coincide con el período.", "Aviso", JOptionPane.INFORMATION_MESSAGE);
        } else {
            cargarDatosEnTabla(egresosCargados);
        }
    }

    private Egreso buscarEgresoEnArrayList(long codigo) {
        if (llamadoParaConsultarCarga()) {
            return egresosCargados.get((int) codigo);
        } else {
            Iterator it = egresosCargados.iterator();
            Egreso retornoEgreso = null;
            while (it.hasNext()) {
                Egreso egreso = (Egreso) it.next();
                if (egreso.getNroAsiento() == codigo) {
                    System.out.println("debería entrar acaaaaaaaaaaaaaaaa");
                    retornoEgreso = egreso;
                    break;
                }
            }
            return retornoEgreso;
        }
    }
    /**/
private void jBtnEliminarAsientoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnEliminarAsientoActionPerformed
    int filaSeleccionada = jTblAsientos.getSelectedRow();
    try {
        comprobarFilaSeleccionada(filaSeleccionada);
    } catch (Exception a) {
        JOptionPane.showMessageDialog(this, "Ningún elemento seleccionado", "Aviso", JOptionPane.INFORMATION_MESSAGE);
    }
    Egreso egresoAEliminar = buscarEgresoEnArrayList(Long.valueOf(String.valueOf(jTblAsientos.getValueAt(jTblAsientos.getSelectedRow(), 0))));
    if (JOptionPane.showConfirmDialog(this, "¿Esta seguro? Se eliminará (de la carga temporal) el asiento: \n Monto:"
            + egresoAEliminar.getMonto(), "Pregunta", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
        if (llamadoParaConsultarCarga()) {
            this.eliminarEgresoDeTablaYColeccion(egresoAEliminar);
        } else {
            try {
                institucion.eliminarEgreso(egresoAEliminar);
                eliminarEgresoDeTablaYColeccion(egresoAEliminar);
            } catch (DAOProblemaConTransaccionException ex) {
                Util.showMessageErrorEnTransaccion(ex);
            }
        }
    }
}//GEN-LAST:event_jBtnEliminarAsientoActionPerformed
    /*
private void jBtnModificarAsientoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnModificarAsientoActionPerformed
    
    Egreso egresoAModificar = null;
    egresoAModificar = buscarEgresoEnArrayList(Long.valueOf(String.valueOf(jTblAsientos.getValueAt(filaSeleccionada, 0))));
    JDialogModificarAsientoEgreso jDialogModificarAsientoEgreso = new JDialogModificarAsientoEgreso(this, true, egresoAModificar, institucion);
    jDialogModificarAsientoEgreso.setVisible(true);
    
}//GEN-LAST:event_jBtnModificarAsientoActionPerformed
     */

    public void comprobarFilaSeleccionada(int filaSeleccionada) throws FilaNoSelecionadaException {
        if ((filaSeleccionada == -1)) {
            throw new FilaNoSelecionadaException();
        }
    }
private void jBtnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVolverActionPerformed
    if (llamadoParaConsultarCarga()) {
        this.padre.setEgresosCargados(egresosCargados);
        this.padre.setVisible(true);
    } else {
        this.getParent().setVisible(true);
    }
    this.dispose();

}//GEN-LAST:event_jBtnVolverActionPerformed

private void jTblAsientosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTblAsientosMouseClicked
    deshabilitarOHabilitarCamposSiEsNecesario();
}//GEN-LAST:event_jTblAsientosMouseClicked
    /*
private void jScrollPane1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jScrollPane1FocusGained
    
}//GEN-LAST:event_jScrollPane1FocusGained
     */
    /*
private void jBtnBuscarAsientosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnBuscarAsientosActionPerformed
    buscarAsientoYCargarJTable();
    if (seBuscaAsientosDelMesCorriente()) {
    deshabilitarOHabilitarBotonesDependeSiEligeElMesCorriente(true);//nombre largo para un metodo;)
    } else {
    deshabilitarOHabilitarBotonesDependeSiEligeElMesCorriente(false);
    }
    Vistas.FuncionesUtiles.FuncionesParaInterfaces.ocultarAnimacion(jXAnimacionEspera2);
}//GEN-LAST:event_jBtnBuscarAsientosActionPerformed
     */

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JDialogConsultaCargaEgreso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JDialogConsultaCargaEgreso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JDialogConsultaCargaEgreso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JDialogConsultaCargaEgreso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogConsultaCargaEgreso dialog = new JDialogConsultaCargaEgreso(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.Box.Filler filler1;
    private javax.swing.Box.Filler filler2;
    private javax.swing.JButton jBtnBuscarAsientos;
    private javax.swing.JButton jBtnEliminarAsiento;
    private javax.swing.JButton jBtnModificarAsiento;
    private javax.swing.JButton jBtnVolver;
    private javax.swing.JComboBox jComboAnio;
    private javax.swing.JComboBox jComboMes;
    private javax.swing.JLabel jLabelAnio2;
    private javax.swing.JLabel jLabelMes2;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPnlBusqueda;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTblAsientos;
    private org.jdesktop.swingx.JXBusyLabel jXAnimacionEspera2;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloAsientos1;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTitulosBuscarAsiento2;
    // End of variables declaration//GEN-END:variables

    private void cargarDatosEnTabla(ArrayList<Egreso> egresos) throws NullPointerException {
        if (egresos.isEmpty()) {
            throw new NullPointerException();// aca me quede...
        }
        ListIterator<Egreso> it = egresos.listIterator();
        DefaultTableModel modeloTabla = (DefaultTableModel) jTblAsientos.getModel();
        while (it.hasNext()) {
            Egreso egreso = it.next();
            Object[] datosEgresos = new Object[6];
            datosEgresos[0] = egreso.getNroAsiento() == 0 ? it.previousIndex() : egreso.getNroAsiento();
            datosEgresos[1] = egreso.getCuenta().getNombre();
            datosEgresos[2] = UtilFechas.getFechaEnFormatoDD_MM_AA(egreso.getFecha());
            datosEgresos[3] = egreso.getDetalle().getDetalle();
            datosEgresos[4] = (egreso.getMonto());
            datosEgresos[5] = (egreso.getPagado());
            modeloTabla.addRow(datosEgresos);
        }
    }

    private boolean llamadoParaConsultarCarga() {
        if (padre != null) {
            return true;
        } else {
            return false;
        }
    }

   public void guardarModificacionEnEgresoEnBD(Egreso egresoAModificar) throws DAOProblemaConTransaccionException {
        if (padre == null) {
            this.institucion.actualizarEgreso(egresoAModificar);
        }
    }

    public void guardarModificacionDeEgresoEnColeccion(Egreso egresoAModificar) {
        int a = egresosCargados.indexOf(egresoAModificar);
        if (egresosCargados.contains(egresoAModificar)) {
            egresosCargados.remove(a);
            egresosCargados.add(a, egresoAModificar);
        } else {
            egresosCargados.add(egresoAModificar);
        }
    }

    public void guardarModificacionesEnJTable(Egreso egresoModificado) {
        int a;
        if(llamadoParaConsultarCarga()){
            a = egresosCargados.indexOf(egresoModificado);
        }else{
            a = egresoModificado.getNroAsiento();
        }
        DefaultTableModel modelo = (DefaultTableModel) this.jTblAsientos.getModel();
        modelo.setValueAt(a, jTblAsientos.getSelectedRow(), 0);
        modelo.setValueAt(UtilFechas.getFechaEnFormatoDD_MM_AA(egresoModificado.getFecha()), jTblAsientos.getSelectedRow(), 2);
        modelo.setValueAt(egresoModificado.getCuenta().getNombre(), jTblAsientos.getSelectedRow(), 1);
        modelo.setValueAt(egresoModificado.getDetalle().getDetalle(), jTblAsientos.getSelectedRow(), 3);
        modelo.setValueAt(egresoModificado.getMonto(), jTblAsientos.getSelectedRow(), 4);
        modelo.setValueAt(egresoModificado.getPagado(), jTblAsientos.getSelectedRow(), 5);
    }

    private boolean seBuscaAsientosDelMesCorriente() {
        int anio = (Integer) this.jComboAnio.getSelectedItem();
        int mes = UtilFechas.mesEnNumero(this.jComboAnio.getSelectedItem().toString());
        if (UtilFechas.esDelMesActual(anio, mes)) {
            return true;
        } else {
            return false;
        }

    }

    private void deshabilitarOHabilitarBotonesDependeSiEligeElMesCorriente(boolean habilitarONo) {
        this.jBtnEliminarAsiento.setEnabled(habilitarONo);
        this.jBtnModificarAsiento.setEnabled(habilitarONo);
    }

    private void eliminarEgresoDeTablaYColeccion(Egreso egresoAEliminar) {
        egresosCargados.remove(egresoAEliminar);
        DefaultTableModel d = (DefaultTableModel) jTblAsientos.getModel();
        d.removeRow(jTblAsientos.getSelectedRow());
    }

    void guardarModificacionDeEgresoEnJTableYEnColeccion(Egreso egresoAModificar) {
        this.guardarModificacionesEnJTable(egresoAModificar);
        this.guardarModificacionDeEgresoEnColeccion(egresoAModificar);
    }
}
