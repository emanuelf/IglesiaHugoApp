package Vistas;

import java.beans.Beans;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.jdesktop.swingx.JXTitledSeparator;

import Modelo.Miembro;
import Vistas.paneles.JPnlDatosBusquedaMiembro;
import Vistas.paneles.JPnlDatosMiembro;
import Vistas.paneles.JPnlTablaMiembros;
import Vistas.paneles.miembros.JPnlBotoneraAbmMiembro;
import Vistas.presentadores.MiembroPresenter.PRESENTACION;

/**
 * @author Emanuel
 */
public class JPnlAbmMiembros extends javax.swing.JPanel {

    public JPnlAbmMiembros() {
        initComponents();
        if (!Beans.isDesignTime()) {
            jPnlBotoneraAbmMiembro.setSeleccionable(jPnlTablaMiembros);
            jPnlTablaMiembros.setObservableBusqueda(jPnlDatosBusquedaMiembro);
            jPnlTablaMiembros.addObserverSeleccion(jPnlDatosMiembro);
            jPnlTablaMiembros.addObserverSeleccion(jPnlBotoneraAbmMiembro);
            jPnlDatosMiembro.setSeleccionable(jPnlTablaMiembros);
            jPnlDatosBusquedaMiembro.setObserver(jPnlTablaMiembros);
        }
    }

    public void setModo(PRESENTACION modo) {
        jPnlBotoneraAbmMiembro.setModo(modo);
        jPnlTablaMiembros.setModo(modo);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXTitledSeparator1 = new JXTitledSeparator();
        jPnlDatosBusquedaMiembro = new JPnlDatosBusquedaMiembro();
        jPnlTablaMiembros = new JPnlTablaMiembros();
        jPnlDatosMiembro = new JPnlDatosMiembro();
        jPnlBotoneraAbmMiembro = new JPnlBotoneraAbmMiembro();

        jXTitledSeparator1.setTitle("Datos de busqueda");

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(Alignment.LEADING)
            .addComponent(jPnlTablaMiembros, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(Alignment.LEADING)
                    .addComponent(jPnlDatosMiembro, GroupLayout.DEFAULT_SIZE, 900, Short.MAX_VALUE)
                    .addComponent(jXTitledSeparator1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPnlDatosBusquedaMiembro, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jPnlBotoneraAbmMiembro, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(jPnlDatosBusquedaMiembro, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(jPnlTablaMiembros, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(jPnlDatosMiembro, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPnlBotoneraAbmMiembro, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JPnlBotoneraAbmMiembro jPnlBotoneraAbmMiembro;
    private JPnlDatosBusquedaMiembro jPnlDatosBusquedaMiembro;
    private JPnlDatosMiembro jPnlDatosMiembro;
    private JPnlTablaMiembros jPnlTablaMiembros;
    private JXTitledSeparator jXTitledSeparator1;
    // End of variables declaration//GEN-END:variables

    public Miembro getMiembroSeleccionado() {
        return jPnlTablaMiembros.getSeleccion();
    }
}
