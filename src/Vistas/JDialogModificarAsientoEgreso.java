/*
 * JDialogModificarAsientoEgreso.java
 * Created on 17/08/2011, 23:24:48
 */
package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.Cuenta;
import Modelo.CuentaEgreso;
import Modelo.Egreso;
import Modelo.InstitucionCentralTesoreria;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author emanuel
 */
public class JDialogModificarAsientoEgreso extends javax.swing.JDialog {

    private boolean egresoModificado;
    private Egreso egresoAModificar;
    private JDialogConsultaCargaEgreso padre;
    private InstitucionCentralTesoreria institucion;
    private ArrayList<CuentaEgreso> cuentasEgreso;
    private int posicionEnArrayList;
    private boolean llamadoParaModificarCarga;

    public JDialogModificarAsientoEgreso(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
    }

    public JDialogModificarAsientoEgreso(JDialogConsultaCargaEgreso padre, boolean modal, Egreso egresoAModificar,
            InstitucionCentralTesoreria institucionCentralTesoreria, int posicionEnArrayList, boolean llamadoParaModificarCarga) {
        this.padre = padre;
        this.setModal(modal);
        initComponents();
        this.llamadoParaModificarCarga = llamadoParaModificarCarga;
        this.institucion = institucionCentralTesoreria;
        this.egresoAModificar = egresoAModificar;
        cargarDatosEnComponentes(this.egresoAModificar);
        this.jXDatePickerFechaAsiento.setDate(new Date());
        cuentasEgreso = this.institucion.getCuentasEgreso();
        cargarComboConTipoCuentaEgreso(cuentasEgreso);
        this.egresoModificado = false;
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelCuerpo = new javax.swing.JPanel();
        jXSeparadorDeTituloModificarAsiento = new org.jdesktop.swingx.JXTitledSeparator();
        jPanel6 = new javax.swing.JPanel();
        jBtnGuardarCambios = new javax.swing.JButton();
        jBtnVolver = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabelFecha = new javax.swing.JLabel();
        jXDatePickerFechaAsiento = new org.jdesktop.swingx.JXDatePicker();
        jComboTiposDeCuentas = new javax.swing.JComboBox();
        jLabelTipoDeCuenta = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabelMonto2 = new javax.swing.JLabel();
        jTxtMonto = new javax.swing.JTextField();
        jTxtMontoPagado = new javax.swing.JTextField();
        jLabelMontoPagado = new javax.swing.JLabel();
        jTxtDetalle = new javax.swing.JTextField();
        jLabelNuevoDetalle = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setModal(true);
        setResizable(false);

        jPanelCuerpo.setPreferredSize(new java.awt.Dimension(500, 600));

        jXSeparadorDeTituloModificarAsiento.setFont(new java.awt.Font("Tahoma", 0, 12));
        jXSeparadorDeTituloModificarAsiento.setHorizontalAlignment(0);
        jXSeparadorDeTituloModificarAsiento.setTitle("Modificar asiento");

        jPanel6.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        jBtnGuardarCambios.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnGuardarCambios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Guardar 30x30.png"))); // NOI18N
        jBtnGuardarCambios.setText("Guardar cambios");
        jBtnGuardarCambios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnGuardarCambiosActionPerformed(evt);
            }
        });
        jPanel6.add(jBtnGuardarCambios);

        jBtnVolver.setFont(new java.awt.Font("Dialog", 0, 12));
        jBtnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolver.setText("Volver");
        jBtnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverActionPerformed(evt);
            }
        });
        jPanel6.add(jBtnVolver);

        jPanel1.setForeground(java.awt.Color.white);

        jLabelFecha.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelFecha.setText("Fecha:");

        jComboTiposDeCuentas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jComboTiposDeCuentasMouseClicked(evt);
            }
        });
        jComboTiposDeCuentas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboTiposDeCuentasActionPerformed(evt);
            }
        });
        jComboTiposDeCuentas.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jComboTiposDeCuentasPropertyChange(evt);
            }
        });

        jLabelTipoDeCuenta.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelTipoDeCuenta.setText("Tipo cuenta:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jXDatePickerFechaAsiento, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE))
                    .addComponent(jLabelFecha))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jComboTiposDeCuentas, 0, 125, Short.MAX_VALUE)
                .addGap(12, 12, 12))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabelTipoDeCuenta)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabelFecha)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jXDatePickerFechaAsiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(jLabelTipoDeCuenta)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jComboTiposDeCuentas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
        );

        jPanel2.setForeground(java.awt.Color.white);

        jLabelMonto2.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelMonto2.setText("Monto:");

        jTxtMonto.setFont(new java.awt.Font("Dialog", 1, 24));
        jTxtMonto.setForeground(new java.awt.Color(0, 204, 51));
        jTxtMonto.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtMonto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTxtMontoMouseClicked(evt);
            }
        });
        jTxtMonto.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTxtMontoCaretUpdate(evt);
            }
        });
        jTxtMonto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtMontoActionPerformed(evt);
            }
        });
        jTxtMonto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTxtMontoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTxtMontoFocusLost(evt);
            }
        });
        jTxtMonto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtMontoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxtMontoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtMontoKeyTyped(evt);
            }
        });

        jTxtMontoPagado.setFont(new java.awt.Font("Dialog", 1, 24));
        jTxtMontoPagado.setForeground(new java.awt.Color(0, 204, 51));
        jTxtMontoPagado.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtMontoPagado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTxtMontoPagadoMouseClicked(evt);
            }
        });
        jTxtMontoPagado.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTxtMontoPagadoCaretUpdate(evt);
            }
        });
        jTxtMontoPagado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtMontoPagadoActionPerformed(evt);
            }
        });
        jTxtMontoPagado.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTxtMontoPagadoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTxtMontoPagadoFocusLost(evt);
            }
        });
        jTxtMontoPagado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtMontoPagadoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxtMontoPagadoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtMontoPagadoKeyTyped(evt);
            }
        });

        jLabelMontoPagado.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelMontoPagado.setText("Monto pagado:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelMontoPagado)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jTxtMontoPagado, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(jTxtMonto, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabelMonto2))
                .addContainerGap(16, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabelMonto2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtMonto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addComponent(jLabelMontoPagado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtMontoPagado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLabelNuevoDetalle.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelNuevoDetalle.setText("Detalle:");

        javax.swing.GroupLayout jPanelCuerpoLayout = new javax.swing.GroupLayout(jPanelCuerpo);
        jPanelCuerpo.setLayout(jPanelCuerpoLayout);
        jPanelCuerpoLayout.setHorizontalGroup(
            jPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jXSeparadorDeTituloModificarAsiento, javax.swing.GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCuerpoLayout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelCuerpoLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jTxtDetalle, javax.swing.GroupLayout.DEFAULT_SIZE, 310, Short.MAX_VALUE))
                    .addComponent(jLabelNuevoDetalle))
                .addContainerGap())
        );
        jPanelCuerpoLayout.setVerticalGroup(
            jPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXSeparadorDeTituloModificarAsiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelNuevoDetalle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtDetalle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnGuardarCambiosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnGuardarCambiosActionPerformed
        if (hayCamposVacios()) {
            JOptionPane.showMessageDialog(this, "Todos los campos son obligatorios.");
            return;
        }
        int respuesta = JOptionPane.showConfirmDialog(this, "¿Guardar los cambios en el egreso?", "Aviso", JOptionPane.YES_NO_OPTION);
        if (respuesta == JOptionPane.YES_OPTION && llamadoParaModificarCarga) {
            guardarCambiosEnEgreso();
            this.padre.guardarModificacionDeEgresoEnJTableYEnColeccion(egresoAModificar);
            this.dispose();
            this.padre.setVisible(true);
        } else if (respuesta == JOptionPane.YES_OPTION && !llamadoParaModificarCarga) {
            try {
                guardarCambiosEnEgreso();
                institucion.actualizarEgreso(egresoAModificar);
                this.padre.guardarModificacionDeEgresoEnJTableYEnColeccion(egresoAModificar);
                this.dispose();
            } catch (DAOProblemaConTransaccionException ex) {
                Vistas.Util.Util.showMessageErrorEnTransaccion(ex);
            }
        }
}//GEN-LAST:event_jBtnGuardarCambiosActionPerformed

    private double parseStringCurrencyADouble(String numeroConSigno) {
        return Double.valueOf(numeroConSigno.replace("$", ""));
    }
    private void jBtnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVolverActionPerformed
        if (!egresoModificado) {
            int respuesta = JOptionPane.showConfirmDialog(this, "No se modificaron los datos del egreso. ¿Salir de todos modos?", "Aviso",
                    JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_OPTION) {
                this.dispose();
                this.padre.setVisible(true);
            } else {
                return;
            }
        }
}//GEN-LAST:event_jBtnVolverActionPerformed

    private void jTxtMontoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTxtMontoMouseClicked
    }//GEN-LAST:event_jTxtMontoMouseClicked

    private void jTxtMontoCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTxtMontoCaretUpdate
        // TODO add your handling code here:
    }//GEN-LAST:event_jTxtMontoCaretUpdate

    private void jTxtMontoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtMontoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTxtMontoActionPerformed

    private void jTxtMontoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtMontoFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_jTxtMontoFocusGained

    private void jTxtMontoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtMontoFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jTxtMontoFocusLost

    private void jTxtMontoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTxtMontoKeyPressed

    private void jTxtMontoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoKeyReleased
        Vistas.Util.Util.limpiarTextoSiSoloHaySigno(jTxtMonto);
    }//GEN-LAST:event_jTxtMontoKeyReleased

    private void jTxtMontoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoKeyTyped
        Vistas.Util.Util.ignorarSiNoEsEntero(evt);
    }//GEN-LAST:event_jTxtMontoKeyTyped

    private void jComboTiposDeCuentasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboTiposDeCuentasMouseClicked
    }//GEN-LAST:event_jComboTiposDeCuentasMouseClicked

    private void jComboTiposDeCuentasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboTiposDeCuentasActionPerformed
    }//GEN-LAST:event_jComboTiposDeCuentasActionPerformed

    private void jComboTiposDeCuentasPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jComboTiposDeCuentasPropertyChange
    }//GEN-LAST:event_jComboTiposDeCuentasPropertyChange

private void jTxtMontoPagadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTxtMontoPagadoMouseClicked
// TODO add your handling code here:
}//GEN-LAST:event_jTxtMontoPagadoMouseClicked

private void jTxtMontoPagadoCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTxtMontoPagadoCaretUpdate
// TODO add your handling code here:
}//GEN-LAST:event_jTxtMontoPagadoCaretUpdate

private void jTxtMontoPagadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtMontoPagadoActionPerformed
}//GEN-LAST:event_jTxtMontoPagadoActionPerformed

private void jTxtMontoPagadoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtMontoPagadoFocusGained
// TODO add your handling code here:
}//GEN-LAST:event_jTxtMontoPagadoFocusGained

private void jTxtMontoPagadoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtMontoPagadoFocusLost
// TODO add your handling code here:
}//GEN-LAST:event_jTxtMontoPagadoFocusLost

private void jTxtMontoPagadoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoPagadoKeyPressed
// TODO add your handling code here:
}//GEN-LAST:event_jTxtMontoPagadoKeyPressed

private void jTxtMontoPagadoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoPagadoKeyReleased
    Vistas.Util.Util.limpiarTextoSiSoloHaySigno(jTxtMontoPagado);
}//GEN-LAST:event_jTxtMontoPagadoKeyReleased

private void jTxtMontoPagadoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoPagadoKeyTyped
    Vistas.Util.Util.formatearJTextFieldTipoMonedaLuegoDeUnKeyEvent(evt, jTxtMontoPagado);
}//GEN-LAST:event_jTxtMontoPagadoKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogModificarAsientoEgreso dialog = new JDialogModificarAsientoEgreso(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnGuardarCambios;
    private javax.swing.JButton jBtnVolver;
    private javax.swing.JComboBox jComboTiposDeCuentas;
    private javax.swing.JLabel jLabelFecha;
    private javax.swing.JLabel jLabelMonto2;
    private javax.swing.JLabel jLabelMontoPagado;
    private javax.swing.JLabel jLabelNuevoDetalle;
    private javax.swing.JLabel jLabelTipoDeCuenta;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanelCuerpo;
    private javax.swing.JTextField jTxtDetalle;
    private javax.swing.JTextField jTxtMonto;
    private javax.swing.JTextField jTxtMontoPagado;
    private org.jdesktop.swingx.JXDatePicker jXDatePickerFechaAsiento;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloModificarAsiento;
    // End of variables declaration//GEN-END:variables

    /**
     * @return the egresoModificado
     */
    public boolean isEgresoModificado() {
        return egresoModificado;
    }

    /**
     * @param egresoModificado the egresoModificado to set
     */
    public void setEgresoModificado(boolean egresoModificado) {
        this.egresoModificado = egresoModificado;
    }

    private void cargarDatosEnComponentes(Egreso egresoAModificar) {
        java.text.DecimalFormat a = new DecimalFormat("$##.##");
        jTxtMonto.setText(a.format(egresoAModificar.getMonto()));
        jXDatePickerFechaAsiento.setDate(egresoAModificar.getFecha().getGregorianChange());
        jTxtDetalle.setText(egresoAModificar.getDetalle().getDetalle());
        jTxtMontoPagado.setText(a.format(egresoAModificar.getPagado()));

    }

    private void cargarComboConTipoCuentaEgreso(ArrayList<CuentaEgreso> cuentasEgreso) {
        String objetoASeleccionar = null;
        for (CuentaEgreso cuentaEgreso : cuentasEgreso) {
            jComboTiposDeCuentas.addItem(cuentaEgreso.getNombre());
            if (cuentaEgreso.equals(egresoAModificar.getCuenta().getNombre())) {
                objetoASeleccionar = cuentaEgreso.getNombre();
            }
        }
        this.jComboTiposDeCuentas.setSelectedItem(objetoASeleccionar);
    }

    private boolean hayCamposVacios() {
        if (jComboTiposDeCuentas.getSelectedIndex() == -1 || jXDatePickerFechaAsiento.getDate() == null || jTxtDetalle.getText().isEmpty()
                || jTxtMonto.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    private void guardarCambiosEnEgreso() {
        egresoAModificar.setCuenta(buscarCuenta(jComboTiposDeCuentas.getSelectedItem().toString()));
        egresoAModificar.getDetalle().setDetalle(this.jTxtDetalle.getText());
        egresoAModificar.setMonto(Double.valueOf(Vistas.Util.Util.getMontoSinSimbolo(jTxtMonto.getText())));
        Date fechaModificada = this.jXDatePickerFechaAsiento.getDate();
        egresoAModificar.setPagado(Vistas.Util.Util.getMontoSinSimbolo(jTxtMontoPagado.getText()));
        egresoAModificar.getFecha().setTime(fechaModificada);
    }

    private Cuenta buscarCuenta(String toString) {
        CuentaEgreso retorno = null;
        for (CuentaEgreso cuentaIngreso : cuentasEgreso) {
            if (cuentaIngreso.getNombre().equals(toString)) {
                retorno = cuentaIngreso;
            }
        }
        return retorno;
    }
}
