package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Excepciones.ObjetoNoEncontradoException;
import Modelo.*;
import static Vistas.Util.Common.cargarFilialesEnCombo;
import static Vistas.Util.Util.cargaMesesEnCombo;
import static Vistas.Util.Util.cargaSecuenciaEnCombo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Set;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class JDialogConsultaRelatorioDeFiliales extends javax.swing.JDialog {

    private InstitucionCentralTesoreria institucionCentralTesoreria;
    private RelatorioFilialMensual relatorio;

    public JDialogConsultaRelatorioDeFiliales(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.jXAnimacionEspera.setVisible(false);
    }

    JDialogConsultaRelatorioDeFiliales(JFrameSistema padre, boolean modal,
            final InstitucionCentralTesoreria institucionCentralTesoreria) {
        this(padre, modal);
        this.institucionCentralTesoreria = institucionCentralTesoreria;
        cargaMesesEnCombo(jComboBoxMes);
        cargaSecuenciaEnCombo(2010, Calendar.getInstance().get(Calendar.YEAR), jComboBoxAnio);
        try {
            cargarFilialesEnCombo(this.institucionCentralTesoreria, jComboBoxFilial);
        } catch (DAOProblemaConTransaccionException ex) {
            JOptionPane.showMessageDialog(this, "Hubo un problema con la base de datos o con una consulta a la misma. \n Si el problema persiste, "
                    + "consulte con el responsable del sistema.");
        }
        agregarEliminarRelatorioActionListener(institucionCentralTesoreria);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXPanelCuerpo = new org.jdesktop.swingx.JXPanel();
        jXSeparadorDeTituloBuscarRelatorios = new org.jdesktop.swingx.JXTitledSeparator();
        jXSeparadorDeTituloDatosRelatorio = new org.jdesktop.swingx.JXTitledSeparator();
        jScrlRelatorio = new javax.swing.JScrollPane();
        jXTblRelatorio = new org.jdesktop.swingx.JXTable();
        jPnlFiltros = new javax.swing.JPanel();
        jPnlDatos = new javax.swing.JPanel();
        jXLblFilial = new org.jdesktop.swingx.JXLabel();
        jComboBoxFilial = new javax.swing.JComboBox();
        jXLblAnio = new org.jdesktop.swingx.JXLabel();
        jComboBoxAnio = new javax.swing.JComboBox();
        jXLblMes = new org.jdesktop.swingx.JXLabel();
        jComboBoxMes = new javax.swing.JComboBox();
        jBtnBuscar = new javax.swing.JButton();
        jXAnimacionEspera = new org.jdesktop.swingx.JXBusyLabel();
        jPnlBotonera = new javax.swing.JPanel();
        jBtnEliminarRelatorio = new javax.swing.JButton();
        jBtnVolver = new javax.swing.JButton();
        jPnlDatosGeneralesDeRelatorio = new javax.swing.JPanel();
        jLblCajaInicial = new javax.swing.JLabel();
        jLblTotalEgresos = new javax.swing.JLabel();
        jLblTotalIngresos = new javax.swing.JLabel();
        jLblPeriodo = new javax.swing.JLabel();
        jLblTotalDeuda = new javax.swing.JLabel();
        jLblCajaInicialDato = new javax.swing.JLabel();
        jLblTotalIngresosDato = new javax.swing.JLabel();
        jLblTotalEgresosDato = new javax.swing.JLabel();
        jLblPeriodoDato = new javax.swing.JLabel();
        jLblTotalDeudaDato = new javax.swing.JLabel();
        jLlbDiezmoFilial = new javax.swing.JLabel();
        jLblDiezmoPastoral = new javax.swing.JLabel();
        jLblDiezmoPastoralDato = new javax.swing.JLabel();
        jLblDiezmoFilialDato = new javax.swing.JLabel();
        jLblCajaFinal = new javax.swing.JLabel();
        jLblCajaFinalDato = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Consulta de relatorios");

        jXPanelCuerpo.setMinimumSize(new java.awt.Dimension(1130, 548));

        jXSeparadorDeTituloBuscarRelatorios.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jXSeparadorDeTituloBuscarRelatorios.setHorizontalAlignment(0);
        jXSeparadorDeTituloBuscarRelatorios.setHorizontalTextPosition(0);
        jXSeparadorDeTituloBuscarRelatorios.setTitle("Buscar relatorios");

        jXSeparadorDeTituloDatosRelatorio.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jXSeparadorDeTituloDatosRelatorio.setHorizontalAlignment(0);
        jXSeparadorDeTituloDatosRelatorio.setTitle("Datos de relatorio");

        jXTblRelatorio.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Tipo cuenta", "Cuenta", "Monto"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jXTblRelatorio.getTableHeader().setReorderingAllowed(false);
        jScrlRelatorio.setViewportView(jXTblRelatorio);
        jXTblRelatorio.getColumnModel().getColumn(0).setMinWidth(130);
        jXTblRelatorio.getColumnModel().getColumn(0).setPreferredWidth(160);
        jXTblRelatorio.getColumnModel().getColumn(0).setMaxWidth(160);
        jXTblRelatorio.getColumnModel().getColumn(2).setMinWidth(75);
        jXTblRelatorio.getColumnModel().getColumn(2).setPreferredWidth(100);
        jXTblRelatorio.getColumnModel().getColumn(2).setMaxWidth(120);

        jPnlDatos.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)), "Período:", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jXLblFilial.setText("Filial:");
        jXLblFilial.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jComboBoxFilial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxFilialActionPerformed(evt);
            }
        });

        jXLblAnio.setText("Año:");
        jXLblAnio.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jComboBoxAnio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxAnioActionPerformed(evt);
            }
        });

        jXLblMes.setText("Mes:");
        jXLblMes.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jBtnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Buscar.png"))); // NOI18N
        jBtnBuscar.setText("Buscar");
        jBtnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnBuscarActionPerformed(evt);
            }
        });

        jXAnimacionEspera.setText("Espere...");
        jXAnimacionEspera.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N

        javax.swing.GroupLayout jPnlDatosLayout = new javax.swing.GroupLayout(jPnlDatos);
        jPnlDatos.setLayout(jPnlDatosLayout);
        jPnlDatosLayout.setHorizontalGroup(
            jPnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlDatosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jXLblFilial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPnlDatosLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jComboBoxFilial, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(17, 17, 17)
                .addGroup(jPnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jXLblAnio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPnlDatosLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jComboBoxAnio, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(17, 17, 17)
                .addGroup(jPnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlDatosLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jComboBoxMes, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jXLblMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addComponent(jBtnBuscar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jXAnimacionEspera, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(93, Short.MAX_VALUE))
        );
        jPnlDatosLayout.setVerticalGroup(
            jPnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlDatosLayout.createSequentialGroup()
                .addGroup(jPnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlDatosLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jXLblFilial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jXLblAnio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jXLblMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboBoxAnio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBoxMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBoxFilial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPnlDatosLayout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(jPnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jBtnBuscar)
                            .addComponent(jXAnimacionEspera, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPnlFiltrosLayout = new javax.swing.GroupLayout(jPnlFiltros);
        jPnlFiltros.setLayout(jPnlFiltrosLayout);
        jPnlFiltrosLayout.setHorizontalGroup(
            jPnlFiltrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlFiltrosLayout.createSequentialGroup()
                .addComponent(jPnlDatos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPnlFiltrosLayout.setVerticalGroup(
            jPnlFiltrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlFiltrosLayout.createSequentialGroup()
                .addComponent(jPnlDatos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jBtnEliminarRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Eliminar egreso 20x20.png"))); // NOI18N
        jBtnEliminarRelatorio.setText("Eliminar");
        jPnlBotonera.add(jBtnEliminarRelatorio);

        jBtnVolver.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolver.setText("Volver");
        jBtnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverActionPerformed(evt);
            }
        });
        jPnlBotonera.add(jBtnVolver);

        jLblCajaInicial.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLblCajaInicial.setText("Caja inicial:");

        jLblTotalEgresos.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLblTotalEgresos.setText("Total egresos:");

        jLblTotalIngresos.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLblTotalIngresos.setText("Total ingresos:");

        jLblPeriodo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLblPeriodo.setText("Período:");

        jLblTotalDeuda.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLblTotalDeuda.setText("Total Deudas:");

        jLblCajaInicialDato.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLblCajaInicialDato.setText("           ");

        jLblTotalIngresosDato.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLblTotalIngresosDato.setText("           ");

        jLblTotalEgresosDato.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLblTotalEgresosDato.setText("           ");

        jLblPeriodoDato.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLblPeriodoDato.setText("           ");

        jLblTotalDeudaDato.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLblTotalDeudaDato.setText("           ");

        jLlbDiezmoFilial.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLlbDiezmoFilial.setText("Diezmo filial:");

        jLblDiezmoPastoral.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLblDiezmoPastoral.setText("Diezmo pastoral:");

        jLblDiezmoPastoralDato.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLblDiezmoPastoralDato.setText("           ");

        jLblDiezmoFilialDato.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLblDiezmoFilialDato.setText("           ");

        jLblCajaFinal.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLblCajaFinal.setText("Caja final:");

        jLblCajaFinalDato.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLblCajaFinalDato.setText("           ");

        javax.swing.GroupLayout jPnlDatosGeneralesDeRelatorioLayout = new javax.swing.GroupLayout(jPnlDatosGeneralesDeRelatorio);
        jPnlDatosGeneralesDeRelatorio.setLayout(jPnlDatosGeneralesDeRelatorioLayout);
        jPnlDatosGeneralesDeRelatorioLayout.setHorizontalGroup(
            jPnlDatosGeneralesDeRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLblTotalIngresosDato))
                    .addComponent(jLblTotalIngresos)
                    .addComponent(jLblPeriodo)
                    .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLblPeriodoDato)))
                .addGap(97, 97, 97)
                .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLblCajaInicialDato))
                    .addComponent(jLblCajaInicial)
                    .addComponent(jLblTotalEgresos)
                    .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLblTotalEgresosDato)))
                .addGap(78, 78, 78)
                .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLblTotalDeuda)
                    .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLblTotalDeudaDato))
                    .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createSequentialGroup()
                        .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLlbDiezmoFilial)
                            .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLblDiezmoFilialDato)))
                        .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createSequentialGroup()
                                .addGap(90, 90, 90)
                                .addComponent(jLblDiezmoPastoralDato))
                            .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createSequentialGroup()
                                .addGap(80, 80, 80)
                                .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLblCajaFinal)
                                    .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addComponent(jLblCajaFinalDato))
                                    .addComponent(jLblDiezmoPastoral))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPnlDatosGeneralesDeRelatorioLayout.setVerticalGroup(
            jPnlDatosGeneralesDeRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createSequentialGroup()
                        .addComponent(jLlbDiezmoFilial)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblDiezmoFilialDato)
                        .addGap(18, 18, 18)
                        .addComponent(jLblTotalDeuda)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblTotalDeudaDato))
                    .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createSequentialGroup()
                        .addComponent(jLblDiezmoPastoral)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblDiezmoPastoralDato)
                        .addGap(18, 18, 18)
                        .addComponent(jLblCajaFinal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblCajaFinalDato))
                    .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createSequentialGroup()
                        .addComponent(jLblPeriodo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblPeriodoDato)
                        .addGap(18, 18, 18)
                        .addComponent(jLblTotalIngresos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblTotalIngresosDato, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPnlDatosGeneralesDeRelatorioLayout.createSequentialGroup()
                        .addComponent(jLblCajaInicial)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLblCajaInicialDato, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(19, 19, 19)
                        .addComponent(jLblTotalEgresos)
                        .addGap(9, 9, 9)
                        .addComponent(jLblTotalEgresosDato)))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jXPanelCuerpoLayout = new javax.swing.GroupLayout(jXPanelCuerpo);
        jXPanelCuerpo.setLayout(jXPanelCuerpoLayout);
        jXPanelCuerpoLayout.setHorizontalGroup(
            jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jXSeparadorDeTituloDatosRelatorio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jXSeparadorDeTituloBuscarRelatorios, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPnlFiltros, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jXPanelCuerpoLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPnlBotonera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPnlDatosGeneralesDeRelatorio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrlRelatorio, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(388, 388, 388))
        );
        jXPanelCuerpoLayout.setVerticalGroup(
            jXPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jXPanelCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXSeparadorDeTituloBuscarRelatorios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPnlFiltros, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jXSeparadorDeTituloDatosRelatorio, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrlRelatorio, javax.swing.GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
                .addGap(13, 13, 13)
                .addComponent(jPnlDatosGeneralesDeRelatorio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(19, 19, 19)
                .addComponent(jPnlBotonera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jXPanelCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, 752, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jXPanelCuerpo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnBuscarActionPerformed
        if (!isSelectedCamposDeBusqueda()) {
            JOptionPane.showMessageDialog(this, "Seleccione la filial y el período del relatorio.", "Aviso", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        int anio = Integer.valueOf(jComboBoxAnio.getSelectedItem().toString());
        int mes = Util.UtilFechas.mesEnNumero(jComboBoxMes.getSelectedItem().toString());
        try {
            Filial filial = (Filial) jComboBoxFilial.getSelectedItem();
            //la filial fue cargada con su nombre en el jCombo. No son necesarias las comprobaciones existencia ni de seleccion o no del jcombo
            //ya que siempre hay una seleccion en este jcombo
            relatorio = institucionCentralTesoreria.devolverRelatorioFilialMensual(filial, anio, mes);
            cargarDatosDeFilialYDeRelatorioEnLaVista(filial, relatorio);
        } catch (DAOProblemaConTransaccionException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.INFORMATION_MESSAGE);
        } catch (ObjetoNoEncontradoException ex1) {
            JOptionPane.showMessageDialog(this, ex1.getMessage(), "Aviso", JOptionPane.INFORMATION_MESSAGE);
        }

}//GEN-LAST:event_jBtnBuscarActionPerformed

    private void jBtnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVolverActionPerformed
        this.dispose();
        this.getParent().setVisible(true);
}//GEN-LAST:event_jBtnVolverActionPerformed

    private void jComboBoxFilialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxFilialActionPerformed
    }//GEN-LAST:event_jComboBoxFilialActionPerformed

    private void jComboBoxAnioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxAnioActionPerformed
    }//GEN-LAST:event_jComboBoxAnioActionPerformed

   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnBuscar;
    private javax.swing.JButton jBtnEliminarRelatorio;
    private javax.swing.JButton jBtnVolver;
    private javax.swing.JComboBox jComboBoxAnio;
    private javax.swing.JComboBox jComboBoxFilial;
    private javax.swing.JComboBox jComboBoxMes;
    private javax.swing.JLabel jLblCajaFinal;
    private javax.swing.JLabel jLblCajaFinalDato;
    private javax.swing.JLabel jLblCajaInicial;
    private javax.swing.JLabel jLblCajaInicialDato;
    private javax.swing.JLabel jLblDiezmoFilialDato;
    private javax.swing.JLabel jLblDiezmoPastoral;
    private javax.swing.JLabel jLblDiezmoPastoralDato;
    private javax.swing.JLabel jLblPeriodo;
    private javax.swing.JLabel jLblPeriodoDato;
    private javax.swing.JLabel jLblTotalDeuda;
    private javax.swing.JLabel jLblTotalDeudaDato;
    private javax.swing.JLabel jLblTotalEgresos;
    private javax.swing.JLabel jLblTotalEgresosDato;
    private javax.swing.JLabel jLblTotalIngresos;
    private javax.swing.JLabel jLblTotalIngresosDato;
    private javax.swing.JLabel jLlbDiezmoFilial;
    private javax.swing.JPanel jPnlBotonera;
    private javax.swing.JPanel jPnlDatos;
    private javax.swing.JPanel jPnlDatosGeneralesDeRelatorio;
    private javax.swing.JPanel jPnlFiltros;
    private javax.swing.JScrollPane jScrlRelatorio;
    private org.jdesktop.swingx.JXBusyLabel jXAnimacionEspera;
    private org.jdesktop.swingx.JXLabel jXLblAnio;
    private org.jdesktop.swingx.JXLabel jXLblFilial;
    private org.jdesktop.swingx.JXLabel jXLblMes;
    private org.jdesktop.swingx.JXPanel jXPanelCuerpo;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloBuscarRelatorios;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloDatosRelatorio;
    private org.jdesktop.swingx.JXTable jXTblRelatorio;
    // End of variables declaration//GEN-END:variables

    private boolean isSelectedCamposDeBusqueda() {
        return (jComboBoxAnio.getSelectedIndex() != -1) && (jComboBoxFilial.getSelectedIndex() != -1) && (jComboBoxMes.getSelectedIndex() != -1);
    }

    private void cargarDatosDeFilialYDeRelatorioEnLaVista(Filial filial, RelatorioFilialMensual relatorio) {
        cargarIngresosEnTabla(relatorio.getIngresoRelatorios());
        cargarEgresosEnTabla(relatorio.getEgresoRelatorios());
        cargarDeudasEnLaTabla(relatorio.getDeudaRelatorios());
        cargarDatosDeRelatorioEnLabels(relatorio);
    }

    private void cargarEgresosEnTabla(Set<EgresoRelatorio> egresos) {
        Iterator<EgresoRelatorio> it = egresos.iterator();
        DefaultTableModel modelo = (DefaultTableModel) this.jXTblRelatorio.getModel();
        String[] filaEgresoTabla = new String[4];
        while (it.hasNext()) {
            EgresoRelatorio egreso = it.next();
            filaEgresoTabla[0] = "Egreso";
            filaEgresoTabla[1] = egreso.getCuentaFilialEgreso().getNombre();
            filaEgresoTabla[2] = String.valueOf(egreso.getMonto());

            modelo.addRow(filaEgresoTabla);
        }
    }

    private void cargarIngresosEnTabla(Set<IngresoRelatorio> ingresos) {
        Iterator<IngresoRelatorio> it = ingresos.iterator();
        DefaultTableModel modelo = (DefaultTableModel) this.jXTblRelatorio.getModel();
        String[] filaIngresoTabla = new String[4];
        while (it.hasNext()) {
            IngresoRelatorio ingreso = it.next();
            filaIngresoTabla[0] = "Ingreso";
            filaIngresoTabla[1] = ingreso.getCuenta().getNombre();
            filaIngresoTabla[2] = String.valueOf(ingreso.getMonto());
            modelo.addRow(filaIngresoTabla);
        }
    }

    private void cargarDatosDeRelatorioEnLabels(RelatorioFilialMensual relatorio) {
        jLblPeriodoDato.setText(new SimpleDateFormat("MM/yy").format(relatorio.getPeriodo().getTime()));
        jLblCajaInicialDato.setText(Double.toString(relatorio.getCajaInicialDeMes()));
        jLblDiezmoFilialDato.setText(Double.toString(relatorio.getDiezmoFilial()));
        jLblDiezmoPastoralDato.setText(Double.toString(relatorio.getDiezmoPastoral()));
        jLblTotalIngresosDato.setText(Double.toString(relatorio.getTotalIngresos()));
        jLblTotalDeudaDato.setText(relatorio.getTotalAdeudado().toString());
        jLblTotalEgresosDato.setText(relatorio.getTotalEgresos().toString());
        jLblCajaFinalDato.setText(Double.toString(relatorio.getCajaFinalDeMes()));
    }

    private void cargarDeudasEnLaTabla(Set<DeudaRelatorio> deudasRelatorio) {
        Iterator<DeudaRelatorio> it = deudasRelatorio.iterator();
        DefaultTableModel modelo = (DefaultTableModel) this.jXTblRelatorio.getModel();
        String[] filaIngresoTabla = new String[4];
        while (it.hasNext()) {
            DeudaRelatorio deuda = it.next();
            filaIngresoTabla[0] = "Deuda";
            filaIngresoTabla[1] = deuda.getCuentaFilialDeuda().getNombre();
            filaIngresoTabla[2] = String.valueOf(deuda.getMonto());
            modelo.addRow(filaIngresoTabla);
        }
    }

    private void limpiar() {
        jLblCajaFinalDato.setText("");
        jLblCajaInicial.setText("");
        jLblDiezmoFilialDato.setText("");
        jLblDiezmoPastoralDato.setText("");
        jLblPeriodoDato.setText("");
        jLblTotalDeudaDato.setText("");
        jLblTotalEgresosDato.setText("");
        jLblTotalIngresosDato.setText("");
    }

    private void agregarEliminarRelatorioActionListener(final InstitucionCentralTesoreria institucionCentralTesoreria) {
        jBtnEliminarRelatorio.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                try {
                    int respuesta = JOptionPane.showConfirmDialog(rootPane, "Esta acción borrará el relatorio.\n ¿Desea continuar?", "Aviso", JOptionPane.YES_NO_OPTION);
                    if (respuesta == JOptionPane.YES_OPTION) {
                        institucionCentralTesoreria.eliminarRelatorio(relatorio);

                    }
                } catch (DAOProblemaConTransaccionException ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(JDialogConsultaRelatorioDeFiliales.this, "Hubo un problema con la base de datos o con una consulta a la misma. \n Si el problema persiste, "
                            + "consulte con el responsable del sistema.");
                }
            }
        });
    }
}
