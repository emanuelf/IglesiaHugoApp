/*
 * JDialogAltaEgreso.java
 *
 * Created on 25/05/2011, 15:21:22
 */
package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.CuentaEgreso;
import Modelo.DetalleEgreso;
import Modelo.Egreso;
import Modelo.InstitucionCentralTesoreria;
import Util.UtilFechas;
import Vistas.Util.Util;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author emanuel
 */
public class JDialogAltaEgreso extends javax.swing.JDialog {

    private ArrayList<Egreso> egresosCargados;
    private InstitucionCentralTesoreria institucion;
    private Set<DetalleEgreso> detallesEgreso;

    /** Creates new form JDialogAltaEgreso */
    public JDialogAltaEgreso(java.awt.Frame parent, boolean modal, InstitucionCentralTesoreria institucion) {
        super(parent, modal);
        initComponents();
        inicializarVariables();
        this.institucion = institucion;
        if (!institucion.getDetallesDeEgresos().isEmpty()) {
            this.detallesEgreso.addAll(institucion.getDetallesDeEgresos().values());
            cargarComboConDetallesEgreso(detallesEgreso);
        }
        setVisible(false);
        cargarComboConTipoCuentaEgreso(this.institucion.getCuentasEgreso());
        jXDatePickerFecha.setDate(new Date());
        asignarCajaEnLabel();
        jLblAvisoDeGuardar.setVisible(false);
        jTxtMontoPagado.setEnabled(false);
        this.setLocationRelativeTo(null);
    }

    private void cargarComboConDetallesEgreso(Collection<DetalleEgreso> detallesEgreso) {
        for (DetalleEgreso detalle : detallesEgreso) {
            jComboDetalles.addItem(detalle.getDetalle());
        }
    }

    private void cargarComboConTipoCuentaEgreso(ArrayList<CuentaEgreso> cuentasEgreso) {
        for (CuentaEgreso cuentaEgreso : cuentasEgreso) {
            jComboTiposDeCuentas.addItem(cuentaEgreso.getNombre());
        }
    }

    public JDialogAltaEgreso(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        jXDatePickerFecha.setDate(new Date());
        this.egresosCargados = new ArrayList<Egreso>();
        this.detallesEgreso = new HashSet<DetalleEgreso>();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel7 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jXSeparadorDeTitulo = new org.jdesktop.swingx.JXTitledSeparator();
        jPanel2 = new javax.swing.JPanel();
        jLabelSaldoDeCaja = new javax.swing.JLabel();
        jLabelSaldoCajaDato = new javax.swing.JLabel();
        jLblAvisoDeGuardar = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jBtnDeshacerAsiento = new javax.swing.JButton();
        jBtnLimpiarCampos = new javax.swing.JButton();
        jBtnVerCarga = new javax.swing.JButton();
        jBtnGuardar = new javax.swing.JButton();
        jBtnCargarUnEgreso = new javax.swing.JButton();
        jLabelNuevoDetalle = new javax.swing.JLabel();
        jTxtDetalle = new javax.swing.JTextField();
        jComboDetalles = new javax.swing.JComboBox();
        jLabelDetalle = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabelTipoDeCuenta = new javax.swing.JLabel();
        jComboTiposDeCuentas = new javax.swing.JComboBox();
        jLabelFecha = new javax.swing.JLabel();
        jXDatePickerFecha = new org.jdesktop.swingx.JXDatePicker();
        jTxtMonto = new javax.swing.JTextField();
        jLabelMonto = new javax.swing.JLabel();
        jLabelMontoPagado = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jTxtMontoPagado = new javax.swing.JTextField();
        jComboFormaDePago = new javax.swing.JComboBox();

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setPreferredSize(new java.awt.Dimension(1130, 548));

        jXSeparadorDeTitulo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jXSeparadorDeTitulo.setHorizontalAlignment(0);
        jXSeparadorDeTitulo.setTitle("Nuevo egreso");

        jLabelSaldoDeCaja.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabelSaldoDeCaja.setText("Saldo de caja:");

        jLabelSaldoCajaDato.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabelSaldoCajaDato.setText("455,25");

        jLblAvisoDeGuardar.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLblAvisoDeGuardar.setForeground(new java.awt.Color(204, 0, 0));
        jLblAvisoDeGuardar.setText("* Hay datos por guardar (los cambios impactarán cuando haga click en 'Guardar')");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLblAvisoDeGuardar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelSaldoDeCaja)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLabelSaldoCajaDato, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLblAvisoDeGuardar, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jLabelSaldoDeCaja)
                .addGap(4, 4, 4)
                .addComponent(jLabelSaldoCajaDato)
                .addContainerGap())
        );

        jPanel6.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT, 5, 0));

        jBtnDeshacerAsiento.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnDeshacerAsiento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnDeshacerAsiento.setText("Volver");
        jBtnDeshacerAsiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnDeshacerAsientoActionPerformed(evt);
            }
        });
        jPanel6.add(jBtnDeshacerAsiento);

        jBtnLimpiarCampos.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnLimpiarCampos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Limpiar campos.png"))); // NOI18N
        jBtnLimpiarCampos.setText("Limpiar campos");
        jBtnLimpiarCampos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnLimpiarCamposActionPerformed(evt);
            }
        });
        jPanel6.add(jBtnLimpiarCampos);

        jBtnVerCarga.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnVerCarga.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Balance ver 20x20.png"))); // NOI18N
        jBtnVerCarga.setText("Ver carga");
        jBtnVerCarga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVerCargaActionPerformed(evt);
            }
        });
        jPanel6.add(jBtnVerCarga);

        jBtnGuardar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Guardar 30x30.png"))); // NOI18N
        jBtnGuardar.setText("Guardar");
        jBtnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnGuardarActionPerformed(evt);
            }
        });
        jPanel6.add(jBtnGuardar);

        jBtnCargarUnEgreso.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnCargarUnEgreso.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/arrow_down_32.png"))); // NOI18N
        jBtnCargarUnEgreso.setText("Cargar");
        jBtnCargarUnEgreso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnCargarUnEgresoActionPerformed(evt);
            }
        });

        jLabelNuevoDetalle.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelNuevoDetalle.setText("Escriba aquí el nuevo detalle:");

        jComboDetalles.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboDetalles.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nuevo detalle..." }));
        jComboDetalles.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jComboDetallesMouseClicked(evt);
            }
        });
        jComboDetalles.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboDetallesActionPerformed(evt);
            }
        });
        jComboDetalles.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jComboDetallesPropertyChange(evt);
            }
        });

        jLabelDetalle.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelDetalle.setText("Detalle:");

        jLabelTipoDeCuenta.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelTipoDeCuenta.setText("Tipo cuenta:");

        jComboTiposDeCuentas.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboTiposDeCuentas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jComboTiposDeCuentasMouseClicked(evt);
            }
        });
        jComboTiposDeCuentas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboTiposDeCuentasActionPerformed(evt);
            }
        });
        jComboTiposDeCuentas.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jComboTiposDeCuentasPropertyChange(evt);
            }
        });

        jLabelFecha.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelFecha.setText("Fecha:");

        jXDatePickerFecha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jXDatePickerFechaActionPerformed(evt);
            }
        });

        jTxtMonto.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jTxtMonto.setForeground(new java.awt.Color(0, 204, 51));
        jTxtMonto.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtMonto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTxtMontoMouseClicked(evt);
            }
        });
        jTxtMonto.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTxtMontoCaretUpdate(evt);
            }
        });
        jTxtMonto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtMontoActionPerformed(evt);
            }
        });
        jTxtMonto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTxtMontoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTxtMontoFocusLost(evt);
            }
        });
        jTxtMonto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtMontoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxtMontoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtMontoKeyTyped(evt);
            }
        });

        jLabelMonto.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelMonto.setText("Monto:");

        jLabelMontoPagado.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelMontoPagado.setText("Monto pagado:");

        jLabel1.setText("Forma de pago:");

        jTxtMontoPagado.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jTxtMontoPagado.setForeground(new java.awt.Color(0, 204, 51));
        jTxtMontoPagado.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtMontoPagado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTxtMontoPagadoMouseClicked(evt);
            }
        });
        jTxtMontoPagado.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTxtMontoPagadoCaretUpdate(evt);
            }
        });
        jTxtMontoPagado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtMontoPagadoActionPerformed(evt);
            }
        });
        jTxtMontoPagado.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTxtMontoPagadoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTxtMontoPagadoFocusLost(evt);
            }
        });
        jTxtMontoPagado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTxtMontoPagadoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTxtMontoPagadoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtMontoPagadoKeyTyped(evt);
            }
        });

        jComboFormaDePago.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboFormaDePago.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Pagado", "Pagado en parte", "Adeudado" }));
        jComboFormaDePago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboFormaDePagoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabelFecha, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelTipoDeCuenta, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jComboTiposDeCuentas, javax.swing.GroupLayout.Alignment.LEADING, 0, 163, Short.MAX_VALUE)
                                            .addComponent(jXDatePickerFecha, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 163, Short.MAX_VALUE))))
                                .addGap(22, 22, 22))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jComboFormaDePago, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelMontoPagado)
                            .addComponent(jLabelMonto)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jTxtMontoPagado, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(14, 14, 14)
                                .addComponent(jTxtMonto))))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(145, 145, 145)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelFecha)
                    .addComponent(jLabelMonto, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jXDatePickerFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelTipoDeCuenta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboTiposDeCuentas, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jTxtMonto, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboFormaDePago, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabelMontoPagado)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxtMontoPagado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jXSeparadorDeTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(15, 15, 15)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jLabelDetalle)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(10, 10, 10)
                                                .addComponent(jTxtDetalle, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE))))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(25, 25, 25)
                                        .addComponent(jBtnCargarUnEgreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(27, 27, 27)
                                        .addComponent(jComboDetalles, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addGap(57, 57, 57))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabelNuevoDetalle))))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXSeparadorDeTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelDetalle)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboDetalles, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelNuevoDetalle)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxtDetalle, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39)
                        .addComponent(jBtnCargarUnEgreso)))
                .addGap(18, 18, 18)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 699, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 367, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private double dameMontoSinSimbolo(String montoConSimbolo) {
        return Double.parseDouble(montoConSimbolo.replace("$", ""));
    }
    private void jBtnLimpiarCamposActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnLimpiarCamposActionPerformed
        limpiarCampos();
    }//GEN-LAST:event_jBtnLimpiarCamposActionPerformed

    private void jBtnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnGuardarActionPerformed
        if (this.egresosCargados.isEmpty() || egresosCargados == null) {
            JOptionPane.showMessageDialog(this, "No hay asientos cargados aún.", "Aviso",
                    JOptionPane.INFORMATION_MESSAGE);
        } else {
            int respuesta = JOptionPane.showConfirmDialog(this, "¿Desea guardar los egresos cargados?", "Aviso",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (respuesta == JOptionPane.YES_OPTION) {
                try {
                    guardarEgresos();
                } catch (DAOProblemaConTransaccionException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage());
                }

            }
        }

    }//GEN-LAST:event_jBtnGuardarActionPerformed

    private void jBtnVerCargaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVerCargaActionPerformed
        if (getEgresosCargados().isEmpty() || this.egresosCargados == null) {
            JOptionPane.showMessageDialog(this, "No hay asientos cargados aún.", "Aviso",
                    JOptionPane.INFORMATION_MESSAGE);
        } else {
            JDialogConsultaCargaEgreso nuevoJDialogConsultaCarga = new JDialogConsultaCargaEgreso(this, true, getEgresosCargados(), this.institucion);
            nuevoJDialogConsultaCarga.setVisible(true);
        }
    }//GEN-LAST:event_jBtnVerCargaActionPerformed

    private void jComboDetallesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboDetallesMouseClicked
        // TODO add your handling code here:
}//GEN-LAST:event_jComboDetallesMouseClicked

    private void jComboDetallesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboDetallesActionPerformed
        if (jComboDetalles.getSelectedItem().toString().equals("Nuevo detalle...")) {
            jTxtDetalle.setEnabled(true);
        } else {
            jTxtDetalle.setEnabled(false);
        }
}//GEN-LAST:event_jComboDetallesActionPerformed

    private void jComboDetallesPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jComboDetallesPropertyChange
        // TODO add your handling code here:
}//GEN-LAST:event_jComboDetallesPropertyChange

    private boolean hayDetalleNuevo() {
        if (jTxtDetalle.isEnabled()) {
            return true;
        } else {
            return false;
        }
    }
    private void jBtnDeshacerAsientoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnDeshacerAsientoActionPerformed
        if (egresosCargados.isEmpty()) {
            this.dispose();
        } else {
            int respuesta = JOptionPane.showConfirmDialog(this, "Hay ingresos cargados. Si sale, estos no serán guardados. \n "
                    + "¿Salir de todos modos?", "Aviso", JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_NO_OPTION) {
                this.dispose();
            }
        }
        /*if (this.getEgresosCargados().isEmpty()) {
        JOptionPane.showMessageDialog(this, "No hay nada para deshacer.");
        } else {
        this.getEgresosCargados().remove(this.getEgresosCargados().size());
        if(this.getEgresosCargados().isEmpty()) this.jLabelAvisoDeGuardar.setVisible(false);
        }*/
}//GEN-LAST:event_jBtnDeshacerAsientoActionPerformed

    private void jBtnCargarUnEgresoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnCargarUnEgresoActionPerformed
        if (!hayCamposVacios()) {
            double montoAPagar = this.dameMontoSinSimbolo(jTxtMonto.getText());
            double pagado = this.dameMontoSinSimbolo(jTxtMontoPagado.getText());
            if (montoAPagar < pagado) {
                JOptionPane.showMessageDialog(this, "El monto a pagar no puede ser menor que el monto pagado", "Aviso", JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            Egreso nuevoEgreso = new Egreso();
            nuevoEgreso.setCuenta(institucion.buscarCuentaPorNombre(jComboTiposDeCuentas.getName()));
            if (hayDetalleNuevo()) {
                nuevoEgreso.setDetalle(new DetalleEgreso(jTxtDetalle.getText()));
            } else {
                Iterator it = detallesEgreso.iterator();
                while (it.hasNext()) {
                    DetalleEgreso detalleEgreso = (DetalleEgreso) it.next();
                    if (detalleEgreso.getDetalle().equals(this.jComboDetalles.getSelectedItem().toString())) {
                        nuevoEgreso.setDetalle(detalleEgreso);
                        break;
                    }
                }
            }
            GregorianCalendar fecha = (GregorianCalendar) Calendar.getInstance();
            fecha.setTime(jXDatePickerFecha.getDate());
            nuevoEgreso.setFecha(fecha);
            nuevoEgreso.setMonto(montoAPagar);
            if (jComboFormaDePago.getSelectedItem().toString().equals("Adeudado")) {
                nuevoEgreso.setPagado(0);
            } else if (!jComboFormaDePago.getSelectedItem().toString().equals("Pagado en parte")) {
                nuevoEgreso.setPagado(pagado);
            } else {
                nuevoEgreso.setPagado(pagado);
            }
            nuevoEgreso.setCuenta(institucion.buscarCuentaPorNombre(jComboTiposDeCuentas.getSelectedItem().toString()));
            cargarEgresoEnEgresosCargados(nuevoEgreso);
        } else {
            JOptionPane.showMessageDialog(this, "Hay campos vacíos. Todos son obligatorios.", "Aviso", JOptionPane.INFORMATION_MESSAGE);
        }
        limpiarCampos();
}//GEN-LAST:event_jBtnCargarUnEgresoActionPerformed

    private void jComboTiposDeCuentasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboTiposDeCuentasMouseClicked
    }//GEN-LAST:event_jComboTiposDeCuentasMouseClicked

    private void jComboTiposDeCuentasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboTiposDeCuentasActionPerformed
    }//GEN-LAST:event_jComboTiposDeCuentasActionPerformed

    private void jComboTiposDeCuentasPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jComboTiposDeCuentasPropertyChange
    }//GEN-LAST:event_jComboTiposDeCuentasPropertyChange

    private void jXDatePickerFechaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jXDatePickerFechaActionPerformed
    }//GEN-LAST:event_jXDatePickerFechaActionPerformed

    private void jTxtMontoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTxtMontoMouseClicked
    }//GEN-LAST:event_jTxtMontoMouseClicked

    private void jTxtMontoCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTxtMontoCaretUpdate
    }//GEN-LAST:event_jTxtMontoCaretUpdate

    private void jTxtMontoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtMontoActionPerformed
    }//GEN-LAST:event_jTxtMontoActionPerformed

    private void jTxtMontoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtMontoFocusGained
    }//GEN-LAST:event_jTxtMontoFocusGained

    private void jTxtMontoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtMontoFocusLost
    }//GEN-LAST:event_jTxtMontoFocusLost

    private void jTxtMontoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoKeyPressed
    }//GEN-LAST:event_jTxtMontoKeyPressed

    private void jTxtMontoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoKeyReleased
        if (jComboFormaDePago.getSelectedItem().toString().equals("Pagado")) {
            jTxtMontoPagado.setText(jTxtMonto.getText());
        }
        Vistas.Util.Util.limpiarTextoSiSoloHaySigno(jTxtMonto);
    }//GEN-LAST:event_jTxtMontoKeyReleased

    private void jTxtMontoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoKeyTyped
        Vistas.Util.Util.formatearJTextFieldTipoMonedaLuegoDeUnKeyEvent(evt, (JTextField) evt.getSource());
    }//GEN-LAST:event_jTxtMontoKeyTyped

    private void jTxtMontoPagadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTxtMontoPagadoMouseClicked
    }//GEN-LAST:event_jTxtMontoPagadoMouseClicked

    private void jTxtMontoPagadoCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTxtMontoPagadoCaretUpdate
    }//GEN-LAST:event_jTxtMontoPagadoCaretUpdate

    private void jTxtMontoPagadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtMontoPagadoActionPerformed
    }//GEN-LAST:event_jTxtMontoPagadoActionPerformed

    private void jTxtMontoPagadoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtMontoPagadoFocusGained
    }//GEN-LAST:event_jTxtMontoPagadoFocusGained

    private void jTxtMontoPagadoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtMontoPagadoFocusLost
    }//GEN-LAST:event_jTxtMontoPagadoFocusLost

    private void jTxtMontoPagadoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoPagadoKeyPressed
    }//GEN-LAST:event_jTxtMontoPagadoKeyPressed

    private void jTxtMontoPagadoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoPagadoKeyReleased
    }//GEN-LAST:event_jTxtMontoPagadoKeyReleased

    private void jTxtMontoPagadoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMontoPagadoKeyTyped
        if (jTxtMontoPagado.getText().equals("$")) {
            jTxtMontoPagado.setText("");
        } else {
            Util.formatearJTextFieldTipoMonedaLuegoDeUnKeyEvent(evt, jTxtMontoPagado);
        }
    }//GEN-LAST:event_jTxtMontoPagadoKeyTyped

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
    }//GEN-LAST:event_formWindowActivated

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
    }//GEN-LAST:event_formWindowOpened

private void jComboFormaDePagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboFormaDePagoActionPerformed
    if (jComboFormaDePago.getSelectedItem().toString().equals("Pagado") && !jTxtMonto.getText().isEmpty()) {
        jTxtMontoPagado.setText(jTxtMonto.getText());
        jTxtMontoPagado.setEnabled(false);
        return;
    }
    if (jComboFormaDePago.getSelectedItem().toString().equals("Adeudado") && !jTxtMonto.getText().isEmpty()) {
        jTxtMontoPagado.setText("$0");
        jTxtMontoPagado.setEnabled(false);
        return;
    }
    jTxtMontoPagado.setText("$0");
    jTxtMontoPagado.setEnabled(true);

}//GEN-LAST:event_jComboFormaDePagoActionPerformed

    private void cargarEgresoEnEgresosCargados(Egreso nuevoEgreso) {
        egresosCargados.add(nuevoEgreso);
        jLblAvisoDeGuardar.setVisible(true);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogAltaEgreso dialog = new JDialogAltaEgreso(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnCargarUnEgreso;
    private javax.swing.JButton jBtnDeshacerAsiento;
    private javax.swing.JButton jBtnGuardar;
    private javax.swing.JButton jBtnLimpiarCampos;
    private javax.swing.JButton jBtnVerCarga;
    private javax.swing.JComboBox jComboDetalles;
    private javax.swing.JComboBox jComboFormaDePago;
    private javax.swing.JComboBox jComboTiposDeCuentas;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelDetalle;
    private javax.swing.JLabel jLabelFecha;
    private javax.swing.JLabel jLabelMonto;
    private javax.swing.JLabel jLabelMontoPagado;
    private javax.swing.JLabel jLabelNuevoDetalle;
    private javax.swing.JLabel jLabelSaldoCajaDato;
    private javax.swing.JLabel jLabelSaldoDeCaja;
    private javax.swing.JLabel jLabelTipoDeCuenta;
    private javax.swing.JLabel jLblAvisoDeGuardar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JTextField jTxtDetalle;
    private javax.swing.JTextField jTxtMonto;
    private javax.swing.JTextField jTxtMontoPagado;
    private org.jdesktop.swingx.JXDatePicker jXDatePickerFecha;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTitulo;
    // End of variables declaration//GEN-END:variables

    private void limpiarCampos() {
        this.jTxtMonto.setText("");
        this.jTxtDetalle.setText("");
        this.jTxtMontoPagado.setText("");
    }

    private void habilitarCamposPermitidos() {
    }

    /**
     * @return the egresosCargados
     */
    public ArrayList<Egreso> getEgresosCargados() {
        return egresosCargados;
    }

    /**
     * @param egresosCargados the egresosCargados to set
     */
    public void setEgresosCargados(ArrayList<Egreso> egresosCargados) {
        this.egresosCargados = egresosCargados;
    }

    private void inicializarVariables() {
        this.egresosCargados = new ArrayList<Egreso>();
        this.detallesEgreso = new HashSet<DetalleEgreso>();
    }

    private void asignarCajaEnLabel() {
        jLabelSaldoCajaDato.setText(Util.formatearTipoMoneda(institucion.consultarCajaDisponible()));
    }

    private boolean hayMontoPagadoEnParte() {
        if ((!jComboFormaDePago.getSelectedItem().toString().equals("Pagado"))) {
            return true;
        } else {
            return false;
        }
    }

    private boolean hayCamposVacios() {
        boolean hayCamposVacios = false;
        String avisoDeCorreccion = "";
        JTextField[] jTextFields = new JTextField[3];
        jTextFields[0] = jTxtMonto;
        if (hayMontoPagadoEnParte()) {
            jTextFields[2] = jTxtMontoPagado;
        }
        if (jComboDetalles.getSelectedItem().toString().contains("Nuevo detalle...")) {
            jTextFields[1] = jTxtDetalle;
        }
        if (Vistas.Util.Util.isEmpty(jTextFields)) {
            if (jTxtDetalle.getText().isEmpty()) {
                avisoDeCorreccion.concat("Ingrese el detalle del egreso \n");
            }
            if (jTxtMonto.getText().isEmpty() || jTxtMonto.getText().isEmpty()) {
                avisoDeCorreccion.concat("Ingrese los montos correspondientes. \n");
            }
        }
        JComboBox[] jComboBoxs = new JComboBox[2];
        if (jTxtDetalle.getText().isEmpty()) {
            jComboBoxs[0] = (jComboDetalles);
        }
        jComboBoxs[1] = (jComboTiposDeCuentas);
        if (Vistas.Util.Util.hayJComboVacios(jComboBoxs)) {
            avisoDeCorreccion = avisoDeCorreccion.concat("Ingrese el detalle y el tipo de cuenta. \n");
        }
        if (jXDatePickerFecha.getDate() == null) {
            avisoDeCorreccion = avisoDeCorreccion.concat("Ingrese la fecha del egreso. \n");
            hayCamposVacios = true;
        }
        if (!avisoDeCorreccion.isEmpty()) {
            JOptionPane.showMessageDialog(this, avisoDeCorreccion, "Aviso", JOptionPane.INFORMATION_MESSAGE);
            hayCamposVacios = true;
        }
        return hayCamposVacios;
    }

    private void guardarEgresos() throws DAOProblemaConTransaccionException {
        institucion.guardarEgresos(getEgresosCargados());
        asignarCajaEnLabel();
        egresosCargados.clear();
        jLblAvisoDeGuardar.setVisible(false);
    }
}
