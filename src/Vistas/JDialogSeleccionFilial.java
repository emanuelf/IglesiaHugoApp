package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Excepciones.FilaNoSelecionadaException;
import Modelo.Filial;
import Modelo.InstitucionCentralTesoreria;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

/**
 *
 * @author Emanuel
 */
public class JDialogSeleccionFilial extends javax.swing.JDialog {

    private InstitucionCentralTesoreria institucion;

   
    public JDialogSeleccionFilial(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
    }

    public JDialogSeleccionFilial(JFrameSistema padre, boolean modal, InstitucionCentralTesoreria institucion) {
        this(padre, modal);
        this.institucion = institucion;
        try {
            cargarFilialesEnCombo(institucion.getFiliales());
        } catch (DAOProblemaConTransaccionException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Problemas al recuperar las filiales", JOptionPane.ERROR);
        }
    }
    
    
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXTitledSeparator1 = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelNroDeRelatorio1 = new javax.swing.JLabel();
        jComboFiliales = new javax.swing.JComboBox();
        jPanel1 = new javax.swing.JPanel();
        jBtnVolver = new javax.swing.JButton();
        jBtnSeguir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jXTitledSeparator1.setHorizontalAlignment(SwingConstants.CENTER);
        jXTitledSeparator1.setTitle("SELECCIÓN DE FILIAL");

        jLabelNroDeRelatorio1.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelNroDeRelatorio1.setText("Nombre de filial:");

        jComboFiliales.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboFilialesActionPerformed(evt);
            }
        });

        jBtnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolver.setText("Volver");
        jBtnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverActionPerformed(evt);
            }
        });
        jPanel1.add(jBtnVolver);

        jBtnSeguir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Ok 20x20.png"))); // NOI18N
        jBtnSeguir.setText("Seguir");
        jBtnSeguir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnSeguirActionPerformed(evt);
            }
        });
        jPanel1.add(jBtnSeguir);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelNroDeRelatorio1)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addComponent(jComboFiliales, 0, 241, Short.MAX_VALUE))))
                            .addComponent(jXTitledSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE))
                        .addContainerGap())
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXTitledSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelNroDeRelatorio1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboFiliales, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboFilialesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboFilialesActionPerformed

        }//GEN-LAST:event_jComboFilialesActionPerformed

    private void jBtnSeguirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnSeguirActionPerformed
        try {
            Filial filial = getFilialSeleccionada();
            JDialogAltaRelatorioDeFilial jDialogAltaRelatorioDeFilial = new JDialogAltaRelatorioDeFilial
                    ((JFrameSistema) getParent(), false, institucion, filial);
            jDialogAltaRelatorioDeFilial.setVisible(true);
            this.dispose();
        } catch (FilaNoSelecionadaException ex) {
            JOptionPane.showMessageDialog(this, "No hay seleccionado ninguna filial", "Seleccione la filial", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_jBtnSeguirActionPerformed

    private void jBtnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVolverActionPerformed
        this.dispose();
        this.getParent().setVisible(true);
    }//GEN-LAST:event_jBtnVolverActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JDialogSeleccionFilial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JDialogSeleccionFilial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JDialogSeleccionFilial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JDialogSeleccionFilial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JDialogSeleccionFilial dialog = new JDialogSeleccionFilial(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnSeguir;
    private javax.swing.JButton jBtnVolver;
    private javax.swing.JComboBox jComboFiliales;
    private javax.swing.JLabel jLabelNroDeRelatorio1;
    private javax.swing.JPanel jPanel1;
    private org.jdesktop.swingx.JXTitledSeparator jXTitledSeparator1;
    // End of variables declaration//GEN-END:variables

    private void cargarFilialesEnCombo(ArrayList<Filial> filiales) {
        Iterator <Filial> itFiliales = filiales.iterator();
        while (itFiliales.hasNext()) {
            Filial filial = itFiliales.next();
            jComboFiliales.addItem(filial.getNombre());
        }
    }

    private Filial getFilialSeleccionada() throws FilaNoSelecionadaException {
        if(jComboFiliales.getSelectedIndex() == -1) throw new Excepciones.FilaNoSelecionadaException();
        return institucion.buscarFilial(jComboFiliales.getSelectedItem().toString());
    }
}
