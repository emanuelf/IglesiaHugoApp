package Vistas;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.BalanceAnual;
import Modelo.BalanceMensual;
import Modelo.InstitucionCentralTesoreria;
import Util.UtilFechas;
import Vistas.Util.Util;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Calendar;
import javax.swing.JOptionPane;

/**
 *
 * @author emanuel
 */
public class JDialogGenerarOConsultarBalance extends javax.swing.JDialog {

    private InstitucionCentralTesoreria institucionCentralTesoreria;
    private boolean llamadoParaBuscarBalance;

    public JDialogGenerarOConsultarBalance(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        jTxtCajaInicial.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                Util.ignorarSiNoEsDouble(e, jTxtCajaInicial.getText());
            }
        });
    }

    JDialogGenerarOConsultarBalance(JFrameSistema padre, boolean modal, InstitucionCentralTesoreria institucionCentralTesoreria,
            boolean llamadoParaBuscarBalance) {
        this(padre, modal);
        this.setLocationRelativeTo(null);
        this.institucionCentralTesoreria = institucionCentralTesoreria;
        this.llamadoParaBuscarBalance = llamadoParaBuscarBalance;
        Vistas.Util.Util.cargaSecuenciaEnCombo(2010, 2030, jComboAnio);
        Vistas.Util.Util.cargaMesesEnCombo(jComboMes);
        if (llamadoParaBuscarBalance) {
            this.jXSeparadorDeTituloGenerarOBuscarBalance.setTitle("Datos de balance a buscar");
            this.jBtnGenerarOConsultarBalance.setText("Consultar");
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelCuerpo = new javax.swing.JPanel();
        jXSeparadorDeTituloGenerarOBuscarBalance = new org.jdesktop.swingx.JXTitledSeparator();
        jLabelTipoBalance = new javax.swing.JLabel();
        jComboTipoBalance = new javax.swing.JComboBox();
        jPnlBotonera = new javax.swing.JPanel();
        jBtnGenerarOConsultarBalance = new javax.swing.JButton();
        jBtnVolverAlInicio = new javax.swing.JButton();
        jComboMes = new javax.swing.JComboBox();
        jLabelMes = new javax.swing.JLabel();
        jLabelAnio = new javax.swing.JLabel();
        jComboAnio = new javax.swing.JComboBox();
        jLblCajaInicial = new javax.swing.JLabel();
        jTxtCajaInicial = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                formKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                formKeyTyped(evt);
            }
        });

        jPanelCuerpo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jPanelCuerpoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jPanelCuerpoKeyReleased(evt);
            }
        });

        jXSeparadorDeTituloGenerarOBuscarBalance.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jXSeparadorDeTituloGenerarOBuscarBalance.setHorizontalAlignment(0);
        jXSeparadorDeTituloGenerarOBuscarBalance.setTitle("Datos de balance a generar");

        jLabelTipoBalance.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelTipoBalance.setText("Tipo balance:");

        jComboTipoBalance.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Mensual", "Anual" }));
        jComboTipoBalance.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboTipoBalanceItemStateChanged(evt);
            }
        });
        jComboTipoBalance.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jComboTipoBalancePropertyChange(evt);
            }
        });

        jPnlBotonera.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        jBtnGenerarOConsultarBalance.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnGenerarOConsultarBalance.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Relatorios generar.png"))); // NOI18N
        jBtnGenerarOConsultarBalance.setText("Generar");
        jBtnGenerarOConsultarBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnGenerarOConsultarBalanceActionPerformed(evt);
            }
        });
        jPnlBotonera.add(jBtnGenerarOConsultarBalance);

        jBtnVolverAlInicio.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnVolverAlInicio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Volver al inicio2.png"))); // NOI18N
        jBtnVolverAlInicio.setText("Volver al inicio");
        jBtnVolverAlInicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverAlInicioActionPerformed(evt);
            }
        });
        jPnlBotonera.add(jBtnVolverAlInicio);

        jLabelMes.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelMes.setText("Mes:");

        jLabelAnio.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelAnio.setText("Anio:");

        jLblCajaInicial.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLblCajaInicial.setText("Caja inicial:");

        jTxtCajaInicial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtCajaInicialKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanelCuerpoLayout = new javax.swing.GroupLayout(jPanelCuerpo);
        jPanelCuerpo.setLayout(jPanelCuerpoLayout);
        jPanelCuerpoLayout.setHorizontalGroup(
            jPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCuerpoLayout.createSequentialGroup()
                .addGroup(jPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPnlBotonera, javax.swing.GroupLayout.DEFAULT_SIZE, 293, Short.MAX_VALUE)
                    .addGroup(jPanelCuerpoLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelCuerpoLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(jPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelTipoBalance)
                                    .addComponent(jLabelMes)
                                    .addGroup(jPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelCuerpoLayout.createSequentialGroup()
                                            .addGap(12, 12, 12)
                                            .addComponent(jComboMes, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelCuerpoLayout.createSequentialGroup()
                                            .addGap(10, 10, 10)
                                            .addComponent(jComboTipoBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jLabelAnio, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelCuerpoLayout.createSequentialGroup()
                                            .addGap(12, 12, 12)
                                            .addComponent(jComboAnio, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                    .addComponent(jLblCajaInicial)
                                    .addComponent(jTxtCajaInicial, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jXSeparadorDeTituloGenerarOBuscarBalance, javax.swing.GroupLayout.DEFAULT_SIZE, 283, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanelCuerpoLayout.setVerticalGroup(
            jPanelCuerpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCuerpoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jXSeparadorDeTituloGenerarOBuscarBalance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelTipoBalance)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboTipoBalance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelMes)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabelAnio)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboAnio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLblCajaInicial)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTxtCajaInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addComponent(jPnlBotonera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelCuerpo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelCuerpo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnGenerarOConsultarBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnGenerarOConsultarBalanceActionPerformed
        if (hayDatosSinCargar()) {
            JOptionPane.showMessageDialog(this, "Todos los datos son obligatorios");
            return;
        }
        double cajaInicial = Double.valueOf(jTxtCajaInicial.getText().isEmpty() ? "0" : jTxtCajaInicial.getText());
        String tipoBalance = jComboTipoBalance.getSelectedItem().toString();
        int anioDelBalanceAGenerar = Integer.valueOf(jComboAnio.getSelectedItem().toString());
        if (tipoBalance.equalsIgnoreCase("Anual")) {
            BalanceAnual balanceAnualAGenerar = null;
            if (!llamadoParaBuscarBalance) {
                try {
                    balanceAnualAGenerar = institucionCentralTesoreria.generarBalanceAnual(anioDelBalanceAGenerar);
                    if (balanceAnualAGenerar == null || balanceAnualAGenerar.vacio()) {
                        JOptionPane.showMessageDialog(this, "No hay balance generado para este año.", "AViso", JOptionPane.INFORMATION_MESSAGE);
                        return;
                    }
                    JDialogBalanceGenerado jDialogbalanceGenerado =
                            new JDialogBalanceGenerado(this, true, balanceAnualAGenerar, institucionCentralTesoreria, cajaInicial);
                    setVisible(false); //mejor hacer que la ventana siguiente vuelva a la ventana del inicio (quizá)
                    jDialogbalanceGenerado.setVisible(true);
                } catch (DAOProblemaConTransaccionException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        } else {
            int mesDelBalanceAGenerar = UtilFechas.mesEnNumero(this.jComboMes.getSelectedItem().toString());
            if (!llamadoParaBuscarBalance) {
                try {
                    BalanceMensual balanceMensualAGenerar = institucionCentralTesoreria.generarBalanceMensual(mesDelBalanceAGenerar, anioDelBalanceAGenerar);
                    if (balanceMensualAGenerar.vacio()) {
                        JOptionPane.showMessageDialog(this, "No hay asientos para este período");
                        return;
                    }
                    this.setVisible(false);
                    JDialogBalanceGenerado jDialogBalanceGenerado =
                            new JDialogBalanceGenerado(this, true, balanceMensualAGenerar, institucionCentralTesoreria, cajaInicial);
                    jDialogBalanceGenerado.setVisible(true);

                } catch (DAOProblemaConTransaccionException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "Aviso", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }
    }//GEN-LAST:event_jBtnGenerarOConsultarBalanceActionPerformed

    private void formKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyTyped
    }//GEN-LAST:event_formKeyTyped

    private void formKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyReleased
    }//GEN-LAST:event_formKeyReleased

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
    }//GEN-LAST:event_formKeyPressed

    private void jPanelCuerpoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPanelCuerpoKeyReleased
    }//GEN-LAST:event_jPanelCuerpoKeyReleased

    private void jPanelCuerpoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPanelCuerpoKeyPressed
    }//GEN-LAST:event_jPanelCuerpoKeyPressed

    private void jBtnVolverAlInicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVolverAlInicioActionPerformed
        dispose();
    }//GEN-LAST:event_jBtnVolverAlInicioActionPerformed

    private void jComboTipoBalancePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jComboTipoBalancePropertyChange
    }//GEN-LAST:event_jComboTipoBalancePropertyChange

    private void jComboTipoBalanceItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboTipoBalanceItemStateChanged
        if (jComboTipoBalance.getSelectedItem().toString().equalsIgnoreCase("Anual")) {
            Util.cargaSecuenciaEnCombo(Calendar.getInstance().get(Calendar.YEAR) - 10, Calendar.getInstance().get(Calendar.YEAR), jComboAnio);
            jComboMes.setEnabled(false);
            jLabelMes.setEnabled(false);

        } else {
            jLabelMes.setText("Mes:");
            Vistas.Util.Util.cargaMesesEnCombo(jComboMes);
            jComboMes.setEnabled(true);
            jComboAnio.setEnabled(true);
            jLabelAnio.setEnabled(true);
        }

    }//GEN-LAST:event_jComboTipoBalanceItemStateChanged

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        this.dispose();
        this.getParent().setVisible(true);
    }//GEN-LAST:event_formWindowClosing

    private void jTxtCajaInicialKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtCajaInicialKeyTyped
    }//GEN-LAST:event_jTxtCajaInicialKeyTyped
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnGenerarOConsultarBalance;
    private javax.swing.JButton jBtnVolverAlInicio;
    private javax.swing.JComboBox jComboAnio;
    private javax.swing.JComboBox jComboMes;
    private javax.swing.JComboBox jComboTipoBalance;
    private javax.swing.JLabel jLabelAnio;
    private javax.swing.JLabel jLabelMes;
    private javax.swing.JLabel jLabelTipoBalance;
    private javax.swing.JLabel jLblCajaInicial;
    private javax.swing.JPanel jPanelCuerpo;
    private javax.swing.JPanel jPnlBotonera;
    private javax.swing.JTextField jTxtCajaInicial;
    private org.jdesktop.swingx.JXTitledSeparator jXSeparadorDeTituloGenerarOBuscarBalance;
    // End of variables declaration//GEN-END:variables

    /**
     * @return the institucionCentralTesoreria
     */
    public InstitucionCentralTesoreria getInstitucionCentralTesoreria() {
        return institucionCentralTesoreria;
    }

    /**
     * @param institucionCentralTesoreria the institucionCentralTesoreria to set
     */
    public void setInstitucionCentralTesoreria(InstitucionCentralTesoreria institucionCentralTesoreria) {
        this.institucionCentralTesoreria = institucionCentralTesoreria;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("CERRAR_SI_ESCAPE")) {
            this.dispose();
        }
    }

    private boolean hayDatosSinCargar() {
        return jComboAnio.getSelectedIndex() == -1 || jComboMes.getSelectedIndex() == -1 || jComboTipoBalance.getSelectedIndex() == -1;
    }
}
