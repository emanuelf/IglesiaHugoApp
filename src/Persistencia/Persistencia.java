package Persistencia;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.Configuracion;
import Modelo.InstitucionCentralTesoreria;
import Modelo.Miembro;
import Modelo.Tesorero;
import java.sql.Connection;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.SessionFactory;

public class Persistencia {

    private static final SessionFactory sessionFactory;

    public static Connection getConnection() {
        return getSessionFactory().openStatelessSession().connection();
    }

    public void setNotNullFalseInEgresoAndIngresoRelatorioIfNecessary() {
        getSession().beginTransaction();
        getSession().createSQLQuery("alter table egresos_relatorios alter codigo drop not null;").executeUpdate();
        getSession().createSQLQuery("alter table ingresos_relatorios alter codigo drop not null;").executeUpdate();
        getSession().createSQLQuery("create or replace function sum_ingresos_por_cuenta(inicio date, fin date) RETURNS double precision LANGUAGE sql as "
                + "\'select sum (monto) from (ingresos natural join asientos) inner join cuentas on cuentas.codigo = asientos.id_cuenta "
                + "where fecha between $1 and $2';").executeUpdate();
        getSession().createSQLQuery("create or replace function sum_egresos_por_cuenta(inicio date, fin date) RETURNS double precision LANGUAGE sql as "
                + "\'select sum (monto) from (egresos natural join asientos) inner join cuentas on cuentas.codigo = asientos.id_cuenta "
                + "where fecha between $1 and $2';").executeUpdate();
        getSession().getTransaction().commit();
        getSession().beginTransaction();
        try{
            getSession().createSQLQuery("alter table egresos_relatorios drop constraint egresos_relatorios_cuenta_key;").executeUpdate();
            getSession().createSQLQuery("alter table ingresos_relatorios drop constraint ingresos_relatorios_cuenta_key;").executeUpdate();
        }catch(HibernateException ex){
            
        }
    }

    public void addMiembroAnonimoSiEsNecesario() {
        Session sesion = this.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        int cantidadDeMiembrosAnonimos = ((Long) sesion.createQuery("select count(*) from Miembro as u where u.apellidos = ?").setString(0, "Anonimo").iterate().next()).intValue();
        if (cantidadDeMiembrosAnonimos < 1) {
            Miembro anonimo = new Miembro();
            anonimo.setApellidos("Anonimo");
            anonimo.setNombres("Anonimo");
            sesion.save(anonimo);
            sesion.getTransaction().commit();
        }
    }

    public void createDefaultConfiguracion() {
        Session sesion = this.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        int configuracionNoCreada = ((Long) sesion.createQuery("select count (id) from Configuracion c ").iterate().next()).intValue();
        if (configuracionNoCreada == 1) {
            return;
        } else {
            sesion.saveOrUpdate(new Configuracion(0.45, 4000));
            sesion.getTransaction().commit();
        }
    }

    public Session getSession() {
        return getSessionFactory().getCurrentSession();
    }

    public void beginTransaction() {
        getSession().beginTransaction();
    }

    public Configuracion getConfiguracion() {
        Session sesion = getSession();
        beginTransaction();
        Configuracion retorno = (Configuracion) sesion.createCriteria(Configuracion.class).uniqueResult();
        commit();
        return retorno;
    }

    public void updateConfiguracion(double limiteDonacion, double porcentajePastor) {
        Configuracion a = getConfiguracion();
        Session sesion = Persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        a.setLimiteDeDonacion(limiteDonacion);
        a.setPorcentajeParaElPastorSobreLosDiezmosYOfrendas(porcentajePastor);
        sesion.update(a);
        sesion.getTransaction().commit();
    }

    public void updateDatosDeInstitucion(InstitucionCentralTesoreria aThis) {
        Session sesion = Persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(aThis);
        sesion.getTransaction().commit();
    }

    public InstitucionCentralTesoreria getInstitucion() {
        Session sesion = getSession();
        sesion.beginTransaction();
        InstitucionCentralTesoreria institucion = (InstitucionCentralTesoreria) sesion.createCriteria(InstitucionCentralTesoreria.class).uniqueResult();
        try {
            commit();

        } catch (HibernateException e) {
        }
        return institucion;
    }

    private void commit() {
        getSession().getTransaction().commit();
    }

    public void añadirUsuarioTesoreroSiEsNecesario() throws DAOProblemaConTransaccionException {
        Session sesion = this.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        int numeroDeTesoreros = ((Long) sesion.createQuery("select count (*) from Tesorero").iterate().next()).intValue();
        if (numeroDeTesoreros != 0) {
            return;
        } else {
            sesion.save(new Tesorero("admin", "1234"));
            try {
                sesion.getTransaction().commit();
            } catch (HibernateException w) {
                throw new Excepciones.DAOProblemaConTransaccionException();
            }

        }
    }

    public enum tipoDeDAO {

        USUARIODAO, PERSONADAO, CUENTADAO, DIRECCIONDAO, FILIALDAO, INGRESOEGRSORELATORIODAO,
        INSTITUCIONDAO, RELATORIODAO, ASIENTODAO, BALANCEDAO, CAJADAO
    }

    static {
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml)
            // config file.
            sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            // Log the exception.
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }

    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public Object get(String tipoDeDAOString) {
        tipoDeDAO tipoDeDAO = null;
        Object tipoDeDAOAux = null;
        if (tipoDeDAOString.equals("ASIENTODAO")) {
            tipoDeDAO = tipoDeDAO.ASIENTODAO;
        }
        if (tipoDeDAOString.equals("BALANCEDAO")) {
            tipoDeDAO = tipoDeDAO.BALANCEDAO;
        }
        if (tipoDeDAOString.equals("CAJADAO")) {
            tipoDeDAO = tipoDeDAO.CAJADAO;
        }
        if (tipoDeDAOString.equals("CUENTADAO")) {
            tipoDeDAO = tipoDeDAO.CUENTADAO;
        }
        if (tipoDeDAOString.equals("DIRECCIONDAO")) {
            tipoDeDAO = tipoDeDAO.DIRECCIONDAO;
        }
        if (tipoDeDAOString.equals("FILIALDAO")) {
            tipoDeDAO = tipoDeDAO.FILIALDAO;
        }
        if (tipoDeDAOString.equals("INGRESORELATORIODAO")) {
            tipoDeDAO = tipoDeDAO.INGRESOEGRSORELATORIODAO;
        }
        if (tipoDeDAOString.equals("INSTITUCIONDAO")) {
            tipoDeDAO = tipoDeDAO.INSTITUCIONDAO;
        }
        if (tipoDeDAOString.equals("PERSONADAO")) {
            tipoDeDAO = tipoDeDAO.PERSONADAO;
        }
        if (tipoDeDAOString.equals("RELATORIODAO")) {
            tipoDeDAO = tipoDeDAO.RELATORIODAO;
        }
        if (tipoDeDAOString.equals("USUARIODAO")) {
            tipoDeDAO = tipoDeDAO.USUARIODAO;
        }

        switch (tipoDeDAO) {

            case ASIENTODAO:
                tipoDeDAOAux = new AsientoDAO(this);
                break;
            case BALANCEDAO:
                tipoDeDAOAux = new BalanceDAO(this);
                break;
            case CAJADAO:
                tipoDeDAOAux = new CajaDAO(this);
                break;
            case CUENTADAO:
                tipoDeDAOAux = new CuentaDAO(this);
                break;
            case DIRECCIONDAO:
                tipoDeDAOAux = new DireccionDAO(this);
                break;
            case FILIALDAO:
                tipoDeDAOAux = new FilialDAO(this);
                break;
            case INGRESOEGRSORELATORIODAO:
                tipoDeDAOAux = new IngresoEgresoRelatorioDAO(this);
                break;
            case INSTITUCIONDAO:
                tipoDeDAOAux = new InstitucionDAO(this);
                break;
            case PERSONADAO:
                tipoDeDAOAux = new PersonaDAO(this);
                break;
            case RELATORIODAO:
                tipoDeDAOAux = new RelatorioDAO(this);
                break;
            case USUARIODAO:
                tipoDeDAOAux = new UsuarioDAO(this);
                break;
        }
        return tipoDeDAOAux;

    }
}
