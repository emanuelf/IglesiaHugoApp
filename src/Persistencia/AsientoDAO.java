package Persistencia;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.Asiento;
import Modelo.DetalleEgreso;
import Modelo.Diezmo;
import Modelo.Donacion;
import Modelo.Egreso;
import Modelo.Ingreso;
import Modelo.IngresoEnCulto;
import Modelo.Ofrenda;
import Modelo.OtroIngreso;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author emanuel
 */
public class AsientoDAO {

    private Persistencia persistencia;

    public AsientoDAO(Persistencia persistencia) {
        this.persistencia = persistencia;
    }

    public void persistir(Modelo.Diezmo diezmo) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction transaccion = sesion.beginTransaction();
        sesion.saveOrUpdate(diezmo);
        try {
            transaccion.commit();
        } catch (HibernateException e) {
            throw new Excepciones.DAOProblemaConTransaccionException();
        }
    }

    public void persistir(Modelo.Donacion donacion) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction transaccion = sesion.beginTransaction();
        sesion.saveOrUpdate(donacion);
        try {
            transaccion.commit();
        } catch (HibernateException e) {
            throw new Excepciones.DAOProblemaConTransaccionException();
        }
    }

    public void persistir(Modelo.RelatorioIngreso relatorio) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction transaccion = sesion.beginTransaction();
        try {
            sesion.saveOrUpdate(relatorio);
            transaccion.commit();
        } catch (HibernateException e) {
            transaccion.rollback();
            throw new Excepciones.DAOProblemaConTransaccionException();
        }
    }

    public void persistir(Ingreso asiento) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction transaccion = sesion.beginTransaction();
        try {
            sesion.saveOrUpdate(asiento);
            transaccion.commit();
        } catch (HibernateException e) {
            transaccion.rollback();
            throw new Excepciones.DAOProblemaConTransaccionException();

        }
    }

    public void pesistir(ArrayList<Egreso> egresos) throws DAOProblemaConTransaccionException {
        for (Egreso egreso : egresos) {
            persistir(egreso);
        }
    }

    public void persistir(Egreso asiento) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction transaccion = sesion.beginTransaction();
        sesion.saveOrUpdate(asiento);
        try {
            transaccion.commit();
        } catch (HibernateException e) {
            transaccion.rollback();
            throw new Excepciones.DAOProblemaConTransaccionException();
        }
    }

    public List<Asiento> buscarAsientos(Integer codigoDeCuenta, GregorianCalendar periodo) {
        ArrayList<Asiento> retornoAsientos = new ArrayList<Asiento>();
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        retornoAsientos = (ArrayList<Asiento>) sesion.createCriteria(Asiento.class).
                add(Restrictions.eq("cuenta", codigoDeCuenta)).list();
        return retornoAsientos;
    }

    public List<Asiento> buscarAsientosEnBalanceMensual(int anio, int mes) {
        return null;
    }

    public List<Ingreso> buscarIngresos(int anio, int mes) {
        ArrayList<Ingreso> listaDeIngresosDelMes = null;
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        GregorianCalendar fechaInicioDeMes = new GregorianCalendar();
        fechaInicioDeMes.set(anio, mes, 1);
        GregorianCalendar fechaFinDeMes = new GregorianCalendar();
        fechaFinDeMes.set(anio, mes,
                fechaInicioDeMes.getActualMaximum(Calendar.DAY_OF_MONTH));
        sesion.beginTransaction();
        ArrayList<Diezmo> listaDeDiezmosDelMes = (ArrayList<Diezmo>) sesion.createQuery("from Diezmo where fecha between :fechaInicioMes and :fechaFinDeMes").
                setCalendar("fechaInicioMes", fechaInicioDeMes).setCalendar("fechaFinDeMes", fechaFinDeMes).list();
        ArrayList<Donacion> listaDeDonacionesDelMes = (ArrayList<Donacion>) sesion.createQuery("from Donacion where fecha between :fechaInicioMes and :fechaFinDeMes").
                setCalendar("fechaInicioMes", fechaInicioDeMes).setCalendar("fechaFinDeMes", fechaFinDeMes).list();
        ArrayList<Ofrenda> listaDeOfrendasDelMes = (ArrayList<Ofrenda>) sesion.createQuery("from Ofrenda where fecha between :fechaInicioMes and :fechaFinDeMes").
                setCalendar("fechaInicioMes", fechaInicioDeMes).setCalendar("fechaFinDeMes", fechaFinDeMes).list();
        ArrayList<OtroIngreso> listaDeOtrosIngresosDelMes = (ArrayList<OtroIngreso>) sesion.createQuery("from OtroIngreso where fecha between :fechaInicioMes and :fechaFinDeMes").
                setCalendar("fechaInicioMes", fechaInicioDeMes).setCalendar("fechaFinDeMes", fechaFinDeMes).list();
        if (listaDeDiezmosDelMes != null || listaDeDonacionesDelMes != null || listaDeOtrosIngresosDelMes != null || listaDeOfrendasDelMes != null) {
            listaDeIngresosDelMes = new ArrayList<Ingreso>();
        }
        if (listaDeDiezmosDelMes != null && listaDeIngresosDelMes != null) {
            listaDeIngresosDelMes.addAll(listaDeDiezmosDelMes);
        }
        if (listaDeDonacionesDelMes != null && listaDeIngresosDelMes != null) {
            listaDeIngresosDelMes.addAll(listaDeDonacionesDelMes);
        }
        if (listaDeOtrosIngresosDelMes != null && listaDeIngresosDelMes != null) {
            listaDeIngresosDelMes.addAll(listaDeOtrosIngresosDelMes);
        }
        if (listaDeOtrosIngresosDelMes != null && listaDeOfrendasDelMes != null) {
            listaDeIngresosDelMes.addAll(listaDeOfrendasDelMes);
        }
        sesion.getTransaction().commit();
        return listaDeIngresosDelMes;
    }

    public List<Egreso> buscarEgresos(int anio, int mes) {
        ArrayList listaDeEgresosDelMes = new ArrayList();
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        GregorianCalendar fechaPrincipioDeMes = new GregorianCalendar(anio, mes, 1);
        GregorianCalendar fechaFinDeMes = new GregorianCalendar(anio, mes,
                30);
        sesion.beginTransaction();
        listaDeEgresosDelMes = (ArrayList) sesion.createQuery("from Egreso as e left join fetch e.detalle where fecha between :fechaPrincipioDeMes and :fechaFinDeMes").
                setCalendar("fechaPrincipioDeMes", fechaPrincipioDeMes).setCalendar("fechaFinDeMes", fechaFinDeMes).list();
        sesion.getTransaction().commit();
        return listaDeEgresosDelMes;
    }

    public HashMap<Integer, DetalleEgreso> getDetallesDeEgresos() {
        HashMap<Integer, DetalleEgreso> retorno = new HashMap<Integer, DetalleEgreso>();
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        List<DetalleEgreso> detallesEgreso = sesion.createCriteria(DetalleEgreso.class).list();
        sesion.getTransaction().commit();
        for (DetalleEgreso detalleEgreso : detallesEgreso) {
            retorno.put(detalleEgreso.getId(), detalleEgreso);
        }
        return retorno;
    }

    public void persistirOfrendas(ArrayList<Ofrenda> ingresosOfrenda) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction transaccion = sesion.beginTransaction();
        for (Ofrenda ingreso : ingresosOfrenda) {
            sesion.saveOrUpdate(ingreso);
        }
        try {
            sesion.flush();
            transaccion.commit();
        } catch (HibernateException ex) {
            transaccion.rollback();
            throw new Excepciones.DAOProblemaConTransaccionException();
        }
    }

    public void persistirDiezmos(ArrayList<Diezmo> ingresosDiezmo) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction transaccion = sesion.beginTransaction();
        for (Diezmo ingreso : ingresosDiezmo) {
            sesion.saveOrUpdate(ingreso);
        }
        try {
            sesion.flush();
            transaccion.commit();
        } catch (HibernateException ex) {
            transaccion.rollback();
            throw new Excepciones.DAOProblemaConTransaccionException();
        }
    }

    public void persistirDonaciones(ArrayList<Donacion> ingresosDonacion) throws DAOProblemaConTransaccionException {
        for (Donacion donacion : ingresosDonacion) {
            persistir(donacion);
        }
    }

    public void eliminarAsiento(Ingreso ingresoAEliminar) throws DAOProblemaConTransaccionException {
        Session sesion = Persistencia.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction transaccion = sesion.beginTransaction();
        sesion.delete(ingresoAEliminar);
        try {
            transaccion.commit();
        } catch (HibernateException ex) {
            transaccion.rollback();
            throw new DAOProblemaConTransaccionException();
        }
    }

    public void eliminarAsiento(Egreso egresoAEliminar) throws DAOProblemaConTransaccionException {
        Session sesion = Persistencia.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction transaccion = sesion.beginTransaction();
        sesion.delete(egresoAEliminar);
        try {
            transaccion.commit();
        } catch (HibernateException ex) {
            if (transaccion.wasCommitted()) {
                return;
            } else {
                transaccion.rollback();
                throw new DAOProblemaConTransaccionException();
            }
        }
    }

    public void actualizarEgreso(Egreso egresoAModificar) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction transaccion = sesion.beginTransaction();
        sesion.update(egresoAModificar);
        try {
            transaccion.commit();
        } catch (HibernateException ex) {
            transaccion.rollback();
            throw new DAOProblemaConTransaccionException();
        }
    }

    public void actualizarIngreso(Ingreso ingresoAModificar) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction transaccion = sesion.beginTransaction();
        sesion.update(ingresoAModificar);
        try {
            transaccion.commit();
        } catch (HibernateException ex) {
            transaccion.rollback();
            throw new DAOProblemaConTransaccionException();
        }
    }

    public void saveIngresoEnCulto(IngresoEnCulto ingresoEnCulto) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        sesion.save(ingresoEnCulto);
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException ex) {
            ex.printStackTrace();
            sesion.getTransaction().rollback();
            throw new DAOProblemaConTransaccionException();
        }

    }

    public IngresoEnCulto getIngresoEnCulto(Date date) {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        IngresoEnCulto ingresoEnCulto = null;
        int cantidadDeIngresoEnCulto = ((Long) sesion.createQuery("select count (*) from IngresoEnCulto where fechaDeCulto = ? ").
                setDate(0, date).iterate().next()).intValue();
        sesion.getTransaction().commit();
        if (cantidadDeIngresoEnCulto != 1) {
            return null; //será null!
        }
        sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        ingresoEnCulto = (IngresoEnCulto) sesion.createCriteria(IngresoEnCulto.class).add(Restrictions.eq("fechaDeCulto", date)).uniqueResult();
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException e) {
            sesion.getTransaction().rollback();
        }
        return ingresoEnCulto;
    }

    public void persistirOtrosIngresos(ArrayList<OtroIngreso> otrosIngresos) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        for (OtroIngreso ingreso : otrosIngresos) {
            sesion.saveOrUpdate(ingreso);
        }
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException ex) {
            sesion.getTransaction().rollback();
            throw new Excepciones.DAOProblemaConTransaccionException();
        }
    }
}
