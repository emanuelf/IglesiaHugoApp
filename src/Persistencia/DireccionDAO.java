/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import Modelo.Direccion;
import org.hibernate.Session;

/**
 *
 * @author emanuel
 */
public class DireccionDAO {

    private Persistencia persistencia;

    public DireccionDAO(Persistencia persistencia) {
        this.persistencia = persistencia;
    }

    public void addDireccion(Direccion direccion) {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        sesion.save(direccion);
        try {
            sesion.getTransaction().commit();
        } catch (Exception e) {
            sesion.getTransaction().rollback();
            new Excepciones.DAOProblemaConTransaccionException();
        }
    }
}
