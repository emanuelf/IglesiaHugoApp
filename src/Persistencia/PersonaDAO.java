package Persistencia;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.*;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author emanuel
 */
public class PersonaDAO {

    private Persistencia persistencia;

    public PersonaDAO(Persistencia persistencia) {
        this.persistencia = persistencia;
    }

    private Criteria criteriaForNombre(Session sesion, String nombre) {
        return sesion.createCriteria(Miembro.class).
                add(Restrictions.ilike("nombres", nombre.toLowerCase(), MatchMode.START)).
                add(Restrictions.eq("activo", true)).
                add(Restrictions.ne("apellidos", "Anonimo")).
                setFetchMode("misDiezmos", FetchMode.JOIN).
                addOrder(Order.desc("nombres"));
    }

    private Criteria criteriaForApellido(Session sesion, String apellido) {
        return sesion.createCriteria(Miembro.class).
                add(Restrictions.ilike("apellidos", apellido.toLowerCase(), MatchMode.START)).
                add(Restrictions.eq("activo", true)).
                add(Restrictions.ne("apellidos", "Anonimo")).
                setFetchMode("misDiezmos", FetchMode.JOIN).
                addOrder(Order.desc("apellidos"));
    }

    private Criteria criteriaForApellidoYNombre(Session sesion, String apellido, String nombre) {
        return sesion.createCriteria(Miembro.class).
                add(Restrictions.ilike("apellidos", apellido.toLowerCase(), MatchMode.START)).add(
                Restrictions.ilike("nombres", nombre.toLowerCase(), MatchMode.START)).
                add(Restrictions.eq("activo", true)).
                add(Restrictions.ne("apellidos", "Anonimo")).
                setFetchMode("misDiezmos", FetchMode.JOIN).
                addOrder(Order.desc("apellidos"));
    }

    public Miembro buscarMiembro(String apellido, String nombre) {
        org.hibernate.classic.Session session = persistencia.getSessionFactory().getCurrentSession();
        Miembro miembro = null;
        miembro = (Miembro) session.createCriteria(Miembro.class).add(Restrictions.idEq(apellido)).add(Restrictions.eq("nombre", nombre));
        return miembro;
    }

    public void agregarMiembro(Miembro miembro) {
        Session session = persistencia.getSessionFactory().openSession();
        session.beginTransaction();
        miembro.setActivo(true);
        session.save(miembro);
        try {
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            session.getTransaction().rollback();

        }
    }

    public boolean existePastor(long dni) {
        Session session = persistencia.getSessionFactory().openSession();
        session.beginTransaction();
        int nro = ((Long) session.createQuery("select count(*) from PastorFilial where dni= ?").setLong(0, dni).iterate().next()).intValue();
        try {
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            session.getTransaction().rollback();
        }
        if (nro <= 0) {
            return false;
        }
        return true;
    }

    public void desvincularMiembro(Miembro miembro) {
        miembro.setActivo(false);
        org.hibernate.classic.Session sesion = persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        sesion.update(miembro);
        sesion.getTransaction().commit();
    }

    public Pastor getDatosDelPastor() throws Excepciones.DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        Pastor pastor = (Pastor) sesion.createCriteria(Pastor.class).uniqueResult();
        sesion.getTransaction().commit();
        return pastor;
    }

    public void registrarPastor(Pastor elPastor) throws Exception {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.createQuery("delete * from Pastor");
        sesion.beginTransaction();
        sesion.saveOrUpdate(elPastor);
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException e) {
            throw new Exception("No se ha podido insertar los datos del nuevo pastor. Pruebe nuevamente.");
        }
    }

    public PastorFilial getDatosDelPastorFilial(String ciudad) {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        Filial filial = (Filial) sesion.createCriteria(Filial.class).createCriteria("direccion").
                add(Restrictions.eq("ciudad", ciudad)).uniqueResult();
        return filial.getPastorFilial();
    }

    public List<Miembro> getMiembros(String nombre, String apellido) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        List<Miembro> listaDeMiembros = new ArrayList<Miembro>();
        if (apellido.isEmpty()) {
            listaDeMiembros = (List<Miembro>) criteriaForNombre(sesion, nombre).list();
        } else if (nombre.isEmpty()) {
            listaDeMiembros = (List<Miembro>) criteriaForApellido(sesion, apellido).list();
        } else {
            listaDeMiembros = (List<Miembro>) criteriaForApellidoYNombre(sesion, apellido, nombre).list();
        }
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException ex) {
            sesion.getTransaction().rollback();
            throw new DAOProblemaConTransaccionException();
        }
        return listaDeMiembros;
    }

    public List<Miembro> getMiembros(String apellido) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        if (!sesion.isOpen()) {
            sesion = this.persistencia.getSessionFactory().openSession();
        }
        sesion.beginTransaction();
        List listaDeMiembros = sesion.createCriteria(Miembro.class).
                setFetchMode("misDiezmos", FetchMode.JOIN).
                add(Restrictions.like("apellidos", "%" + apellido.toLowerCase() + "%")).
                add(Restrictions.eq("activo", true)).
                list();
        sesion.getTransaction().commit();
        return listaDeMiembros;
    }

    public Miembro buscarMiembro(long id) {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        Miembro unMiembro = (Miembro) sesion.createCriteria(Miembro.class).add(Restrictions.idEq(new Long(id))).uniqueResult();
        return unMiembro;
    }

    public List<PastorFilial> getPastoresDeFiliales() {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        ArrayList retorno = (ArrayList) sesion.createCriteria(PastorFilial.class).list();
        sesion.getTransaction().commit();
        return retorno;
    }

    public void persistir(Pastor pastor) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(pastor);
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException e) {
            throw new DAOProblemaConTransaccionException();

        }
    }

    public void persistir(Miembro miembro) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        Transaction transaccion = sesion.beginTransaction();
        sesion.merge(miembro);
        try {
            transaccion.commit();
        } catch (HibernateException e) {
            throw new DAOProblemaConTransaccionException();

        }
    }

    public int getCantidadDeMiembros() {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        Transaction transaccion = sesion.beginTransaction();
        int cantidadDeMiembros = ((Long) sesion.createQuery("select count (*) from Miembro ").iterate().next()).intValue();
        transaccion.commit();
        return cantidadDeMiembros;
    }

    public Miembro getMiembroAnonimo() {
        Miembro retorno;
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        retorno = (Miembro) sesion.createCriteria(Miembro.class).add(Restrictions.eq("apellidos", "Anonimo")).uniqueResult();
        sesion.getTransaction().commit();
        return retorno;
    }

    public ArrayList<Donante> getDonantes() {
        ArrayList<Donante> retorno = new ArrayList();
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        retorno = (ArrayList<Donante>) sesion.createCriteria(Donante.class).list();
        sesion.getTransaction().commit();
        return retorno;
    }
}
