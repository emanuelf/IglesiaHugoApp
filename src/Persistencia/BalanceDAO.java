package Persistencia;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.Balance;
import Modelo.BalanceAnual;
import Modelo.BalanceMensual;
import Modelo.Egreso;
import Modelo.Ingreso;
import Util.UtilFechas;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author emanuel
 */
public class BalanceDAO {

    private Persistencia persistencia;

    public BalanceDAO(Persistencia persistencia) {
        this.persistencia = persistencia;
    }

    public List<Ingreso> getIngresosDelMes(int anio, int mes) throws DAOProblemaConTransaccionException {
        ArrayList<Ingreso> retornoIngresosDelMes = new ArrayList();
        retornoIngresosDelMes = (ArrayList<Ingreso>) ((AsientoDAO) this.persistencia.get("ASIENTODAO")).buscarIngresos(anio, mes);
        return retornoIngresosDelMes;
    }

    public List<Egreso> getEgresosDelMes(int anio, int mes) throws DAOProblemaConTransaccionException {
        ArrayList<Egreso> retornoEgresosDelMes = new ArrayList();
        retornoEgresosDelMes = (ArrayList<Egreso>) ((AsientoDAO) this.persistencia.get("ASIENTODAO")).buscarEgresos(anio, mes);
        return retornoEgresosDelMes;
    }

    public List<Ingreso> getIngresosDelAnio(int anio) throws DAOProblemaConTransaccionException {
        GregorianCalendar principioDelAnio = new GregorianCalendar(anio, 0, 1);
        GregorianCalendar finDelAnio = new GregorianCalendar(anio, 11, 31);
        ArrayList<Ingreso> retornoIngresosDelAnio = new ArrayList();
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        retornoIngresosDelAnio = (ArrayList<Ingreso>) sesion.createQuery("from Ingreso where fecha between :principioDelAnio and :finDelAnio").
                        setCalendar("principioDelAnio", principioDelAnio).  
                        setCalendar("finDelAnio", finDelAnio).
                        list();
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException ex) {
            sesion.getTransaction().rollback();
            throw new DAOProblemaConTransaccionException();
        }
        return retornoIngresosDelAnio;
    }

    public List<Egreso> getEgresosDelAnio(int anio) throws DAOProblemaConTransaccionException {
        GregorianCalendar principioDelAnio = new GregorianCalendar(anio, 0, 1);
        GregorianCalendar finDelAnio = new GregorianCalendar(anio, 11, 31);
        ArrayList<Egreso> retornoEgresosDelAnio = new ArrayList();
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        retornoEgresosDelAnio = (ArrayList<Egreso>) sesion.createQuery("from Egreso where fecha between :principioDelAnio and :finDelAnio").setCalendar("principioDelAnio", principioDelAnio).setCalendar("finDelAnio", finDelAnio).list();
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException ex) {
            sesion.getTransaction().rollback();
            throw new DAOProblemaConTransaccionException();
        }
        return retornoEgresosDelAnio;
    }

    public void persistirBalance(BalanceAnual balance) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(balance);
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException e) {
            sesion.getTransaction().rollback();
            throw new Excepciones.DAOProblemaConTransaccionException();
        }

    }

    public void persistirBalance(BalanceMensual balance) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(balance);
        sesion.getTransaction().commit();
    }

    public Balance getBalanceAnual(int anio) throws DAOProblemaConTransaccionException {
        BalanceAnual balanceAnual = null;
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        balanceAnual = (BalanceAnual) sesion.createCriteria(BalanceAnual.class).add(Restrictions.eq("anio", anio)).
                setFetchMode("ingresos", FetchMode.JOIN).setFetchMode("egresos", FetchMode.JOIN).uniqueResult();
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException ex) {
            sesion.getTransaction().rollback();
            throw new DAOProblemaConTransaccionException();
        }
        return balanceAnual;
    }

    public Balance getBalanceMensual(int anio, int mes) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        BalanceMensual balanceMensual = null;
        sesion.beginTransaction();
        balanceMensual = (BalanceMensual) sesion.createCriteria(BalanceMensual.class).setFetchMode("ingresos", FetchMode.JOIN).
                setFetchMode("egresos", FetchMode.JOIN).add(Restrictions.eq("anio", anio)).add(Restrictions.eq("mes", mes)).uniqueResult();
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException ex) {
            sesion.getTransaction().rollback();
            throw new DAOProblemaConTransaccionException();
        }
        return balanceMensual;
    }
}
