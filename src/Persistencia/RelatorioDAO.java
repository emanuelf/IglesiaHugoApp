package Persistencia;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.Filial;
import Modelo.RelatorioFilial;
import Modelo.RelatorioFilialAnual;
import Modelo.RelatorioFilialMensual;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author emanuel
 */
public class RelatorioDAO {

    private Persistencia persistencia;

    public RelatorioDAO(Persistencia persistencia) {
        this.persistencia = persistencia;
    }

    public void persistirOGuardar(RelatorioFilial relatorio) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        sesion.save(relatorio);
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException e) {
            sesion.getTransaction().rollback();
            throw new Excepciones.DAOProblemaConTransaccionException();
        }
    }

    public void persistirOGuardar(RelatorioFilialAnual relatorio) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        sesion.save(relatorio);
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException e) {
            sesion.getTransaction().rollback();
            throw new Excepciones.DAOProblemaConTransaccionException();
        }
    }

    public void save(RelatorioFilialMensual relatorio) throws DAOProblemaConTransaccionException {
        Session sesion = persistencia.getSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(relatorio);
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException e) {
            sesion.getTransaction().rollback();
            throw new Excepciones.DAOProblemaConTransaccionException();
        }

    }

    public Filial loadRelatoriosDeFilial(Filial filial) throws DAOProblemaConTransaccionException {
        //TODO: seguir aca para que no tenga problemas con devolver valores null de propiedades primitivas
        Session sesion = persistencia.getSession();
        sesion.beginTransaction();
        filial = (Filial) sesion.createCriteria(Filial.class).
                add(Restrictions.idEq(filial.getId())).
                setFetchMode("misRelatorios", FetchMode.JOIN).
                uniqueResult();
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException ex) {
            sesion.getTransaction().rollback();
            throw new DAOProblemaConTransaccionException();
        }
        return filial;
    }

    public void remove(RelatorioFilialMensual relatorio) throws DAOProblemaConTransaccionException {
        Session session = persistencia.getSession();
        session.beginTransaction();
        session.delete(relatorio);
        try {
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            session.getTransaction().rollback();
            ex.printStackTrace();
            ((ConstraintViolationException) ex).getSQLException().getNextException().printStackTrace();
            throw new DAOProblemaConTransaccionException();
        }
    }
}
