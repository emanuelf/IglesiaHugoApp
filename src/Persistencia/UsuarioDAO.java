/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.Secretario;
import Modelo.Tesorero;
import Modelo.Usuario;
import java.util.ArrayList;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author emanuel
 */
public class UsuarioDAO {

    private Persistencia persistencia;

    public UsuarioDAO(Persistencia persistencia) {
        this.persistencia = persistencia;
    }

    /**
    @return: null, si no se encuentra el usuario en la BD.
     *        El usuario buscado en caso contrario
     */
    
    public Session getSession(){
        return persistencia.getSessionFactory().getCurrentSession();
    }
    public Usuario buscarUsuario(String nombre, String contrasenia) {
        Session session = getSession();
        Usuario usuario = null;
        session.beginTransaction();
        int cantidadDeUsuariosTesoreros = ((Long) session.createQuery("select count (*) from Tesorero as usuario where usuario.nombre = ? and"
                + " contrasenia = :contrasenia").
                setString(0, nombre).setString("contrasenia", contrasenia).iterate().next()).intValue();
        if(cantidadDeUsuariosTesoreros==0) {
            int cantidadDeUsuariosSecretarios = ((Long) session.createQuery("select count (*) from Secretario as usuario where usuario.nombre = ? and "
                    + "contrasenia = :contrasenia").
                setString(0, nombre).setString("contrasenia", contrasenia).iterate().next()).intValue();
            if(cantidadDeUsuariosSecretarios !=0){
                usuario = (Secretario) session.createCriteria(Secretario.class).add(Restrictions.eq("nombre", nombre)).uniqueResult();
                session.getTransaction().commit();
                return usuario;
            }else{
                return null;
            }
        }
        usuario = (Tesorero) session.createCriteria(Tesorero.class).add(Restrictions.eq("nombre", nombre)).uniqueResult();        
        session.getTransaction().commit();
        return usuario;
    }

    public void eliminarUsuario(Usuario usuario) throws DAOProblemaConTransaccionException {
        org.hibernate.classic.Session session = persistencia.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.delete(usuario);
        try {
            session.getTransaction().commit();
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            throw new Excepciones.DAOProblemaConTransaccionException();
        }
    }

    public void persistirOGuardar(Tesorero objeto) throws DAOProblemaConTransaccionException {
        org.hibernate.classic.Session session = persistencia.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction transaccion = session.beginTransaction();
        session.saveOrUpdate(objeto);
        try {
            transaccion.commit();
        } catch (HibernateException e) {
            transaccion.rollback();
            throw new Excepciones.DAOProblemaConTransaccionException();
        }


    }

    public void persistirOGuardar(Secretario objeto) throws DAOProblemaConTransaccionException {
        org.hibernate.classic.Session session = persistencia.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction transaccion = session.beginTransaction();
        session.saveOrUpdate(objeto);
        try {
            transaccion.commit();
        } catch (HibernateException e) {
            transaccion.rollback();
            throw new Excepciones.DAOProblemaConTransaccionException();
        }


    }

    public ArrayList<Usuario> getAllUsuarios() {
        ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
        ArrayList<Tesorero> usuariosTesoreros;
        ArrayList<Secretario> usuariosSecretarios;
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        usuariosSecretarios = (ArrayList<Secretario>) sesion.createCriteria(Secretario.class).list();
        usuariosTesoreros = (ArrayList<Tesorero>) sesion.createCriteria(Tesorero.class).list();
        sesion.getTransaction().commit();
        usuarios.addAll(usuariosTesoreros);
        usuarios.addAll(usuariosSecretarios);
        return usuarios;
    }

    public int getCantidadDeUsuariosTesoreros() {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        if(!sesion.isOpen()) sesion= this.persistencia.getSessionFactory().openSession();
        sesion.beginTransaction();
        int retorno= ((Integer)sesion.createQuery("select count (*) from tesorero t").iterate().next()).intValue();
        sesion.getTransaction().commit();
        return retorno;
    }

    public void updateUsuario(Usuario usuarioAModificar) {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        if(!sesion.isOpen()) sesion= this.persistencia.getSessionFactory().openSession();
        sesion.beginTransaction();
        sesion.update(usuarioAModificar);
        sesion.getTransaction().commit();
    }
}
