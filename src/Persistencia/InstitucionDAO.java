/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import Excepciones.DAOProblemaConTransaccionException;

import Modelo.InstitucionBase;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author emanuel
 */
public class InstitucionDAO {

    private Persistencia persistencia;

    public InstitucionDAO(Persistencia persistencia) {
        this.persistencia = persistencia;
    }

    public void persistir(InstitucionBase institucion) throws DAOProblemaConTransaccionException{
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(institucion);
        try{
            sesion.getTransaction().commit();
        }catch (HibernateException e){
            sesion.getTransaction().rollback();
            throw  new Excepciones.DAOProblemaConTransaccionException();
        }
    }
}
