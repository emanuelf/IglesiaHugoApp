/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import Excepciones.DAOProblemaConTransaccionException;
import Excepciones.DAOYaExisteRegistroException;
import Modelo.Filial;
import Modelo.PastorFilial;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author emanuel
 */
public class FilialDAO {

    private Persistencia persistencia;

    public FilialDAO(Persistencia persistencia) {
        this.persistencia = persistencia;
    }

    public Filial getFilial(String nombre) {
        Filial filial = null;
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        filial = (Filial) sesion.createQuery("from Filial where lower(nombre) = :nombre").setString("nombre", nombre.toLowerCase()).uniqueResult();
        sesion.getTransaction().commit();
        return filial;
    }

    public List<Filial> getFiliales() throws DAOProblemaConTransaccionException {
        List listADevolver = new ArrayList();
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        listADevolver = sesion.createCriteria(Filial.class).list();
        try {
            sesion.getTransaction().commit();
        } catch (RuntimeException e) {
            sesion.getTransaction().rollback();
            throw new DAOProblemaConTransaccionException();
        }
        return listADevolver;
    }

    public void persistir(Filial filial) throws Excepciones.DAOProblemaConTransaccionException{
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        if (!sesion.isOpen()) {
            sesion = this.persistencia.getSessionFactory().openSession();
        }
         sesion.beginTransaction();
        sesion.saveOrUpdate(filial);
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException e) {
            throw new DAOProblemaConTransaccionException();
        }
    }
    
      public boolean existePastor (long dni){
        Session session = persistencia.getSessionFactory().openSession();
        session.beginTransaction();
        int nro = ((Long) session.createQuery("select count(*) from PastorFilial where dni= ?").setLong(0, dni).iterate().next()).intValue();
        try {
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            session.getTransaction().rollback();
        }
        if(nro <=0) return false;
        return true;
    }

    public void deleteFilial(Filial filialSeleccionada) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(filialSeleccionada);
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException ex) {
            throw new Excepciones.DAOProblemaConTransaccionException();
        }finally{
            if(sesion.isOpen()) sesion.close();
        }
    }

    public void actualizarFilial(Filial filialAModificar) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        sesion.update(filialAModificar);
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException e) {
            throw new DAOProblemaConTransaccionException();
        }
    }

    public String getFilial(PastorFilial pastorFilial) {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        Filial filial = (Filial) sesion.createCriteria(Filial.class).add(Restrictions.eq("pastorFilial", pastorFilial)).uniqueResult();
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException e) {
            
        }
        return filial.getNombre();
    }
}
