package Persistencia;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.Caja;
import Modelo.CajaDisponible;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;


/**
 *
 * @author emanuel
 */
public class CajaDAO {

    private Persistencia persistencia;

    public CajaDAO(Persistencia persistencia) {
        this.persistencia = persistencia;
    }

    public void actualizarCaja(CajaDisponible cajaDisponible) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        Transaction transaction= sesion.beginTransaction();
        CajaDisponible cajaDisponible1 = (CajaDisponible) sesion.createCriteria(CajaDisponible.class).
                uniqueResult();
        if(cajaDisponible1==null){
            sesion.save(cajaDisponible);
            return;
        }
        cajaDisponible1.setDisponibilidadEnPesos(cajaDisponible.getDisponibilidadEnPesos());
        sesion.saveOrUpdate(cajaDisponible1);
        try{
            transaction.commit();
        } catch (HibernateException e){
            transaction.rollback();
            throw new Excepciones.DAOProblemaConTransaccionException();
        }
     
    }

    public Caja getCajaDisponible(){
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        CajaDisponible cajaDisponible= (CajaDisponible) sesion.createCriteria(CajaDisponible.class).uniqueResult();
        if (cajaDisponible ==null) {
            cajaDisponible=new CajaDisponible();
        }
        sesion.getTransaction().commit();
        return cajaDisponible;
    }
    


   
}
