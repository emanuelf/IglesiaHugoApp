/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import Excepciones.DAOProblemaConTransaccionException;
import Modelo.Cuenta;
import Modelo.CuentaEgreso;
import Modelo.CuentaFilial;
import Modelo.CuentaFilialDeuda;
import Modelo.CuentaFilialEgreso;
import Modelo.CuentaFilialIngreso;
import Modelo.CuentaIngreso;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author emanuel
 */
public class CuentaDAO {

    private Persistencia persistencia;

    public CuentaDAO(Persistencia persistencia) {
        this.persistencia = persistencia;
    }

    public List<Modelo.CuentaIngreso> getCuentasIngreso() throws DAOProblemaConTransaccionException {
        ArrayList<Modelo.CuentaIngreso> cuentas = new ArrayList();
        Session sesion = Persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        cuentas = (ArrayList<CuentaIngreso>) sesion.createCriteria(CuentaIngreso.class).
                add(Restrictions.eq("activo", true)).list();
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException ex) {
            sesion.getTransaction().rollback();
            throw new DAOProblemaConTransaccionException();
        }
        return cuentas;
    }

    public List<Modelo.CuentaEgreso> getCuentasEgreso() throws DAOProblemaConTransaccionException {
        ArrayList<Modelo.CuentaEgreso> cuentas = new ArrayList();
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        cuentas = (ArrayList<CuentaEgreso>) sesion.createCriteria(CuentaEgreso.class).
                add(Restrictions.eq("activo", true)).list();
        try {
            sesion.getTransaction().commit();

        } catch (HibernateException ex) {
            sesion.getTransaction().rollback();
            throw new DAOProblemaConTransaccionException();
        }
        return cuentas;
    }

    public void agregarNuevaCuenta(Modelo.CuentaEgreso cuenta) throws DAOProblemaConTransaccionException {
        org.hibernate.classic.Session session = persistencia.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.saveOrUpdate(cuenta);
        try {
            session.getTransaction().commit();
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            throw new Excepciones.DAOProblemaConTransaccionException();
        }
    }

    public void agregarNuevaCuenta(Modelo.CuentaIngreso cuenta) throws DAOProblemaConTransaccionException {
        org.hibernate.classic.Session session = persistencia.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.saveOrUpdate(cuenta);
        try {
            session.getTransaction().commit();
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            throw new Excepciones.DAOProblemaConTransaccionException();
        }
    }

    public Cuenta buscarCuentaPorCodigo(int codigo) {
        Session session = Persistencia.getSessionFactory().getCurrentSession();
        if (!session.isOpen()) {
            session = Persistencia.getSessionFactory().openSession();
        }
        Cuenta retorno = null;
        session.beginTransaction();
        retorno = (CuentaEgreso) session.createCriteria(CuentaEgreso.class).add(Restrictions.idEq(codigo)).uniqueResult();
        if (retorno == null) {
            retorno = (CuentaIngreso) session.createCriteria(CuentaIngreso.class).add(Restrictions.idEq(codigo)).uniqueResult();
        }
        try {
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            session.getTransaction().rollback();
        }
        return retorno;

    }

    public Cuenta buscarCuentaPorNombre(String nombre) {
        org.hibernate.classic.Session session = persistencia.getSessionFactory().openSession();
        Cuenta retorno = null;
        session.beginTransaction();
        retorno = (CuentaIngreso) session.createCriteria(CuentaIngreso.class).add(Restrictions.eq("nombre", nombre)).uniqueResult();
        retorno = (CuentaEgreso) session.createCriteria(CuentaEgreso.class).add(Restrictions.eq("nombre", nombre)).uniqueResult();
        try {
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            session.getTransaction().rollback();
        }
        return retorno;
    }

    public void eliminarCuenta(Cuenta cuenta) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction();
        cuenta.setActivo(false);
        sesion.update(cuenta);
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException e) {
            sesion.getTransaction().rollback();
            throw new DAOProblemaConTransaccionException();
        }
    }

    public Collection<CuentaFilialIngreso> getCuentasFilialesIngreso() throws DAOProblemaConTransaccionException {
        ArrayList<CuentaFilialIngreso> cuentaFilials = new ArrayList<CuentaFilialIngreso>();
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction().begin();
        cuentaFilials.addAll(sesion.createCriteria(CuentaFilialIngreso.class).list());
        try {
            sesion.getTransaction().commit();
            return cuentaFilials;
        } catch (HibernateException ex) {
            throw new Excepciones.DAOProblemaConTransaccionException();
        }
    }

    public ArrayList<CuentaFilialDeuda> getCuentasFilialesDeuda() throws DAOProblemaConTransaccionException {
        ArrayList<CuentaFilialDeuda> cuentasFiliales = new ArrayList<CuentaFilialDeuda>();
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction().begin();
        cuentasFiliales.addAll(sesion.createCriteria(CuentaFilialDeuda.class).list());
        try {
            sesion.getTransaction().commit();
            return cuentasFiliales;
        } catch (HibernateException ex) {
            throw new Excepciones.DAOProblemaConTransaccionException();
        }
    }

    public Collection<CuentaFilialEgreso> getCuentasFilialesEgreso() throws DAOProblemaConTransaccionException {
        ArrayList<CuentaFilialEgreso> cuentaFiliales = new ArrayList<CuentaFilialEgreso>();
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction().begin();
        cuentaFiliales = (ArrayList<CuentaFilialEgreso>) sesion.createCriteria(CuentaFilialEgreso.class).list();
        try {
            sesion.getTransaction().commit();
            return cuentaFiliales;
        } catch (HibernateException ex) {
            throw new Excepciones.DAOProblemaConTransaccionException();
        }
    }

    public void addCuenta(CuentaFilial cuentaFilial) throws DAOProblemaConTransaccionException {
        Session sesion = this.persistencia.getSessionFactory().getCurrentSession();
        sesion.beginTransaction().begin();
        sesion.saveOrUpdate(cuentaFilial);
        try {
            sesion.getTransaction().commit();
        } catch (HibernateException ex) {
            throw new Excepciones.DAOProblemaConTransaccionException();
        }

    }

    public void addCuentasClasicasSiEsNecesario() {
        Session sesion = this.persistencia.getSessionFactory().openSession();
        sesion.beginTransaction();
        int cantidad = ((Long) sesion.createQuery("select count (*) from CuentaIngreso as ci where ci.nombre = :diezmo or ci.nombre=:ofrenda"
                + " or ci.nombre =:donacion").setString("diezmo", "Diezmo").setString("ofrenda", "Ofrenda").setString("donacion", "Donacion").iterate().next()).intValue();
        if (cantidad == 0) {
            sesion.saveOrUpdate(new CuentaIngreso("Diezmo", 1));
            sesion.saveOrUpdate(new CuentaIngreso("Ofrenda", 2));
            sesion.saveOrUpdate(new CuentaIngreso("Donacion", 3));
            sesion.getTransaction().commit();

        }
    }

    public int cantidadCuentasEgreso() {
        Session sesion = this.persistencia.getSessionFactory().openSession();
        sesion.beginTransaction();
        int retorno = ((Long) sesion.createQuery("select count (*) from CuentaEgreso where activo = ? ").
                setBoolean(0, true).iterate().next()).intValue();
        sesion.getTransaction().commit();
        return retorno;
    }
}
