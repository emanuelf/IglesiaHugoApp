package util;

import java.awt.print.PrinterException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.Attribute;
import javax.print.attribute.PrintServiceAttributeSet;

public class PrinterUtils {

    public static boolean hayImpresoraDisponible() {
        PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);
        return services.length > 0;
    }
    
    public static boolean hayImpresoraPorDefaultDisponible(){
        if(hayImpresoraDisponible()){
            return getImpresoraDefault() != null;
        }else{
            return false;
        }
    }

    public static PrintService[] getImpresorasDisponibles() {
        PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);
        return services;
    }

    public static PrintService getImpresoraDefault() {
        return PrintServiceLookup.lookupDefaultPrintService();
    }

    private static void printAvailable() {

        // busca los servicios de impresion...
        PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);

        // -- ver los atributos de las impresoras...
        for (PrintService printService : services) {

            PrintServiceAttributeSet printServiceAttributeSet = printService.getAttributes();

            // todos los atributos de la impresora
            Attribute[] a = printServiceAttributeSet.toArray();
        }

    }

    private static void printDefault() {
        // tu impresora por default
        PrintService service = PrintServiceLookup.lookupDefaultPrintService();

    }

   public static void pruebaImpresion() throws PrinterException {
        if(!hayImpresoraPorDefaultDisponible()){
            throw new PrinterException("No hay ninguna impresora configurada como predeterminada.");
        }
    }
}
